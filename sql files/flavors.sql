-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 16, 2018 at 06:18 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `developer_sdc`
--

-- --------------------------------------------------------

--
-- Table structure for table `flavors`
--

DROP TABLE IF EXISTS `flavors`;
CREATE TABLE IF NOT EXISTS `flavors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `flavor_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `flavors`
--

INSERT INTO `flavors` (`id`, `flavor_name`, `description`, `img`, `created_at`, `updated_at`) VALUES
(1, 'Notification message', '<p>Please note that our products may contain and/or come into contact with nuts, dairy, and/or soy products.</p>', '', '2018-12-12 18:30:00', '2018-12-16 05:53:10'),
(2, 'test 2', '<p>test 1 description</p>', 'flavor-20181216112545.jpg', '2018-12-12 18:30:00', '2018-12-16 05:55:45'),
(3, 'test 3', 'test 3', 'ss', '2018-12-13 18:30:00', '2018-12-13 18:30:00'),
(4, 'test 1', '<p>test 1 description</p>', 'flavor-20181213224903.png', '2018-12-12 18:30:00', '2018-12-13 17:19:03');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
