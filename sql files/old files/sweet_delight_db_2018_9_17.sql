-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2018 at 09:22 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sweet_delight_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(0, 0, 'iJ7wnTkwcMHWV3v0CCPSsS1wzU31o4Sw', 1, '2018-09-16 14:17:43', '2018-09-16 14:17:43', '2018-09-16 14:17:43'),
(1, 1, 'RCsr7Qt1bBMqFwlW4bBVvtu02UBBQ0hr', 1, '2015-07-11 06:09:31', '2015-07-11 06:09:31', '2015-07-11 06:09:31'),
(5, 7, 'KB71pV1DYudR9TzPPQnGxRXQBoo7jV0E', 1, '2016-01-06 17:35:57', '2016-01-06 17:35:57', '2016-01-06 17:35:57'),
(8, 10, 'Bs5wlBYLO997Z8lcLpO19pj40fx2qVuV', 1, '2016-01-07 18:20:38', '2016-01-07 18:20:38', '2016-01-07 18:20:38'),
(9, 11, '0dsYg0Ft0IZFKMP7qIs1nbpK0f8gif6y', 1, '2016-01-07 18:48:38', '2016-01-07 18:48:38', '2016-01-07 18:48:38'),
(10, 12, 'yfD7K4mMa3eend4vvBBPNVbyFeX3UGZF', 1, '2016-01-07 18:57:38', '2016-01-07 18:57:38', '2016-01-07 18:57:38'),
(14, 9, 'i7J1jS1CbeCsMb40JdWwQLSFfoOWdngc', 1, '2017-01-23 07:05:39', '2017-01-23 07:05:39', '2017-01-23 07:05:39'),
(15, 10, '8eiPuz7KrCZXYjiMEY1wmkLuPbmMrSwI', 1, '2017-12-27 14:10:24', '2017-12-27 14:10:24', '2017-12-27 14:10:24'),
(16, 11, 'htzbXZnJuH098fhWA5e8V5QLberPA1IA', 1, '2018-01-07 14:18:27', '2018-01-07 14:18:27', '2018-01-07 14:18:27'),
(18, 12, 'PnJ03QoBnJMegHXmGCXh6bcoXArycqr5', 1, '2018-01-29 16:15:58', '2018-01-29 16:15:58', '2018-01-29 16:15:58'),
(20, 15, 'ivsgJeobsGYHI1oHtQo5oTzkBOb6A9WL', 1, '2018-02-01 04:44:24', '2018-02-01 04:44:24', '2018-02-01 04:44:24'),
(21, 17, 'jDkUMCYv0VpNfdrlsxKEFa2wCI78R7DP', 1, '2018-02-01 04:45:53', '2018-02-01 04:45:53', '2018-02-01 04:45:53'),
(22, 18, 'WCbfDN6mTvtTfu2DHELZfkgDgd7B6phA', 1, '2018-02-01 04:58:20', '2018-02-01 04:58:20', '2018-02-01 04:58:20'),
(23, 19, 'RYh4C97v7B33siSZNyEiIssiaEYJABrL', 1, '2018-02-07 15:33:02', '2018-02-07 15:33:02', '2018-02-07 15:33:02'),
(24, 22, '4348c3Ic0jpsgNIEefnYCV63duaZSg7h', 1, '2018-02-21 11:58:03', '2018-02-21 11:58:03', '2018-02-21 11:58:03'),
(25, 23, 'peX1noTVzdJ6J8tmqW0zjw8KQSCHTyu2', 1, '2018-02-26 15:17:22', '2018-02-26 15:17:22', '2018-02-26 15:17:22'),
(26, 24, 'z2epl8Kt4TueSiYFItM28FsfPniUNghO', 1, '2018-02-26 15:22:36', '2018-02-26 15:22:36', '2018-02-26 15:22:36'),
(27, 26, 'n3p2GaPvZy8jmcZlDsRQi8zkOpoPjsA6', 1, '2018-02-26 15:24:22', '2018-02-26 15:24:22', '2018-02-26 15:24:22'),
(28, 27, 'sz7dMXWrcgPqP6g2rfzAmx0EU9cVEOGk', 1, '2018-03-02 21:00:19', '2018-03-02 21:00:19', '2018-03-02 21:00:19'),
(29, 29, 'xVCqM5zHberJ9cmzqbtbY40Ei4t5fanC', 1, '2018-03-18 19:50:18', '2018-03-18 19:50:18', '2018-03-18 19:50:18'),
(30, 30, 'jaP1CVT0kBiDIMtelRqCXgjmkVuA4fs7', 1, '2018-04-22 14:57:07', '2018-04-22 14:57:07', '2018-04-22 14:57:07'),
(31, 34, 'EFtLa51MamU7mpA9NjGCG0S7DXTg7f3H', 1, '2018-05-10 17:21:10', '2018-05-10 17:21:10', '2018-05-10 17:21:10'),
(32, 36, 'bz9PPiM2FvBoKC1NTaPz7JMJSC9WQAfK', 1, '2018-05-10 17:40:13', '2018-05-10 17:40:13', '2018-05-10 17:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `animation_type`
--

CREATE TABLE `animation_type` (
  `animation_type_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `css_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `animation_type`
--

INSERT INTO `animation_type` (`animation_type_id`, `type`, `description`, `status`, `created_at`, `updated_at`, `css_class`) VALUES
(1, 'Slide from Left', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'FromLeft'),
(2, 'Slide from Right', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'FromRight'),
(3, 'Slide from Top', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'FromTop'),
(4, 'Slide from Bottom', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'FromBottom');

-- --------------------------------------------------------

--
-- Table structure for table `cake_type`
--

CREATE TABLE `cake_type` (
  `cake_type_id` int(11) NOT NULL,
  `type` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cake_type`
--

INSERT INTO `cake_type` (`cake_type_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Wedding Cake', 1, '2018-02-12 17:41:10', '2018-02-12 17:41:10'),
(2, 'Birthday Cake', 1, '2018-02-12 17:41:18', '2018-02-12 17:41:18'),
(3, 'Other Cake', 1, '2018-02-12 17:41:34', '2018-02-12 17:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_api` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tasting_days` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tasting_err` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qoute_days` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qoute_err` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_api` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `twitter` text COLLATE utf8_unicode_ci,
  `tasting_success` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `company_name`, `zip`, `email`, `phone`, `location`, `background_image`, `fb`, `fb_api`, `instagram`, `youtube`, `tasting_days`, `tasting_err`, `qoute_days`, `qoute_err`, `working_time`, `google_api`, `status`, `created_at`, `updated_at`, `twitter`, `tasting_success`) VALUES
(1, 'Sweet Delights Cakery', '99501', 'info@sweetdelightscakery.com', '(+248) 787-7766', '<p>1755 Poppleton Dr, West Bloomfield Township,</p>\r\n<p>MI 48324, USA</p>', NULL, 'https://www.facebook.com/sdcakery/', NULL, 'https://www.facebook.com/sdcakery/', 'https://www.facebook.com/sdcakery/', '60', 'Sorry, we\'re not available on your date. Please try again another date', '60', 'Sorry, we’re not available for that date!', '', NULL, 1, '0000-00-00 00:00:00', '2018-05-10 16:54:23', 'https://www.facebook.com/sdcakery/', '');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usps_domestic_rate` int(11) NOT NULL DEFAULT '0',
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `usps_domestic_rate`, `zip`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Sri Lanka', 0, '+0094', '165', '2017-12-24 10:42:36', '2017-12-24 10:42:36'),
(2, 'India', 0, '+93', '1', '2018-01-11 13:27:16', '2018-01-11 13:27:16'),
(3, 'America', 1, '', '20', '2018-05-07 18:30:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `no_of_coupons` int(11) NOT NULL,
  `value` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `name`, `no_of_coupons`, `value`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'US $400 discount coupon', 9, 100, '2018-03-19', '2018-05-31', '2018-05-14 20:12:26', '2018-05-14 20:12:26');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_items`
--

CREATE TABLE `coupon_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` text COLLATE utf8_unicode_ci NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon_items`
--

INSERT INTO `coupon_items` (`id`, `code`, `coupon_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '5AAEB7DB8717', 1, 1, '2018-03-18 19:02:51', '2018-05-14 20:07:32'),
(2, '5AAEB7DB8E6A', 1, 0, '2018-03-18 19:02:51', '2018-05-10 06:26:11'),
(3, '5AAEB7DB9A61', 1, 0, '2018-03-18 19:02:51', '2018-05-10 06:32:00'),
(4, '5AAEB7DBAC34', 1, 0, '2018-03-18 19:02:51', '2018-05-10 06:43:25'),
(5, '5AF9E7073F7D', 1, 0, '2018-05-14 19:44:07', '2018-05-14 19:44:07'),
(6, '5AF9ED2F3600', 1, 0, '2018-05-14 20:10:23', '2018-05-14 20:10:23'),
(7, '5AF9ED2F46D9', 1, 0, '2018-05-14 20:10:23', '2018-05-14 20:10:23'),
(8, '5AF9ED2F511A', 1, 0, '2018-05-14 20:10:23', '2018-05-14 20:10:23'),
(9, '5AF9ED2F684C', 1, 0, '2018-05-14 20:10:23', '2018-05-14 20:10:23');

-- --------------------------------------------------------

--
-- Table structure for table `fonts-list`
--

CREATE TABLE `fonts-list` (
  `id` int(15) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `unicode` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fonts-list`
--

INSERT INTO `fonts-list` (`id`, `type`, `icon`, `unicode`) VALUES
(1, 'fa', 'fa-adjust', '&#xf042;'),
(2, 'fa', 'fa-adn', '&#xf170;'),
(3, 'fa', 'fa-align-center', '&#xf037;'),
(4, 'fa', 'fa-align-justify', '&#xf039;'),
(5, 'fa', 'fa-align-left', '&#xf036;'),
(6, 'fa', 'fa-align-right', '&#xf038;'),
(7, 'fa', 'fa-ambulance', '&#xf0f9;'),
(8, 'fa', 'fa-anchor', '&#xf13d;'),
(9, 'fa', 'fa-android', '&#xf17b;'),
(10, 'fa', 'fa-angellist', '&#xf209;'),
(11, 'fa', 'fa-angle-double-down', '&#xf103;'),
(12, 'fa', 'fa-angle-double-left', '&#xf100;'),
(13, 'fa', 'fa-angle-double-right', '&#xf101;'),
(14, 'fa', 'fa-angle-double-up', '&#xf102;'),
(15, 'fa', 'fa-angle-down', '&#xf107;'),
(16, 'fa', 'fa-angle-left', '&#xf104;'),
(17, 'fa', 'fa-angle-right', '&#xf105;'),
(18, 'fa', 'fa-angle-up', '&#xf106;'),
(19, 'fa', 'fa-apple', '&#xf179;'),
(20, 'fa', 'fa-archive', '&#xf187;'),
(21, 'fa', 'fa-area-chart', '&#xf1fe;'),
(22, 'fa', 'fa-arrow-circle-down', '&#xf0ab;'),
(23, 'fa', 'fa-arrow-circle-left', '&#xf0a8;'),
(24, 'fa', 'fa-arrow-circle-o-down', '&#xf01a;'),
(25, 'fa', 'fa-arrow-circle-o-left', '&#xf190;'),
(26, 'fa', 'fa-arrow-circle-o-right', '&#xf18e;'),
(27, 'fa', 'fa-arrow-circle-o-up', '&#xf01b;'),
(28, 'fa', 'fa-arrow-circle-right', '&#xf0a9;'),
(29, 'fa', 'fa-arrow-circle-up', '&#xf0aa;'),
(30, 'fa', 'fa-arrow-down', '&#xf063;'),
(31, 'fa', 'fa-arrow-left', '&#xf060;'),
(32, 'fa', 'fa-arrow-right', '&#xf061;'),
(33, 'fa', 'fa-arrow-up', '&#xf062;'),
(34, 'fa', 'fa-arrows', '&#xf047;'),
(35, 'fa', 'fa-arrows-alt', '&#xf0b2;'),
(36, 'fa', 'fa-arrows-h', '&#xf07e;'),
(37, 'fa', 'fa-arrows-v', '&#xf07d;'),
(38, 'fa', 'fa-asterisk', '&#xf069;'),
(39, 'fa', 'fa-at', '&#xf1fa;'),
(40, 'fa', 'fa-automobile(alias)', '&#xf1b9;'),
(41, 'fa', 'fa-backward', '&#xf04a;'),
(42, 'fa', 'fa-ban', '&#xf05e;'),
(43, 'fa', 'fa-bank(alias)', '&#xf19c;'),
(44, 'fa', 'fa-bar-chart', '&#xf080;'),
(45, 'fa', 'fa-bar-chart-o(alias)', '&#xf080;'),
(46, 'fa', 'fa-barcode', '&#xf02a;'),
(47, 'fa', 'fa-bars', '&#xf0c9;'),
(48, 'fa', 'fa-bed', '&#xf236;'),
(49, 'fa', 'fa-beer', '&#xf0fc;'),
(50, 'fa', 'fa-behance', '&#xf1b4;'),
(51, 'fa', 'fa-behance-square', '&#xf1b5;'),
(52, 'fa', 'fa-bell', '&#xf0f3;'),
(53, 'fa', 'fa-bell-o', '&#xf0a2;'),
(54, 'fa', 'fa-bell-slash', '&#xf1f6;'),
(55, 'fa', 'fa-bell-slash-o', '&#xf1f7;'),
(56, 'fa', 'fa-bicycle', '&#xf206;'),
(57, 'fa', 'fa-binoculars', '&#xf1e5;'),
(58, 'fa', 'fa-birthday-cake', '&#xf1fd;'),
(59, 'fa', 'fa-bitbucket', '&#xf171;'),
(60, 'fa', 'fa-bitbucket-square', '&#xf172;'),
(61, 'fa', 'fa-bitcoin(alias)', '&#xf15a;'),
(62, 'fa', 'fa-bold', '&#xf032;'),
(63, 'fa', 'fa-bolt', '&#xf0e7;'),
(64, 'fa', 'fa-bomb', '&#xf1e2;'),
(65, 'fa', 'fa-book', '&#xf02d;'),
(66, 'fa', 'fa-bookmark', '&#xf02e;'),
(67, 'fa', 'fa-bookmark-o', '&#xf097;'),
(68, 'fa', 'fa-briefcase', '&#xf0b1;'),
(69, 'fa', 'fa-btc', '&#xf15a;'),
(70, 'fa', 'fa-bug', '&#xf188;'),
(71, 'fa', 'fa-building', '&#xf1ad;'),
(72, 'fa', 'fa-building-o', '&#xf0f7;'),
(73, 'fa', 'fa-bullhorn', '&#xf0a1;'),
(74, 'fa', 'fa-bullseye', '&#xf140;'),
(75, 'fa', 'fa-bus', '&#xf207;'),
(76, 'fa', 'fa-buysellads', NULL),
(77, 'fa', 'fa-cab(alias)', NULL),
(78, 'fa', 'fa-calculator', NULL),
(79, 'fa', 'fa-calendar', NULL),
(80, 'fa', 'fa-calendar-o', NULL),
(81, 'fa', 'fa-camera', NULL),
(82, 'fa', 'fa-camera-retro', NULL),
(83, 'fa', 'fa-car', NULL),
(84, 'fa', 'fa-caret-down', NULL),
(85, 'fa', 'fa-caret-left', NULL),
(86, 'fa', 'fa-caret-right', NULL),
(87, 'fa', 'fa-caret-square-o-down', NULL),
(88, 'fa', 'fa-caret-square-o-left', NULL),
(89, 'fa', 'fa-caret-square-o-right', NULL),
(90, 'fa', 'fa-caret-square-o-up', NULL),
(91, 'fa', 'fa-caret-up', NULL),
(92, 'fa', 'fa-cart-arrow-down', NULL),
(93, 'fa', 'fa-cart-plus', NULL),
(94, 'fa', 'fa-cc', NULL),
(95, 'fa', 'fa-cc-amex', NULL),
(96, 'fa', 'fa-cc-discover', NULL),
(97, 'fa', 'fa-cc-mastercard', NULL),
(98, 'fa', 'fa-cc-paypal', NULL),
(99, 'fa', 'fa-cc-stripe', NULL),
(100, 'fa', 'fa-cc-visa', NULL),
(101, 'fa', 'fa-certificate', NULL),
(102, 'fa', 'fa-chain(alias)', NULL),
(103, 'fa', 'fa-chain-broken', NULL),
(104, 'fa', 'fa-check', NULL),
(105, 'fa', 'fa-check-circle', NULL),
(106, 'fa', 'fa-check-circle-o', NULL),
(107, 'fa', 'fa-check-square', NULL),
(108, 'fa', 'fa-check-square-o', NULL),
(109, 'fa', 'fa-chevron-circle-down', NULL),
(110, 'fa', 'fa-chevron-circle-left', NULL),
(111, 'fa', 'fa-chevron-circle-right', NULL),
(112, 'fa', 'fa-chevron-circle-up', NULL),
(113, 'fa', 'fa-chevron-down', NULL),
(114, 'fa', 'fa-chevron-left', NULL),
(115, 'fa', 'fa-chevron-right', NULL),
(116, 'fa', 'fa-chevron-up', NULL),
(117, 'fa', 'fa-child', NULL),
(118, 'fa', 'fa-circle', NULL),
(119, 'fa', 'fa-circle-o', NULL),
(120, 'fa', 'fa-circle-o-notch', NULL),
(121, 'fa', 'fa-circle-thin', NULL),
(122, 'fa', 'fa-clipboard', NULL),
(123, 'fa', 'fa-clock-o', NULL),
(124, 'fa', 'fa-close(alias)', NULL),
(125, 'fa', 'fa-cloud', NULL),
(126, 'fa', 'fa-cloud-download', NULL),
(127, 'fa', 'fa-cloud-upload', NULL),
(128, 'fa', 'fa-cny(alias)', NULL),
(129, 'fa', 'fa-code', NULL),
(130, 'fa', 'fa-code-fork', NULL),
(131, 'fa', 'fa-codepen', NULL),
(132, 'fa', 'fa-coffee', NULL),
(133, 'fa', 'fa-cog', NULL),
(134, 'fa', 'fa-cogs', NULL),
(135, 'fa', 'fa-columns', NULL),
(136, 'fa', 'fa-comment', NULL),
(137, 'fa', 'fa-comment-o', NULL),
(138, 'fa', 'fa-comments', NULL),
(139, 'fa', 'fa-comments-o', NULL),
(140, 'fa', 'fa-compass', NULL),
(141, 'fa', 'fa-compress', NULL),
(142, 'fa', 'fa-connectdevelop', NULL),
(143, 'fa', 'fa-copy(alias)', NULL),
(144, 'fa', 'fa-copyright', NULL),
(145, 'fa', 'fa-credit-card', NULL),
(146, 'fa', 'fa-crop', NULL),
(147, 'fa', 'fa-crosshairs', NULL),
(148, 'fa', 'fa-css3', NULL),
(149, 'fa', 'fa-cube', NULL),
(150, 'fa', 'fa-cubes', NULL),
(151, 'fa', 'fa-cut(alias)', NULL),
(152, 'fa', 'fa-cutlery', NULL),
(153, 'fa', 'fa-dashboard(alias)', NULL),
(154, 'fa', 'fa-dashcube', NULL),
(155, 'fa', 'fa-database', NULL),
(156, 'fa', 'fa-dedent(alias)', NULL),
(157, 'fa', 'fa-delicious', NULL),
(158, 'fa', 'fa-desktop', NULL),
(159, 'fa', 'fa-deviantart', NULL),
(160, 'fa', 'fa-diamond', NULL),
(161, 'fa', 'fa-digg', NULL),
(162, 'fa', 'fa-dollar(alias)', NULL),
(163, 'fa', 'fa-dot-circle-o', NULL),
(164, 'fa', 'fa-download', NULL),
(165, 'fa', 'fa-dribbble', NULL),
(166, 'fa', 'fa-dropbox', NULL),
(167, 'fa', 'fa-drupal', NULL),
(168, 'fa', 'fa-edit(alias)', NULL),
(169, 'fa', 'fa-eject', NULL),
(170, 'fa', 'fa-ellipsis-h', NULL),
(171, 'fa', 'fa-ellipsis-v', NULL),
(172, 'fa', 'fa-empire', NULL),
(173, 'fa', 'fa-envelope', NULL),
(174, 'fa', 'fa-envelope-o', NULL),
(175, 'fa', 'fa-envelope-square', NULL),
(176, 'fa', 'fa-eraser', NULL),
(177, 'fa', 'fa-eur', NULL),
(178, 'fa', 'fa-euro(alias)', NULL),
(179, 'fa', 'fa-exchange', NULL),
(180, 'fa', 'fa-exclamation', NULL),
(181, 'fa', 'fa-exclamation-circle', NULL),
(182, 'fa', 'fa-exclamation-triangle', NULL),
(183, 'fa', 'fa-expand', NULL),
(184, 'fa', 'fa-external-link', NULL),
(185, 'fa', 'fa-external-link-square', NULL),
(186, 'fa', 'fa-eye', NULL),
(187, 'fa', 'fa-eye-slash', NULL),
(188, 'fa', 'fa-eyedropper', NULL),
(189, 'fa', 'fa-facebook', NULL),
(190, 'fa', 'fa-facebook-f(alias)', NULL),
(191, 'fa', 'fa-facebook-official', NULL),
(192, 'fa', 'fa-facebook-square', NULL),
(193, 'fa', 'fa-fast-backward', NULL),
(194, 'fa', 'fa-fast-forward', NULL),
(195, 'fa', 'fa-fax', NULL),
(196, 'fa', 'fa-female', NULL),
(197, 'fa', 'fa-fighter-jet', NULL),
(198, 'fa', 'fa-file', NULL),
(199, 'fa', 'fa-file-archive-o', NULL),
(200, 'fa', 'fa-file-audio-o', NULL),
(201, 'fa', 'fa-file-code-o', NULL),
(202, 'fa', 'fa-file-excel-o', NULL),
(203, 'fa', 'fa-file-image-o', NULL),
(204, 'fa', 'fa-file-movie-o(alias)', NULL),
(205, 'fa', 'fa-file-o', NULL),
(206, 'fa', 'fa-file-pdf-o', NULL),
(207, 'fa', 'fa-file-photo-o(alias)', NULL),
(208, 'fa', 'fa-file-picture-o(alias)', NULL),
(209, 'fa', 'fa-file-powerpoint-o', NULL),
(210, 'fa', 'fa-file-sound-o(alias)', NULL),
(211, 'fa', 'fa-file-text', NULL),
(212, 'fa', 'fa-file-text-o', NULL),
(213, 'fa', 'fa-file-video-o', NULL),
(214, 'fa', 'fa-file-word-o', NULL),
(215, 'fa', 'fa-file-zip-o(alias)', NULL),
(216, 'fa', 'fa-files-o', NULL),
(217, 'fa', 'fa-film', NULL),
(218, 'fa', 'fa-filter', NULL),
(219, 'fa', 'fa-fire', NULL),
(220, 'fa', 'fa-fire-extinguisher', NULL),
(221, 'fa', 'fa-flag', NULL),
(222, 'fa', 'fa-flag-checkered', NULL),
(223, 'fa', 'fa-flag-o', NULL),
(224, 'fa', 'fa-flash(alias)', NULL),
(225, 'fa', 'fa-flask', NULL),
(226, 'fa', 'fa-flickr', NULL),
(227, 'fa', 'fa-floppy-o', NULL),
(228, 'fa', 'fa-folder', NULL),
(229, 'fa', 'fa-folder-o', NULL),
(230, 'fa', 'fa-folder-open', NULL),
(231, 'fa', 'fa-folder-open-o', NULL),
(232, 'fa', 'fa-font', NULL),
(233, 'fa', 'fa-forumbee', NULL),
(234, 'fa', 'fa-forward', NULL),
(235, 'fa', 'fa-foursquare', NULL),
(236, 'fa', 'fa-frown-o', NULL),
(237, 'fa', 'fa-futbol-o', NULL),
(238, 'fa', 'fa-gamepad', NULL),
(239, 'fa', 'fa-gavel', NULL),
(240, 'fa', 'fa-gbp', NULL),
(241, 'fa', 'fa-ge(alias)', NULL),
(242, 'fa', 'fa-gear(alias)', NULL),
(243, 'fa', 'fa-gears(alias)', NULL),
(244, 'fa', 'fa-genderless(alias)', NULL),
(245, 'fa', 'fa-gift', NULL),
(246, 'fa', 'fa-git', NULL),
(247, 'fa', 'fa-git-square', NULL),
(248, 'fa', 'fa-github', NULL),
(249, 'fa', 'fa-github-alt', NULL),
(250, 'fa', 'fa-github-square', NULL),
(251, 'fa', 'fa-gittip(alias)', NULL),
(252, 'fa', 'fa-glass', NULL),
(253, 'fa', 'fa-globe', NULL),
(254, 'fa', 'fa-google', NULL),
(255, 'fa', 'fa-google-plus', NULL),
(256, 'fa', 'fa-google-plus-square', NULL),
(257, 'fa', 'fa-google-wallet', NULL),
(258, 'fa', 'fa-graduation-cap', NULL),
(259, 'fa', 'fa-gratipay', NULL),
(260, 'fa', 'fa-group(alias)', NULL),
(261, 'fa', 'fa-h-square', NULL),
(262, 'fa', 'fa-hacker-news', NULL),
(263, 'fa', 'fa-hand-o-down', NULL),
(264, 'fa', 'fa-hand-o-left', NULL),
(265, 'fa', 'fa-hand-o-right', NULL),
(266, 'fa', 'fa-hand-o-up', NULL),
(267, 'fa', 'fa-hdd-o', NULL),
(268, 'fa', 'fa-header', NULL),
(269, 'fa', 'fa-headphones', NULL),
(270, 'fa', 'fa-heart', NULL),
(271, 'fa', 'fa-heart-o', NULL),
(272, 'fa', 'fa-heartbeat', NULL),
(273, 'fa', 'fa-history', NULL),
(274, 'fa', 'fa-home', NULL),
(275, 'fa', 'fa-hospital-o', NULL),
(276, 'fa', 'fa-hotel(alias)', NULL),
(277, 'fa', 'fa-html5', NULL),
(278, 'fa', 'fa-ils', NULL),
(279, 'fa', 'fa-image(alias)', NULL),
(280, 'fa', 'fa-inbox', NULL),
(281, 'fa', 'fa-indent', NULL),
(282, 'fa', 'fa-info', NULL),
(283, 'fa', 'fa-info-circle', NULL),
(284, 'fa', 'fa-inr', NULL),
(285, 'fa', 'fa-instagram', NULL),
(286, 'fa', 'fa-institution(alias)', NULL),
(287, 'fa', 'fa-ioxhost', NULL),
(288, 'fa', 'fa-italic', NULL),
(289, 'fa', 'fa-joomla', NULL),
(290, 'fa', 'fa-jpy', NULL),
(291, 'fa', 'fa-jsfiddle', NULL),
(292, 'fa', 'fa-key', NULL),
(293, 'fa', 'fa-keyboard-o', NULL),
(294, 'fa', 'fa-krw', NULL),
(295, 'fa', 'fa-language', NULL),
(296, 'fa', 'fa-laptop', NULL),
(297, 'fa', 'fa-lastfm', NULL),
(298, 'fa', 'fa-lastfm-square', NULL),
(299, 'fa', 'fa-leaf', NULL),
(300, 'fa', 'fa-leanpub', NULL),
(301, 'fa', 'fa-legal(alias)', NULL),
(302, 'fa', 'fa-lemon-o', NULL),
(303, 'fa', 'fa-level-down', NULL),
(304, 'fa', 'fa-level-up', NULL),
(305, 'fa', 'fa-life-bouy(alias)', NULL),
(306, 'fa', 'fa-life-buoy(alias)', NULL),
(307, 'fa', 'fa-life-ring', NULL),
(308, 'fa', 'fa-life-saver(alias)', NULL),
(309, 'fa', 'fa-lightbulb-o', NULL),
(310, 'fa', 'fa-line-chart', NULL),
(311, 'fa', 'fa-link', NULL),
(312, 'fa', 'fa-linkedin', NULL),
(313, 'fa', 'fa-linkedin-square', NULL),
(314, 'fa', 'fa-linux', NULL),
(315, 'fa', 'fa-list', NULL),
(316, 'fa', 'fa-list-alt', NULL),
(317, 'fa', 'fa-list-ol', NULL),
(318, 'fa', 'fa-list-ul', NULL),
(319, 'fa', 'fa-location-arrow', NULL),
(320, 'fa', 'fa-lock', NULL),
(321, 'fa', 'fa-long-arrow-down', NULL),
(322, 'fa', 'fa-long-arrow-left', NULL),
(323, 'fa', 'fa-long-arrow-right', NULL),
(324, 'fa', 'fa-long-arrow-up', NULL),
(325, 'fa', 'fa-magic', NULL),
(326, 'fa', 'fa-magnet', NULL),
(327, 'fa', 'fa-mail-forward(alias)', NULL),
(328, 'fa', 'fa-mail-reply(alias)', NULL),
(329, 'fa', 'fa-mail-reply-all(alias)', NULL),
(330, 'fa', 'fa-male', NULL),
(331, 'fa', 'fa-map-marker', NULL),
(332, 'fa', 'fa-mars', NULL),
(333, 'fa', 'fa-mars-double', NULL),
(334, 'fa', 'fa-mars-stroke', NULL),
(335, 'fa', 'fa-mars-stroke-h', NULL),
(336, 'fa', 'fa-mars-stroke-v', NULL),
(337, 'fa', 'fa-maxcdn', NULL),
(338, 'fa', 'fa-meanpath', NULL),
(339, 'fa', 'fa-medium', NULL),
(340, 'fa', 'fa-medkit', NULL),
(341, 'fa', 'fa-meh-o', NULL),
(342, 'fa', 'fa-mercury', NULL),
(343, 'fa', 'fa-microphone', NULL),
(344, 'fa', 'fa-microphone-slash', NULL),
(345, 'fa', 'fa-minus', NULL),
(346, 'fa', 'fa-minus-circle', NULL),
(347, 'fa', 'fa-minus-square', NULL),
(348, 'fa', 'fa-minus-square-o', NULL),
(349, 'fa', 'fa-mobile', NULL),
(350, 'fa', 'fa-mobile-phone(alias)', NULL),
(351, 'fa', 'fa-money', NULL),
(352, 'fa', 'fa-moon-o', NULL),
(353, 'fa', 'fa-mortar-board(alias)', NULL),
(354, 'fa', 'fa-motorcycle', NULL),
(355, 'fa', 'fa-music', NULL),
(356, 'fa', 'fa-navicon(alias)', NULL),
(357, 'fa', 'fa-neuter', NULL),
(358, 'fa', 'fa-newspaper-o', NULL),
(359, 'fa', 'fa-openid', NULL),
(360, 'fa', 'fa-outdent', NULL),
(361, 'fa', 'fa-pagelines', NULL),
(362, 'fa', 'fa-paint-brush', NULL),
(363, 'fa', 'fa-paper-plane', NULL),
(364, 'fa', 'fa-paper-plane-o', NULL),
(365, 'fa', 'fa-paperclip', NULL),
(366, 'fa', 'fa-paragraph', NULL),
(367, 'fa', 'fa-paste(alias)', NULL),
(368, 'fa', 'fa-pause', NULL),
(369, 'fa', 'fa-paw', NULL),
(370, 'fa', 'fa-paypal', NULL),
(371, 'fa', 'fa-pencil', NULL),
(372, 'fa', 'fa-pencil-square', NULL),
(373, 'fa', 'fa-pencil-square-o', NULL),
(374, 'fa', 'fa-phone', NULL),
(375, 'fa', 'fa-phone-square', NULL),
(376, 'fa', 'fa-photo(alias)', NULL),
(377, 'fa', 'fa-picture-o', NULL),
(378, 'fa', 'fa-pie-chart', NULL),
(379, 'fa', 'fa-pied-piper', NULL),
(380, 'fa', 'fa-pied-piper-alt', NULL),
(381, 'fa', 'fa-pinterest', NULL),
(382, 'fa', 'fa-pinterest-p', NULL),
(383, 'fa', 'fa-pinterest-square', NULL),
(384, 'fa', 'fa-plane', NULL),
(385, 'fa', 'fa-play', NULL),
(386, 'fa', 'fa-play-circle', NULL),
(387, 'fa', 'fa-play-circle-o', NULL),
(388, 'fa', 'fa-plug', NULL),
(389, 'fa', 'fa-plus', NULL),
(390, 'fa', 'fa-plus-circle', NULL),
(391, 'fa', 'fa-plus-square', NULL),
(392, 'fa', 'fa-plus-square-o', NULL),
(393, 'fa', 'fa-power-off', NULL),
(394, 'fa', 'fa-print', NULL),
(395, 'fa', 'fa-puzzle-piece', NULL),
(396, 'fa', 'fa-qq', NULL),
(397, 'fa', 'fa-qrcode', NULL),
(398, 'fa', 'fa-question', NULL),
(399, 'fa', 'fa-question-circle', NULL),
(400, 'fa', 'fa-quote-left', NULL),
(401, 'fa', 'fa-quote-right', NULL),
(402, 'fa', 'fa-ra(alias)', NULL),
(403, 'fa', 'fa-random', NULL),
(404, 'fa', 'fa-rebel', NULL),
(405, 'fa', 'fa-recycle', NULL),
(406, 'fa', 'fa-reddit', NULL),
(407, 'fa', 'fa-reddit-square', NULL),
(408, 'fa', 'fa-refresh', NULL),
(409, 'fa', 'fa-remove(alias)', NULL),
(410, 'fa', 'fa-renren', NULL),
(411, 'fa', 'fa-reorder(alias)', NULL),
(412, 'fa', 'fa-repeat', NULL),
(413, 'fa', 'fa-reply', NULL),
(414, 'fa', 'fa-reply-all', NULL),
(415, 'fa', 'fa-retweet', NULL),
(416, 'fa', 'fa-rmb(alias)', NULL),
(417, 'fa', 'fa-road', NULL),
(418, 'fa', 'fa-rocket', NULL),
(419, 'fa', 'fa-rotate-left(alias)', NULL),
(420, 'fa', 'fa-rotate-right(alias)', NULL),
(421, 'fa', 'fa-rouble(alias)', NULL),
(422, 'fa', 'fa-rss', NULL),
(423, 'fa', 'fa-rss-square', NULL),
(424, 'fa', 'fa-rub', NULL),
(425, 'fa', 'fa-ruble(alias)', NULL),
(426, 'fa', 'fa-rupee(alias)', NULL),
(427, 'fa', 'fa-save(alias)', NULL),
(428, 'fa', 'fa-scissors', NULL),
(429, 'fa', 'fa-search', NULL),
(430, 'fa', 'fa-search-minus', NULL),
(431, 'fa', 'fa-search-plus', NULL),
(432, 'fa', 'fa-sellsy', NULL),
(433, 'fa', 'fa-send(alias)', NULL),
(434, 'fa', 'fa-send-o(alias)', NULL),
(435, 'fa', 'fa-server', NULL),
(436, 'fa', 'fa-share', NULL),
(437, 'fa', 'fa-share-alt', NULL),
(438, 'fa', 'fa-share-alt-square', NULL),
(439, 'fa', 'fa-share-square', NULL),
(440, 'fa', 'fa-share-square-o', NULL),
(441, 'fa', 'fa-shekel(alias)', NULL),
(442, 'fa', 'fa-sheqel(alias)', NULL),
(443, 'fa', 'fa-shield', NULL),
(444, 'fa', 'fa-ship', NULL),
(445, 'fa', 'fa-shirtsinbulk', NULL),
(446, 'fa', 'fa-shopping-cart', NULL),
(447, 'fa', 'fa-sign-in', NULL),
(448, 'fa', 'fa-sign-out', NULL),
(449, 'fa', 'fa-signal', NULL),
(450, 'fa', 'fa-simplybuilt', NULL),
(451, 'fa', 'fa-sitemap', NULL),
(452, 'fa', 'fa-skyatlas', NULL),
(453, 'fa', 'fa-skype', NULL),
(454, 'fa', 'fa-slack', NULL),
(455, 'fa', 'fa-sliders', NULL),
(456, 'fa', 'fa-slideshare', NULL),
(457, 'fa', 'fa-smile-o', NULL),
(458, 'fa', 'fa-soccer-ball-o(alias)', NULL),
(459, 'fa', 'fa-sort', NULL),
(460, 'fa', 'fa-sort-alpha-asc', NULL),
(461, 'fa', 'fa-sort-alpha-desc', NULL),
(462, 'fa', 'fa-sort-amount-asc', NULL),
(463, 'fa', 'fa-sort-amount-desc', NULL),
(464, 'fa', 'fa-sort-asc', NULL),
(465, 'fa', 'fa-sort-desc', NULL),
(466, 'fa', 'fa-sort-down(alias)', NULL),
(467, 'fa', 'fa-sort-numeric-asc', NULL),
(468, 'fa', 'fa-sort-numeric-desc', NULL),
(469, 'fa', 'fa-sort-up(alias)', NULL),
(470, 'fa', 'fa-soundcloud', NULL),
(471, 'fa', 'fa-space-shuttle', NULL),
(472, 'fa', 'fa-spinner', NULL),
(473, 'fa', 'fa-spoon', NULL),
(474, 'fa', 'fa-spotify', NULL),
(475, 'fa', 'fa-square', NULL),
(476, 'fa', 'fa-square-o', NULL),
(477, 'fa', 'fa-stack-exchange', NULL),
(478, 'fa', 'fa-stack-overflow', NULL),
(479, 'fa', 'fa-star', NULL),
(480, 'fa', 'fa-star-half', NULL),
(481, 'fa', 'fa-star-half-empty(alias)', NULL),
(482, 'fa', 'fa-star-half-full(alias)', NULL),
(483, 'fa', 'fa-star-half-o', NULL),
(484, 'fa', 'fa-star-o', NULL),
(485, 'fa', 'fa-steam', NULL),
(486, 'fa', 'fa-steam-square', NULL),
(487, 'fa', 'fa-step-backward', NULL),
(488, 'fa', 'fa-step-forward', NULL),
(489, 'fa', 'fa-stethoscope', NULL),
(490, 'fa', 'fa-stop', NULL),
(491, 'fa', 'fa-street-view', NULL),
(492, 'fa', 'fa-strikethrough', NULL),
(493, 'fa', 'fa-stumbleupon', NULL),
(494, 'fa', 'fa-stumbleupon-circle', NULL),
(495, 'fa', 'fa-subscript', NULL),
(496, 'fa', 'fa-subway', NULL),
(497, 'fa', 'fa-suitcase', NULL),
(498, 'fa', 'fa-sun-o', NULL),
(499, 'fa', 'fa-superscript', NULL),
(500, 'fa', 'fa-support(alias)', NULL),
(501, 'fa', 'fa-table', NULL),
(502, 'fa', 'fa-tablet', NULL),
(503, 'fa', 'fa-tachometer', NULL),
(504, 'fa', 'fa-tag', NULL),
(505, 'fa', 'fa-tags', NULL),
(506, 'fa', 'fa-tasks', NULL),
(507, 'fa', 'fa-taxi', NULL),
(508, 'fa', 'fa-tencent-weibo', NULL),
(509, 'fa', 'fa-terminal', NULL),
(510, 'fa', 'fa-text-height', NULL),
(511, 'fa', 'fa-text-width', NULL),
(512, 'fa', 'fa-th', NULL),
(513, 'fa', 'fa-th-large', NULL),
(514, 'fa', 'fa-th-list', NULL),
(515, 'fa', 'fa-thumb-tack', NULL),
(516, 'fa', 'fa-thumbs-down', NULL),
(517, 'fa', 'fa-thumbs-o-down', NULL),
(518, 'fa', 'fa-thumbs-o-up', NULL),
(519, 'fa', 'fa-thumbs-up', NULL),
(520, 'fa', 'fa-ticket', NULL),
(521, 'fa', 'fa-times', NULL),
(522, 'fa', 'fa-times-circle', NULL),
(523, 'fa', 'fa-times-circle-o', NULL),
(524, 'fa', 'fa-tint', NULL),
(525, 'fa', 'fa-toggle-down(alias)', NULL),
(526, 'fa', 'fa-toggle-left(alias)', NULL),
(527, 'fa', 'fa-toggle-off', NULL),
(528, 'fa', 'fa-toggle-on', NULL),
(529, 'fa', 'fa-toggle-right(alias)', NULL),
(530, 'fa', 'fa-toggle-up(alias)', NULL),
(531, 'fa', 'fa-train', NULL),
(532, 'fa', 'fa-transgender', NULL),
(533, 'fa', 'fa-transgender-alt', NULL),
(534, 'fa', 'fa-trash', NULL),
(535, 'fa', 'fa-trash-o', NULL),
(536, 'fa', 'fa-tree', NULL),
(537, 'fa', 'fa-trello', NULL),
(538, 'fa', 'fa-trophy', NULL),
(539, 'fa', 'fa-truck', NULL),
(540, 'fa', 'fa-try', NULL),
(541, 'fa', 'fa-tty', NULL),
(542, 'fa', 'fa-tumblr', NULL),
(543, 'fa', 'fa-tumblr-square', NULL),
(544, 'fa', 'fa-turkish-lira(alias)', NULL),
(545, 'fa', 'fa-twitch', NULL),
(546, 'fa', 'fa-twitter', NULL),
(547, 'fa', 'fa-twitter-square', NULL),
(548, 'fa', 'fa-umbrella', NULL),
(549, 'fa', 'fa-underline', NULL),
(550, 'fa', 'fa-undo', NULL),
(551, 'fa', 'fa-university', NULL),
(552, 'fa', 'fa-unlink(alias)', NULL),
(553, 'fa', 'fa-unlock', NULL),
(554, 'fa', 'fa-unlock-alt', NULL),
(555, 'fa', 'fa-unsorted(alias)', NULL),
(556, 'fa', 'fa-upload', NULL),
(557, 'fa', 'fa-usd', NULL),
(558, 'fa', 'fa-user', NULL),
(559, 'fa', 'fa-user-md', NULL),
(560, 'fa', 'fa-user-plus', NULL),
(561, 'fa', 'fa-user-secret', NULL),
(562, 'fa', 'fa-user-times', NULL),
(563, 'fa', 'fa-users', NULL),
(564, 'fa', 'fa-venus', NULL),
(565, 'fa', 'fa-venus-double', NULL),
(566, 'fa', 'fa-venus-mars', NULL),
(567, 'fa', 'fa-viacoin', NULL),
(568, 'fa', 'fa-video-camera', NULL),
(569, 'fa', 'fa-vimeo-square', NULL),
(570, 'fa', 'fa-vine', NULL),
(571, 'fa', 'fa-vk', NULL),
(572, 'fa', 'fa-volume-down', NULL),
(573, 'fa', 'fa-volume-off', NULL),
(574, 'fa', 'fa-volume-up', NULL),
(575, 'fa', 'fa-warning(alias)', NULL),
(576, 'fa', 'fa-wechat(alias)', NULL),
(577, 'fa', 'fa-weibo', NULL),
(578, 'fa', 'fa-weixin', NULL),
(579, 'fa', 'fa-whatsapp', NULL),
(580, 'fa', 'fa-wheelchair', NULL),
(581, 'fa', 'fa-wifi', NULL),
(582, 'fa', 'fa-windows', NULL),
(583, 'fa', 'fa-won(alias)', NULL),
(584, 'fa', 'fa-wordpress', NULL),
(585, 'fa', 'fa-wrench', NULL),
(586, 'fa', 'fa-xing', NULL),
(587, 'fa', 'fa-xing-square', NULL),
(588, 'fa', 'fa-yahoo', NULL),
(589, 'fa', 'fa-yelp', NULL),
(590, 'fa', 'fa-yen(alias)', NULL),
(591, 'fa', 'fa-youtube', NULL),
(592, 'fa', 'fa-youtube-play', NULL),
(593, 'fa', 'fa-youtube-square', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `path` text NOT NULL,
  `filename` text,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(2, 'Registered User', '{\"user\":1,\"index\":1}', '2014-09-23 07:24:50', '2016-02-19 06:59:55'),
(3, 'Administrator', '{\"index\":1,\"report.order_qty\":1,\"admin\":1,\"admin.index\":1,\"user.manage\":1,\"user.user\":1,\"user.view\":1,\"product.organize\":1,\"user.edit\":1,\"jobroles.add\":1,\"jobroles.edit\":1,\"report.detailsummary\":1,\"jobroles.delete\":1,\"jobroles.view\":1,\"user.activate\":1,\"user.deactivate\":1,\"report.product_prices\":1,\"branch.add\":1,\"branch.edit\":1,\"branch.view\":1,\"branch.delete\":1,\"employee.add\":1,\"employee.edit\":1,\"employee.delete\":1,\"employee.view\":1,\"product.add\":1,\"product.edit\":1,\"product.delete\":1,\"product.view\":1,\"product-cat.add\":1,\"product-cat.edit\":1,\"product-cat.view\":1,\"inquiry-type.add\":1,\"inquiry-type.edit\":1,\"inquiry-type.delete\":1,\"inquiry-type.view\":1,\"agent.add\":1,\"agent.edit\":1,\"agent.delete\":1,\"agent.view\":1,\"attachment.add\":1,\"attachment.delete\":1,\"attachment.view\":1,\"attachment-cat.add\":1,\"attachment-cat.edit\":1,\"attachment-cat.delete\":1,\"attachment-cat.view\":1,\"city.add\":1,\"city.edit\":1,\"city.delete\":1,\"city.view\":1,\"agent-type.add\":1,\"agent-type.edit\":1,\"agent-type.delete\":1,\"agent-type.view\":1,\"json.agenttypelist\":1,\"json.agentlist\":1,\"offer-type.add\":1,\"offer-type.edit\":1,\"offer-type.delete\":1,\"offer-type.view\":1,\"offer.add\":1,\"offer.edit\":1,\"offer.delete\":1,\"offer.view\":1,\"json.offertypelist\":1,\"json.offerslist\":1,\"offer.details\":1,\"json.attachmentcategorylist\":1,\"customer-status.add\":1,\"customer-status.edit\":1,\"customer-status.delete\":1,\"customer-status.view\":1,\"json.customerstatuslist\":1,\"json.branchlist\":1,\"inquiry.detail\":1,\"target.view\":1,\"target.add\":1,\"target.edit\":1,\"target.delete\":1,\"json.targetlist\":1,\"offer.check\":1,\"quotation.new\":1,\"quotation.send\":1,\"ajax.send\":1,\"customer.details\":1,\"json.customerinquirylist\":1,\"customer.json_detail\":1,\"distributor.upload\":1,\"permission.add\":1,\"distributor.edit\":1,\"distributor.list\":1,\"json.distributorlist\":1,\"ajax.distributorlist\":1,\"segment.add\":1,\"segment.edit\":1,\"segment.view\":1,\"segment.delete\":1,\"category.add\":1,\"category.edit\":1,\"category.view\":1,\"category.list\":1,\"category.delete\":1,\"ajax.set_featured\":1,\"ajax.addtarget\":1,\"ajax.loadtarget\":1,\"product.images\":1,\"ajax.deleteimage\":1,\"ajax.setstatus\":1,\"ajax.addcart\":1,\"price.add\":1,\"price.edit\":1,\"price.list\":1,\"json.pricelist\":1,\"stock.view\":1,\"json.stocklist\":1,\"admin.inbox\":1,\"admin.reply\":1,\"report.conversionrate\":1,\"report.toporders\":1,\"report.salesbyagents\":1,\"report.inventory\":1,\"json.conversionrate\":1,\"json.overall_target_achievement\":1,\"report.order_histrory\":1,\"slider_image.view\":1,\"slider_image.edit\":1,\"slider_image.add\":1,\"json.categorylist\":1,\"json.productlist\":1,\"json.employeelist\":1,\"json.product_prices\":1,\"json.orderhistory\":1,\"user.distributor\":1,\"distributor.view\":1,\"json.userlist\":1,\"user.reset\":1,\"data.grabber\":1,\"pending.download\":1,\"user.add\":1,\"stock.add\":1,\"ajax.added_target\":1,\"product.upload\":1,\"target.upload\":1,\"contact.list\":1,\"contact.add\":1,\"contact.edit\":1,\"contact.delete\":1,\"json.contact_list\":1,\"report.distributor_forecast\":1,\"forecast.export-monthly\":1,\"ajax.load_monthly_requirnment_exportuser\":1}', '2014-09-30 07:09:12', '2016-03-10 06:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `lft` int(11) NOT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `label`, `link`, `icon`, `parent`, `permissions`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Root Menu', '#', NULL, NULL, NULL, 1, 66, 0, 1, '2016-09-20 18:30:00', '2017-12-24 11:09:01'),
(2, 'MENU MANAGEMENT', 'menu/list', NULL, 1, '[\"menu.add\",\"admin\"]', 2, 3, 1, 1, '2016-09-20 18:30:00', '2017-12-24 11:09:01'),
(3, 'BRANCH MANAGEMENT', 'branch/list', NULL, 1, '[\"branch.list\",\"branch.add\",\"branch.edit\",\"branch.delete\",\"admin\"]', 16, 17, 1, 1, '2016-09-21 05:25:43', '2017-12-24 11:09:01'),
(4, 'USER MANAGEMENT', '#', NULL, 1, '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', 6, 15, 1, 1, '2016-09-21 05:26:25', '2017-12-24 11:09:01'),
(5, 'PERMISSION', 'permission/list', NULL, 4, '[\"admin\"]', 7, 8, 2, 1, '2016-09-21 05:26:51', '2017-12-24 11:09:01'),
(6, 'ROLE', 'user/role/list', NULL, 4, '[\"admin\"]', 9, 10, 2, 1, '2016-09-21 05:27:15', '2017-12-24 11:09:01'),
(7, 'USER', 'user/list', NULL, 4, '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', 11, 12, 2, 1, '2016-09-21 05:27:51', '2017-12-24 11:09:01'),
(24, 'GALLERY', 'gallery/list', NULL, 35, '[\"gallery.list\",\"admin\"]', 19, 20, 2, 1, '2017-01-19 01:30:29', '2017-12-24 11:09:01'),
(33, 'BLOG', 'blog/list', NULL, 1, '[\"blog.add\",\"blog.edit\",\"blog.list\",\"blog.delete\",\"admin\"]', 54, 55, 1, 1, '2017-04-03 17:33:22', '2018-03-28 06:49:56'),
(34, 'GALLERY CATEGORY', 'gallery/category/list', NULL, 35, '[\"admin\"]', 21, 22, 2, 1, '2017-09-07 17:27:27', '2017-12-24 11:09:01'),
(35, 'GALLERY MANAGEMENT', '#', NULL, 1, '[\"admin\",\"admin\"]', 18, 23, 1, 1, '2017-09-07 17:29:35', '2017-12-24 11:09:01'),
(36, 'PRODUCT CREATION', '#', NULL, 1, '[\"admin\"]', 46, 51, 1, 1, '2017-09-08 17:46:40', '2018-03-28 06:49:56'),
(37, 'CATEGORY', 'product/creation/category/list', NULL, 36, '[\"admin\"]', 47, 48, 2, 1, '2017-09-08 17:47:26', '2018-03-28 06:49:56'),
(38, 'PRODUCT', 'product/creation/list', NULL, 36, '[\"admin\"]', 49, 50, 2, 1, '2017-09-08 17:48:30', '2018-03-28 06:49:56'),
(39, 'SLIDESHOW', 'slider/list', NULL, 1, '[\"admin\"]', 42, 43, 1, 1, '2017-09-11 05:59:39', '2018-03-28 06:49:56'),
(40, 'TESTIMONIALS', 'testimonials/list', NULL, 1, '[\"admin\"]', 38, 39, 1, 1, '2017-09-11 06:31:15', '2018-03-28 06:49:56'),
(42, 'Cake Type', 'caketype/list', NULL, 1, '[\"caketype.list\",\"caketype.add\",\"caketype.edit\",\"caketype.delete\",\"admin\"]', 54, 55, 1, 1, NULL, '2018-03-28 06:49:56'),
(43, 'publication', 'publication/list', NULL, 1, '[\"publication.list\",\"publication.add\",\"publication.edt\",\"publication.delete\",\"admin\"]', 50, 51, 1, 1, NULL, '2018-03-28 06:49:56'),
(44, 'Private Classes', 'privateclass/category/list', NULL, 1, '[\"admin\"]', 24, 25, 1, 1, '2017-09-18 09:09:46', '2018-03-28 05:50:44'),
(47, 'In the News', 'news/list', NULL, 1, '[\"admin\"]', 52, 53, 1, 1, '2017-09-25 13:50:32', '2018-03-28 06:49:56'),
(48, 'Quotes Request', 'quote/list', NULL, 1, '[\"admin\"]', 44, 45, 1, 1, '2017-10-01 08:18:10', '2018-03-28 06:49:56'),
(49, 'Tutorial Management', '#', NULL, 1, '[\"admin\"]', 32, 37, 1, 1, '2017-10-05 11:33:46', '2018-03-28 06:49:56'),
(50, 'Tutorial Category', 'recipesCategory/list', NULL, 49, '[\"admin\"]', 33, 34, 2, 1, '2017-10-05 11:34:46', '2018-03-28 06:49:56'),
(51, 'Tutorial list', 'recipe/list', NULL, 49, '[\"admin\"]', 35, 36, 2, 1, '2017-10-05 11:35:22', '2018-03-28 06:49:56'),
(52, 'Coupon Management', 'coupon/list', NULL, 1, '[\"admin\"]', 56, 57, 1, 1, '2017-10-17 16:59:22', '2018-03-28 06:49:56'),
(54, 'tasting management', '#', NULL, 1, '[\"admin\"]', 26, 31, 1, 1, '2017-10-17 18:22:15', '2018-03-28 05:50:44'),
(55, 'Tasting people type', 'tasting/people/type/list', NULL, 54, '[\"admin\"]', 27, 28, 2, 1, '2017-10-17 18:23:16', '2018-03-28 05:50:44'),
(56, 'Schedule a Tasting', 'tasting/schedule/list', NULL, 54, '[\"admin\"]', 29, 30, 2, 1, '2017-10-17 18:24:29', '2018-03-28 05:50:44'),
(57, 'System Settings', '#', NULL, 1, '[\"admin\"]', 58, 65, 1, 1, '2017-12-24 10:41:46', '2018-03-28 06:49:56'),
(58, 'Country List', 'country/list', NULL, 57, '[\"admin\"]', 59, 60, 2, 1, '2017-12-24 10:42:11', '2018-03-28 06:49:56'),
(59, 'Configure page', 'config/add', NULL, 57, '[\"admin\"]', 61, 62, 2, 1, '2017-12-24 11:02:28', '2018-03-28 06:49:56'),
(60, 'Pages List', 'pages/list', NULL, 57, '[\"admin\"]', 63, 64, 2, 1, '2017-12-24 11:05:04', '2018-03-28 06:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2017_09_04_143153_slider_table', 7),
('2017_11_12_232812_config_table', 8),
('2017_12_21_223821_create_table_counrties', 8),
('2017_12_30_181039_add_css_class_column_to_animation_type_table', 9),
('2018_01_06_171335_add_twitter_column_to_config', 10),
('2018_01_16_150501_create_comments_table', 11),
('2018_01_25_205542_add_code_column_to_coupon_table', 12),
('2018_02_08_202943_add_column_to_blog_table_and_in_the_news_table', 13),
('2018_03_20_223440_add_cover_image_to_blog', 14),
('2018_03_23_204344_add_cover_image_to_news_table', 14),
('2018_03_27_211154_add_tasting_success_config_table', 14),
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2015_05_16_172701_create_tables', 1),
('2015_05_16_180134_alter_users_table', 2),
('2015_05_25_211027_create_menu_table', 3),
('2015_05_26_103954_alter_menu_table', 4),
('2015_05_26_114356_alter_menu_table', 5),
('2014_07_02_230147_migration_cartalyst_sentinel', 6),
('2017_09_04_143153_slider_table', 7),
('2017_11_12_232812_config_table', 8),
('2017_12_21_223821_create_table_counrties', 8),
('2017_12_30_181039_add_css_class_column_to_animation_type_table', 9),
('2018_01_06_171335_add_twitter_column_to_config', 10),
('2018_01_16_150501_create_comments_table', 11),
('2018_01_25_205542_add_code_column_to_coupon_table', 12),
('2018_02_08_202943_add_column_to_blog_table_and_in_the_news_table', 13),
('2018_03_20_223440_add_cover_image_to_blog', 14),
('2018_03_23_204344_add_cover_image_to_news_table', 14),
('2018_03_27_211154_add_tasting_success_config_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Normal Registered User', 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:39'),
(2, 'menu.add', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-03 02:32:41'),
(3, 'menu.list', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:54'),
(4, 'menu.edit', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:57'),
(5, 'menu.status', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:25:01'),
(6, 'admin', 'Super Admin Permission', 1, 1, '2015-07-24 18:30:00', '2015-07-24 18:30:00'),
(7, 'index', 'Home Page Permission', 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:25:03'),
(8, 'menu.delete', NULL, 1, 1, '2015-09-06 09:00:06', '2015-09-06 09:00:09'),
(9, 'user.add', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(10, 'user.edit', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(11, 'user.delete', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(12, 'user.list', NULL, 1, 1, '2015-10-19 18:30:00', '2015-10-20 14:31:57'),
(13, 'user.role.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(14, 'user.role.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(15, 'user.role.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(16, 'user.role.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(17, 'permission.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(18, 'permission.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(19, 'permission.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(20, 'permission.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(21, 'permission.group.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(22, 'permission.group.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(23, 'permission.group.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(24, 'permission.group.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(25, 'user.status', NULL, 1, 1, '2015-12-18 18:30:47', '2015-12-18 18:30:47'),
(26, 'device.list', 'DEVICE LIST', 1, 1, '2017-01-23 13:14:34', '2017-03-12 17:23:58'),
(27, 'dashboard', 'DASHBOARD', 1, 1, '2017-01-23 13:18:39', '2017-01-23 13:18:39'),
(28, 'device.add', 'DEVICE ADD', 1, 1, '2017-01-23 13:19:46', '2017-01-23 13:19:46'),
(29, 'device.edit', 'DEVICE EDIT', 1, 1, '2017-01-23 13:20:02', '2017-01-23 13:20:02'),
(30, 'device.delete', 'DEVICE DELETE', 1, 1, '2017-01-23 13:20:24', '2017-01-23 13:20:24'),
(31, 'gallery.list', 'GALLERY LIST', 1, 1, '2017-01-23 13:21:05', '2017-01-23 13:21:05'),
(32, 'gallery.add', 'Gallery ADD', 1, 1, '2017-01-23 13:21:23', '2017-01-23 13:21:23'),
(33, 'gallery.edit', 'GALLERY EDIT', 1, 1, '2017-01-23 13:21:36', '2017-01-23 13:21:36'),
(34, 'gallery.delete', 'GALLERY DELETE', 1, 1, '2017-01-23 13:21:53', '2017-01-23 13:21:53'),
(35, 'reservation.list', 'RESERVATION LIST', 1, 1, '2017-01-28 05:34:27', '2017-01-28 05:34:27'),
(36, 'product.list', 'PRODUCT LIST', 1, 1, '2017-02-20 09:07:26', '2017-02-20 09:07:26'),
(37, 'product.add', 'PRODUCT ADD', 1, 1, '2017-02-20 09:07:49', '2017-02-20 09:07:49'),
(38, 'product.edit', 'PRODUCT EDIT', 1, 1, '2017-02-20 09:08:08', '2017-02-20 09:08:08'),
(39, 'product.delete', 'PRODUCT DELETE', 1, 1, '2017-03-14 15:24:22', '2017-03-14 15:24:22'),
(40, 'series.list', 'SERIES LIST', 1, 1, '2017-03-14 15:31:49', '2017-03-14 15:31:49'),
(41, 'series.add', 'SERIES ADD', 1, 1, '2017-03-14 15:32:03', '2017-03-14 15:32:03'),
(42, 'series.edit', 'SERIES EDIT', 1, 1, '2017-03-14 15:32:17', '2017-03-14 15:32:17'),
(43, 'series.delete', 'SERIES DELETE', 1, 1, '2017-03-14 15:32:33', '2017-03-14 15:32:33'),
(44, 'feature.add', 'ADD FEATURE', 1, 1, '2017-03-26 04:12:51', '2017-03-26 04:12:51'),
(45, 'feature.edit', 'EDIT FEATURE', 1, 1, '2017-03-26 04:13:08', '2017-03-26 04:13:08'),
(46, 'feature.list', 'LIST FEATURE', 1, 1, '2017-03-26 04:13:23', '2017-03-26 04:13:23'),
(47, 'feature.delete', 'DELETE FEATURE', 1, 1, '2017-03-26 04:13:41', '2017-03-26 04:13:41'),
(48, 'blog', 'BLOG', 1, 1, '2017-04-03 17:32:46', '2017-04-03 17:32:46'),
(49, 'blog.add', 'ADD BLOG', 1, 1, '2017-05-08 16:17:46', '2017-05-08 16:17:46'),
(50, 'blog.edit', 'EDIT BLOG', 1, 1, '2017-05-08 16:18:01', '2017-05-08 16:18:01'),
(51, 'blog.list', 'LIST BLOG', 1, 1, '2017-05-08 16:18:13', '2017-05-08 16:18:13'),
(52, 'blog.delete', 'DELETE BLOG', 1, 1, '2017-05-08 16:18:28', '2017-05-08 16:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(0, 0, '5xbe0ZeNCG4cO4GaccNbkZcAdoKrjokb', '2018-09-16 14:17:43', '2018-09-16 14:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `private_class_registation`
--

CREATE TABLE `private_class_registation` (
  `id` int(11) NOT NULL,
  `class` int(11) DEFAULT NULL,
  `parent_is_reg` int(11) NOT NULL DEFAULT '0',
  `parent_full_name` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `private_class_registation`
--

INSERT INTO `private_class_registation` (`id`, `class`, `parent_is_reg`, `parent_full_name`, `first_name`, `last_name`, `phone`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'parent #1', 'child f1', 'child l1', 'p1', 'lakshitha.infomail@gmail.com', 1, '2017-10-16 19:22:56', '2017-10-16 19:22:56'),
(2, 1, 1, 'Mayura Lakshitha Hettiarachchi', 'Mayura', 'Hettiarachchi', '719781498', 't.rajapaksha2016@gmail.com', 1, '2017-10-16 19:33:52', '2017-10-16 19:33:52'),
(3, 1, 1, '', 'Mayura', 'Hettiarachchi', '719781498', 'lakshitha.infomail@gmail.com', 1, '2017-10-16 19:34:36', '2017-10-16 19:34:36'),
(4, 1, 1, '', 'Mayura', 'Hettiarachchi', '719781498', 'lakshitha.infomail@gmail.com', 1, '2017-10-16 20:37:00', '2017-10-16 20:37:00'),
(5, 1, 1, '', 'Mayura', 'Hettiarachchi', '719781498', 't.rajapaksha2016@gmail.com', 1, '2017-10-16 20:37:41', '2017-10-16 20:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `publication_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`publication_id`, `image`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ACD Magazine.png', 'ACD magazine', 1, '2018-02-13 13:53:19', '2018-02-13 13:53:19'),
(2, 'Cake Central Magazine.png', 'Cake Central magazine', 1, '2018-02-13 13:53:47', '2018-02-13 13:53:47'),
(3, 'Cake Masters Magazine.png', 'Cake Masters', 1, '2018-02-13 13:54:09', '2018-02-13 13:54:09'),
(4, 'DIY Weddings Magazine.png', 'DIY Weddings Magazine', 1, '2018-02-13 14:02:41', '2018-02-13 14:02:41'),
(5, 'suger indias.png', 'Suger, India\'s cake art Magazine', 1, '2018-02-13 14:03:15', '2018-02-13 14:03:15'),
(6, 'Cake new Masters Magazine.png', 'Cake Central magazine', 1, '2018-03-18 19:37:54', '2018-03-18 19:37:54');

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE `quote` (
  `quote_id` int(11) NOT NULL,
  `cake_type` int(11) NOT NULL,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requested_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completed_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `province` text NOT NULL,
  `post_code` text NOT NULL,
  `venue` text NOT NULL,
  `details` text,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text,
  `email` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ip` text,
  `ip_location` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`quote_id`, `cake_type`, `event_date`, `requested_date`, `completed_date`, `province`, `post_code`, `venue`, `details`, `first_name`, `last_name`, `phone`, `email`, `status`, `ip`, `ip_location`, `created_at`, `updated_at`) VALUES
(1, 0, '2017-11-29 18:30:00', '2017-10-15 16:55:29', '0000-00-00 00:00:00', 'western province', '10118', 'colombo', '', 'Mayura', 'Hettiarachchi', '719781498', 'lakshitha.infomail@gmail.com', 1, '::1', NULL, '2017-10-15 16:55:29', '2017-10-15 16:55:29'),
(2, 0, '2018-05-26 18:01:00', '2018-02-07 18:03:05', '0000-00-00 00:00:00', 'western province', '10118', 'sdfsd', 'fsdfasdfasdfasdf', 'Mayura', 'Hettiarachchi', '719781498', 'rullzzm@gmail.com', 1, '::1', NULL, '2018-02-07 18:03:05', '2018-02-07 18:03:05'),
(3, 2, '2018-06-09 09:12:00', '2018-02-26 09:12:43', '0000-00-00 00:00:00', 'western province', '10118', 'asdfasdf', ' ', 'Mayura', 'Hettiarachchi', '719781498', 'rullzzm@gmail.com', 1, '::1', NULL, '2018-02-26 09:12:43', '2018-02-26 09:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `recipe_id` int(11) NOT NULL,
  `name` text,
  `cover_path` text,
  `cover_file` text,
  `intro` text,
  `main` text,
  `lesson` text,
  `material` text,
  `ingrediant` text,
  `recipes` text,
  `pdf_path` text,
  `pdf_file` text,
  `note` text,
  `category` int(11) DEFAULT NULL,
  `skill_level` varchar(255) DEFAULT NULL,
  `cake_type` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `free_paid` int(11) NOT NULL DEFAULT '0',
  `premium` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`recipe_id`, `name`, `cover_path`, `cover_file`, `intro`, `main`, `lesson`, `material`, `ingrediant`, `recipes`, `pdf_path`, `pdf_file`, `note`, `category`, `skill_level`, `cake_type`, `price`, `free_paid`, `premium`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fsdfsd', 'uploads/images/recipe', 'recipe-cover-20180915230844.jpg', '', '', 'fsdfs', 'fsdf', 'fsdf', 'sdfsff', 'uploads/pdf/recipe', '', 'fdsf', 1, 'Beginner', '1', 3322, 0, 1, 1, '2018-09-15 17:38:44', '2018-09-15 17:38:44'),
(2, 'fdsfsfdf', 'uploads/images/recipe', 'recipe-cover-20180915230919.jpg', '455', '64', 'fsfsdf', 'dfdsfsd', 'fsdfs', 'fsdf', 'uploads/pdf/recipe', '', 'dfsdf', 1, 'Beginner', '1', 123, 1, 0, 1, '2018-09-15 17:39:19', '2018-09-15 17:39:19');

-- --------------------------------------------------------

--
-- Table structure for table `recipes_category`
--

CREATE TABLE `recipes_category` (
  `recipes_category_id` int(11) NOT NULL,
  `name` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_category`
--

INSERT INTO `recipes_category` (`recipes_category_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'category #1', 1, '2017-10-23 09:00:55', '2017-10-23 09:00:55');

-- --------------------------------------------------------

--
-- Table structure for table `recipe_photo`
--

CREATE TABLE `recipe_photo` (
  `recipe_photo_id` int(11) NOT NULL,
  `recipe_id` int(11) DEFAULT NULL,
  `path` text,
  `filename` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe_photo`
--

INSERT INTO `recipe_photo` (`recipe_photo_id`, `recipe_id`, `path`, `filename`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'C:\\xampp\\htdocs\\sdcadmin-development\\core\\storage\\uploads/images/images', 'recipe-cover-01200px-Sponge_cake_at_Top_Cantonese_Restaurant.jpg', 1, '2018-01-14 11:32:27', '2018-01-14 11:32:27'),
(2, 2, 'C:\\xampp\\htdocs\\sdcadmin-development\\core\\storage\\uploads/images/images', 'recipe-cover-121609c32045ae20cc3d0b515e4269e77.jpg', 1, '2018-01-14 11:32:27', '2018-01-14 11:32:27'),
(3, 2, 'C:\\xampp\\htdocs\\sdcadmin-development\\core\\storage\\uploads/images/images', 'recipe-cover-2basic-sponge-cake_1980x1320-118404-1.jpg', 1, '2018-01-14 11:32:27', '2018-01-14 11:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'registered', '    Registered    ', '{\"front\":true,\"my-account\":true,\"index\":true}', 1, '2015-10-26 04:16:31', '2015-12-18 21:54:02'),
(2, 'administrator', '      Administrator      ', '{\"admin\":true,\"my-account\":true}', 1, '2015-12-18 18:30:47', '2015-12-18 21:57:50'),
(3, 'sdc-admin', 'SDC ADMIN', '{\"admin\":true}', 1, '2017-01-23 07:04:57', '2017-09-11 07:38:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(9, 2, '2017-03-12 17:22:26', '2017-03-12 17:22:26'),
(9, 3, '2017-03-12 17:22:26', '2017-03-12 17:22:26'),
(10, 1, '2017-12-27 14:10:24', '2017-12-27 14:10:24'),
(11, 1, '2018-01-07 14:18:27', '2018-01-07 14:18:27'),
(12, 1, '2018-01-29 16:15:58', '2018-01-29 16:15:58'),
(15, 1, '2018-02-01 04:44:24', '2018-02-01 04:44:24'),
(17, 1, '2018-02-01 04:45:53', '2018-02-01 04:45:53'),
(18, 1, '2018-02-01 04:58:20', '2018-02-01 04:58:20'),
(19, 1, '2018-02-07 15:33:02', '2018-02-07 15:33:02'),
(22, 1, '2018-02-21 11:58:03', '2018-02-21 11:58:03'),
(23, 1, '2018-02-26 15:17:22', '2018-02-26 15:17:22'),
(24, 1, '2018-02-26 15:22:36', '2018-02-26 15:22:36'),
(26, 1, '2018-02-26 15:24:22', '2018-02-26 15:24:22'),
(27, 1, '2018-03-02 21:00:19', '2018-03-02 21:00:19'),
(29, 1, '2018-03-18 19:50:18', '2018-03-18 19:50:18'),
(30, 1, '2018-04-22 14:57:07', '2018-04-22 14:57:07'),
(34, 1, '2018-05-10 17:21:10', '2018-05-10 17:21:10'),
(36, 1, '2018-05-10 17:40:13', '2018-05-10 17:40:13'),
(0, 1, '2018-09-01 02:38:24', '2018-09-01 02:38:24'),
(0, 1, '2018-09-09 02:58:56', '2018-09-09 02:58:56'),
(0, 1, '2018-09-15 18:28:02', '2018-09-15 18:28:02'),
(0, 1, '2018-09-16 14:17:43', '2018-09-16 14:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `sa_billing_detail`
--

CREATE TABLE `sa_billing_detail` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` text NOT NULL,
  `province` text NOT NULL,
  `postcode` text NOT NULL,
  `street_addresss_1` text NOT NULL,
  `street_addresss_2` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `shipping_method` int(11) DEFAULT '1',
  `is_primary` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_billing_detail`
--

INSERT INTO `sa_billing_detail` (`id`, `country_id`, `city`, `province`, `postcode`, `street_addresss_1`, `street_addresss_2`, `first_name`, `last_name`, `phone`, `email`, `shipping_method`, `is_primary`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Malabe', 'western province', '10118', 'No 153, jayagath Mw,', 'Hokandara north, Hokandara', 'Lakshitha', 'Hettiarachchi', '0713399099', 'rullzzm@gmail.com', 1, 1, 9, '2018-02-03 05:17:31', '2018-02-03 05:17:31', NULL),
(2, 1, 'Malabe', 'Malabe', '10118', 'No 153, jayagath Mw,', 'Hokandara north, Hokandara', 'Lakshitha', 'Hettiarachchi', '719781498', 'rullzzm@gmail.com', 1, 1, 22, '2018-02-21 11:58:46', '2018-02-21 11:58:46', NULL),
(7, 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, 1, '2018-05-08 15:03:01', '2018-05-09 16:02:50', NULL),
(8, 3, 'new york', 'hsajf', '80000', '25sacac', 'ascsac', 'COOL', 'baby', '248481818', 'insaf.zak@gmail.com', 1, 1, 1, '2018-05-10 18:13:25', '2018-05-10 18:13:25', NULL),
(0, 2, 'fsdfd', 'sdfsdf', '4234', 'fsdfsd', 'sfsdf', 'dsfs', 'fsdf', '534534534535', 'mp.chathuranga@gmail.com', 1, 1, 0, '2018-09-15 18:28:38', '2018-09-15 18:28:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_blog`
--

CREATE TABLE `sa_blog` (
  `id` int(11) NOT NULL,
  `name` text,
  `video_url` text,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `home` tinyint(1) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `front_image` text NOT NULL COMMENT 'main page of this image will be save as thum_imagename.png',
  `cover_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_blog`
--

INSERT INTO `sa_blog` (`id`, `name`, `video_url`, `start_date`, `end_date`, `description`, `home`, `status`, `created_at`, `updated_at`, `front_image`, `cover_image`) VALUES
(1, '“BLOOMING LOVE” VALENTINE’S CAKE TUTORIAL', 'https://www.youtube.com/watch?v=kfLng4Gf944', '2017-12-30', '2018-03-31', '<p>Hi Everyone, Wanted to share my &ldquo;Blooming Love&rdquo; Valentine&rsquo;s Cake Tutorial. In this tutorial you will learn a lot of new techniques using Wafer paper and other mediums.</p>\r\n<p>For a limited time you can purchase this Tutorial for $15 at&nbsp;<a href=\"onlineTutorial\">Tutorial page</a>.</p>\r\n<p>Trailer of&nbsp;the&nbsp;tutorial video</p>\r\n<p>The list of words below are the most frequently used 2265 words in Spoken English. The words were selected by analyzing more than 250,000 words from hundreds of conversations. The only words to make it into the list were those that were also found in the BNC top 3,000, The COCA 5,000, and the Longman 3,000.&nbsp;<br /><br />The frequency number is how often the word appears out of 250,000 words. For example if you divide 250,000 by 9243, then we can see that the word \"the\" appears once in every 27 words. However, this number should only be used as a general guide. The numbers can change depending on the topic, demographic, and many other variables. Although the number cannot be accurate, it can be used to provide insight into the frequency of words in relation to other words.&nbsp;<br /><br />The Type column refers to what type of word it is. In many cases, a word can be multiple types. The first one listed is more frequently used than the second. For example, the word \"can\" is used as an auxiliary verb such as, \"I can ride a bike.\" Also, it is a noun such as can of food. In this particular case, \"can\" is used commonly in both ways, but it is more frequently used as an auxiliary verb. In some cases, the third or fourth type might not be used regularly.&nbsp;<br /><br />There can be typos or errors. &nbsp;If you find anything that is incorrect, please email talkenglish@talkenglish.com. &nbsp;We will do our best to correct everything.</p>', 0, 1, '2017-12-30 17:48:35', '2018-03-18 18:09:33', 'blooming-love-valentines-cake-tutorial-1521396572.png', ''),
(2, 'GRAVITY DEFYING MINION CAKE', 'https://www.youtube.com/watch?v=fkIMdzRvsfQ', '2017-12-30', '2018-03-23', '<p>I always wanted to do a gravity defying Minion cake and when I was given the Minion cake by Rachel Jones in the <a href=\"https://www.facebook.com/MeMyselfandICollaboration\">Me, Myself, and I Collaboration&nbsp;</a>it was the perfect opportunity for me to make this gravity defying cake.&nbsp; My Minion is holding on to some balloons when a wind comes and sweeps him off the ground and he is terrified if he will ever come down. Hope you like it and follow me at&nbsp;<a href=\"https://www.facebook.com/sdcakery\">Sweet Delights Cakery</a>&nbsp;&lt;3</p>', 0, 1, '2017-12-30 18:08:53', '2018-03-18 18:10:51', 'gravity-defying-minion-cake-1521396650.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `sa_blog_image`
--

CREATE TABLE `sa_blog_image` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `path` text,
  `filename` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_blog_image`
--

INSERT INTO `sa_blog_image` (`id`, `blog_id`, `path`, `filename`, `created_at`, `updated_at`) VALUES
(1, 18, 'core/storage/uploads/images/blog', 'blog-20171024193112-0.jpg', '2017-12-31 04:14:33', '2017-10-24 14:01:12'),
(2, 19, 'core/storage/uploads/images/blog', 'blog-20171228232827-0.PNG', '2017-12-31 04:14:37', '2017-12-28 17:58:27'),
(3, 20, 'core/storage/uploads/images/blog', 'blog-20171228232955-0.jpg', '2017-12-31 04:14:42', '2017-12-28 17:59:55'),
(7, 1, 'core/storage/uploads/images/blog', 'blog-20180318233932-0.PNG', '2018-03-18 18:09:32', '2018-03-18 18:09:32'),
(8, 2, 'core/storage/uploads/images/blog', 'blog-20180318234050-0.PNG', '2018-03-18 18:10:50', '2018-03-18 18:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `sa_branch`
--

CREATE TABLE `sa_branch` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_branch`
--

INSERT INTO `sa_branch` (`id`, `name`, `code`, `address`, `tel`, `city_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HEADOFFICE', '-', 'NEGAMBO', '756388155', 4, 1, '2016-09-29 16:31:30', '2016-10-13 09:48:48', NULL),
(2, 'KANDY', 'K-001', 'Kandy', '7995', 6, 1, '2017-01-18 19:03:04', '2017-01-18 19:03:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_colour`
--

CREATE TABLE `sa_colour` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_colour`
--

INSERT INTO `sa_colour` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'WHITE', '2017-04-22 14:19:27', '0000-00-00 00:00:00'),
(2, 'YELLOW', '2017-04-22 14:19:27', '0000-00-00 00:00:00'),
(3, 'BLUE', '2017-04-22 14:19:42', '0000-00-00 00:00:00'),
(4, 'RED', '2017-04-22 14:19:42', '0000-00-00 00:00:00'),
(5, 'GREEN', '2017-04-22 14:19:51', '0000-00-00 00:00:00'),
(6, 'GOLD', '2017-04-27 06:05:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sa_comment`
--

CREATE TABLE `sa_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sa_comment`
--

INSERT INTO `sa_comment` (`id`, `blog_id`, `name`, `email`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'Lakshitha M Hettiarachchi', 'rullzzm@gmail.com', 'Hi Everyone, Wanted to share my “Blooming Love” Valentine’s Cake Tutorial. In this tutorial you will learn a lot of new techniques using Wafer paper and other mediums.\n\nFor a limited time you can purchase this Tutorial for $15 at Tutorial page.\n\nTrailer of the tutorial video\n\nThe list of words below are the most frequently used 2265 words in Spoken English. The words were selected by analyzing more than 250,000 words from hundreds of conversations. The only words to make it into the list were those that were also found in the BNC top 3,000, The COCA 5,000, and the Longman 3,000. \n\nThe frequency number is how often the word appears out of 250,000 words. For example if you divide 250,000 by 9243, then we can see that the word \"the\" appears once in every 27 words. However, this number should only be used as a general guide. The numbers can change depending on the topic, demographic, and many other variables. Although the number cannot be accurate, it can be used to provide insight into the frequency of words in relation to other words. \n\nThe Type column refers to what type of word it is. In many cases, a word can be multiple types. The first one listed is more frequently used than the second. For example, the word \"can\" is used as an auxiliary verb such as, \"I can ride a bike.\" Also, it is a noun such as can of food. In this particular case, \"can\" is used commonly in both ways, but it is more frequently used as an auxiliary verb. In some cases, the third or fourth type might not be used regularly. \n\nThere can be typos or errors.  If you find anything that is incorrect, please email talkenglish@talkenglish.com.  We will do our best to correct everything.', '2018-01-16 17:17:12', '2018-01-16 17:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `sa_country`
--

CREATE TABLE `sa_country` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_country`
--

INSERT INTO `sa_country` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SRI LANKA', '2017-12-11 06:01:19', '0000-00-00 00:00:00', NULL),
(2, 'ENGLAND', '2017-12-11 06:01:19', '0000-00-00 00:00:00', NULL),
(3, 'AMERICA', '2017-12-11 06:01:35', '0000-00-00 00:00:00', NULL),
(4, 'AFRICA', '2017-12-11 06:01:35', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_device`
--

CREATE TABLE `sa_device` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_device`
--

INSERT INTO `sa_device` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MOBILE', 1, '2017-03-30 19:39:24', '2017-03-30 19:39:24', NULL),
(2, 'EARPHONE', 1, '2017-03-30 19:40:11', '2017-03-30 19:40:11', NULL),
(3, 'SHELL / BACK', 1, '2017-03-30 19:40:28', '2017-03-30 19:40:28', NULL),
(4, 'BATTERY', 1, '2017-03-30 19:40:35', '2017-03-30 19:40:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_discounts`
--

CREATE TABLE `sa_discounts` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_feature`
--

CREATE TABLE `sa_feature` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_feature`
--

INSERT INTO `sa_feature` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Display', '', 1, '2017-04-13 07:19:58', '2017-04-24 06:53:09'),
(3, 'RAM', '', 1, '2017-04-13 07:20:08', '2017-04-13 07:20:08'),
(4, 'ROM', '', 1, '2017-04-13 07:20:14', '2017-04-13 07:20:14'),
(5, 'Operating System', '', 1, '2017-04-13 07:20:29', '2017-04-18 06:32:10'),
(6, 'Battery', '', 1, '2017-04-13 07:20:44', '2017-04-13 07:20:44'),
(7, 'CPU', '', 1, '2017-04-13 07:21:02', '2017-04-13 07:21:02'),
(8, 'Camera', '', 1, '2017-04-13 07:21:29', '2017-04-13 07:21:29'),
(9, 'Networks', '', 1, '2017-04-13 07:23:22', '2017-04-13 07:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `sa_gallery`
--

CREATE TABLE `sa_gallery` (
  `id` int(11) NOT NULL,
  `album_name` text NOT NULL,
  `description` text,
  `categories` text NOT NULL,
  `path` text NOT NULL,
  `filename` text,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_gallery`
--

INSERT INTO `sa_gallery` (`id`, `album_name`, `description`, `categories`, `path`, `filename`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'Police Box 3D cake', '2018 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '[\"1\"]', 'uploads/images/gallery', 'gallery-cover-20171224190003.jpg', 9, '2017-12-24 13:30:03', '2018-01-18 14:36:11', NULL, 1),
(2, 'Birthday Cake 01', 'A hyper realistic version of a childhood favorite: Popeye! This cake was created for Avalon’s very own “Sugar Spooks” halloween cake collaboration. This cake was partly inspired by the earlier work of Rick Baker.', '[\"2\"]', 'uploads/images/gallery', 'gallery-cover-20171224190050.jpg', 9, '2017-12-24 13:30:50', '2018-01-18 15:45:22', NULL, 1),
(3, 'Birthday Cake 02', '', '[\"2\"]', 'uploads/images/gallery', 'gallery-cover-20171224190106.jpg', 9, '2017-12-24 13:31:06', '2017-12-24 13:31:06', NULL, 1),
(4, 'Grooms cake 01', '', '[\"3\"]', 'uploads/images/gallery', 'gallery-cover-20171224190201.jpg', 9, '2017-12-24 13:32:01', '2017-12-24 13:32:01', NULL, 1),
(5, 'Sculpted cake 01', '', '[\"4\"]', 'uploads/images/gallery', 'gallery-cover-20171224190249.jpg', 9, '2017-12-24 13:32:49', '2017-12-24 13:32:49', NULL, 1),
(6, 'Sugar Flowers 01', 'rlkjkjflkjkrljkfdjfkejrkllkhjgfgkjglfdg', '[\"5\"]', 'uploads/images/gallery', 'gallery-cover-20171224190350.jpg', 9, '2017-12-24 13:33:50', '2018-09-16 12:13:29', NULL, 1),
(7, 'Wedding cake 01', '', '[\"6\"]', 'uploads/images/gallery', 'gallery-cover-20171224190435.jpg', 9, '2017-12-24 13:34:35', '2017-12-24 13:34:35', NULL, 1),
(8, 'Wedding cake 02', '', '[\"6\"]', 'uploads/images/gallery', 'gallery-cover-20171224190511.jpg', 9, '2017-12-24 13:35:11', '2017-12-24 13:35:11', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_gallery_category`
--

CREATE TABLE `sa_gallery_category` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_gallery_category`
--

INSERT INTO `sa_gallery_category` (`id`, `name`, `created_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '3D Cakes', 9, 1, '2017-12-24 13:25:19', '2017-12-24 13:25:19', NULL),
(2, 'Birthday Cakes', 9, 1, '2017-12-24 13:25:28', '2017-12-24 13:25:28', NULL),
(3, 'Grooms Cakes', 9, 1, '2017-12-24 13:25:44', '2017-12-24 13:25:44', NULL),
(4, 'Sculpted Cakes', 9, 1, '2017-12-24 13:25:52', '2017-12-24 13:25:52', NULL),
(5, 'Sugar Flowers', 9, 1, '2017-12-24 13:25:58', '2017-12-24 13:25:58', NULL),
(6, 'Wedding Cakes', 9, 1, '2017-12-24 13:26:05', '2017-12-24 13:26:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_gallery_image`
--

CREATE TABLE `sa_gallery_image` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `filename` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_gallery_image`
--

INSERT INTO `sa_gallery_image` (`id`, `gallery_id`, `path`, `filename`, `created_at`, `updated_at`) VALUES
(1, 1, 'uploads/images/gallery', 'gallery-sub-20171224190003-0.jpg', '2017-12-24 13:30:03', '2017-12-24 13:30:03'),
(2, 2, 'uploads/images/gallery', 'gallery-sub-20171224190050-0.jpg', '2017-12-24 13:30:50', '2017-12-24 13:30:50'),
(3, 3, 'uploads/images/gallery', 'gallery-sub-20171224190106-0.jpg', '2017-12-24 13:31:06', '2017-12-24 13:31:06'),
(4, 4, 'uploads/images/gallery', 'gallery-sub-20171224190201-0.jpg', '2017-12-24 13:32:01', '2017-12-24 13:32:01'),
(5, 5, 'uploads/images/gallery', 'gallery-sub-20171224190249-0.jpg', '2017-12-24 13:32:49', '2017-12-24 13:32:49'),
(6, 6, 'uploads/images/gallery', 'gallery-sub-20171224190350-0.jpg', '2017-12-24 13:33:50', '2017-12-24 13:33:50'),
(7, 7, 'uploads/images/gallery', 'gallery-sub-20171224190435-0.jpg', '2017-12-24 13:34:35', '2017-12-24 13:34:35'),
(8, 7, 'uploads/images/gallery', 'gallery-sub-20171224190435-1.jpg', '2017-12-24 13:34:35', '2017-12-24 13:34:35'),
(9, 7, 'uploads/images/gallery', 'gallery-sub-20171224190435-2.jpg', '2017-12-24 13:34:35', '2017-12-24 13:34:35'),
(10, 8, 'uploads/images/gallery', 'gallery-sub-20171224190511-0.jpg', '2017-12-24 13:35:11', '2017-12-24 13:35:11'),
(0, 6, 'uploads/images/gallery', 'gallery-sub-20180916174329-0.jpg', '2018-09-16 12:13:29', '2018-09-16 12:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `sa_last_shipping`
--

CREATE TABLE `sa_last_shipping` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` text NOT NULL,
  `province` text NOT NULL,
  `postcode` text NOT NULL,
  `street_addresss_1` text NOT NULL,
  `street_addresss_2` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `shipping_method` int(11) DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_last_shipping`
--

INSERT INTO `sa_last_shipping` (`id`, `country_id`, `city`, `province`, `postcode`, `street_addresss_1`, `street_addresss_2`, `first_name`, `last_name`, `phone`, `email`, `shipping_method`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Malabe', 'western province', '10118', 'No 153, jayagath Mw,', 'Hokandara north, Hokandara', 'Lakshitha', 'Hettiarachchi', '0713399099', 'rullzzm@gmail.com', 1, 9, '2018-02-03 05:17:31', '2018-02-03 05:17:31', NULL),
(2, 2, '', '', '', '', '', '', '', '', '', 1, 26, '2018-03-24 13:28:28', '2018-03-24 13:28:28', NULL),
(3, 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-01 17:34:56', '2018-05-10 18:13:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_news`
--

CREATE TABLE `sa_news` (
  `id` int(11) NOT NULL,
  `title` varchar(180) NOT NULL,
  `volume_no` varchar(180) DEFAULT NULL,
  `description` text NOT NULL,
  `video_link` text,
  `status` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cover_image` text NOT NULL,
  `front_image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_news`
--

INSERT INTO `sa_news` (`id`, `title`, `volume_no`, `description`, `video_link`, `status`, `created_at`, `updated_at`, `cover_image`, `front_image`) VALUES
(1, '2014 Michigan ICES Sugar Art Showcase Grand Prize', '10', '<p>This&nbsp;is a question we get from time to time. There are instances&nbsp;when you&rsquo;re given an assignment, not by&nbsp;<a href=\"https://wordcounter.net/\">word count</a>, but assigned by the number of pages. For example, &ldquo;Write a paper four pages long.&rdquo; If you get an assignment to write four pages, one of the first questions that will likely come to mind is, &ldquo;How many words are in four pages?&rdquo; If you need to figure out words per page, you can use a&nbsp;<a href=\"https://wordcounter.net/words-per-page\">words per page calculator</a>.</p>\r\n<p>&nbsp;</p>\r\n<p>The truth is there is no definitive answer to this question. The number of words it will take to fill a page will depend on a number of factors including the type of font used, the font size, spacing elements, the margins used on the paper, the paragraph length, etc. For example, if the assignment says the writing should be in 10-point font, it&rsquo;s going to take a greater number of words to fill a page than if the assignment requires a 12-point font.</p>\r\n<p>For those who need a general rule of thumb, a typical page which has 1-inch margins and is typed in 12 point font with standard spacing elements will be approximately 500 words when typed single spaced. For assignments that require double spacing, it would take approximately 250 words to fill the page. For an assignment that requires you to write four pages, you can make the estimation that you&rsquo;ll need to write approximately 2000 words for a single spaced paper, or&nbsp;<a href=\"https://wordcounter.net/blog/2015/07/28/10541_1000-words-a-day-writing-challenge.html\">1000 words</a>&nbsp;if the assignment is double spaced. Again, the type of font used can make the word count higher or lower, but it&rsquo;s a good rule of thumb for those who are simply looking for a general estimation.</p>\r\n<p>Since there can be a large variation on the number of words needed to fill a page, most papers are no longer assigned by page count. They are instead assigned by word count. That is, an assignment, essay or paper will likely be assigned as 1500 &ndash; 2000 words rather than 3 &ndash; 4 pages. This way it is much more difficult for the writer to &ldquo;game the system&rdquo; by using large fonts and excessive spacing to meet the writing criteria.</p>\r\n<p>If you are given a writing assignment with a page number, the best thing to do is go directly to the person who made the assignment and ask for a word count. This will take away all the variations and help ensure your writing assignment meets expectations. If it&rsquo;s for something informal and you simply need a general guideline, you can find it below.</p>', 'https://www.youtube.com/watch?v=rbKszsHqd6c', 0, '2018-09-15 18:15:57', '2017-12-31 11:16:18', '', ''),
(2, 'How to Decorate a Cake Properly with Strawberry Flavor', '5', '<p>This&nbsp;is a question we get from time to time. There are instances&nbsp;when you&rsquo;re given an assignment, not by&nbsp;<a href=\"https://wordcounter.net/\">word count</a>, but assigned by the number of pages. For example, &ldquo;Write a paper four pages long.&rdquo; If you get an assignment to write four pages, one of the first questions that will likely come to mind is, &ldquo;How many words are in four pages?&rdquo; If you need to figure out words per page, you can use a&nbsp;<a href=\"https://wordcounter.net/words-per-page\">words per page calculator</a>.</p>\r\n<p>&nbsp;</p>\r\n<p>The truth is there is no definitive answer to this question. The number of words it will take to fill a page will depend on a number of factors including the type of font used, the font size, spacing elements, the margins used on the paper, the paragraph length, etc. For example, if the assignment says the writing should be in 10-point font, it&rsquo;s going to take a greater number of words to fill a page than if the assignment requires a 12-point font.</p>\r\n<p>For those who need a general rule of thumb, a typical page which has 1-inch margins and is typed in 12 point font with standard spacing elements will be approximately 500 words when typed single spaced. For assignments that require double spacing, it would take approximately 250 words to fill the page. For an assignment that requires you to write four pages, you can make the estimation that you&rsquo;ll need to write approximately 2000 words for a single spaced paper, or&nbsp;<a href=\"https://wordcounter.net/blog/2015/07/28/10541_1000-words-a-day-writing-challenge.html\">1000 words</a>&nbsp;if the assignment is double spaced. Again, the type of font used can make the word count higher or lower, but it&rsquo;s a good rule of thumb for those who are simply looking for a general estimation.</p>\r\n<p>Since there can be a large variation on the number of words needed to fill a page, most papers are no longer assigned by page count. They are instead assigned by word count. That is, an assignment, essay or paper will likely be assigned as 1500 &ndash; 2000 words rather than 3 &ndash; 4 pages. This way it is much more difficult for the writer to &ldquo;game the system&rdquo; by using large fonts and excessive spacing to meet the writing criteria.</p>\r\n<p>If you are given a writing assignment with a page number, the best thing to do is go directly to the person who made the assignment and ask for a word count. This will take away all the variations and help ensure your writing assignment meets expectations. If it&rsquo;s for something informal and you simply need a general guideline, you can find it below.</p>', 'https://www.youtube.com/watch?v=9HkX6OPkGag', 0, '2018-09-15 18:16:01', '2017-12-31 11:18:32', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sa_news_image`
--

CREATE TABLE `sa_news_image` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `file_name` text NOT NULL,
  `type` char(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_news_image`
--

INSERT INTO `sa_news_image` (`id`, `news_id`, `path`, `file_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'core/storage/uploads/images/news', 'news-front-20171231164618-0.png', 'front', '2017-12-31 11:16:18', '2017-12-31 11:16:18'),
(2, 1, 'core/storage/uploads/images/news', 'news-featured-20171231164618-0.jpg', 'featured', '2018-02-02 09:03:40', '2017-12-31 11:16:18'),
(3, 1, 'core/storage/uploads/images/news', 'news-featured-20171231164618-1.jpg', 'featured', '2018-02-02 09:04:39', '2017-12-31 11:16:18'),
(4, 1, 'core/storage/uploads/images/news', 'news-featured-20171231164618-2.jpg', 'featured', '2018-02-02 09:04:34', '2017-12-31 11:16:18'),
(5, 1, 'core/storage/uploads/images/news', 'news-featured-20171231164618-3.jpg', 'featured', '2018-02-02 09:04:29', '2017-12-31 11:16:18'),
(6, 2, 'core/storage/uploads/images/news', 'news-front-20171231164832-0.png', 'front', '2017-12-31 11:18:32', '2017-12-31 11:18:32'),
(7, 2, 'core/storage/uploads/images/news', 'news-featured-20171231164832-0.png', 'featured', '2018-02-02 09:03:47', '2017-12-31 11:18:32'),
(8, 2, 'core/storage/uploads/images/news', 'news-featured-20171231164832-1.PNG', 'featured', '2018-02-02 09:04:10', '2017-12-31 11:18:32'),
(9, 2, 'core/storage/uploads/images/news', 'news-featured-20171231164832-2.PNG', 'featured', '2018-02-02 09:04:15', '2017-12-31 11:18:32'),
(10, 2, 'core/storage/uploads/images/news', 'news-featured-20171231164832-3.PNG', 'featured', '2018-02-02 09:04:20', '2017-12-31 11:18:32'),
(11, 2, 'core/storage/uploads/images/news', 'news-featured-20171231164832-4.PNG', 'featured', '2018-02-02 09:04:24', '2017-12-31 11:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `sa_offer`
--

CREATE TABLE `sa_offer` (
  `id` int(11) NOT NULL,
  `offer` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_offers`
--

CREATE TABLE `sa_offers` (
  `id` int(11) NOT NULL,
  `offer` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_pages`
--

CREATE TABLE `sa_pages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `allies_name` varchar(250) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tittle` text NOT NULL,
  `tag_line` text NOT NULL,
  `path` text NOT NULL,
  `filename` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_paypal_transaction`
--

CREATE TABLE `sa_paypal_transaction` (
  `id` int(11) NOT NULL,
  `TOKEN` text NOT NULL,
  `TIMESTAMP` timestamp NULL DEFAULT NULL,
  `EMAIL` text NOT NULL,
  `PAYERID` text NOT NULL,
  `FIRSTNAME` text NOT NULL,
  `LASTNAME` text NOT NULL,
  `COUNTRYCODE` text NOT NULL,
  `CURRENCYCODE` text NOT NULL,
  `AMT` double NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `INVNUM` text NOT NULL,
  `PAYMENTREQUESTINFO_0_ERRORCODE` text NOT NULL,
  `SHIPPINGAMT` double NOT NULL,
  `TAXAMT` double NOT NULL,
  `CORRELATIONID` text NOT NULL,
  `ACK` text NOT NULL,
  `VERSION` text NOT NULL,
  `PAYMENTINFO_0_SECUREMERCHANTACCOUNTID` text,
  `PAYMENTINFO_0_PAYMENTSTATUS` text,
  `PAYMENTINFO_0_TRANSACTIONID` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_paypal_transaction`
--

INSERT INTO `sa_paypal_transaction` (`id`, `TOKEN`, `TIMESTAMP`, `EMAIL`, `PAYERID`, `FIRSTNAME`, `LASTNAME`, `COUNTRYCODE`, `CURRENCYCODE`, `AMT`, `DESCRIPTION`, `INVNUM`, `PAYMENTREQUESTINFO_0_ERRORCODE`, `SHIPPINGAMT`, `TAXAMT`, `CORRELATIONID`, `ACK`, `VERSION`, `PAYMENTINFO_0_SECUREMERCHANTACCOUNTID`, `PAYMENTINFO_0_PAYMENTSTATUS`, `PAYMENTINFO_0_TRANSACTIONID`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1805101138251', '2018-05-10 00:38:54', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 1.1, 'Order # 1805101138251 Invoice', '', '0', 1, 0, 'bdbe5c0b62c29', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '45R8640905988052P', '2018-05-10 06:08:53', '2018-05-10 06:08:59', NULL),
(2, '1805101146001', '2018-05-10 00:46:29', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 1.1, 'Order # 1805101146001 Invoice', '', '0', 1, 0, '4c7142d036ddd', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '9TN16318JU306112C', '2018-05-10 06:16:28', '2018-05-10 06:16:34', NULL),
(3, '1805101150581', '2018-05-10 00:51:28', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 0.5, 'Order # 1805101150581 Invoice', '', '0', 0.5, 0, 'eb61b524eecd', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '56L07642TB433492R', '2018-05-10 06:21:26', '2018-05-10 06:21:33', NULL),
(4, 'EC-1YE28864D4099480X', '2018-05-10 01:13:20', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 1.7, 'Order # 1805101212491 Invoice', '1805101212491', '0', 1.5, 0, 'd409a10d9d6af', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '1TH64428EY968452X', '2018-05-10 06:43:19', '2018-05-10 06:43:25', NULL),
(5, 'EC-3B429211US0559004', '2018-05-10 03:35:28', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 0.6, 'Order # 1805101434551 Invoice', '1805101434551', '0', 0.5, 0, 'bc3b4ff4a9e89', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '8BL27284T10425611', '2018-05-10 09:05:26', '2018-05-10 09:05:33', NULL),
(6, 'EC-7X513409JU662860R', '2018-05-10 03:37:54', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 0.6, 'Order # 1805101437291 Invoice', '1805101437291', '0', 0.5, 0, 'b0223f619573e', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '99B896039C917050T', '2018-05-10 09:07:53', '2018-05-10 09:07:59', NULL),
(7, 'EC-54P25508M02232152', '2018-05-10 12:45:04', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 1.2, 'Order # 1805102344331 Invoice', '1805102344331', '0', 1, 0, 'ea83c4d677b58', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '1HK34487KU094844C', '2018-05-10 18:15:02', '2018-05-10 18:15:09', NULL),
(8, 'EC-7J6704137N000092K', '2018-05-14 14:37:21', 'insaf.zak-buyer@gmail.com', 'JDAQE3GDSNF9J', 'test', 'buyer', 'US', 'USD', 65.6, 'Order # 1805150135209 Invoice', '1805150135209', '0', 165, 0, '5abb3f50b2c53', 'Success', '123', '2TLF9BVTBGUUY', '2TLF9BVTBGUUY', '00B44813F2055811Y', '2018-05-14 20:07:25', '2018-05-14 20:07:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_privateclass_category`
--

CREATE TABLE `sa_privateclass_category` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `datetime` timestamp NULL DEFAULT NULL,
  `noofseat` int(11) NOT NULL,
  `price` double NOT NULL,
  `path` text NOT NULL,
  `filename` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_privateclass_category`
--

INSERT INTO `sa_privateclass_category` (`id`, `name`, `description`, `datetime`, `noofseat`, `price`, `path`, `filename`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'Orange cakes class', 'Description of orange cake icing class \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '2017-11-08 04:00:00', 30, 120, 'uploads/images/privateclass', 'privateclass-cover-20171027131839.jpg', 9, '2017-10-27 07:43:48', '2017-10-27 07:48:39', NULL, 1),
(2, 'cake Icing class', 'Description of cake icing class \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '2017-11-24 07:00:00', 30, 165, 'uploads/images/privateclass', 'privateclass-cover-20171027210958.jpg', 9, '2017-10-27 15:39:58', '2017-10-27 15:39:58', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_privateclass_image`
--

CREATE TABLE `sa_privateclass_image` (
  `id` int(11) NOT NULL,
  `privateclass_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `filename` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_private_class`
--

CREATE TABLE `sa_private_class` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` text NOT NULL,
  `parent_name` text NOT NULL,
  `requested_date` timestamp NULL DEFAULT NULL,
  `ip_address` text NOT NULL,
  `ip_address_location` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=PENDING,2=CONFIRMED',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_privetclass_date_time`
--

CREATE TABLE `sa_privetclass_date_time` (
  `id` int(11) NOT NULL,
  `privateclass_cat_id` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `start` varchar(100) NOT NULL,
  `end` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_privetclass_date_time`
--

INSERT INTO `sa_privetclass_date_time` (`id`, `privateclass_cat_id`, `date`, `start`, `end`, `created_at`, `updated_at`) VALUES
(39, 1, '2018-07-22', 'Array', 'Array', '2018-07-22 09:31:34', '2018-07-22 09:31:34'),
(40, 2, '2018-07-17', 'Array', 'Array', '2018-07-22 15:28:30', '2018-07-22 15:28:30'),
(41, 3, '', 'Array', 'Array', '2018-07-23 08:28:47', '2018-07-23 08:28:47'),
(42, 4, '', 'Array', 'Array', '2018-07-23 08:34:44', '2018-07-23 08:34:44'),
(43, 5, '', 'Array', 'Array', '2018-07-23 08:37:04', '2018-07-23 08:37:04'),
(44, 6, '', 'Array', 'Array', '2018-07-23 09:02:04', '2018-07-23 09:02:04'),
(45, 7, '', 'Array', 'Array', '2018-07-23 09:05:59', '2018-07-23 09:05:59'),
(46, 8, '', 'Array', 'Array', '2018-07-23 09:08:05', '2018-07-23 09:08:05'),
(47, 9, '', 'Array', 'Array', '2018-07-23 09:49:07', '2018-07-23 09:49:07'),
(48, 10, '', '', '', '2018-07-23 10:00:46', '2018-07-23 10:00:46'),
(49, 10, '', '', '', '2018-07-23 10:00:46', '2018-07-23 10:00:46'),
(50, 11, '2018-07-23', 'Array', 'Array', '2018-07-23 10:02:29', '2018-07-23 10:02:29'),
(51, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(52, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(53, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(54, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(55, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(56, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(57, 13, '2018-08-16', '1', '1', '2018-07-23 10:05:32', '2018-07-23 10:05:32'),
(58, 13, '2018-07-31', '1', '1', '2018-07-23 10:05:32', '2018-07-23 10:05:32'),
(59, 14, '2018-07-23', 'Array', 'Array', '2018-07-23 10:09:14', '2018-07-23 10:09:14'),
(60, 21, '2018-07-02', 'Array', 'Array', '2018-07-23 10:20:48', '2018-07-23 10:20:48'),
(61, 28, '2018-07-23', '445', '333', '2018-07-23 10:36:14', '2018-07-23 10:36:14'),
(62, 29, '2018-07-25', '65', '67', '2018-07-23 10:36:53', '2018-07-23 10:36:53'),
(63, 29, '2018-07-27', '76', '767', '2018-07-23 10:36:53', '2018-07-23 10:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `sa_product`
--

CREATE TABLE `sa_product` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `model_no` text,
  `colour` text,
  `launch_date` date DEFAULT NULL,
  `series_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `description` text,
  `old_price` double NOT NULL DEFAULT '0',
  `price` double NOT NULL,
  `special_feature` text,
  `instock` int(11) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_productcreation`
--

CREATE TABLE `sa_productcreation` (
  `id` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `description` text NOT NULL,
  `categories` text NOT NULL,
  `price` double NOT NULL,
  `width` double NOT NULL,
  `length` double NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `qtyonhand` double NOT NULL,
  `popularity` double NOT NULL,
  `usps_mail_type` int(11) NOT NULL,
  `usps_service_type` int(11) NOT NULL,
  `usps_size` int(11) NOT NULL,
  `pounds` double NOT NULL,
  `ounces` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_productcreation`
--

INSERT INTO `sa_productcreation` (`id`, `product_name`, `description`, `categories`, `price`, `width`, `length`, `height`, `weight`, `qtyonhand`, `popularity`, `usps_mail_type`, `usps_service_type`, `usps_size`, `pounds`, `ounces`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'Police Box Cake has many more characters than a normal item for testing', '<p><span style=\"color: #333399; font-size: 14pt;\"><strong>Police Box Cake </strong></span></p>\r\n<p><span style=\"color: #333399; font-size: 14pt;\"><strong>Description goes here</strong></span></p>', '[\"1\"]', 0.1, 10, 25, 20, 1, 10, 8, 1, 1, 2, 0, 1, 9, '2018-05-10 04:38:39', '2018-05-10 04:27:01', NULL, 1),
(2, 'Sculpted Minion Cake', '<p><span style=\"color: #993300; font-size: 14pt;\">Sculpted cake description</span></p>', '[\"1\"]', 0.1, 25, 30, 30, 1.5, 2, 5, 1, 1, 2, 0, 1, 9, '2018-05-10 04:38:42', '2018-05-10 04:27:33', NULL, 1),
(3, 'Cake Tutorial 01', '<p><span style=\"color: #ff6600; font-size: 14pt;\">cake tutorial</span></p>', '[\"2\"]', 0.1, 10, 15, 15, 1.5, 1, 10, 1, 1, 2, 0, 1, 9, '2018-05-10 04:38:46', '2018-05-10 04:27:51', NULL, 1),
(4, 'TEST PRODUCT 1', '<p>fwef</p>', '[\"1\"]', 0.1, 21, 2, 3, 25, 10, 0, 1, 1, 2, 0, 1, 1, '2018-09-16 08:23:45', '2018-09-16 08:23:45', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_productcreation_category`
--

CREATE TABLE `sa_productcreation_category` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_productcreation_category`
--

INSERT INTO `sa_productcreation_category` (`id`, `name`, `created_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cakes', 9, 1, '2017-12-24 15:53:43', '2017-12-24 15:53:43', NULL),
(2, 'Tutorials', 9, 1, '2017-12-24 15:53:54', '2017-12-24 15:53:54', NULL),
(3, 'Recipes', 9, 1, '2017-12-24 15:54:26', '2017-12-24 15:54:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_productcreation_image`
--

CREATE TABLE `sa_productcreation_image` (
  `id` int(11) NOT NULL,
  `productCreation_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `filename` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_productcreation_image`
--

INSERT INTO `sa_productcreation_image` (`id`, `productCreation_id`, `path`, `filename`, `created_at`, `updated_at`) VALUES
(1, 1, 'uploads/images/ProductCreation', 'product-creation-20171224214617-0.png', '2017-12-24 16:16:17', '2017-12-24 16:16:17'),
(2, 2, 'uploads/images/ProductCreation', 'product-creation-20171224215059-0.jpg', '2017-12-24 16:20:59', '2017-12-24 16:20:59'),
(3, 3, 'uploads/images/ProductCreation', 'product-creation-20171224220005-0.jpg', '2017-12-24 16:30:05', '2017-12-24 16:30:05'),
(4, 4, 'uploads/images/ProductCreation', 'product-creation-20180509221701-0.jpg', '2018-05-09 16:47:01', '2018-05-09 16:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `sa_product_feature`
--

CREATE TABLE `sa_product_feature` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `feature_value` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_product_has_category_many`
--

CREATE TABLE `sa_product_has_category_many` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_product_has_category_many`
--

INSERT INTO `sa_product_has_category_many` (`id`, `category_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2017-12-24 16:16:17', '2018-01-14 12:08:44', '2018-01-14 12:08:44'),
(2, 1, 2, '2017-12-24 16:20:59', '2018-05-10 04:27:33', '2018-05-10 04:27:33'),
(3, 2, 3, '2017-12-24 16:30:05', '2018-05-10 04:27:51', '2018-05-10 04:27:51'),
(4, 1, 1, '2018-01-14 12:08:45', '2018-01-14 15:22:55', '2018-01-14 15:22:55'),
(5, 1, 1, '2018-01-14 15:22:55', '2018-05-10 04:27:02', '2018-05-10 04:27:02'),
(6, 1, 4, '2018-05-09 16:47:01', '2018-05-10 04:28:07', '2018-05-10 04:28:07'),
(7, 1, 1, '2018-05-10 04:27:02', '2018-05-10 04:27:02', NULL),
(8, 1, 2, '2018-05-10 04:27:33', '2018-05-10 04:27:33', NULL),
(9, 2, 3, '2018-05-10 04:27:51', '2018-05-10 04:27:51', NULL),
(10, 1, 4, '2018-05-10 04:28:07', '2018-05-10 04:28:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_product_image`
--

CREATE TABLE `sa_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `path` text,
  `filename` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sa_purchase`
--

CREATE TABLE `sa_purchase` (
  `id` int(11) NOT NULL,
  `inoice_no` text NOT NULL,
  `amount` double NOT NULL,
  `shipping_amount` double NOT NULL DEFAULT '0',
  `coupon_code` text,
  `coupon_value` double NOT NULL DEFAULT '0',
  `currency_code` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` text NOT NULL,
  `province` text NOT NULL,
  `postcode` text NOT NULL,
  `street_addresss_1` text NOT NULL,
  `street_addresss_2` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `shipping_method` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_purchase`
--

INSERT INTO `sa_purchase` (`id`, `inoice_no`, `amount`, `shipping_amount`, `coupon_code`, `coupon_value`, `currency_code`, `country_id`, `city`, `province`, `postcode`, `street_addresss_1`, `street_addresss_2`, `first_name`, `last_name`, `phone`, `email`, `shipping_method`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, '1805101150581', 0.5, 0.5, '5AAEB7DB8717', 0.1, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 06:21:26', '2018-05-10 06:21:26', NULL, 1),
(2, '1805101154201', -9.4, 0.5, '5AAEB7DB8E6A', 10, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 06:24:20', '2018-05-10 06:24:20', NULL, 1),
(3, '1805101156111', -9.4, 0.5, '5AAEB7DB8E6A', 10, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 06:26:11', '2018-05-10 06:26:11', NULL, 1),
(4, '1805101202001', -9.4, 0.5, '5AAEB7DB9A61', 10, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 06:32:00', '2018-05-10 06:32:00', NULL, 1),
(5, '1805101212491', 1.7, 1.5, '5AAEB7DBAC34', 0.1, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 06:43:19', '2018-05-10 06:43:19', NULL, 1),
(6, '1805101434551', 0.6, 0.5, '', 0, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 09:05:26', '2018-05-10 09:05:26', NULL, 1),
(7, '1805101437291', 0.6, 0.5, '', 0, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 09:07:53', '2018-05-10 09:07:53', NULL, 1),
(8, '1805102344331', 1.2, 1, '', 0, 'USD', 3, 'Los Angeles', 'California (CA)', '85001', 'STREET 1', 'STREET 2', 'INSAF', 'ZAKARIYA', '0773799390', 'insaf.zak@gmail.com', 1, 1, '2018-05-10 18:15:02', '2018-05-10 18:15:02', NULL, 1),
(9, '1805150135209', 65.6, 165, '5AAEB7DB8717', 100, 'USD', 1, 'Malabe', 'western province', '10118', 'No 153, jayagath Mw,', 'Hokandara north, Hokandara', 'Lakshitha', 'Hettiarachchi', '0713399099', 'rullzzm@gmail.com', 1, 9, '2018-05-14 20:07:25', '2018-05-14 20:07:25', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_purchase_item`
--

CREATE TABLE `sa_purchase_item` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `unit_price` double NOT NULL,
  `qty` double NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=product,2=tutorial',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_purchase_item`
--

INSERT INTO `sa_purchase_item` (`id`, `purchase_id`, `item_id`, `name`, `unit_price`, `qty`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:21:26', '2018-05-10 06:21:26', NULL),
(2, 2, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:24:20', '2018-05-10 06:24:20', NULL),
(3, 3, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:26:11', '2018-05-10 06:26:11', NULL),
(4, 4, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:32:00', '2018-05-10 06:32:00', NULL),
(5, 5, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:43:19', '2018-05-10 06:43:19', NULL),
(6, 5, 3, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 06:43:19', '2018-05-10 06:43:19', NULL),
(7, 5, 1, 'Sculpted Minion Cake', 0.1, 1, 1, '2018-05-10 06:43:19', '2018-09-08 05:58:41', NULL),
(8, 6, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 09:05:26', '2018-05-10 09:05:26', NULL),
(9, 7, 3, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 09:07:53', '2018-05-10 09:07:53', NULL),
(10, 8, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 18:15:02', '2018-05-10 18:15:02', NULL),
(11, 8, 3, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 18:15:02', '2018-05-10 18:15:02', NULL),
(12, 9, 1, 'Police Box Cake has many more characters than a normal item for testing', 0.1, 3, 1, '2018-05-14 20:07:25', '2018-05-14 20:07:25', NULL),
(13, 9, 3, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-14 20:07:25', '2018-05-14 20:07:25', NULL),
(14, 9, 0, 'Sculpted Minion Cake', 0.1, 1, 1, '2018-05-14 20:07:25', '2018-09-08 05:50:37', NULL),
(15, 9, 4, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-14 20:07:25', '2018-05-14 20:07:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_rating`
--

CREATE TABLE `sa_rating` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `review` varchar(255) DEFAULT NULL,
  `star_count` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_rating`
--

INSERT INTO `sa_rating` (`id`, `product_id`, `name`, `email`, `contact_no`, `review`, `star_count`, `created_by`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 4, 'dsfs fsdf', 'mp.chathuranga@gmail.com', '534534534535', 'JKDJDFJSLKDFJWERKJELFJKSJFLKLWJREJLKFJFLKJFLJELKJELKFJLSFSDLF', 8, '0', '2018-09-16 08:21:37', '2018-09-16 08:21:37', '0000-00-00 00:00:00'),
(2, 4, 'dsfs fsdf', 'mp.chathuranga@gmail.com', '534534534535', 'SDFFSFDFLFMS;FKF;LFKSDFLFK;LFSDFSDL;F', 10, '0', '2018-09-16 08:23:45', '2018-09-16 08:23:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sa_shipping_method`
--

CREATE TABLE `sa_shipping_method` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_shipping_method`
--

INSERT INTO `sa_shipping_method` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Standard International Post', 'Free  (3-6 week)', '2017-12-12 15:55:43', '0000-00-00 00:00:00', NULL),
(2, 'Excluseve International Post', 'Postage $ 10 (2-4 week)', '2017-12-12 15:55:43', '0000-00-00 00:00:00', NULL),
(3, 'Premium Post', 'Postage $ 50 (1-2 week)', '2017-12-12 15:56:30', '0000-00-00 00:00:00', NULL),
(4, 'For VIP clients', '$ 100 (3 days)', '2017-12-12 15:56:30', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_shopping_cart`
--

CREATE TABLE `sa_shopping_cart` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `product_name` varchar(35) DEFAULT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_shopping_cart`
--

INSERT INTO `sa_shopping_cart` (`id`, `product_id`, `type`, `product_name`, `price`, `qty`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, 'Paid tutorial', 150, 1, 9, '2018-02-05 11:47:22', '2018-02-05 11:47:52', '2018-02-05 11:47:52'),
(2, 1, 1, 'Police Box Cake has many more chara', 220, 1, 9, '2018-02-05 11:47:34', '2018-02-05 12:12:02', '2018-02-05 12:12:02'),
(3, 2, 2, 'Paid tutorial', 150, 1, 9, '2018-02-05 11:48:04', '2018-02-05 12:12:02', '2018-02-05 12:12:02'),
(4, 1, 1, 'Police Box Cake has many more chara', 220, 7, 9, '2018-02-06 03:32:38', '2018-02-22 10:23:34', '2018-02-22 10:23:34'),
(5, 1, 1, 'Police Box Cake has many more chara', 220, 2, 22, '2018-02-21 11:59:35', '2018-02-21 12:57:08', NULL),
(6, 2, 1, 'Sculpted Minion Cake', 275, 1, 9, '2018-02-22 08:36:02', '2018-02-22 10:23:38', '2018-02-22 10:23:38'),
(7, 3, 1, 'Cake Tutorial 01', 260, 1, 9, '2018-02-22 08:36:02', '2018-02-22 10:23:43', '2018-02-22 10:23:43'),
(8, 3, 1, 'Cake Tutorial 01', 260, 1, 9, '2018-02-26 07:11:23', '2018-02-26 07:11:31', '2018-02-26 07:11:31'),
(9, 1, 1, 'Police Box Cake has many more chara', 220, 3, 9, '2018-02-26 07:35:51', '2018-05-14 20:07:32', '2018-05-14 20:07:32'),
(10, 1, 1, 'Police Box Cake has many more chara', 220, 1, 26, '2018-03-13 09:16:06', '2018-03-24 13:31:56', NULL),
(11, 2, 2, 'Paid tutorial', 150, 4, 26, '2018-03-24 03:34:39', '2018-03-24 13:33:26', NULL),
(12, 1, 1, 'Police Box Cake has many more chara', 220, 1, 1, '2018-05-01 17:31:27', '2018-05-06 17:34:14', '2018-05-06 17:34:14'),
(13, 3, 1, 'Cake Tutorial 01', 260, 1, 1, '2018-05-01 17:31:31', '2018-05-05 17:22:02', '2018-05-05 17:22:02'),
(14, 1, 1, 'Police Box Cake has many more chara', 220, 1, 1, '2018-05-06 17:34:23', '2018-05-09 16:58:37', '2018-05-09 16:58:37'),
(15, 3, 1, 'Cake Tutorial 01', 260, 1, 1, '2018-05-08 16:33:21', '2018-05-09 16:57:23', '2018-05-09 16:57:23'),
(16, 2, 1, 'Sculpted Minion Cake', 275, 1, 1, '2018-05-08 16:33:25', '2018-05-09 16:57:18', '2018-05-09 16:57:18'),
(17, 4, 1, 'TEST PRODUCT 1', 258, 1, 1, '2018-05-09 16:48:07', '2018-05-09 16:57:14', '2018-05-09 16:57:14'),
(18, 4, 1, 'TEST PRODUCT 1', 258, 1, 1, '2018-05-09 16:57:57', '2018-05-10 05:23:13', '2018-05-10 05:23:13'),
(19, 1, 1, 'Police Box Cake has many more chara', 220, 1, 1, '2018-05-09 17:00:52', '2018-05-10 05:23:13', '2018-05-10 05:23:13'),
(20, 2, 1, 'Sculpted Minion Cake', 275, 1, 1, '2018-05-09 17:00:55', '2018-05-10 05:23:13', '2018-05-10 05:23:13'),
(21, 3, 1, 'Cake Tutorial 01', 260, 1, 1, '2018-05-09 17:00:58', '2018-05-10 05:23:13', '2018-05-10 05:23:13'),
(22, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 05:35:51', '2018-05-10 05:38:24', '2018-05-10 05:38:24'),
(23, 2, 1, 'Sculpted Minion Cake', 0.1, 1, 1, '2018-05-10 05:35:53', '2018-05-10 05:38:24', '2018-05-10 05:38:24'),
(24, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 05:40:23', '2018-05-10 05:42:44', '2018-05-10 05:42:44'),
(25, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 05:43:40', '2018-05-10 06:03:44', '2018-05-10 06:03:44'),
(26, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:06:11', '2018-05-10 06:08:59', '2018-05-10 06:08:59'),
(27, 3, 1, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 06:06:13', '2018-05-10 06:08:59', '2018-05-10 06:08:59'),
(28, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:14:00', '2018-05-10 06:16:34', '2018-05-10 06:16:34'),
(29, 3, 1, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 06:14:02', '2018-05-10 06:16:34', '2018-05-10 06:16:34'),
(30, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:19:22', '2018-05-10 06:21:33', '2018-05-10 06:21:33'),
(31, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:23:35', '2018-05-10 06:24:20', '2018-05-10 06:24:20'),
(32, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:25:29', '2018-05-10 06:26:11', '2018-05-10 06:26:11'),
(33, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:31:25', '2018-05-10 06:32:00', '2018-05-10 06:32:00'),
(34, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 06:42:10', '2018-05-10 06:43:25', '2018-05-10 06:43:25'),
(35, 3, 1, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 06:42:12', '2018-05-10 06:43:25', '2018-05-10 06:43:25'),
(36, 2, 1, 'Sculpted Minion Cake', 0.1, 1, 1, '2018-05-10 06:42:16', '2018-05-10 06:43:25', '2018-05-10 06:43:25'),
(37, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 1, '2018-05-10 08:12:55', '2018-05-10 09:05:33', '2018-05-10 09:05:33'),
(38, 3, 1, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 09:07:07', '2018-05-10 09:07:59', '2018-05-10 09:07:59'),
(39, 4, 1, 'TEST PRODUCT 1', 0.1, 3, 1, '2018-05-10 09:25:30', '2018-05-10 18:15:09', '2018-05-10 18:15:09'),
(40, 3, 1, 'Cake Tutorial 01', 0.1, 1, 1, '2018-05-10 17:44:32', '2018-05-10 18:15:09', '2018-05-10 18:15:09'),
(41, 3, 1, 'Cake Tutorial 01', 0.1, 1, 9, '2018-05-14 19:39:27', '2018-05-14 20:07:32', '2018-05-14 20:07:32'),
(42, 2, 1, 'Sculpted Minion Cake', 0.1, 1, 9, '2018-05-14 19:39:27', '2018-05-14 20:07:32', '2018-05-14 20:07:32'),
(43, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 9, '2018-05-14 19:39:27', '2018-05-14 20:07:32', '2018-05-14 20:07:32'),
(0, 2, 2, 'Paid tutorial', 150, 1, 9, '2018-09-08 06:49:23', '2018-09-16 13:20:18', '2018-09-16 13:20:18'),
(0, 4, 1, 'TEST PRODUCT 1', 0.1, 1, 9, '2018-09-15 18:30:13', '2018-09-16 13:20:22', '2018-09-16 13:20:22'),
(0, 3, 1, 'Cake Tutorial 01', 0.1, 1, 9, '2018-09-16 13:20:33', '2018-09-16 13:20:39', '2018-09-16 13:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `sa_sort_type`
--

CREATE TABLE `sa_sort_type` (
  `id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_sort_type`
--

INSERT INTO `sa_sort_type` (`id`, `value`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Sort by newest', '2018-02-12 18:30:00', '0000-00-00 00:00:00', NULL),
(2, 2, 'Sort by oldest', '2018-02-12 18:30:00', '0000-00-00 00:00:00', NULL),
(3, 3, 'Sort by popularity', NULL, '0000-00-00 00:00:00', NULL),
(4, 4, 'Sort by price: High to low', '2018-02-12 18:30:00', '0000-00-00 00:00:00', NULL),
(5, 5, 'Sort by price: Low to high', '2018-02-12 18:30:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_tasting_people_type`
--

CREATE TABLE `sa_tasting_people_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_tasting_people_type`
--

INSERT INTO `sa_tasting_people_type` (`id`, `name`, `description`, `price`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Couple only', '', 30, 1, 9, '2017-10-17 18:50:13', '2018-01-14 07:15:43', NULL),
(2, 'Couple + 1 Guest', '', 45, 1, 9, '2017-10-17 18:50:32', '2018-01-14 07:16:21', NULL),
(3, 'Couple + 2 Guests', '', 60, 1, 9, '2018-01-14 07:16:47', '2018-01-14 07:16:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_usps_mail_type`
--

CREATE TABLE `sa_usps_mail_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_usps_mail_type`
--

INSERT INTO `sa_usps_mail_type` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'LETTER', '2018-05-08 08:21:33', '0000-00-00 00:00:00', NULL),
(2, 'FLAT', '2018-05-08 08:21:33', '0000-00-00 00:00:00', NULL),
(3, 'PARCEL', '2018-05-08 08:21:52', '0000-00-00 00:00:00', NULL),
(4, 'POSTCARD', '2018-05-08 08:21:52', '0000-00-00 00:00:00', NULL),
(5, 'PACKAGE SERVICE', '2018-05-08 08:22:06', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_usps_service_type`
--

CREATE TABLE `sa_usps_service_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_usps_service_type`
--

INSERT INTO `sa_usps_service_type` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'FIRST CLASS', '2018-05-08 07:55:03', '2018-05-08 07:55:03', NULL),
(2, 'FIRST CLASS COMMERCIAL', '2018-05-08 07:55:03', '2018-05-08 07:55:03', NULL),
(3, 'PRIORITY', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(4, 'PRIORITY COMMERCIAL', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(5, 'PRIORITY HFP COMMERCIAL', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(6, 'EXPRESS', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(7, 'EXPRESS COMMERCIAL', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(8, 'EXPRESS SH', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(9, 'EXPRESS SH COMMERCIAL', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(10, 'EXPRESS HFP', '2018-05-08 07:56:33', '2018-05-08 07:56:33', NULL),
(11, 'EXPRESS HFP COMMERCIAL', '2018-05-08 08:17:01', '2018-05-08 08:17:01', NULL),
(12, 'PARCEL', '2018-05-08 08:17:01', '2018-05-08 08:17:01', NULL),
(13, 'MEDIA', '2018-05-08 08:17:31', '2018-05-08 08:17:31', NULL),
(14, 'LIBRARY', '2018-05-08 08:17:31', '2018-05-08 08:17:31', NULL),
(15, 'ONLINE', '2018-05-08 08:17:47', '2018-05-08 08:17:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_usps_size`
--

CREATE TABLE `sa_usps_size` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_usps_size`
--

INSERT INTO `sa_usps_size` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'LARGE', '2018-05-08 14:48:43', '2018-05-08 14:48:43', NULL),
(2, 'REGULAR', '2018-05-08 14:48:43', '2018-05-08 14:48:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `slides_id` int(10) UNSIGNED NOT NULL,
  `animation_type` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `animation_txt` text COLLATE utf8_unicode_ci,
  `btn_txt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `btn_txt_color` text COLLATE utf8_unicode_ci,
  `btn_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `btn_clr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`slides_id`, `animation_type`, `image`, `animation_txt`, `btn_txt`, `btn_txt_color`, `btn_url`, `btn_clr`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'slider-1.png', '<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 36pt; color: #993300;\">Have your <strong>wedding cakes</strong></span></p>\r\n<p><span style=\"font-size: 36pt; color: #993300;\"><strong>designed</strong> by <strong>Us</strong></span></p>', 'Request a Quotation', '#ffffff', 'quotes', '#000000', 1, '2017-12-24 11:37:39', '2018-03-08 16:59:40'),
(2, 2, 'slider-2.png', '<p><span style=\"font-size: 36pt;\"><span style=\"color: #993300;\"><strong>Sweet Delights Cakery</strong></span></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 36pt;\"><span style=\"color: #ff0000;\">offers</span></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 36pt;\"><span style=\"color: #993300;\"><strong>Online classes</strong></span></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\"><span style=\"color: #ff0000;\"><em>To see </em></span></span></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\"><span style=\"background-color: #ffff00; color: #ff0000;\"><em>our complete selection</em></span></span></p>', 'Click here', '#ffffff', 'onlineTutorials', '#c60000', 1, '2017-12-24 11:40:22', '2018-03-08 16:59:05'),
(3, 3, 'slider-3.png', '<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\"><span style=\"font-size: 36pt;\"><strong><span style=\"color: #333399;\">Our wide Verity</span></strong></span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #99cc00; font-size: 36pt;\">of sculpted</span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #008080; font-size: 36pt;\">&amp; special occation</span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #000080; font-size: 36pt;\">cakes</span></p>', 'Check our Selection', '#ffffff', 'productload?type=cake&category=all', '#000000', 1, '2017-12-24 11:44:44', '2018-03-08 17:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `tasting`
--

CREATE TABLE `tasting` (
  `id` int(11) NOT NULL,
  `people_type_id` int(11) DEFAULT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `details` text,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `venue` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `accepted_date` timestamp NULL DEFAULT NULL,
  `ip_address` text NOT NULL,
  `ip_location` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasting`
--

INSERT INTO `tasting` (`id`, `people_type_id`, `event_date`, `details`, `first_name`, `last_name`, `province`, `post_code`, `venue`, `phone`, `email`, `accepted_date`, `ip_address`, `ip_location`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2018-04-06 18:30:00', 'dfsgsdfgsdfgdsfgdsf', 'Mayura', 'Hettiarachchi', 'western province', '10118', 'colombo', '719781498', 'rullzzm@gmail.com', '2018-02-01 15:00:30', '::1', '', 3, '2018-02-01 15:00:17', '2018-02-01 15:04:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonials_id` int(11) NOT NULL,
  `name` text,
  `testimonial` text,
  `url` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonials_id`, `name`, `testimonial`, `url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'John Smith', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'quote', 1, '2018-03-19 01:10:45', '2018-03-19 01:10:45'),
(2, 'Lakshitha M Hettiarachchi', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '', 1, '2018-03-19 01:17:55', '2018-03-19 01:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2015-07-11 06:05:27', '2015-07-11 06:05:27'),
(2, NULL, 'ip', '127.0.0.1', '2015-07-11 06:05:28', '2015-07-11 06:05:28'),
(3, NULL, 'global', NULL, '2015-07-11 06:06:28', '2015-07-11 06:06:28'),
(4, NULL, 'ip', '127.0.0.1', '2015-07-11 06:06:28', '2015-07-11 06:06:28'),
(5, NULL, 'global', NULL, '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(6, NULL, 'ip', '127.0.0.1', '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(7, 1, 'user', NULL, '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(8, NULL, 'global', NULL, '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(9, NULL, 'ip', '127.0.0.1', '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(10, 1, 'user', NULL, '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(11, NULL, 'global', NULL, '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(12, NULL, 'ip', '127.0.0.1', '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(13, 1, 'user', NULL, '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(14, NULL, 'global', NULL, '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(15, NULL, 'ip', '127.0.0.1', '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(16, 1, 'user', NULL, '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(17, NULL, 'global', NULL, '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(18, NULL, 'ip', '127.0.0.1', '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(19, 1, 'user', NULL, '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(20, NULL, 'global', NULL, '2015-08-20 03:57:44', '2015-08-20 03:57:44'),
(21, NULL, 'ip', '127.0.0.1', '2015-08-20 03:57:44', '2015-08-20 03:57:44'),
(22, 1, 'user', NULL, '2015-08-20 03:57:45', '2015-08-20 03:57:45'),
(23, NULL, 'global', NULL, '2015-08-20 04:52:53', '2015-08-20 04:52:53'),
(24, NULL, 'ip', '127.0.0.1', '2015-08-20 04:52:53', '2015-08-20 04:52:53'),
(25, 1, 'user', NULL, '2015-08-20 04:52:54', '2015-08-20 04:52:54'),
(26, NULL, 'global', NULL, '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(27, NULL, 'ip', '127.0.0.1', '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(28, 1, 'user', NULL, '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(29, NULL, 'global', NULL, '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(30, NULL, 'ip', '127.0.0.1', '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(31, 1, 'user', NULL, '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(32, NULL, 'global', NULL, '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(33, NULL, 'ip', '127.0.0.1', '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(34, 1, 'user', NULL, '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(35, NULL, 'global', NULL, '2015-08-20 04:56:18', '2015-08-20 04:56:18'),
(36, NULL, 'ip', '127.0.0.1', '2015-08-20 04:56:19', '2015-08-20 04:56:19'),
(37, 1, 'user', NULL, '2015-08-20 04:56:19', '2015-08-20 04:56:19'),
(38, NULL, 'global', NULL, '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(39, NULL, 'ip', '127.0.0.1', '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(40, 1, 'user', NULL, '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(41, NULL, 'global', NULL, '2015-08-24 15:06:12', '2015-08-24 15:06:12'),
(42, NULL, 'ip', '127.0.0.1', '2015-08-24 15:06:12', '2015-08-24 15:06:12'),
(43, NULL, 'global', NULL, '2015-08-24 15:08:25', '2015-08-24 15:08:25'),
(44, NULL, 'ip', '127.0.0.1', '2015-08-24 15:08:25', '2015-08-24 15:08:25'),
(45, NULL, 'global', NULL, '2015-08-24 15:09:09', '2015-08-24 15:09:09'),
(46, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:09', '2015-08-24 15:09:09'),
(47, NULL, 'global', NULL, '2015-08-24 15:09:44', '2015-08-24 15:09:44'),
(48, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:44', '2015-08-24 15:09:44'),
(49, NULL, 'global', NULL, '2015-08-24 15:09:49', '2015-08-24 15:09:49'),
(50, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:50', '2015-08-24 15:09:50'),
(51, NULL, 'global', NULL, '2015-08-24 15:11:29', '2015-08-24 15:11:29'),
(52, NULL, 'ip', '127.0.0.1', '2015-08-24 15:11:29', '2015-08-24 15:11:29'),
(53, NULL, 'global', NULL, '2015-08-25 07:26:45', '2015-08-25 07:26:45'),
(54, NULL, 'ip', '127.0.0.1', '2015-08-25 07:26:45', '2015-08-25 07:26:45'),
(55, NULL, 'global', NULL, '2015-08-26 07:48:20', '2015-08-26 07:48:20'),
(56, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:21', '2015-08-26 07:48:21'),
(57, NULL, 'global', NULL, '2015-08-26 07:48:23', '2015-08-26 07:48:23'),
(58, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:23', '2015-08-26 07:48:23'),
(59, NULL, 'global', NULL, '2015-08-26 07:48:27', '2015-08-26 07:48:27'),
(60, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:27', '2015-08-26 07:48:27'),
(61, NULL, 'global', NULL, '2015-08-26 07:48:31', '2015-08-26 07:48:31'),
(62, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:31', '2015-08-26 07:48:31'),
(63, NULL, 'global', NULL, '2015-08-26 07:48:36', '2015-08-26 07:48:36'),
(64, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:36', '2015-08-26 07:48:36'),
(65, NULL, 'global', NULL, '2015-08-26 07:48:48', '2015-08-26 07:48:48'),
(66, NULL, 'global', NULL, '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(67, NULL, 'ip', '127.0.0.1', '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(68, 1, 'user', NULL, '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(69, NULL, 'global', NULL, '2015-08-30 06:42:57', '2015-08-30 06:42:57'),
(70, NULL, 'ip', '127.0.0.1', '2015-08-30 06:42:57', '2015-08-30 06:42:57'),
(71, NULL, 'global', NULL, '2015-08-30 06:51:13', '2015-08-30 06:51:13'),
(72, NULL, 'ip', '127.0.0.1', '2015-08-30 06:51:14', '2015-08-30 06:51:14'),
(73, NULL, 'global', NULL, '2015-09-06 11:03:36', '2015-09-06 11:03:36'),
(74, NULL, 'ip', '127.0.0.1', '2015-09-06 11:03:36', '2015-09-06 11:03:36'),
(75, NULL, 'global', NULL, '2015-09-18 05:45:18', '2015-09-18 05:45:18'),
(76, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:18', '2015-09-18 05:45:18'),
(77, NULL, 'global', NULL, '2015-09-18 05:45:22', '2015-09-18 05:45:22'),
(78, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:22', '2015-09-18 05:45:22'),
(79, NULL, 'global', NULL, '2015-09-18 05:45:30', '2015-09-18 05:45:30'),
(80, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:30', '2015-09-18 05:45:30'),
(81, NULL, 'global', NULL, '2015-09-18 05:45:34', '2015-09-18 05:45:34'),
(82, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:34', '2015-09-18 05:45:34'),
(83, NULL, 'global', NULL, '2015-09-18 05:45:40', '2015-09-18 05:45:40'),
(84, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:40', '2015-09-18 05:45:40'),
(85, NULL, 'global', NULL, '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(86, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(87, 1, 'user', NULL, '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(88, NULL, 'global', NULL, '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(89, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(90, 1, 'user', NULL, '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(91, NULL, 'global', NULL, '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(92, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(93, 1, 'user', NULL, '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(94, NULL, 'global', NULL, '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(95, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(96, 1, 'user', NULL, '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(97, NULL, 'global', NULL, '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(98, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(99, 1, 'user', NULL, '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(100, NULL, 'global', NULL, '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(101, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(102, 1, 'user', NULL, '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(103, NULL, 'global', NULL, '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(104, NULL, 'ip', '127.0.0.1', '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(105, 1, 'user', NULL, '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(106, NULL, 'global', NULL, '2015-11-07 11:26:55', '2015-11-07 11:26:55'),
(107, NULL, 'ip', '127.0.0.1', '2015-11-07 11:26:55', '2015-11-07 11:26:55'),
(108, NULL, 'global', NULL, '2015-11-07 11:27:01', '2015-11-07 11:27:01'),
(109, NULL, 'ip', '127.0.0.1', '2015-11-07 11:27:01', '2015-11-07 11:27:01'),
(110, NULL, 'global', NULL, '2015-11-18 18:09:36', '2015-11-18 18:09:36'),
(111, NULL, 'ip', '127.0.0.1', '2015-11-18 18:09:36', '2015-11-18 18:09:36'),
(112, NULL, 'global', NULL, '2015-11-18 18:09:43', '2015-11-18 18:09:43'),
(113, NULL, 'ip', '127.0.0.1', '2015-11-18 18:09:43', '2015-11-18 18:09:43'),
(114, NULL, 'global', NULL, '2015-11-18 18:12:14', '2015-11-18 18:12:14'),
(115, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:14', '2015-11-18 18:12:14'),
(116, NULL, 'global', NULL, '2015-11-18 18:12:27', '2015-11-18 18:12:27'),
(117, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:27', '2015-11-18 18:12:27'),
(118, NULL, 'global', NULL, '2015-11-18 18:12:36', '2015-11-18 18:12:36'),
(119, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:36', '2015-11-18 18:12:36'),
(120, NULL, 'global', NULL, '2015-11-18 20:27:03', '2015-11-18 20:27:03'),
(121, NULL, 'ip', '::1', '2015-11-18 20:27:03', '2015-11-18 20:27:03'),
(122, NULL, 'global', NULL, '2015-11-18 20:27:06', '2015-11-18 20:27:06'),
(123, NULL, 'ip', '::1', '2015-11-18 20:27:06', '2015-11-18 20:27:06'),
(124, NULL, 'global', NULL, '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(125, NULL, 'ip', '::1', '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(126, 1, 'user', NULL, '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(127, NULL, 'global', NULL, '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(128, NULL, 'ip', '::1', '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(129, 1, 'user', NULL, '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(130, NULL, 'global', NULL, '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(131, NULL, 'ip', '::1', '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(132, 1, 'user', NULL, '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(133, NULL, 'global', NULL, '2015-11-24 01:14:15', '2015-11-24 01:14:15'),
(134, NULL, 'ip', '::1', '2015-11-24 01:14:15', '2015-11-24 01:14:15'),
(135, NULL, 'global', NULL, '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(136, NULL, 'ip', '::1', '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(137, 1, 'user', NULL, '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(138, NULL, 'global', NULL, '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(139, NULL, 'ip', '::1', '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(140, 1, 'user', NULL, '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(141, NULL, 'global', NULL, '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(142, NULL, 'ip', '127.0.0.1', '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(143, 1, 'user', NULL, '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(144, NULL, 'global', NULL, '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(145, NULL, 'ip', '127.0.0.1', '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(146, 1, 'user', NULL, '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(147, NULL, 'global', NULL, '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(148, NULL, 'ip', '127.0.0.1', '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(149, 1, 'user', NULL, '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(150, NULL, 'global', NULL, '2016-01-09 00:28:55', '2016-01-09 00:28:55'),
(151, NULL, 'ip', '192.168.1.121', '2016-01-09 00:28:55', '2016-01-09 00:28:55'),
(153, NULL, 'global', NULL, '2016-01-09 00:29:26', '2016-01-09 00:29:26'),
(154, NULL, 'ip', '192.168.1.121', '2016-01-09 00:29:26', '2016-01-09 00:29:26'),
(156, NULL, 'global', NULL, '2016-01-09 00:29:41', '2016-01-09 00:29:41'),
(157, NULL, 'ip', '192.168.1.121', '2016-01-09 00:29:41', '2016-01-09 00:29:41'),
(159, NULL, 'global', NULL, '2016-01-09 00:30:07', '2016-01-09 00:30:07'),
(160, NULL, 'ip', '192.168.1.121', '2016-01-09 00:30:07', '2016-01-09 00:30:07'),
(162, NULL, 'global', NULL, '2016-01-11 17:50:41', '2016-01-11 17:50:41'),
(163, NULL, 'ip', '192.168.1.142', '2016-01-11 17:50:41', '2016-01-11 17:50:41'),
(165, NULL, 'global', NULL, '2016-01-11 17:50:50', '2016-01-11 17:50:50'),
(166, NULL, 'ip', '192.168.1.142', '2016-01-11 17:50:50', '2016-01-11 17:50:50'),
(168, NULL, 'global', NULL, '2016-01-11 17:51:03', '2016-01-11 17:51:03'),
(169, NULL, 'ip', '192.168.1.142', '2016-01-11 17:51:03', '2016-01-11 17:51:03'),
(171, NULL, 'global', NULL, '2016-01-11 17:51:16', '2016-01-11 17:51:16'),
(172, NULL, 'ip', '192.168.1.142', '2016-01-11 17:51:16', '2016-01-11 17:51:16'),
(174, NULL, 'global', NULL, '2016-01-11 17:52:23', '2016-01-11 17:52:23'),
(175, NULL, 'ip', '192.168.1.142', '2016-01-11 17:52:23', '2016-01-11 17:52:23'),
(177, NULL, 'global', NULL, '2016-01-11 17:52:58', '2016-01-11 17:52:58'),
(178, NULL, 'ip', '192.168.1.142', '2016-01-11 17:52:58', '2016-01-11 17:52:58'),
(180, NULL, 'global', NULL, '2016-01-11 18:06:27', '2016-01-11 18:06:27'),
(181, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:27', '2016-01-11 18:06:27'),
(183, NULL, 'global', NULL, '2016-01-11 18:06:33', '2016-01-11 18:06:33'),
(184, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:33', '2016-01-11 18:06:33'),
(186, NULL, 'global', NULL, '2016-01-11 18:06:36', '2016-01-11 18:06:36'),
(187, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:36', '2016-01-11 18:06:36'),
(189, NULL, 'global', NULL, '2016-01-11 18:06:41', '2016-01-11 18:06:41'),
(190, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:41', '2016-01-11 18:06:41'),
(192, NULL, 'global', NULL, '2016-01-11 18:18:58', '2016-01-11 18:18:58'),
(193, NULL, 'ip', '192.168.1.142', '2016-01-11 18:18:58', '2016-01-11 18:18:58'),
(195, NULL, 'global', NULL, '2016-01-24 06:25:32', '2016-01-24 06:25:32'),
(196, NULL, 'ip', '127.0.0.1', '2016-01-24 06:25:32', '2016-01-24 06:25:32'),
(197, NULL, 'global', NULL, '2016-01-26 21:27:09', '2016-01-26 21:27:09'),
(198, NULL, 'ip', '127.0.0.1', '2016-01-26 21:27:09', '2016-01-26 21:27:09'),
(200, NULL, 'global', NULL, '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(201, NULL, 'ip', '127.0.0.1', '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(202, 1, 'user', NULL, '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(203, NULL, 'global', NULL, '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(204, NULL, 'ip', '127.0.0.1', '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(205, 1, 'user', NULL, '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(206, NULL, 'global', NULL, '2016-02-24 06:45:41', '2016-02-24 06:45:41'),
(207, NULL, 'ip', '192.168.1.59', '2016-02-24 06:45:41', '2016-02-24 06:45:41'),
(209, NULL, 'global', NULL, '2016-02-24 06:47:03', '2016-02-24 06:47:03'),
(210, NULL, 'ip', '192.168.1.59', '2016-02-24 06:47:03', '2016-02-24 06:47:03'),
(212, NULL, 'global', NULL, '2016-02-24 06:47:06', '2016-02-24 06:47:06'),
(213, NULL, 'ip', '192.168.1.59', '2016-02-24 06:47:06', '2016-02-24 06:47:06'),
(215, NULL, 'global', NULL, '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(216, NULL, 'ip', '127.0.0.1', '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(217, 1, 'user', NULL, '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(218, NULL, 'global', NULL, '2016-03-28 09:32:49', '2016-03-28 09:32:49'),
(219, NULL, 'ip', '127.0.0.1', '2016-03-28 09:32:49', '2016-03-28 09:32:49'),
(220, NULL, 'global', NULL, '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(221, NULL, 'ip', '127.0.0.1', '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(222, 1, 'user', NULL, '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(223, NULL, 'global', NULL, '2016-03-30 11:37:40', '2016-03-30 11:37:40'),
(224, NULL, 'ip', '::1', '2016-03-30 11:37:41', '2016-03-30 11:37:41'),
(225, NULL, 'global', NULL, '2016-05-02 04:01:37', '2016-05-02 04:01:37'),
(226, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:37', '2016-05-02 04:01:37'),
(227, NULL, 'global', NULL, '2016-05-02 04:01:40', '2016-05-02 04:01:40'),
(228, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:40', '2016-05-02 04:01:40'),
(229, NULL, 'global', NULL, '2016-05-02 04:01:43', '2016-05-02 04:01:43'),
(230, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:43', '2016-05-02 04:01:43'),
(231, NULL, 'global', NULL, '2016-05-02 04:01:49', '2016-05-02 04:01:49'),
(232, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:49', '2016-05-02 04:01:49'),
(233, NULL, 'global', NULL, '2016-05-02 04:26:12', '2016-05-02 04:26:12'),
(234, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:12', '2016-05-02 04:26:12'),
(235, NULL, 'global', NULL, '2016-05-02 04:26:40', '2016-05-02 04:26:40'),
(236, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:40', '2016-05-02 04:26:40'),
(237, NULL, 'global', NULL, '2016-05-02 04:26:58', '2016-05-02 04:26:58'),
(238, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:58', '2016-05-02 04:26:58'),
(239, NULL, 'global', NULL, '2016-05-02 04:27:01', '2016-05-02 04:27:01'),
(240, NULL, 'ip', '192.168.1.41', '2016-05-02 04:27:01', '2016-05-02 04:27:01'),
(241, NULL, 'global', NULL, '2016-05-02 04:27:07', '2016-05-02 04:27:07'),
(242, NULL, 'ip', '192.168.1.41', '2016-05-02 04:27:07', '2016-05-02 04:27:07'),
(243, NULL, 'global', NULL, '2017-01-15 22:13:01', '2017-01-15 22:13:01'),
(244, NULL, 'ip', '::1', '2017-01-15 22:13:01', '2017-01-15 22:13:01'),
(245, NULL, 'global', NULL, '2017-01-15 22:53:12', '2017-01-15 22:53:12'),
(246, NULL, 'ip', '::1', '2017-01-15 22:53:12', '2017-01-15 22:53:12'),
(248, NULL, 'global', NULL, '2017-01-15 22:53:40', '2017-01-15 22:53:40'),
(249, NULL, 'ip', '::1', '2017-01-15 22:53:40', '2017-01-15 22:53:40'),
(251, NULL, 'global', NULL, '2017-01-18 19:05:37', '2017-01-18 19:05:37'),
(252, NULL, 'ip', '::1', '2017-01-18 19:05:37', '2017-01-18 19:05:37'),
(253, NULL, 'global', NULL, '2017-01-25 06:29:52', '2017-01-25 06:29:52'),
(254, NULL, 'ip', '::1', '2017-01-25 06:29:52', '2017-01-25 06:29:52'),
(255, NULL, 'global', NULL, '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(256, NULL, 'ip', '112.135.7.121', '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(257, 9, 'user', NULL, '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(258, NULL, 'global', NULL, '2017-04-24 06:17:15', '2017-04-24 06:17:15'),
(259, NULL, 'ip', '::1', '2017-04-24 06:17:15', '2017-04-24 06:17:15'),
(260, NULL, 'global', NULL, '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(261, NULL, 'ip', '123.231.108.3', '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(262, 9, 'user', NULL, '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(263, NULL, 'global', NULL, '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(264, NULL, 'ip', '123.231.108.3', '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(265, 9, 'user', NULL, '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(266, NULL, 'global', NULL, '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(267, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(268, 9, 'user', NULL, '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(269, NULL, 'global', NULL, '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(270, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(271, 9, 'user', NULL, '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(272, NULL, 'global', NULL, '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(273, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(274, 9, 'user', NULL, '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(275, NULL, 'global', NULL, '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(276, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(277, 9, 'user', NULL, '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(278, NULL, 'global', NULL, '2017-08-30 06:27:01', '2017-08-30 06:27:01'),
(279, NULL, 'ip', '113.59.213.196', '2017-08-30 06:27:01', '2017-08-30 06:27:01'),
(280, NULL, 'global', NULL, '2017-09-18 11:32:04', '2017-09-18 11:32:04'),
(281, NULL, 'ip', '::1', '2017-09-18 11:32:04', '2017-09-18 11:32:04'),
(282, NULL, 'global', NULL, '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(283, NULL, 'ip', '::1', '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(284, 9, 'user', NULL, '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(285, NULL, 'global', NULL, '2017-10-17 16:52:42', '2017-10-17 16:52:42'),
(286, NULL, 'ip', '::1', '2017-10-17 16:52:42', '2017-10-17 16:52:42'),
(287, NULL, 'global', NULL, '2018-01-09 15:24:36', '2018-01-09 15:24:36'),
(288, NULL, 'ip', '::1', '2018-01-09 15:24:36', '2018-01-09 15:24:36'),
(289, NULL, 'global', NULL, '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(290, NULL, 'ip', '::1', '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(291, 12, 'user', NULL, '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(292, NULL, 'global', NULL, '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(293, NULL, 'ip', '::1', '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(294, 12, 'user', NULL, '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(295, NULL, 'global', NULL, '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(296, NULL, 'ip', '::1', '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(297, 12, 'user', NULL, '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(298, NULL, 'global', NULL, '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(299, NULL, 'ip', '::1', '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(300, 12, 'user', NULL, '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(301, NULL, 'global', NULL, '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(302, NULL, 'ip', '::1', '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(303, 18, 'user', NULL, '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(304, NULL, 'global', NULL, '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(305, NULL, 'ip', '::1', '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(306, 18, 'user', NULL, '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(307, NULL, 'global', NULL, '2018-02-12 04:48:18', '2018-02-12 04:48:18'),
(308, NULL, 'ip', '::1', '2018-02-12 04:48:18', '2018-02-12 04:48:18'),
(309, NULL, 'global', NULL, '2018-02-14 09:44:29', '2018-02-14 09:44:29'),
(310, NULL, 'ip', '::1', '2018-02-14 09:44:29', '2018-02-14 09:44:29'),
(311, NULL, 'global', NULL, '2018-02-14 09:44:38', '2018-02-14 09:44:38'),
(312, NULL, 'ip', '::1', '2018-02-14 09:44:38', '2018-02-14 09:44:38'),
(313, NULL, 'global', NULL, '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(314, NULL, 'global', NULL, '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(315, NULL, 'ip', '::1', '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(316, NULL, 'ip', '::1', '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(317, NULL, 'global', NULL, '2018-02-15 16:19:27', '2018-02-15 16:19:27'),
(318, NULL, 'ip', '::1', '2018-02-15 16:19:27', '2018-02-15 16:19:27'),
(319, NULL, 'global', NULL, '2018-02-18 05:14:10', '2018-02-18 05:14:10'),
(320, NULL, 'ip', '::1', '2018-02-18 05:14:10', '2018-02-18 05:14:10'),
(321, NULL, 'global', NULL, '2018-02-18 05:14:16', '2018-02-18 05:14:16'),
(322, NULL, 'ip', '::1', '2018-02-18 05:14:16', '2018-02-18 05:14:16'),
(323, NULL, 'global', NULL, '2018-02-18 05:14:21', '2018-02-18 05:14:21'),
(324, NULL, 'ip', '::1', '2018-02-18 05:14:21', '2018-02-18 05:14:21'),
(325, NULL, 'global', NULL, '2018-02-18 05:14:31', '2018-02-18 05:14:31'),
(326, NULL, 'ip', '::1', '2018-02-18 05:14:31', '2018-02-18 05:14:31'),
(327, NULL, 'global', NULL, '2018-02-18 06:38:25', '2018-02-18 06:38:25'),
(328, NULL, 'ip', '::1', '2018-02-18 06:38:25', '2018-02-18 06:38:25'),
(329, NULL, 'global', NULL, '2018-02-19 17:36:44', '2018-02-19 17:36:44'),
(330, NULL, 'ip', '::1', '2018-02-19 17:36:44', '2018-02-19 17:36:44'),
(331, NULL, 'global', NULL, '2018-02-25 17:41:01', '2018-02-25 17:41:01'),
(332, NULL, 'ip', '::1', '2018-02-25 17:41:01', '2018-02-25 17:41:01'),
(333, NULL, 'global', NULL, '2018-02-26 04:46:57', '2018-02-26 04:46:57'),
(334, NULL, 'ip', '::1', '2018-02-26 04:46:57', '2018-02-26 04:46:57'),
(335, NULL, 'global', NULL, '2018-02-26 04:47:14', '2018-02-26 04:47:14'),
(336, NULL, 'ip', '::1', '2018-02-26 04:47:14', '2018-02-26 04:47:14'),
(337, NULL, 'global', NULL, '2018-02-26 04:47:49', '2018-02-26 04:47:49'),
(338, NULL, 'ip', '::1', '2018-02-26 04:47:49', '2018-02-26 04:47:49'),
(339, NULL, 'global', NULL, '2018-02-26 04:48:00', '2018-02-26 04:48:00'),
(340, NULL, 'ip', '::1', '2018-02-26 04:48:00', '2018-02-26 04:48:00'),
(341, NULL, 'global', NULL, '2018-02-26 04:49:39', '2018-02-26 04:49:39'),
(342, NULL, 'ip', '::1', '2018-02-26 04:49:39', '2018-02-26 04:49:39'),
(343, NULL, 'global', NULL, '2018-02-26 04:53:08', '2018-02-26 04:53:08'),
(344, NULL, 'ip', '::1', '2018-02-26 04:53:08', '2018-02-26 04:53:08'),
(345, NULL, 'global', NULL, '2018-02-26 05:03:13', '2018-02-26 05:03:13'),
(346, NULL, 'ip', '::1', '2018-02-26 05:03:13', '2018-02-26 05:03:13'),
(347, NULL, 'global', NULL, '2018-02-26 05:04:52', '2018-02-26 05:04:52'),
(348, NULL, 'ip', '::1', '2018-02-26 05:04:52', '2018-02-26 05:04:52'),
(349, NULL, 'global', NULL, '2018-02-26 05:08:19', '2018-02-26 05:08:19'),
(350, NULL, 'ip', '::1', '2018-02-26 05:08:19', '2018-02-26 05:08:19'),
(351, NULL, 'global', NULL, '2018-02-26 05:09:22', '2018-02-26 05:09:22'),
(352, NULL, 'ip', '::1', '2018-02-26 05:09:22', '2018-02-26 05:09:22'),
(353, NULL, 'global', NULL, '2018-02-26 05:11:25', '2018-02-26 05:11:25'),
(354, NULL, 'ip', '::1', '2018-02-26 05:11:25', '2018-02-26 05:11:25'),
(355, NULL, 'global', NULL, '2018-02-26 05:12:45', '2018-02-26 05:12:45'),
(356, NULL, 'ip', '::1', '2018-02-26 05:12:45', '2018-02-26 05:12:45'),
(357, NULL, 'global', NULL, '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(358, NULL, 'ip', '::1', '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(359, 9, 'user', NULL, '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(360, NULL, 'global', NULL, '2018-02-26 07:10:30', '2018-02-26 07:10:30'),
(361, NULL, 'ip', '::1', '2018-02-26 07:10:30', '2018-02-26 07:10:30'),
(362, NULL, 'global', NULL, '2018-02-26 14:23:51', '2018-02-26 14:23:51'),
(363, NULL, 'ip', '::1', '2018-02-26 14:23:51', '2018-02-26 14:23:51'),
(364, NULL, 'global', NULL, '2018-02-26 14:24:26', '2018-02-26 14:24:26'),
(365, NULL, 'ip', '::1', '2018-02-26 14:24:26', '2018-02-26 14:24:26'),
(366, NULL, 'global', NULL, '2018-02-26 17:06:07', '2018-02-26 17:06:07'),
(367, NULL, 'ip', '::1', '2018-02-26 17:06:07', '2018-02-26 17:06:07'),
(368, NULL, 'global', NULL, '2018-02-28 05:00:30', '2018-02-28 05:00:30'),
(369, NULL, 'ip', '::1', '2018-02-28 05:00:30', '2018-02-28 05:00:30'),
(370, NULL, 'global', NULL, '2018-02-28 18:09:39', '2018-02-28 18:09:39'),
(371, NULL, 'ip', '::1', '2018-02-28 18:09:39', '2018-02-28 18:09:39'),
(372, NULL, 'global', NULL, '2018-02-28 18:17:32', '2018-02-28 18:17:32'),
(373, NULL, 'ip', '::1', '2018-02-28 18:17:32', '2018-02-28 18:17:32'),
(374, NULL, 'global', NULL, '2018-03-04 11:25:55', '2018-03-04 11:25:55'),
(375, NULL, 'ip', '::1', '2018-03-04 11:25:55', '2018-03-04 11:25:55'),
(376, NULL, 'global', NULL, '2018-03-24 03:30:53', '2018-03-24 03:30:53'),
(377, NULL, 'ip', '::1', '2018-03-24 03:30:53', '2018-03-24 03:30:53'),
(378, 26, 'user', NULL, '2018-03-24 03:30:53', '2018-03-24 03:30:53'),
(379, NULL, 'global', NULL, '2018-05-14 19:39:00', '2018-05-14 19:39:00'),
(380, NULL, 'ip', '::1', '2018-05-14 19:39:01', '2018-05-14 19:39:01'),
(381, NULL, 'global', NULL, '2018-05-14 19:39:12', '2018-05-14 19:39:12'),
(382, NULL, 'ip', '::1', '2018-05-14 19:39:12', '2018-05-14 19:39:12'),
(0, NULL, 'global', NULL, '2018-06-09 02:57:10', '2018-06-09 02:57:10'),
(0, NULL, 'ip', '::1', '2018-06-09 02:57:10', '2018-06-09 02:57:10'),
(0, 9, 'user', NULL, '2018-06-09 02:57:10', '2018-06-09 02:57:10'),
(0, NULL, 'global', NULL, '2018-09-09 15:13:14', '2018-09-09 15:13:14'),
(0, NULL, 'ip', '::1', '2018-09-09 15:13:14', '2018-09-09 15:13:14'),
(0, 0, 'user', NULL, '2018-09-09 15:13:14', '2018-09-09 15:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_id` text COLLATE utf8_unicode_ci NOT NULL,
  `g_id` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `branch` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `supervisor_id` int(25) DEFAULT NULL,
  `lft` int(25) DEFAULT NULL,
  `rgt` int(25) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fb_id`, `g_id`, `first_name`, `last_name`, `username`, `email`, `mobile`, `branch`, `password`, `permissions`, `last_login`, `supervisor_id`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, '', '', 'Super', 'Administrator', 'super.admin', 'admin@admin.lk', NULL, 0, '$2y$10$vJ0/.9l8qByoN/ZFJlzew.U3SUrYnsI6QoPezSfeo9qDQYLJSjC/O', '{\"admin\":true,\"index\":true}', '2018-05-10 17:44:31', NULL, 7, 14, 0, 1, '2015-07-11 06:09:31', '2018-05-10 17:44:31'),
(9, '', '', 'SDC', 'Developer', 'developer', 'developer@admin.com', NULL, 1, '$2y$10$ijimT7X0fQADncEnlwgRZelKPlCoLE81YVjgQlX25F64Ulf5JHiaG', NULL, '2018-09-16 13:19:48', 1, 12, 13, 1, 1, '2017-01-23 07:05:38', '2018-09-16 14:15:49'),
(34, '', '', 'dfghj', 'fghjkl', 'insaf.zak@gmail.com', 'insaf.zak@gmail.com', NULL, 0, '$2y$10$PIOznmoJT7zRR22J02iWVOUUtKH8jDiX13ORWAnjGLdB.dzhfG.Am', NULL, '2018-05-10 17:21:10', NULL, 15, 16, 0, NULL, '2018-05-10 17:21:10', '2018-05-10 17:21:44'),
(36, '', '', 'Insaf', 'zakariya', 'insaf.zak1@gmail.com', 'insaf.zak1@gmail.com', NULL, 0, '$2y$10$UXIbnctDd/86bCyYU87coOA9mi0TY9xkH6.OJoq2yKLHy41h7PK2K', NULL, '2018-05-10 17:40:13', NULL, 17, 18, 0, NULL, '2018-05-10 17:40:13', '2018-05-10 17:40:32'),
(0, '', '', 'chathu', 'dissanayaka', 'mp.chathurangafb@gmail.com', 'mp.chathurangafb@gmail.com', NULL, 0, '$2y$10$mFhqpZI1kGmOLzq5W6CNa.073kMWrNekm2syOXfT.KzRCvhgvexfS', NULL, '2018-09-16 14:17:43', NULL, 25, 26, 0, NULL, '2018-09-01 02:38:24', '2018-09-16 14:17:43'),
(0, '', '', 'chathuragna', 'indunil', 'mp.chathuranga@gmail.com', 'mp.chathuranga@gmail.com', NULL, 0, '$2y$10$V0pap/qte70ceOVwl6eua.YNTlObyEK0JZmSnlcDe25GdN6vEqG6S', NULL, '2018-09-16 14:17:43', NULL, 19, 20, 0, NULL, '2018-09-09 02:58:55', '2018-09-16 14:17:43'),
(0, '', '', 'chathuragnarer', 'indunilfsdf', 'chathu@info.com', 'chathu@info.com', NULL, 0, '$2y$10$T8KuVD8OJ.jYO0MBLtlpUu5hNBMZWuJ5UlK4UW/spnEHeDKX3gS5K', NULL, '2018-09-16 14:17:43', NULL, 21, 22, 0, NULL, '2018-09-15 18:28:02', '2018-09-16 14:17:43'),
(0, '', '', 'dsfshghfhgf', 'fsdfhgfhhfh', 'mp.chathurangsssa@gmail.com', 'mp.chathurangsssa@gmail.com', NULL, 0, '$2y$10$6zemmWoucAWsEebP/s9yw.jdqWSMMDLaeUrbfEyUM/6uB.A8iC86.', NULL, '2018-09-16 14:17:43', NULL, 23, 24, 0, NULL, '2018-09-16 14:17:43', '2018-09-16 14:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `sbu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`, `sbu_id`, `created_at`, `updated_at`) VALUES
(2, 2, 1, '2016-03-23 09:03:10', '2016-03-23 09:03:10'),
(3, 3, 2, '2016-03-23 09:26:12', '2016-03-23 09:26:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animation_type`
--
ALTER TABLE `animation_type`
  ADD PRIMARY KEY (`animation_type_id`),
  ADD UNIQUE KEY `animation_type_id` (`animation_type_id`);

--
-- Indexes for table `cake_type`
--
ALTER TABLE `cake_type`
  ADD PRIMARY KEY (`cake_type_id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_items`
--
ALTER TABLE `coupon_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fonts-list`
--
ALTER TABLE `fonts-list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `private_class_registation`
--
ALTER TABLE `private_class_registation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`publication_id`),
  ADD UNIQUE KEY `publication_id` (`publication_id`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`quote_id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`recipe_id`);

--
-- Indexes for table `recipes_category`
--
ALTER TABLE `recipes_category`
  ADD PRIMARY KEY (`recipes_category_id`);

--
-- Indexes for table `recipe_photo`
--
ALTER TABLE `recipe_photo`
  ADD PRIMARY KEY (`recipe_photo_id`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `sa_offer`
--
ALTER TABLE `sa_offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_offers`
--
ALTER TABLE `sa_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_privetclass_date_time`
--
ALTER TABLE `sa_privetclass_date_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_rating`
--
ALTER TABLE `sa_rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `recipe_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sa_offer`
--
ALTER TABLE `sa_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sa_offers`
--
ALTER TABLE `sa_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sa_privetclass_date_time`
--
ALTER TABLE `sa_privetclass_date_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `sa_rating`
--
ALTER TABLE `sa_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
