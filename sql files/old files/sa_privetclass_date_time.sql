-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2018 at 09:55 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sweet_delight_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sa_privetclass_date_time`
--

CREATE TABLE `sa_privetclass_date_time` (
  `id` int(11) NOT NULL,
  `privateclass_cat_id` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `start` varchar(100) NOT NULL,
  `end` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_privetclass_date_time`
--

INSERT INTO `sa_privetclass_date_time` (`id`, `privateclass_cat_id`, `date`, `start`, `end`, `created_at`, `updated_at`) VALUES
(39, 1, '2018-07-22', 'Array', 'Array', '2018-07-22 09:31:34', '2018-07-22 09:31:34'),
(40, 2, '2018-07-17', 'Array', 'Array', '2018-07-22 15:28:30', '2018-07-22 15:28:30'),
(41, 3, '', 'Array', 'Array', '2018-07-23 08:28:47', '2018-07-23 08:28:47'),
(42, 4, '', 'Array', 'Array', '2018-07-23 08:34:44', '2018-07-23 08:34:44'),
(43, 5, '', 'Array', 'Array', '2018-07-23 08:37:04', '2018-07-23 08:37:04'),
(44, 6, '', 'Array', 'Array', '2018-07-23 09:02:04', '2018-07-23 09:02:04'),
(45, 7, '', 'Array', 'Array', '2018-07-23 09:05:59', '2018-07-23 09:05:59'),
(46, 8, '', 'Array', 'Array', '2018-07-23 09:08:05', '2018-07-23 09:08:05'),
(47, 9, '', 'Array', 'Array', '2018-07-23 09:49:07', '2018-07-23 09:49:07'),
(48, 10, '', '', '', '2018-07-23 10:00:46', '2018-07-23 10:00:46'),
(49, 10, '', '', '', '2018-07-23 10:00:46', '2018-07-23 10:00:46'),
(50, 11, '2018-07-23', 'Array', 'Array', '2018-07-23 10:02:29', '2018-07-23 10:02:29'),
(51, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(52, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(53, 12, '2018-07-24', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(54, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(55, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(56, 12, '2018-07-30', '6', '7', '2018-07-23 10:04:15', '2018-07-23 10:04:15'),
(57, 13, '2018-08-16', '1', '1', '2018-07-23 10:05:32', '2018-07-23 10:05:32'),
(58, 13, '2018-07-31', '1', '1', '2018-07-23 10:05:32', '2018-07-23 10:05:32'),
(59, 14, '2018-07-23', 'Array', 'Array', '2018-07-23 10:09:14', '2018-07-23 10:09:14'),
(60, 21, '2018-07-02', 'Array', 'Array', '2018-07-23 10:20:48', '2018-07-23 10:20:48'),
(61, 28, '2018-07-23', '445', '333', '2018-07-23 10:36:14', '2018-07-23 10:36:14'),
(62, 29, '2018-07-25', '65', '67', '2018-07-23 10:36:53', '2018-07-23 10:36:53'),
(63, 29, '2018-07-27', '76', '767', '2018-07-23 10:36:53', '2018-07-23 10:36:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sa_privetclass_date_time`
--
ALTER TABLE `sa_privetclass_date_time`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sa_privetclass_date_time`
--
ALTER TABLE `sa_privetclass_date_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
