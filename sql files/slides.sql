/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : sweetdelights_db

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 29/03/2019 13:01:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for slides
-- ----------------------------
DROP TABLE IF EXISTS `slides`;
CREATE TABLE `slides`  (
  `slides_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `animation_type` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `animation_txt` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `btn_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NOT NULL,
  `updated_at` timestamp(0) NOT NULL,
  `btn_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `btn_animation_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message_first_line_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `message_second_line_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `message_third_line_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `message_fourth_line_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `top_first` int(11) NULL DEFAULT NULL,
  `maxwidth_first` int(11) NULL DEFAULT NULL,
  `top_second` int(11) NULL DEFAULT NULL,
  `maxwidth_second` int(11) NULL DEFAULT NULL,
  `top_third` int(11) NULL DEFAULT NULL,
  `maxwidth_third` int(11) NULL DEFAULT NULL,
  `top_fourth` int(11) NULL DEFAULT NULL,
  `maxwidth_fourth` int(11) NULL DEFAULT NULL,
  `img_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`slides_id`) USING BTREE,
  UNIQUE INDEX `slides_id`(`slides_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of slides
-- ----------------------------
INSERT INTO `slides` VALUES (3, 1, 'main-slider-fD0l8bFXkZqU.png', '', 'shop', 1, '2019-03-28 23:44:17', '2019-03-29 12:48:10', 'main-slider-wp6RXBwqBnBr.png', 'center', 'image', 'main-slider-AnBLsV4KORK1.png', 'main-slider-K5mw2oWwg9MD.png', 'main-slider-wpNWjJePNhWr.png', 'main-slider-AnSVQwiKJJQe.png', 10, 100, 15, 50, 40, 50, 50, 50, 'center');

SET FOREIGN_KEY_CHECKS = 1;
