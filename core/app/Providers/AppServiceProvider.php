<?php namespace App\Providers;

use Validator;
use Schema;
use Illuminate\Support\ServiceProvider;
use App\Validators\ReCaptcha;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        // Validator::extend('recaptcha','App\\Validators\\ReCaptcha@validate');
        Validator::extend('recaptcha', function($attribute, $value, $parameters, $validator) {
			$recaptach = new ReCaptcha;
			return $recaptach->validate($attribute, $value, $parameters, $validator);
        });
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
