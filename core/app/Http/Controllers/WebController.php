<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReCaptchataTestFormRequest;
use CakeTypeManager\Models\CakeType;
use Illuminate\Http\Request;
use File;
use NewsManage\Models\News;
use PrivateClassManage\Models\PrivateClassCategory;
use PrivateClassManage\Models\PrivateClassRegistration;
use PrivateClassManage\Models\PrivetclassDateTime;
use PrivateClassManage\Models\PrivateReg;
use GalleryManage\Models\Gallery;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\GalleryImage;
use RecipesManager\Models\Recipes;
use PublicationManager\Models\Publication;
use TastingManage\Models\Tasting;
use TastingManage\Models\TastingPeopleType;
use SliderManager\Models\Slider;
use App\Models\ShoppingCart;
use App\Models\BillingDetail;
use CountryManager\Models\Country;
use App\Models\LastShipping;
use App\Models\ShippingMethod;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\PaypalTransaction;
use App\Models\SortType;
use App\Models\Rating;
use App\Models\TermsOfService;
use RecipesCategoryManager\Models\RecipesCategory;
use App\Models\Subscriber;
use Response;
use Validator;
use Input;
use DB;
use Session;
use Mail;
use Cart;
use PayPal;
use Usps;
use Carbon\Carbon;
use App\Helpers\QueryArraySort;
use TestimonialsManager\Models\Testimonials;
use UserManage\Models\User;
use UserRoles\Models\UserRole;
use App\Http\Requests\RegisterRequest;
use QuoteManager\Models\Quote;
use BlogManage\Models\Blog;
use BlogManage\Models\BlogImage;
use BlogManage\Models\Comment;
use ProductCreationManage\Models\ProductCreationCategory;
use ProductCreationManage\Models\ProductCreation;
use ProductCreationManage\Models\ProductCreationImage;
use ProductCreationManage\Models\ProductHasCategory;
use HonorManage\Models\Honor;
use FlavorManage\Models\Flavor;
use PricingManage\Models\Pricing;
use Sentinel;
use PDF;
use View;
use Permissions\Models\Permission;
use Hash;

class WebController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Web Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function test() {

        return $rate = Usps::domesticRate('FIRST CLASS', 'LETTER', 99501, 72201, 0, 3.5, '', 'REGULAR');
        return json_decode($rate);
        return response()->json(
            Usps::domesticRate()
        );
    }

    public function index() {

        $slider_details = Slider::with('animationType')->get();
        $blogs = Blog::with('getImages')->where('home', '=', 1)->where('status', '=', 1)
        ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
        ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
        ->limit(2)->orderby('id', 'desc')->get();
        //	var_dump($blogs);die;

        $testimonials = Testimonials::where('status', '=', 1)->get();
        $news = News::where('status', '=', 1)->limit(2)->orderby('id', 'desc')->get();

        $publicationImages = Publication::all();
        // dd($news);
        return view('front.index')->with(['slider_details' => $slider_details, 'blogs' => $blogs, 'testimonials' => $testimonials, 'news' => $news, 'publicationImages' => $publicationImages]);
    }

    public function about() {

        return view('front.about');
    }

    public function gallery($val) {
        if ($val == 0) {
            $gal = Gallery::has('getImages')->where('status', '=', 1)->get();
            $gal = QueryArraySort::orderBy($gal);
        } else {
            $gal = collect();
            $gal0 = Gallery::has('getImages')->where('status', '=', 1)->get();
            foreach ($gal0 as $item) {
                $categories = json_decode($item->categories);
                if ($categories) {
                    if (in_array($val, $categories)) {
                        $gal->push($item);
                    }
                }
            }
            $gal = QueryArraySort::orderBy($gal, 'catPriorValue DESC, priority ASC');
        }
        $cat = GalleryCategory::where('status', '=', 1)->get();
        // $img = array();
        // $i = 1;
        // foreach ($gal as $item) {
        //     $dd = array();
        //     $c = json_decode($item->categories);
        //     $ca = GalleryCategory::whereIn('id', $c)->first();
        //     array_push($dd, $ca->name, $item->album_name, $item->path, $item->filename, $item->id);
        //     array_push($img, $dd);
        //     $i++;
        // }
        return view('front.gallery')->with(['cat' => $cat, 'gal' => $gal, 'val' => $val]);
    }

    public function pricing() {
        $pricing = Pricing::where('id', '<>', 1)->get();
        $diposit = Pricing::where('id', 1)->first();
        return view('front.pricing', compact('pricing', 'diposit'));
    }

    public function flavors() {
        //id 1 will be the message in the bottom of the page
        $flavors = Flavor::with('basicTypes', 'premiumTypes')->where('id', '<>', 1)->get();
        $msg = Flavor::where('id', 1)->first();
        return view('front.flavors', compact('flavors', 'msg'));
    }

    public function quotes() {
        $cake = CakeType::where('status', '=', 1)->get();
        $config = \ConfigManage\Models\Config::find(1);
        return view('front.quotes')->with(['cake' => $cake, 'config' => $config]);
    }

    public function contact() {

        $details = \ConfigManage\Models\Config::find(1);

        return view('front.contact', compact('details'));
    }

    public function checkout1() {

        $country = Country::orderBy('name', 'asc')->get();
        $user = Sentinel::getUser();
        $billing_details = BillingDetail::where('created_by', $user->id)->with(['getCountry'])->get();

        return view('front.checkout1')->with(['country' => $country, 'billing_details' => $billing_details]);
    }

    public function checkout1_submit() {
        $user = Sentinel::getUser();
        $billing_detail = 0;
        if (Input::get('submit_type') == 'exsist_address') {
            $billing_detail = BillingDetail::find(Input::get('billing_id'));
            # code...
        } else if (Input::get('submit_type') == 'new_address') {
            $country = Input::get('country');
            $city = Input::get('city');
            $province = Input::get('province');
            $postcode = Input::get('postcode');
            $address1 = Input::get('address1');
            $address2 = Input::get('address2');
            $first_name = Input::get('first_name');
            $last_name = Input::get('last_name');
            $phone = Input::get('phone');
            $email = Input::get('email');

            $billing_detail = BillingDetail::Create([
                'country_id' => $country,
                'city' => $city,
                'province' => $province,
                'postcode' => $postcode,
                'street_addresss_1' => $address1,
                'street_addresss_2' => $address2,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'email' => $email,
                'is_primary' => 1,
                'created_by' => $user->id
            ]);
        }

        $country = $billing_detail->country_id;
        $city = $billing_detail->city;
        $province = $billing_detail->province;
        $postcode = $billing_detail->postcode;
        $address1 = $billing_detail->street_addresss_1;
        $address2 = $billing_detail->street_addresss_2;
        $first_name = $billing_detail->first_name;
        $last_name = $billing_detail->last_name;
        $phone = $billing_detail->phone;
        $email = $billing_detail->email;

        $lastShipping = LastShipping::where('created_by', $user->id)->first();
        if ($lastShipping) {
            $lastShipping->country_id = $country;
            $lastShipping->city = $city;
            $lastShipping->province = $province;
            $lastShipping->postcode = $postcode;
            $lastShipping->street_addresss_1 = $address1;
            $lastShipping->street_addresss_2 = $address2;
            $lastShipping->first_name = $first_name;
            $lastShipping->last_name = $last_name;
            $lastShipping->phone = $phone;
            $lastShipping->email = $email;

            $lastShipping->save();
        } else {
            LastShipping::Create([
                'country_id' => $country,
                'city' => $city,
                'province' => $province,
                'postcode' => $postcode,
                'street_addresss_1' => $address1,
                'street_addresss_2' => $address2,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'email' => $email,
                'created_by' => $user->id
            ]);
        }

        return redirect('checkout2');
    }

    public function checkout2() {

        $user = Sentinel::getUser();
        $lastShipping = LastShipping::where('created_by', $user->id)->first();
        $shippingMethod = ShippingMethod::get();
        return view('front.checkout2')->with(['shippingMethod' => $shippingMethod, 'lastShipping' => $lastShipping]);
    }

    public function checkout2_submit() {
        $shipping_method = Input::get('shipping_method');

        $user = Sentinel::getUser();
        $lastShipping = LastShipping::where('created_by', $user->id)->first();
        if ($lastShipping) {
            $lastShipping->shipping_method = $shipping_method;

            $lastShipping->save();
            return redirect('checkout3');
        }
    }

    public function checkout3() {
        return view('front.checkout3');
    }

    public function checkout3_submit() {
        $payment_method = Input::get('payment_method');
        if ($payment_method == 'paypal') {
            return redirect('checkout4');
        } else {
            return 'SORRY THIS PAYMENT TYPE IS NOT VALIED , PLEASE SELECT PAYPAL';
        }
    }

    public function checkout4() {
        $user = Sentinel::getUser();
        // $lastShipping=LastShipping::where('created_by',$user->id)->with(['getShippingMethod'])->first();
        $lastShipping = LastShipping::where('created_by', $user->id)->with(['getShippingMethod', 'getCountry'])->first();
        $country = Country::find($lastShipping->country_id);
        $shipping_charges = $country->price;
        $condition1 = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'Express Shipping',
            'type' => 'shipping',
            'target' => 'subtotal',
            'value' => '+' . $shipping_charges,
        ));
        Cart::condition($condition1);
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $cartArray = $cartCollection->toArray();
        $shoppingCartArray_product = array();
        $shoppingCartArray_tutorial = array();
        foreach ($cartArray as $key => $value) {
            $cart_id = explode('-', $value['id']);
            $type = $cart_id[0];
            $id = $cart_id[1];

            if ($type == 'P') {
                /* THIS IS DEFAULT PRODUCT */
                $selected_product = ProductCreation::with(['getImages'])->find($id);
                $path = '/core/storage/' . $selected_product->getImages[0]->path . '/' . $selected_product->getImages[0]->filename;

                array_push($shoppingCartArray_product, array(
                    'id' => $id,
                    'type' => 1,
                    'cart_id' => $value['id'],
                    'name' => $value['name'],
                    'price' => $selected_product->price,
                    'quantity' => $value['quantity'],
                    'details' => $selected_product,
                    'image' => url($path)
                )
            );
            } else if ($type == 'T') {
                /* THIS IS TOUTORIALS */
                $selected_product = Recipes::with(['getImages'])->find($id);
                $path = '/core/storage/' . $selected_product->getImages[0]->cover_path . '/' . $selected_product->getImages[0]->cover_file;

                array_push($shoppingCartArray_tutorial, array(
                    'id' => $id,
                    'type' => 2,
                    'cart_id' => $value['id'],
                    'name' => $value['name'],
                    'price' => $selected_product->price,
                    'quantity' => $value['quantity'],
                    'image' => url($path)
                )
            );
            }
        }
        return view('front.checkout4')
        ->with([
            'user' => $user,
            'lastShipping' => $lastShipping,
            'cartCollection' => $cartCollection,
            'shoppingCartArray_product' => $shoppingCartArray_product,
            'shoppingCartArray_tutorial' => $shoppingCartArray_tutorial,
            'cartTotalQuantity' => $cartTotalQuantity,
            'subTotal' => $subTotal,
            'total' => $total,
            'shipping_charges' => $shipping_charges
        ]);
    }

    public function checkout4_submit() {
        $provider = PayPal::setProvider('express_checkout');
        $user = Sentinel::getUser();
        $invoice_id = date('ymdHis') . $user->id;
        $shoppingCartArray = $this->shopping_cart_creation_for_checkout($invoice_id);
        $response = $provider->setExpressCheckout($shoppingCartArray, true);
        print_r($response);
        // return redirect($response['paypal_link']);
    }

    public function paypal_success() {
        $user = Sentinel::getUser();
        $provider = PayPal::setProvider('express_checkout');
        $token = Input::get('token');
        $PayerID = Input::get('PayerID');
        $response = $provider->getExpressCheckoutDetails($token);
        $invoice_id = $response['INVNUM'];
        $invoice_count = Purchase::where('inoice_no', $invoice_id)->get();
        if (count($invoice_count) == 0) {
            if ($response['BILLINGAGREEMENTACCEPTEDSTATUS'] == 1 & $response['ACK'] == 'Success') {
                $amount = $response['AMT'];
                $shipping_amount = $response['SHIPPINGAMT'];
                $currency_code = $response['CURRENCYCODE'];
                $timestamp = $response['TIMESTAMP'];
                $lastShipping = LastShipping::where('created_by', $user->id)->first();
                if ($lastShipping) {
                    $purchase_detail = Purchase::Create([
                        'inoice_no' => $invoice_id,
                        'amount' => $amount,
                        'currency_code' => $currency_code,
                        'country_id' => $lastShipping->country_id,
                        'city' => $lastShipping->city,
                        'province' => $lastShipping->province,
                        'postcode' => $lastShipping->postcode,
                        'street_addresss_1' => $lastShipping->street_addresss_1,
                        'street_addresss_2' => $lastShipping->street_addresss_2,
                        'first_name' => $lastShipping->first_name,
                        'last_name' => $lastShipping->last_name,
                        'phone' => $lastShipping->phone,
                        'email' => $lastShipping->email,
                        'shipping_method' => $lastShipping->shipping_method,
                        'shipping_amount' => $shipping_amount,
                        'created_by' => $user->id,
                        'status' => 0
                    ]);
                    if ($purchase_detail) {
                        $shoppingCartArray = $this->shopping_cart_creation_for_checkout($invoice_id);
                        foreach ($shoppingCartArray['items_Id'] as $key => $item) {
                            PurchaseItem::Create([
                                'purchase_id' => $purchase_detail->id,
                                'item_id' => $item['item_id'],
                                'name' => $item['name'],
                                'unit_price' => $item['price'],
                                'qty' => $item['qty'],
                                'type' => $item['type']
                            ]);
                        }

                        $paypalTransaction = PaypalTransaction::Create([
                            'TOKEN' => $response['INVNUM'],
                            'TIMESTAMP' => $response['TIMESTAMP'],
                            'EMAIL' => $response['EMAIL'],
                            'PAYERID' => $response['PAYERID'],
                            'FIRSTNAME' => $response['FIRSTNAME'],
                            'LASTNAME' => $response['LASTNAME'],
                            'COUNTRYCODE' => $response['COUNTRYCODE'],
                            'CURRENCYCODE' => $response['CURRENCYCODE'],
                            'AMT' => $response['AMT'],
                            'DESCRIPTION' => $response['DESC'],
                            'PAYMENTREQUESTINFO_0_ERRORCODE' => $response['PAYMENTREQUESTINFO_0_ERRORCODE'],
                            'SHIPPINGAMT' => $response['SHIPPINGAMT'],
                            'TAXAMT' => $response['TAXAMT'],
                            'CORRELATIONID' => $response['CORRELATIONID'],
                            'ACK' => $response['ACK'],
                            'VERSION' => $response['VERSION'],
                        ]);
                        if ($paypalTransaction) {
                            $response = $provider->doExpressCheckoutPayment($shoppingCartArray, $token, $PayerID);
                            $paypalTransaction->PAYMENTINFO_0_SECUREMERCHANTACCOUNTID = $response['PAYMENTINFO_0_SECUREMERCHANTACCOUNTID'];
                            $paypalTransaction->PAYMENTINFO_0_PAYMENTSTATUS = $response['PAYMENTINFO_0_SECUREMERCHANTACCOUNTID'];
                            $paypalTransaction->$response['PAYMENTINFO_0_TRANSACTIONID'];
                            $paypalTransaction->save();
                            $purchase_detail->status = 1;
                            $purchase_detail->save();
                            ShoppingCart::where('created_by', $user->id)->Delete();
                            Cart::clear();
                            return redirect('paypal/payment/recipt');
                        }
                    }
                }
            } else {

            }
        } else {
            return 'DUPLICATE REFRESH';
        }
    }

    public function shopping_cart_creation_for_checkout($invoice_id) {

        $cartCollection = Cart::getContent();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $condition = Cart::getCondition('Express Shipping');
        ;
        $shipping_cost = $condition->getValue();
        $cartArray = $cartCollection->toArray();
        $shoppingCartArray = [
            'items' => array(),
            'items_Id' => array(),
            'invoice_id' => $invoice_id,
            'invoice_description' => 'Order # ' . $invoice_id . ' Invoice',
            'return_url' => url('/paypal/payment/success'),
            'cancel_url' => url('/paypal/payment/reject'),
            'subtotal' => $subTotal,
            'total' => $total,
            'shipping' => (float) $shipping_cost
        ];

        foreach ($cartArray as $key => $value) {

            $cart_id = explode('-', $value['id']);
            $type = $cart_id[0];
            $id = $cart_id[1];
            if ($cart_id[0] == 'P') {
                $type = 1;
            } else if ($cart_id[0] == 'T') {
                $type = 2;
            }


            // $selected_product=ProductCreation::with(['getImages'])->find($value['id']);
            array_push($shoppingCartArray['items'], array(
                'name' => $value['name'],
                'price' => $value['price'],
                'qty' => $value['quantity']
            )
        );
            array_push($shoppingCartArray['items_Id'], array(
                'item_id' => $id,
                'name' => $value['name'],
                'price' => $value['price'],
                'qty' => $value['quantity'],
                'type' => $type
            )
        );
        }
        array_push($shoppingCartArray['items'], array(
            'name' => 'coupon',
            'price' => -100,
            'qty' => 1
        )
    );

        return $shoppingCartArray;
    }

    public function paypal_recipt() {
        return view('front.paypal_recipt');
    }

    public function paypal_reject() {
        // $provider = PayPal::setProvider('express_checkout');
        // $token=Input::get('token');
        // return $response = $provider->getExpressCheckoutDetails($token);
        // print_r($response);
        return redirect('my-cart');
    }

    public function PrivateClassesLand($val) {
        if ($val == 0) {
            $pc = PrivateClassCategory::with('getDateTime')->get();
        } else {
            $pc = PrivateClassCategory::with('getDateTime')->where('skill_level', $val)->get();
        }

        // var_dump($pc);die;
        return view('front.PrivateClassesLand')->with([
            'clz' => $pc
        ]);
    }

    public function PrivateClassesReg($id) {

        $ca = DB::table('sa_privateclass_category')
        ->join('sa_privetclass_date_time', 'sa_privateclass_category.id', '=', 'sa_privetclass_date_time.privateclass_cat_id')
        ->select('sa_privateclass_category.*', 'sa_privetclass_date_time.*')
        ->where('status', '=', 1)
        ->where('sa_privateclass_category.id', '=', $id)
        ->get();
        $dt = PrivetclassDateTime::where('privateclass_cat_id', $id)->get();
        $ca = PrivateClassCategory::with('getDateTime', 'getImages', 'getSkillLevel')->find($id);
        $date_time = $ca->getDateTime;

        if (Sentinel::getUser()) {
            $user = Sentinel::getUser();
        } else {
            $user = NULL;
        }
        //$ca=PrivateClassCategory::where('status','=',1)->where('id','=',$id)->get();
        //var_dump($ca);die;

        return view('front.PrivateClassesReg', [
            'cat' => $ca,
            'user' => $user,
            'class' => $id,
            'dt' => $dt,
            'date_time' => $date_time
        ]);
    }

    public function blog() {

            $blogs = Blog::with('getImages')
                ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
                ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
                ->where('status', '=', 1)->paginate(4);

            $recent_blog = Blog::with('getImages')
                ->orderBy('start_date', 'desc')
                ->take(3)->get();

            $featured_blogs = Blog::with('getImages')
                ->where('featuredpost','=',1)
                ->take(3)->get();

            $arr_tags = [];
            $tags = Blog::get(['tags']);
            // dd($tags);exit;
            foreach ($tags as $tag){
                // echo $tag;
                $temp = explode(',', $tag->tags);
                foreach ($temp as $t){
                    if($t != null) {
                        array_push($arr_tags, $t);
                    }
                }
            }
            $keywords = ['Birthday', 'Wedding', 'Cakes', 'Flowers', 'Colorful'];

            return view('front.blog', ['keywords' => $keywords])->with(['blogs'=> $blogs, 'recent_blogs'=>$recent_blog, 'featured_blogs'=>$featured_blogs, 'tags' => $arr_tags]);



           }

           public function searchByTag($tag){

                   $blogs = Blog::with('getImages')
                       ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
                       ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
                       ->where('status', '=', 1)
                       ->where('tags', 'like', '%' . $tag . '%')->paginate(10);

                   $recent_blog = Blog::with('getImages')
                       ->orderBy('start_date', 'desc')
                       ->take(3)->get();

                   $featured_blogs = Blog::with('getImages')
                       ->where('featuredpost','=',1)
                       ->take(3)->get();

                   $arr_tags = [];
                   $tags = Blog::get(['tags']);
                   // dd($tags);exit;
                   foreach ($tags as $tag){
                       // echo $tag;
                       $temp = explode(',', $tag->tags);
                       foreach ($temp as $t){
                           if($t != null) {
                               array_push($arr_tags, $t);
                           }
                       }
                   }
                   $keywords = ['Birthday', 'Wedding', 'Cakes', 'Flowers', 'Colorful'];

                   return view('front.blog', ['keywords' => $keywords])->with(['blogs'=> $blogs, 'recent_blogs'=>$recent_blog, 'featured_blogs'=>$featured_blogs, 'tags' => $arr_tags]);


               }


    public function blogSearch(Request $request) {
        $blogs1 = Blog::with('getImages')
        ->where(function ($q) use ($request) {
            if ($request->has('qu')) {
                $q->where('name', 'like', '%' . $request['qu'] . '%')
                ->orWhere('description', 'like', '%' . $request['qu'] . '%');
            }
        })
        ->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))
        ->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
        ->where('status', '=', 1)->paginate(10);

        $finalArr = array();

        foreach ($blogs1 as $blog) {
            $arr = array(
                'id' => $blog->id,
                'name' => $blog->name,
                'description' => substr(strip_tags($blog->description), 0, 1200),
                'date' => Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d'),
                'month' => substr(Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'), 0, 3),
                'front_image' => $blog->front_image
            );
            array_push($finalArr, $arr);
        }

        return $finalArr;

        $keywords = ['Birthday', 'Wedding', 'Cakes', 'Flowers', 'Colorful'];

        return view('front.blog', ['keywords' => $keywords])->with('blogs', $blogs);
    }

    public function blogStdSearch(Request $request) {
        $blogs1 = Blog::with('getImages')
        ->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))
        ->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
        ->where('status', '=', 1)->paginate(10);

        $finalArr = array();

        foreach ($blogs1 as $blog) {
            $arr = array(
                'id' => $blog->id,
                'name' => $blog->name,
                'description' => substr(strip_tags($blog->description), 0, 1200),
                'date' => Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d'),
                'month' => substr(Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'), 0, 3),
                'front_image' => $blog->front_image
            );
            array_push($finalArr, $arr);
        }

        return $finalArr;

        $keywords = ['Birthday', 'Wedding', 'Cakes', 'Flowers', 'Colorful'];

        return view('front.blog', ['keywords' => $keywords])->with('blogs', $blogs);
    }

    public function blogDetails($id) {
        $blog = Blog::with('comments')->find($id);
        // dd($blog);
        $blogs = Blog::where('status', '=', 1)->with(['getImages'])
        ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
        ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
        ->limit(2)->get();
        if ($blog != NULL)
            return view('front.blogDetails')->with('blog', $blog)->with('latest_blogs', $blogs);
    }

    public function cart_loading_from_db() {
        if ($user = Sentinel::check()) {

            Cart::clear();
            $exsistProductDB = ShoppingCart::where('created_by', $user->id)->get();
            foreach ($exsistProductDB as $key => $singleProduct) {
                $product_array = array();
                if ($singleProduct->type == 1) {
                    /* THIS IS DEFAULT PRODUCT */
                    $selected_product = ProductCreation::find($singleProduct->product_id);
                    if (!empty($selected_product)) {
                        $product_array = array(
                            'product_id' => $selected_product->id,
                            'product_name' => $selected_product->product_name,
                            'price' => $selected_product->price,
                            'qty' => $singleProduct->qty,
                            'type' => 1,
                            'cart_id' => 'P-' . $selected_product->id
                        );
                    }
                    if ($selected_product->checkInSale()) {
                        $product_array['price'] = $selected_product->sale_price;
                    }
                } else if ($singleProduct->type == 2) {
                    /* THIS IS TOUTORIALS */
                    $selected_product = Recipes::find($singleProduct->product_id);
                    $product_array = array(
                        'product_id' => $selected_product->recipe_id,
                        'product_name' => $selected_product->name,
                        'price' => $selected_product->price,
                        'qty' => $singleProduct->qty,
                        'type' => 2,
                        'cart_id' => 'T-' . $selected_product->recipe_id
                    );
                }
                if (!empty($product_array)) {
                    Cart::add(array(
                        'id' => $product_array['cart_id'],
                        'name' => $product_array['product_name'],
                        'price' => $product_array['price'],
                        'quantity' => $product_array['qty'],
                        'attributes' => array(
                            'cart_status' => 'DB'
                        )
                    ));
                }
            }
        }
    }

    public function myCart() {
        $this->cart_loading_from_db();
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $coupon_condition = Cart::getCondition('coupon');
        $cartArray = $cartCollection->toArray();
        $shoppingCartArray_product = array();
        $shoppingCartArray_tutorial = array();
        foreach ($cartArray as $key => $value) {
            $cart_id = explode('-', $value['id']);
            $type = $cart_id[0];
            $id = $cart_id[1];

            if ($type == 'P') {
                /* THIS IS DEFAULT PRODUCT */
                $selected_product = ProductCreation::with(['getImages'])->find($id);
                $path = '/core/storage/' . $selected_product->getImages[0]->path . '/' . $selected_product->getImages[0]->filename;

                if ($selected_product->checkInSale()) {
                    array_push($shoppingCartArray_product, array(
                        'id' => $id,
                        'type' => 1,
                        'cart_id' => $value['id'],
                        'name' => $value['name'],
                        'price' => $selected_product->sale_price,
                        'quantity' => $value['quantity'],
                        'details' => $selected_product,
                        'image' => url($path)
                    )
                );
                } else {
                    array_push($shoppingCartArray_product, array(
                        'id' => $id,
                        'type' => 1,
                        'cart_id' => $value['id'],
                        'name' => $value['name'],
                        'price' => $selected_product->price,
                        'quantity' => $value['quantity'],
                        'details' => $selected_product,
                        'image' => url($path)
                    )
                );
                }
            } else if ($type == 'T') {
                /* THIS IS TOUTORIALS */
                $selected_product = Recipes::with(['getImages'])->find($id);
                $path = '/core/storage/' . $selected_product->getImages[0]->cover_path . '/' . $selected_product->getImages[0]->cover_file;

                array_push($shoppingCartArray_tutorial, array(
                    'id' => $id,
                    'type' => 2,
                    'cart_id' => $value['id'],
                    'name' => $value['name'],
                    'price' => $selected_product->price,
                    'quantity' => $value['quantity'],
                    'image' => url($path)
                )
            );
            }
        }

        return view('front.myCart')
        ->with([
            'cartCollection' => $cartCollection,
            'shoppingCartArray_product' => $shoppingCartArray_product,
            'shoppingCartArray_tutorial' => $shoppingCartArray_tutorial,
            'cartTotalQuantity' => $cartTotalQuantity,
            'coupon_condition' => $coupon_condition,
            'subTotal' => $subTotal,
            'total' => $total
        ]);
    }

    public function faq() {
        return view('front.faq');
    }

    public function privacyPolicy() {
        return view('front.privacyPolicy');
    }

    public function TermsOfService() {
        // $ts = TermsOfService::find(1);
        return view('front.TermsOfService');
    }

    public function news() {
        $news = News::with('getImages')->orderBy('created_at', 'asc')->paginate(12);
        $honor = Honor::get();
        $count = $honor->count();
        return view('front.news')->with(['news' => $news, 'honor' => $honor, 'count' => $count]);
    }

    public function newsDetails($id) {
        $news = News::with('getImages')->findOrFail($id);

        return view('front.newsDetails', compact('news'));
    }

    public function register() {
        return view('front.register');
    }

    public function productload($category, $search_passed, $sort, $mprice, $hprice) {
        $this->cart_loading_from_db();
        $sortType = SortType::get();
        $categories = ProductCreationCategory::get();

        $category = $category;
        $search = $search_passed;
        $sort = $sort;
        ////////VARIABLES////////////////////
        $min_price = NULL;
        $max_price = NULL;
        $price_filter_query = '';
        ////////////////PRICE FILTERING/////////////////////
        if (is_numeric($mprice)) {
            $min_price = $mprice;
        }
        if (is_numeric($hprice)) {
            $max_price = $hprice;
        }


        $current_sort = SortType::find($sort);
        $query = ProductCreation::with(['getImages']);

        $category_list = DB::table('sa_productcreation_category')
        ->leftJoin('sa_product_has_category_many', 'sa_productcreation_category.id', '=', 'sa_product_has_category_many.category_id')
        ->leftJoin('sa_productcreation', 'sa_product_has_category_many.product_id', '=', 'sa_productcreation.id')
        ->selectRaw('sa_productcreation_category.id,sa_productcreation_category.name,count(sa_product_has_category_many.product_id) AS count')
        ->where('sa_product_has_category_many.deleted_at', '=', NULL)
        ->where('sa_productcreation.deleted_at', '=', NULL)
        ->groupBy('category_id')->orderby('sa_productcreation_category.id', 'asc')->get();

        //////////////////CONDITIOND/////////////////////////////

        if ($category != 'all') {
            $product_ids = ProductHasCategory::where('category_id', $category)
            ->get(['product_id']);

            $query = $query->whereIn('id', $product_ids);
        }

        if (is_numeric($mprice)) {
            $query = $query->where(function($q1) use($mprice){
                $q1->where(function($q2) use($mprice){
                    $q2->where("on_sale", 'on')->where('sale_price', '>=', $mprice);
                })
                ->orWhere(function($q3) use($mprice){
                    $q3->whereNull("on_sale")->where('price', '>=', $mprice);
                });
            });
        }

        if (is_numeric($hprice)) {

            $query = $query->where(function($q4) use($hprice){
                $q4->where(function($q21) use($hprice){
                    $q21->where("on_sale", 'on')->where('sale_price', '<=', $hprice);
                })
                ->orWhere(function($q31) use($hprice){
                    $q31->whereNull("on_sale")->where('price', '<=', $hprice);
                });
            });
        }

        if ($search != 'all') {
            $query = $query->where('product_name', 'LIKE', "%{$search}%");
        }
        /*
          THIS IS SORTING TYPE VALUE NO
          1.NEW TO OLD
          2.OLD TO NEW
          3.POULARITY
          4.HIGH PRICE TO LOW PRICE
          5.LOW PRICE TO HIGH PRICE
         */

          if ($current_sort->value == 1) {
            $query = $query->orderBy('created_at', 'DESC');
        } else if ($current_sort->value == 2) {
            $query = $query->orderBy('created_at', 'ASC');
        } else if ($current_sort->value == 3) {
            $query = $query->orderBy('popularity', 'DESC');
        } else if ($current_sort->value == 4) {
            $query = $query->orderBy('price', 'DESC');
        } else if ($current_sort->value == 5) {
            $query = $query->orderBy('price', 'ASC');
        } else if ($current_sort->value == 6) {
            $query = $query->where('on_sale', "on");
            $query = $query->orderBy('end_date', 'ASC');
        }

        $finel_result = $query->paginate(20);
        ;



        // if($category=='all'){
        //       $product=ProductCreation::with(['getImages'])
        //       ->whereBetween('price', [$min_price, $max_price])
        //       ->where('product_name','LIKE',"%{$search}%")
        //       ->orderBy($current_sort->order_by,$current_sort->asc_or_desc)
        //       ->paginate(20);
        // }else{
        //      $product_ids=ProductHasCategory::where('category_id',$category)
        //      ->get(['product_id']);
        //     $product=ProductCreation::with(['getImages'])
        //      ->whereIn('id',$product_ids)
        //      ->whereBetween('price', [$min_price, $max_price])
        //       ->where('product_name','LIKE',"%{$search}%")
        //      ->orderBy($current_sort->order_by,$current_sort->asc_or_desc)
        //      ->paginate(20);
        // }

        return view('front.productCatalog')
        ->with([
            'category' => $category,
            'categories' => $categories,
            'product' => $finel_result,
            'search' => $search_passed,
            'sort' => $sort,
            'mprice' => $mprice,
            'hprice' => $hprice,
            'sortType' => $sortType,
            'category_list' => $category_list
        ]);
    }

    public function productDetails($id) {
        $this->cart_loading_from_db();
        $current_product = ProductCreation::with(['getImages','reviews'])->find($id);
        // dd($current_product);
        // return Cart::get('P-'.$current_product->id);
        $categories = ProductHasCategory::with(['getProductCategory'])->where('product_id', $id)->get();
        $related_product = ProductHasCategory::with(['getProduct.getImages'])
        ->whereIN('category_id', $current_product->categories)
        ->whereNotIN('product_id', [$id])
        ->groupby('product_id')
        ->distinct()
        ->get();

        return view('front.productDetails')
        ->with([
            'current_product' => $current_product,
            'related_product' => $related_product,
            'categories' => $categories
        ]);
    }

    public function productDetailSubmit($id) {

        $created_by = 0;
        if ($user = Sentinel::check()) {
            $created_by = $user->id;
        }

        $rating = Rating::Create([
            'product_id' => $id,
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'contact_no' => Input::get('contact_no'),
            'review' => Input::get('review'),
            'star_count' => Input::get('rate') * 2,
            'created_by' => $created_by,
        ]);
        if ($rating) {
            $totalrate = Rating::where('product_id', $id)->sum('star_count');
            $totalcount = Rating::where('product_id', $id)->count();
            $rate = 0;
            if ($totalcount > 0) {
                $rate = $totalrate / $totalcount;
            }


            $productcreationupdate = ProductCreation::where('id', $id)->update(['popularity' => $rate]);
            if ($productcreationupdate) {
                $notification = array(
                    'message' => 'Review submitted successfully',
                    'alert-type' => 'success'
                );
                return redirect('product-detail/' . $id)->with($notification);
            }
        }
    }

    public function membershipList() {
        return view('front.membershipList');
    }

    public function onlineTutorials() {
        $sql = DB::table('recipe')
        ->orderBy('recipe_id', 'desc')
        ->where('status', '=', '1')
        ->paginate(9);

        $type = RecipesCategory::where('status', '=', '1')
        ->get();

        $coutn = DB::table('recipe')
        ->select(DB::raw('count(*) as premium_count'))
        ->where('status', '=', '1')
        ->get();
        //  var_dump($type);die;

        return view('front.onlineTutorials', ['data' => $sql, 'count ' => $coutn, 'type' => $type]);
    }

    public function tasting() {
        $tastingPeopleType = TastingPeopleType::get();
        $config = \ConfigManage\Models\Config::find(1);
        return view('front.tasting')->with(['tastingPeopleType' => $tastingPeopleType, 'config' => $config, 'modal' => '0']);
    }

    public function tasting_submit(Request $request) {
        //dd($request->all());
        $this->validate($request, [
            'datetime' => 'required',
            'venu' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'servings' => 'required|numeric|min:1',
            'email' => 'required|email',
            'g-recaptcha-response' => 'required',
        ]);

        $config = \ConfigManage\Models\Config::find(1);
        $tastingPeopleType = TastingPeopleType::get();

        $days = \ConfigManage\Models\Config::find(1)->qoute_days ? \ConfigManage\Models\Config::find(1)->tasting_days : 1;

        $allowedDate = \Carbon\Carbon::now()->addDays($days);
        $date = \Carbon\Carbon::parse($request->datetime);

        if ($date->lt($allowedDate)) {
            return back()->withErrors(['date' => 'Date not available']);
        }

        $people_count = 1;
        $datetime = $request->get('datetime');
        $province = $request->get('province');
        $postcode = $request->get('postcode');
        $details = $request->get('details');
        $venu = $request->get('venu');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $phone_no = $request->get('phone_no');
        $email = $request->get('email');
        $ip = $request->ip();


        $tasting_success = Tasting::Create([
            'people_type_id' => $people_count,
            'event_date' => $datetime,
            'details' => $details,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'province' => $province,
            'post_code' => $postcode,
            'venue' => $venu,
            'phone' => $phone_no,
            'servings' => $request->servings,
            'email' => $email,
            'ip_address' => $ip
        ]);
        if ($tasting_success) {
            return view('front.tasting')->with([
                'tastingPeopleType' => $tastingPeopleType,
                'config' => $config,
                'modal' => '1']);
        }
    }

    public function categorizedImage($id) {
        if ($id == 0) {
            $img = Gallery::where('status', '=', 1)
            ->get();
            return $img;
        } else {
            $img = Gallery::where('status', '=', 1)
            ->where('categories', '=', $id)
            ->get();
            return $img;
        }
    }

    // public function album($id){
    //     $img=DB::table('sa_gallery_image')
    //         ->select('sa_gallery_image.filename','album_name','sa_gallery_image.path','description')
    //         ->leftJoin('sa_gallery','sa_gallery_image.gallery_id','=','sa_gallery.id')
    //         ->where('sa_gallery.id','=',$id)
    //         ->get();
    //     $album='';
    //     foreach ($img as $i){
    //         $album=$i->album_name;
    //         $desc=$i->description;
    //     }
    //     return view('front.singleAlbum')
    //         ->with([
    //             'img'=>$img,
    //             'album'=>$album,
    //             'dec' => $desc
    //         ]);
    // }

    public function album($id) {
        $gallery = Gallery::with(['getImages'])->find($id);


        return view('front.singleAlbum')
        ->with([
            'gallery' => $gallery
        ]);
    }

    public function submitQuote(ReCaptchataTestFormRequest $request) {
        $this->validate($request, [
            'caketype' => 'required_if:inqType,2',
            'date' => 'required_if:inqType,2',
            // 'province' => 'required_if:inqType,2',
            // 'postcode' => 'required_if:inqType,2',
            'fn' => 'required',
            'ln' => 'required',
            'email' => 'required|email',
            'inqType' => 'required|in:1,2'
        ]);

        if ($request->inqType == 2) {
            $days = \ConfigManage\Models\Config::find(1)->qoute_days ? \ConfigManage\Models\Config::find(1)->qoute_days : 1;

            $allowedDate = \Carbon\Carbon::now()->addDays($days);
            $date = \Carbon\Carbon::parse($request->date);
            // 				dd($date->lt($allowedDate));
            // dd([$allowedDate , $date ]);
            if ($date->lt($allowedDate)) {
                return back()->withErrors(['date' => 'Date not available']);
            }

            $ip = $request->ip();
            $data = \Location::get($ip);


            $quote = new Quote();
            $quote->cake_type = $request->caketype;
            $quote->event_date = $request->date;
            $quote->completed_date = NULL;
            // $quote->province = $request->province;
            // $quote->post_code = $request->postcode;
            $quote->province = 'N/A';
            $quote->post_code = 'N/A';
            $quote->venue = $request->venue;
            $quote->details = $request->msg;
            $quote->first_name = $request->fn;
            $quote->last_name = $request->ln;
            $quote->phone = $request->phone;
            $quote->email = $request->email;
            $quote->ip = $request->ip();
            $quote->ip_location = $data->countryCode;
            $quote->save();

            try {
                $quote->sendAdminQuotationInquiryEmail();
            } catch (\Throwable $th) {
                // throw $th;
            }
            $msg = "Quoted Successfully!";
        } else {

            $config = \ConfigManage\Models\Config::find(1);

            $inquiry = [
                'first_name' => $request->fn,
                'last_name' => $request->ln,
                'phone' => $request->phone,
                'email' => $request->email,
                'msg' => $request->msg,
            ];

            Mail::send('front.mail.contact-email', ['inquiry' => $inquiry], function ($msg) use ($config) {
                $msg->to($config->email)->subject('Inquiry');
            });


            $msg = "Inquiry Successfully";
        }


        return redirect('quotes')->with(['success' => true,
            'success.message' => $msg,
            'success.title' => 'Well Done!']);
    }

    public function regClz(Request $request) {

        $reg = new PrivateClassRegistration();
        if ($request->parent == true) {
            $reg->parent_is_reg = 1;
            $reg->parent_full_name = $request->parentName;
        } else {
            $reg->parent_is_reg = 0;
            $reg->parent_full_name = "";
        }
        $reg->class_datetime_id = $request->datetime_id;
        $reg->first_name = $request->fn;
        $reg->last_name = $request->ln;
        $reg->phone = $request->phone;
        $reg->email = $request->email;
        $reg->class = $request->class_id;
        //$reg->status =  1; set default by db, 1=pending 2=approved
        $reg->save();
        try {
            $reg->sendAdminRegEmail();
        } catch (\Throwable $th) {
            //throw $th;
        }

        return redirect('/')->with(['success' => true,
            'success.message' => 'Class registation Successfully!',
            'success.title' => 'Well Done!']);
    }
/*
      public function cartADD(){
       $product_id= Input::get('product_id');
       $product_type= Input::get('product_type');
       $qty= Input::get('qty');
       $product_array=array();
       if ($product_type==1) {

            $selected_product=ProductCreation::find($product_id);
            if ($selected_product->on_sale=='on' & date('Y-m-d')<=$selected_product->end_date & date('Y-m-d')>=$selected_product->start_date) {
                    $product_array=array(
                                    'product_id'=>$selected_product->id,
                                    'product_name'=>$selected_product->product_name,
                                    'price'=>$selected_product->sale_price,
                                    'qty'=>$qty,
                                    'type'=>1,
                                    'cart_id'=>'P-'.$selected_product->id

                                );
*/

                                public function cartADD(Request $request) {

                                   $rules = [
                                       'product_id' => 'required',
                                       'product_type' => 'required|in:1,2',
                                       'qty' => 'required|numeric|min:1'
                                   ]   ;

                                   if(Input::get('product_type') == 1){
                                    $rules['product_id'] = 'required|exists:sa_productcreation,id';
                                }else{
                                    $rules['product_id'] = 'required|exists:recipe,recipe_id';
                                }
                                $validator = Validator::make($request->all(), $rules);

                                if($validator->fails()){
                                    return response()->json([
                                        'errors' => $validator->errors()
                                    ], 422);
                                }


                                $product_id = Input::get('product_id');
                                $product_type = Input::get('product_type');
                                $qty = Input::get('qty');
                                $product_array = array();

                                if ($product_type == 1) {
                                    /* THIS IS DEFAULT PRODUCT */
                                    $selected_product = ProductCreation::find($product_id);
                                    if($qty > $selected_product->qtyonhand){
                                        return response()->json([
                                            'errors' => [
                                               'qty' => 'Purchase qty is limited to '.$selected_product->qtyonhand.'.'
                                           ]
                                       ], 422);
                                    }
                                    if ($selected_product->checkInSale()) {
                                        $product_array = array(
                                            'product_id' => $selected_product->id,
                                            'product_name' => $selected_product->product_name,
                                            'price' => $selected_product->sale_price,
                                            'qty' => $qty,
                                            'type' => 1,
                                            'cart_id' => 'P-' . $selected_product->id
                                        );

                                    } else {
                                        $product_array = array(
                                            'product_id' => $selected_product->id,
                                            'product_name' => $selected_product->product_name,
                                            'price' => $selected_product->price,
                                            'qty' => $qty,
                                            'type' => 1,
                                            'cart_id' => 'P-' . $selected_product->id
                                        );
                                    }
                                } else if ($product_type == 2) {
                                    /* THIS IS TOUTORIALS */
                                    $selected_product = Recipes::find($product_id);
                                    $product_array = array(
                                        'product_id' => $selected_product->recipe_id,
                                        'product_name' => $selected_product->name,
                                        'price' => $selected_product->price,
                                        'qty' => $qty,
                                        'type' => 2,
                                        'cart_id' => 'T-' . $selected_product->recipe_id
                                    );
                                }

                                if (!empty($product_array)) {
                                    if ($user = Sentinel::check()) {
                                        $exsistProduct = ShoppingCart::where('product_id', $product_array['product_id'])
                                        ->where('type', $product_array['type'])
                                        ->where('created_by', $user->id)
                                        ->get();
                                        if (count($exsistProduct) > 0) {
                                            $newQTY = ($exsistProduct[0]->qty) + $product_array['qty'];
                                            $result = ShoppingCart::where('product_id', $product_array['product_id'])
                                            ->where('type', $product_array['type'])
                                            ->where('created_by', $user->id)
                                            ->update([
                                                'qty' => $newQTY,
                                                'price' => $product_array['price'],
                                            ]);
                                            return $result;
                                        } else {
                                            ShoppingCart::Create([
                                                'product_id' => $product_array['product_id'],
                                                'product_name' => $product_array['product_name'],
                                                'price' => $product_array['price'],
                                                'qty' => $product_array['qty'],
                                                'type' => $product_array['type'],
                                                'created_by' => $user->id,
                                            ]);
                                            return 1;
                                        }
                                    } else {

                                        if (!empty(Cart::get($product_array['cart_id']))) {
                                            Cart::update($product_array['cart_id'], array(
                                                'quantity' => $product_array['qty']
                                            ));
                                            return 1;
                                        } else {

                                            Cart::add(array(
                                                'id' => $product_array['cart_id'],
                                                'name' => $product_array['product_name'],
                                                'price' => $product_array['price'],
                                                'quantity' => $product_array['qty'],
                                                'attributes' => array(
                                                    'cart_status' => 'OFFLINE'
                                                )
                                            ));
                                            return 1;
                                        }
                                    }

                                }else{
                                    return 0;
                                }

                            }

    // public function cartREMOVE($id,$type){
    //     $cart_id=0;
    //     if ($type==1) {
    //         $cart_id='P-'.$id;
    //     }else if ($type==2) {
    //         $cart_id='T-'.$id;
    //     } else {
    //         return 0;
    //     }
    // }

                            public function cartREMOVE($id, $type) {
                                $cart_id = 0;
                                if ($type == 1) {
                                    $cart_id = 'P-' . $id;
                                } else if ($type == 2) {
                                    $cart_id = 'T-' . $id;
                                }

                                Cart::remove($cart_id);
                                if ($user = Sentinel::check()) {
                                    $result = ShoppingCart::where('product_id', $id)
                                    ->where('type', $type)
                                    ->where('created_by', $user->id)
                                    ->first();
                                    $result->delete();
                                }
                                return 1;
                            }

                            public function cartUpdate($id, $qty, $type) {
                                if ($qty <= 0) {
                                    $this->cartREMOVE($id, $type);
                                } else {
                                    $cart_id = 0;
                                    if ($type == 1) {
                                        $cart_id = 'P-' . $id;
                                    } else if ($type == 2) {
                                        $cart_id = 'T-' . $id;
                                    }
                                    Cart::update($cart_id, array(
                                        'quantity' => array(
                                            'relative' => false,
                                            'value' => $qty
                                        ))
                                );
                                    if ($user = Sentinel::check()) {
                                        $result = ShoppingCart::where('product_id', $id)
                                        ->where('created_by', $user->id)
                                        ->where('type', $type)
                                        ->update(['qty' => $qty]);
                                    }
                                }

                                return redirect('my-cart');
                            }

                            public function profileView() {
                                if (Sentinel::check()) {
                                    // $user = Sentinel::getUser();
                                    // $user = $user->load('getPurchases');
                                    $user = User::with('getPurchases.getItems.getProduct')->where('id', Sentinel::getUser()->id)->first();
                                    $details = BillingDetail::whereis_primary(1)->wherecreated_by($user->id)->first();
                                    $countries = Country::get();
                                    $image_url_array = [];
                                    $image_config_array = [];

                                    if ($details != null) {
                                        if ($details->profile_picture != null) {
                                            array_push($image_url_array, "<img style='height:160px' src='" . url('') . '/core/storage/' . $user->avatar_path . '/' . $user->avatar . "'>");
                                            array_push($image_config_array, array('key' => $user->id, 'url' => url('')));
                                        }
                                    }
                                    return view('layouts.front.profile', compact('user', 'details', 'countries', 'image_url_array', 'image_config_array'));
                                }
                                return back();
                            }

                            public function updateProfile(\Illuminate\Http\Request $request) {
                                if (Sentinel::check()) {
                                    $user = Sentinel::getUser();


                                    $validator = Validator::make($request->all(), [
                                        'first_name' => 'required|max:255',
                                        'last_name' => 'required|max:255',
                                        'email' => 'email|unique:users,email,' . $user->id,
                                        'street_addresss_1' => 'required',
                                        'street_addresss_2' => 'required',
                                        'city' => 'required',
                                        'province' => 'required',
                                        'postcode' => 'required',
                                        'country_id' => 'required|exists:countries,id',
                                    ]);

                                    if ($validator->fails()) {
                                      return redirect()->back()->withErrors($validator)->withInput();
                                  }

                                  $user->update([
                                    'first_name' => $request->first_name,
                                    'last_name' => $request->last_name,
                                    'email' => $request->email,
                                ]);
                                  $path = 'uploads/images/user';
                                  if (!file_exists(storage_path($path))) {
                                    File::makeDirectory(storage_path($path));
                                }
                                $project_fileName = '';
                                if (Input::hasFile('profImage')) {
                                    $file = Input::file('profImage');
                                    $extn = $file->getClientOriginalExtension();
                                    $destinationPath = storage_path($path);
                                    $project_fileName = 'user-' . date('YmdHis') . '-.' . $extn;
                                    $file->move($destinationPath, $project_fileName);
                                    $user->update([
                                        'avatar_path' => $path,
                                        'avatar' => $project_fileName
                                    ]);
                                }

                                $details = BillingDetail::whereis_primary(1)->wherecreated_by($user->id)->first();
                                $details->update([
                                    'street_addresss_1' => $request->street_addresss_1,
                                    'street_addresss_2' => $request->street_addresss_2,
                                    'city' => $request->city,
                                    'province' => $request->province,
                                    'postcode' => $request->postcode,
                                    'country_id' => $request->country_id
                                ]);

                                return back()->with(['success' => 'true', 'success.message' => 'User Profile Updated Successfully', 'success.title' => 'User Profile']);
                            }

                            return back();
                        }

                        public function password(\Illuminate\Http\Request $request) {
                            if (Sentinel::check()) {
                                $user = Sentinel::getUser();
                                $validator = Validator::make($request->all(), ['old_password' => 'required', 'password' => 'required|min:6|confirmed']);
                                if ($validator->fails()) {
                                  return redirect()->back()->withErrors($validator)->withInput();
                              }

            // dd($this->validate($request, ['old_password' => 'required', 'password' => 'required|min:6|confirmed']));

                              if (Hash::check($request->password, $user->password)) {
                                return back()->withErrors(['old_password' => 'Old password did not match']);
                            }

                            $user->update(['password' => bcrypt($request->password)]);

                            return back()->with(['success' => 'true', 'success.message' => 'Password Updated Successfully', 'success.title' => 'Password']);
                        }
                        return back();
                    }

                    public function tastConfirm($id) {
                        $tasting = Tasting::with('getPeopleType')->findOrFail($id);

                        if ($tasting->status != -1) {
                            return view('front.tasting-confirm', compact('tasting'));
                        }

                        return redirect('/');
                    }

                    public function tastPayPal($id) {
                        $tasting = Tasting::with('getPeopleType')->findOrFail($id);

                        $provider = PayPal::setProvider('express_checkout');

                        $invoice_id = date('ymdHis') . '-' . $tasting->id;
                        $shoppingCartArray = [
                            'items' => [
                                ['name' => 'Tasting Appointment',
                                'price' => $tasting->getPeopleType->price,
                                'qty' => 1]
                            ],
                            'invoice_id' => $invoice_id,
                            'invoice_description' => 'Tasting Appointment # ' . $invoice_id,
                            'return_url' => url('/paypal/payment/tasting'),
                            'cancel_url' => url('tasting-confirm/' . $tasting->id),
                            'total' => $tasting->getPeopleType->price
                        ];
                        $response = $provider->setExpressCheckout($shoppingCartArray, true);
                        if(isset($response['paypal_link'])){
                            return redirect($response['paypal_link']);
                        }
                        $notification = array(
                            'message' => "Sorry, Couldn't connect to PayPal. Please contact administrator.",
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }

                    public function tastingPaymentSuccess(Request $request) {
                        $provider = PayPal::setProvider('express_checkout');

                        $response = $provider->getExpressCheckoutDetails($request->token);

        // dd();
                        $id = explode('-', $response['INVNUM'])[1];


        $tasting = Tasting::with('getPeopleType')->findOrFail($id);
        $tasting->status = 3;
        $tasting->save();
        try {
            $tasting->sendRecieptEmail();
            $tasting->sendAdminRecieptEmail();
        } catch (\Exception  $e) {
            //throw $th;
                        }
                        return redirect('tasting-confirm/' . $tasting->id);
                    }

                    public function cancelTasting($id) {
                        $tasting = Tasting::findOrFail($id);
                        $tasting->status = -1;
                        $tasting->save();

                        return redirect('/');
                        ;
                    }

                    public function reschedule($id) {
        // $tasting =  Tasting::findOrFail($id);
        // $tasting->status = 0;
        // $tasting->save();

                        return redirect('/');
                    }

                    public function addComment(Request $request) {
                        $this->validate($request, [
                            'name' => 'required|max:255',
                            'email' => 'required|max:255|email',
                            'content' => 'required',
                            'blog_id' => 'required|exists:sa_blog,id'
                        ], [
                            'name.required' => 'Name is required',
                            'name.max' => 'Name has excessed the character limit',
                            'email.required' => 'Email is required',
                            'email.max' => 'Email has excessed the characters limit',
                            'content.required' => 'Comment can not be empty',
                            'blog_id.required' => 'Invalid data',
                            'blog_id.exists' => 'Invalid data',
                        ]);

                        Comment::create($request->all());

                        return response(200);
                    }

    public function billingDetail() {//var_dump('helo');die;
    $country = Country::orderBy('name', 'asc')->get();
    $user = Sentinel::getUser();
    $user = $user->load('getBillingDetails');
    return view('front.billingDetails')->with([
        'country' => $country,
        'user' => $user
    ]);
    ;
}

public function billingDetail2(Request $request) {
        //var_dump('helo');die;
    $country = $request->input('country');
    $city = Input::get('city');
    $province = Input::get('province');
    $postcode = Input::get('postcode');
    $address1 = Input::get('address1');
    $address2 = Input::get('address2');
    $first_name = Input::get('first_name');
    $last_name = Input::get('last_name');
    $phone = Input::get('phone');
    $email = Input::get('email');

    $user = Sentinel::getUser();

    $billing_detail = BillingDetail::Create([
        'country_id' => $country,
        'city' => $city,
        'province' => $province,
        'postcode' => $postcode,
        'street_addresss_1' => $address1,
        'street_addresss_2' => $address2,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone' => $phone,
        'email' => $email,
        'is_primary' => 1,
        'created_by' => $user->id
    ]);
    if ($billing_detail) {
        return redirect('/')->with(['success' => true,
            'success.message' => 'Billing information has been updated',
            'success.title' => 'Good Job!']);
    }
}

public function beginner(Request $request) {
    $data = "hello";
    return $data;
}

public function newsletterSignUp(Request $request) {

    $email = Subscriber::create([
        'email' => $request->email
    ]);
    $user = $email;

        // check the email sending function
        //Mail::send('emails.newsletter-sign-up-email', [], function($message) use ($user) {
        //            $message->to($user, '')->subject('Subscription to Newsletter');
        //        });

    $notification = array(
        'message' => 'You have been successfully added to our mailing list',
        'alert-type' => 'success'
    );
    return back()->with($notification);
    return redirect('/faq')->with($notification);
}


    // public function newsletterSignUp(Request $request) {

    //     $email = Subscriber::create([
    //                 'email' => $request->email
    //     ]);
    //     $user = $email;

    //     check the email sending function
    //     Mail::send('emails.newsletter-sign-up-email', [], function($message) use ($user) {
    //                 $message->to($user, '')->subject('Subscription to Newsletter');
    //             });

    //     $notification = array(
    //         'message' => 'You have been successfully added to our mailing list',
    //         'alert-type' => 'success'
    //     );
    //     return back()->with($notification);
    //     return redirect('/faq')->with($notification);
    // }



public function checkProQty(Request $request)
{
    $rules = [
       'product_id' => 'required|exists:sa_productcreation,id',
       'qty' => 'required|numeric|min:1'
   ]   ;

   $validator = Validator::make($request->all(), $rules);

   if($validator->fails()){
    return response()->json([
        'errors' => $validator->errors()
    ], 422);
}

$selected_product = ProductCreation::find($request->product_id);
if($request->qty > $selected_product->qtyonhand){
    return response()->json([
        'errors' => [
            'qty' => 'Purchase qty is limited to '.$selected_product->qtyonhand.'.'
        ]
    ], 422);
}

}

}
