<?php namespace App\Http\Controllers;

use Input;
use Sentinel;
use Session;
use Socialite;
use DB;
use Cart;
use File;
use Mail;
use Validator;

use UserManage\Models\User;
use UserRoles\Models\UserRole;
use ProductCreationManage\Models\ProductCreation;
use App\Models\ShoppingCart;
use App\Models\TermsOfService;
use RecipesManager\Models\Recipes;
use Illuminate\Support\Str;

class AuthController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Welcome Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}
	 /**
     * Online user Registration View
     *
     * @return Redirect to online user add view
     */
    public function userRegisterView() {
    	$ts = TermsOfService::find(1);
        return view('layouts.front.register')->with([
        	'ts' => $ts
        ]);
    }
    /**
	 * Online user Registration data to database
	 *
	 * @return Redirect to online user add
	 */
	public function userRegister() {
		$validator = Validator::make(request()->all(), [
			// 'g-recaptcha-response' => 'required|recaptcha',
		], [
			"g-recaptcha-response.required" => "Please complete the Recaptcha",
			"g-recaptcha-response.recaptcha" => "Error checking recaptcha"
		]);
		if($validator->fails()){
			return redirect('user/register')->with(['error' => true,
							'error.message' => $validator->errors()->first('g-recaptcha-response'),
							'error.title'  => 'Recaptcha Error']);
		}
		$credentials = [
		    'login' => Input::get('email'),
		];

		 $user_exsist = Sentinel::findByCredentials($credentials);
		 if (!$user_exsist) {
		 	try {

				$path='uploads/images/user';
				if (!file_exists(storage_path($path))) {
					 File::makeDirectory(storage_path($path));
				}
				$project_fileName = '';
				if (Input::hasFile('profImage')) {
											$file = Input::file('profImage');
											$extn =$file->getClientOriginalExtension();
											$destinationPath = storage_path($path);
											$project_fileName = 'user-' .date('YmdHis') .'-.' . $extn;
											$file->move($destinationPath, $project_fileName);

					}
				DB::transaction(function () use(&$path, &$project_fileName){

                                    
				$user = Sentinel::registerAndActivate([
					'email' => Input::get('email'),
					'username' => Input::get('email'),
					'password' => Input::get('password'),
					'first_name' => Input::get('fname'),
					'last_name' => Input::get('lname'),
                                        'varifyToken' => Str::random(40),
					'avatar_path' => $path,
					'avatar' => $project_fileName,
                                       
				]);

				if (!$user) {
					throw new TransactionException('', 100);
				}

				$user->makeRoot();

				$role = Sentinel::findRoleById(1);
				$role->users()->attach($user);

				User::rebuild();
                                
				Sentinel::login($user);
				if ($user) {
                                   
					 $cartCollection = Cart::getContent();
					 $cartArray=$cartCollection->toArray();
					 foreach ($cartArray as $key => $value) {
                                             
					 	if ($value['attributes']['cart_status']=='OFFLINE') {
					 		$cart_id=explode('-', $value['id']);
					        $type=$cart_id[0];
					        $id=$cart_id[1];
					        $product_array=array();
					         if ($type=='P') {
					             /*THIS IS DEFAULT PRODUCT */
					            $selected_product=ProductCreation::with(['getImages'])->find($id);
					            $product_array=array(
	                                            'product_id'=>$selected_product->id,
	                                            'product_name'=>$selected_product->product_name,
	                                            'price'=>$selected_product->price,
	                                            'qty'=>$value['quantity'],
	                                            'type'=>1,
	                                            'cart_id'=>'P-'.$selected_product->id

	                                        );



					        }else if ($type=='T') {
					              /*THIS IS TOUTORIALS*/
					            $selected_product=Recipes::with(['getImages'])->find($id);
					            $product_array=array(
	                                            'product_id'=>$selected_product->recipe_id,
	                                            'product_name'=>$selected_product->name,
	                                            'price'=>$selected_product->price,
	                                            'qty'=>$value['quantity'],
	                                            'type'=>2,
	                                            'cart_id'=>'T-'.$selected_product->recipe_id

	                                        );

					        }
					         if (!empty($product_array)) {
					         	$exsistProduct=ShoppingCart::where('product_id',$product_array['product_id'])
	                                            ->where('type',$product_array['type'])
	                                            ->where('created_by',$user->id)
	                                            ->get();
				                if(count($exsistProduct)>0){
				                    $newQTY=($exsistProduct[0]->qty)+$product_array['qty'];
				                   $result=ShoppingCart::where('product_id', $product_array['product_id'])
				                                        ->where('type',$product_array['type'])
				                                        ->where('created_by',$user->id)
				                                        ->update(['qty' => $newQTY]);

				                }else{
				                    ShoppingCart::Create([
				                    'product_id'=>$product_array['product_id'],
				                    'product_name'=>$product_array['product_name'],
				                    'price'=>$product_array['price'],
				                    'qty'=>$product_array['qty'],
				                    'type'=>$product_array['type'],
				                    'created_by'=>$user->id,
				                    ]);

				                }
					         }


					 	}
					 }
                                        
					# code...
				}
                                
			});

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
                      //  die("sssssaaaa");
                        $user = User::where('email',Input::get('email'))->first();
                       // die($user);
                       $this->sendEmail($user);
					   //return $user
					   Sentinel::logout();
					   Cart::clear();

                      return redirect('/')->with(['success' => true,
                        'success.message' => 'Please check your email account for the confirmation email.',
                        'success.title' => 'Successfully Registered']);;
			//return redirect('billing-detail');
		}else{
			return redirect('user/register')->with(['error' => true,
							'error.message' => "Sorry, Given email is already registered.",
							'error.title' => 'Ops!']);
		}



		// return redirect('/')->with(['success' => true,
		// 		'success.message' => 'Registered successfully!',
		// 		'success.title' => 'Registered!']);

	}

/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function loginView() {
		try {
			if (!Sentinel::check()) {
				return view('layouts.front.login');
			} else {
				$redirect = Session::get('loginRedirect', '');
				Session::forget('loginRedirect');
				return redirect($redirect);
			}
		} catch (\Exception $e) {
			return view('layouts.front.register')->withErrors(['login' => $e->getMessage()]);
		}
	}


	public function login() {


            $username = Input::get('user_name1');
            $regStaus = DB::table('users')->where(['regStatus'=>'1','username'=>$username])->first();
            if($regStaus){
		$credentials = array(
			'username' => Input::get('user_name1'),
			'password' => Input::get('password1'),
                        
		);
            }
            else{
                $credentials = array(
			'username' => '',
			'password' => '',
                        
		);
            }
                
               // Sentinel::authenticate($credentials);
              // die(Sentinel::authenticate($credentials));

		if (Input::get('login-remember')) {
			$remember = true;
		} else {
			$remember = false;
		}


		try {

			$user = Sentinel::authenticate($credentials, $remember);
			if ($user) {
				 $cartCollection = Cart::getContent();
				 $cartArray=$cartCollection->toArray();
				 foreach ($cartArray as $key => $value) {
				 	if ($value['attributes']['cart_status']=='OFFLINE') {
				 		$cart_id=explode('-', $value['id']);
				        $type=$cart_id[0];
				        $id=$cart_id[1];
				        $product_array=array();
				         if ($type=='P') {
				             /*THIS IS DEFAULT PRODUCT */
				            $selected_product=ProductCreation::with(['getImages'])->find($id);
				            if ($selected_product->on_sale=='on' & date('Y-m-d')<=$selected_product->end_date & date('Y-m-d')>=$selected_product->start_date) {
				            	$product_array=array(
                                            'product_id'=>$selected_product->id,
                                            'product_name'=>$selected_product->product_name,
                                            'price'=>$selected_product->sale_price,
                                            'qty'=>$value['quantity'],
                                            'type'=>1,
                                            'cart_id'=>'P-'.$selected_product->id

                                        );
				            } else {
				            	$product_array=array(
                                            'product_id'=>$selected_product->id,
                                            'product_name'=>$selected_product->product_name,
                                            'price'=>$selected_product->price,
                                            'qty'=>$value['quantity'],
                                            'type'=>1,
                                            'cart_id'=>'P-'.$selected_product->id

                                        );
				            }





				        }else if ($type=='T') {
				              /*THIS IS TOUTORIALS*/
				            $selected_product=Recipes::with(['getImages'])->find($id);
				            $product_array=array(
                                            'product_id'=>$selected_product->recipe_id,
                                            'product_name'=>$selected_product->name,
                                            'price'=>$selected_product->price,
                                            'qty'=>$value['quantity'],
                                            'type'=>2,
                                            'cart_id'=>'T-'.$selected_product->recipe_id

                                        );

				        }
				         if (!empty($product_array)) {
				         	$exsistProduct=ShoppingCart::where('product_id',$product_array['product_id'])
                                            ->where('type',$product_array['type'])
                                            ->where('created_by',$user->id)
                                            ->get();
			                if(count($exsistProduct)>0){
			                    $newQTY=($exsistProduct[0]->qty)+$product_array['qty'];
			                   $result=ShoppingCart::where('product_id', $product_array['product_id'])
			                                        ->where('type',$product_array['type'])
			                                        ->where('created_by',$user->id)
			                                        ->update(['qty' => $newQTY]);

			                }else{
			                    ShoppingCart::Create([
			                    'product_id'=>$product_array['product_id'],
			                    'product_name'=>$product_array['product_name'],
			                    'price'=>$product_array['price'],
			                    'qty'=>$product_array['qty'],
			                    'type'=>$product_array['type'],
			                    'created_by'=>$user->id,
			                    ]);

			                }
				         }


				 	}
				 }

				 // Cart::clear();

	           if($user->hasAnyAccess(['front'])){
					$redirect = Session::get('loginRedirect', '/');
				}else{
					$redirect = Session::get('loginRedirect', 'admin');
				}
				Session::forget('loginRedirect');

				return redirect($redirect);

			} else {
				return redirect('user/login')->with([ 'error' => true,
				'error.message'=> 'Incorrect Username OR Password !',
				'error.title' => 'Try Again!']);
			}
		} catch (\Exception $e) {
			return $msg = $e->getMessage();
		}


	}
	/*
		*	@method logout()
		*	@description Logging out the logged in user
		*	@return URL redirection
	*/
	public function logout() {
		Sentinel::logout();
		Cart::clear();
		return redirect()->to('/');

	}


     /**
     * Redirect the user to the FACEBOOK authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
 	/**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from FACEBOOK.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {

        $user = Socialite::driver('facebook')->user();
        // print_r($user);
        // return 'k';
        $user_details=$user;
        if(User::where('email',$user_details->email)->exists()){
        	$fbloged_user=User::where('email',$user_details->email)->get();
        	$fbloged_user=$fbloged_user[0];
        	$user_login = Sentinel::findById($fbloged_user->id);
			Sentinel::login($user_login);
        	return redirect('/');
        }else{
        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([
						'email' =>$user_details->email,
						'username' => $user_details->email,
						'password' => '123456',
						'first_name' => $user_details->name,
						'fb_id' =>$user_details->id
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;


				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }






        // $user->token;
    }/**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleGoogleCallback()
    {

	    $user = Socialite::driver('google')->scopes(['profile','email'])->user();
	     $user_details=$user;
        if(User::where('email',$user_details->email)->exists()){
        	$googleloged_user=User::where('email',$user_details->email)->get();
        	$googleloged_user=$googleloged_user[0];
        	$user_login = Sentinel::findById($googleloged_user->id);
			Sentinel::login($user_login);
        	return redirect('/');
        }else{
        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([
						'email' =>$user_details->email,
						'username' => $user_details->email,
						'password' => '123456',
						'first_name' => $user_details->name,
						'g_id' =>$user_details->id
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;


				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }

    }
    
    public function sendEmail($thisUser){
        
        //die($thisUser->email);
  
        $token = $thisUser->varifyToken;
        $email = $thisUser->email;
        $url = route('user.varifiedEmail',['email'=>$email,'token'=>$token]);
    
         Mail::send('emails.varifyEmail',['url' =>$url, 'user' => $thisUser], function($message) use ($thisUser) {
         $message->to($thisUser->email, '')->subject('Email Confirmation for Sweet Delights Cakery Account');
      });
//        
    }
    
    public function varifiedEmail($email,$token){
		 $users = User::where('email', $email)
		 ->where('varifyToken', $token)
		 ->first();
         if($users){
             $users->regStatus = 1;
             $users->status = 1;
             $users->varifyToken = NULL;
			 $users->save();

			$user = Sentinel::findById($users->id);

			if(Sentinel::login($user)){
				return redirect('billing-detail');
			}
			return redirect('user/login');
			
         }
         else{
             return redirect('/');
         }
    }
    
    
    
}
