<?php

namespace App\Http\Controllers;

/**
 * Description of OnlineTutorialsController
 *
 * @author chathuranga
 */
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use RecipesManager\Models\Recipes;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Sentinel;
use App\Models\ShoppingCart;
use ProductCreationManage\Models\ProductCreation;
use Input;
use Session;
use Mail;
use Cart;
use PayPal;

class OnlineTutorialsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function filterMembership(Request $request) {
        $getData = Recipes::where('free_paid', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('recipe_id', 'desc')
            ->paginate(1);


        // var_dump($free_value);die;

        return $getData;
    }

    public function freeCount(Request $request) {
        $count = DB::table('recipe')
            ->select(DB::raw('count(*) as free_count'))
            ->where('free_paid', '=', 0)
            ->where('status', '=', 1)
            ->get();
        // var_dump($free_value);die;
        return $count;
    }

    public function filterPremium(Request $request) {
        $getData = Recipes::orderBy('recipe_id', 'desc')
            ->where('premium', '=', 1)
            ->where('status', '=', 1)
            ->get();


        return $getData;
    }

    public function premiumCount(Request $request) {
        $count = DB::table('recipe')
            ->select(DB::raw('count(*) as premium_count'))
            ->where('premium', '=', 1)
            ->where('status', '=', 1)
            ->get();
        // var_dump($free_value);die;
        return $count;
    }

    public function filterPaid(Request $request) {
        $getData = Recipes::orderBy('recipe_id', 'desc')
            ->where('free_paid', '=', 1)
            ->where('status', '=', 1)
            ->get();

        
        return $getData;
    }

    public function paidCount(Request $request) {
        $count = DB::table('recipe')
            ->select(DB::raw('count(*) as premium_count'))
            ->where('free_paid', '=', 1)
            ->where('status', '=', 1)
            ->get();
        // var_dump($free_value);die;
        return $count;
    }

    public function filterAll(Request $request)
    {
        // var_dump("helo");die;
        $sql = Recipes::orderBy('recipe_id', 'desc')
          ->where('status', '=', '1');

          if ($request->type && $request->type != 'all') {
            $sql->whereHas('categories', function($q) use ($request){
              $q->where('recipes_category_id', $request->type);
            });
          }

          $sql =  $sql->get();



        return $sql;
    }


    public function shortPriceLowest(Request $request) {
        $id = $_GET['id'];
        $member_id = $_GET['member_id'];
        $data = DB::table('recipe');


        if ($member_id == "") {
            if ($id == 1) {
                $data = $data->orderBy('price', 'asc');
            }
            if ($id == 2) {
                $data = $data->orderBy('price', 'desc');
            }
            if ($id == 3) {
                $data = $data->orderBy('created_at', 'desc');
            }

            if ($id == 4) {
                $data = $data->orderBy('created_at', 'asc');
            }
        } else {
            $member_ids = $member_id;
            if ($member_ids == 1) {
                if ($id == 2) {
                    $data = $data->orderBy('price', 'desc');
                    $data = $data->where('free_paid', '=', 0)->where('status', '=', 1);
                }
                if ($id == 1) {
                    $data = $data->orderBy('price', 'asc');
                    $data = $data->where('free_paid', '=', 0)->where('status', '=', 1);
                }
                if ($id == 3) {
                    $data = $data->orderBy('created_at', 'desc');
                    $data = $data->where('free_paid', '=', 0)->where('status', '=', 1);
                }
                if ($id == 4) {
                    $data = $data->orderBy('created_at', 'asc');
                    $data = $data->where('free_paid', '=', 0)->where('status', '=', 1);
                }
            }

            if ($member_ids == 2) {
                if ($id == 2) {
                    $data = $data->orderBy('price', 'desc');
                    $data = $data->where('premium', '=', 1)->where('status', '=', 1);
                }
                if ($id == 1) {
                    $data = $data->orderBy('price', 'asc');
                    $data = $data->where('premium', '=', 1)->where('status', '=', 1);
                }
                if ($id == 3) {
                    $data = $data->orderBy('created_at', 'desc');
                    $data = $data->where('premium', '=', 1)->where('status', '=', 1);
                }
                if ($id == 4) {
                    $data = $data->orderBy('created_at', 'asc');
                    $data = $data->where('premium', '=', 1)->where('status', '=', 1);
                }
            }

            if ($member_ids == 3) {
                if ($id == 2) {
                    $data = $data->orderBy('price', 'desc');
                    $data = $data->where('free_paid', '=', 1)->where('status', '=', 1);
                }
                if ($id == 1) {
                    $data = $data->orderBy('price', 'asc');
                    $data = $data->where('free_paid', '=', 1)->where('status', '=', 1);
                }
                if ($id == 3) {
                    $data = $data->orderBy('created_at', 'desc');
                    $data = $data->where('free_paid', '=', 1)->where('status', '=', 1);
                }
                if ($id == 4) {
                    $data = $data->orderBy('created_at', 'asc');
                    $data = $data->where('free_paid', '=', 1)->where('status', '=', 1);
                }
            }
        }
        $data = $data->get();

        return $data;
    }

    public function viewAll($id, $recipe_id) {
        //   var_dump(getSamdata($recipe_id));die;
        $getsamedata = $this->getSamdata($id,$recipe_id);

        $loged_user = null;
        if ($user = Sentinel::check()) {
            $loged_user = Sentinel::getUser();
        }

        $ids = $id;
        $getData = DB::table('recipe')
            ->leftJoin('recipe_photo', 'recipe.recipe_id', '=', 'recipe_photo.recipe_id')
            ->orderBy('recipe.recipe_id', 'desc')
            ->where('recipe.recipe_id', '=', $recipe_id)
            ->get();
        //var_dump( $user->id);die;
        $getPayDetails = null;
        $user = Sentinel::check();
        if ($user = Sentinel::check()) {
            $getPayDetails = DB::table('sa_purchase')
                ->leftJoin('sa_purchase_item', 'sa_purchase.id', '=', 'sa_purchase_item.purchase_id')
                ->where('sa_purchase.created_by', '=', $user->id)
                ->where('sa_purchase_item.item_id', '=', $recipe_id)
                ->get();
        }
        //var_dump( $getPayDetails);die;
        $show_button = true;
        $show_data = false;
        if ($getPayDetails) {
            $show_button = false;
            $show_data = true;
        } else {
            $show_button = true;
            $show_data = false;
        }
        $tutorial_data = array();
        $tutorial_text = null;
        // var_dump( $show_data);die;
        //var_dump($ids);die;
        if ($ids == 'free') {
            if ($loged_user != null) {


                foreach ($getData as $setdata) {
                    $tutorial_data = array(
                        'recipe_id' => $recipe_id,
                        'name' => $setdata->name,
                        'cover_path' => $setdata->cover_path,
                        'cover_file' => $setdata->cover_file,
                        'intro' => $setdata->intro,
                        'main' => $setdata->main,
                        'lesson' => $setdata->lesson,
                        'material' => $setdata->material,
                        'ingrediant' => $setdata->ingrediant,
                        'recipes' => $setdata->recipes,
                        'pdf_path' => $setdata->pdf_path,
                        'pdf_file' => $setdata->pdf_file,
                        'note' => $setdata->note,
                        'price' => $setdata->price,
                        'category' => $setdata->category,
                        'free_paid' => $setdata->free_paid,
                        'mor_button' => false,
                        'login' => true,
                        'pay_button' => false,
                        'pay_title' => 'Shop',
                        'show_details' => true,
                        // 'tutorial_text' => 'All You Need to Do Follow this Tutorial'
                        'tutorial_text' => ''
                    );
                }
            } else {

                foreach ($getData as $setdata) {
                    $tutorial_data = array(
                        'recipe_id' => $recipe_id,
                        'name' => $setdata->name,
                        'main' => $setdata->main,
                        'cover_path' => $setdata->cover_path,
                        'cover_file' => $setdata->cover_file,
                        'intro' => $setdata->intro,
                        'price' => $setdata->price,
                        'category' => $setdata->category,
                        'mor_button' => "More Details",
                        'login' => false,
                        'pay_button' => false,
                        'pay_title' => 'Shop',
                        'tutorial_text' => 'Sign Up With Us To Access All Free Tutorials'
                    );
                }
            }
        } else {

            if ($loged_user != null) {


                foreach ($getData as $setdata) {
                    $tutorial_data = array(
                        'recipe_id' => $recipe_id,
                        'name' => $setdata->name,
                        'cover_path' => $setdata->cover_path,
                        'cover_file' => $setdata->cover_file,
                        'intro' => $setdata->intro,
                        'main' => $setdata->main,
                        'lesson' => $setdata->lesson,
                        'material' => $setdata->material,
                        'ingrediant' => $setdata->ingrediant,
                        'recipes' => $setdata->recipes,
                        'pdf_path' => $setdata->pdf_path,
                        'pdf_file' => $setdata->pdf_file,
                        'note' => $setdata->note,
                        'price' => $setdata->price,
                        'category' => $setdata->category,
                        'premium' => $setdata->premium,
                        'mor_button' => false,
                        'login' => true,
                        'pay_button' => $show_button,
                        'pay_title' => 'Shop',
                        'show_details' => $show_data,
                        'tutorial_text' => 'Become Premium member watch this video'
                    );
                }
            } else {

                foreach ($getData as $setdata) {
                    $tutorial_data = array(
                        'recipe_id' => $recipe_id,
                        'name' => $setdata->name,
                        'cover_path' => $setdata->cover_path,
                        'cover_file' => $setdata->cover_file,
                        'intro' => $setdata->intro,
                        'main' => $setdata->main,
                        'price' => $setdata->price,
                        'category' => $setdata->category,
                        'mor_button' => "More Details",
                        'login' => false,
                        'pay_button' => $show_button,
                        'premium' => $setdata->premium,
                        'pay_title' => 'Shop',
                        'tutorial_text' => 'We are Very Happy to Serve You the Best'
                    );
                }
            }
        }
       
        return view('front.tutorialsView')->with(['id' => $id, 'getData' => $tutorial_data, 'slider' => $getData, 'getsamedata'=>$getsamedata]);
    }

    public function getSamdata($id,$recipe_id) {
        $getData = "";

        if ($id == 'free') {

            $getData = DB::table('recipe')
                ->orderBy('recipe_id', 'desc')
                ->where('free_paid', '=', 1)
                ->where('status', '=', 1)
                ->get();
        }elseif ($id == 'paid') {
            $getData = DB::table('recipe')
                ->orderBy('recipe_id', 'desc')
                ->where('premium', '=', 1)
                ->where('status', '=', 1)
                ->get();
        }
      //  var_dump($getData);die;
        return $getData;
    }

public function paidCount1(Request $request){
    var_dump("hello");die;
  }
 public function skill(Request $request){
            $skill = $_GET['skill'];
            $member_id = $_GET['member_id'];
            $type = $_GET['type'];
            $data = DB::table('recipe');

           if($member_id != ""){

            if($skill != '' && $member_id == 1){
            $data  = $data->where('free_paid', '=', 0);
            $data =  $data->where('status', '=', 1);
            $data =  $data->where('skill_level', '=', $skill);
            $data =  $data->where('cake_type', '=', $type);
            }else if($skill != '' && $member_id == 2){
                //   var_dump($skill."  "."premium");
                $data  = $data->where('premium', '=', 1);
                $data =  $data->where('status', '=', 1);
                $data =  $data->where('skill_level', '=', $skill);
                $data =  $data->where('cake_type', '=', $type);

            }else{
            //  var_dump($skill."  "."paid");
            //$data  = $data->where('free_paid', '=', 1);
            $data =  $data->where('status', '=', 1);
            $data =  $data->where('skill_level', '=', $skill);
            $data =  $data->where('cake_type', '=', $type);

            }
          }else{
            if($skill != ''){

            $data =  $data->where('status', '=', 1);
            $data =  $data->where('skill_level', '=', $skill);
            }else if($skill != '' && $member_id == 2){
                //   var_dump($skill."  "."premium");

                $data =  $data->where('status', '=', 1);
                $data =  $data->where('skill_level', '=', $skill);

            }else{
            //  var_dump($skill."  "."paid");

            $data =  $data->where('status', '=', 1);
            $data =  $data->where('skill_level', '=', $skill);

            }


          }
            $data = $data->orderBy('recipe_id', 'desc');
            $data  =  $data->get();
            return $data;


      }
}
