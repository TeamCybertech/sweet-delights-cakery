<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);
    Route::get('placeOrder', [
      'as' => 'index', 'uses' => 'WebController@placeOrder'
    ]);

     Route::get('checkout1', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout1'
    ]);
     Route::post('checkout1', [
          'as' => 'index', 'uses' => 'ShoppingCartController@checkout1_submit'
        ]);

    Route::get('checkout2', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout2'
    ]);
    Route::post('checkout2', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout2_submit'
    ]);

    Route::get('checkout3', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout3'
    ]);

    Route::post('checkout3', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout3_submit'
    ]);

    Route::get('checkout4', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout4'
    ]);
     Route::post('checkout4', [
      'as' => 'index', 'uses' => 'ShoppingCartController@checkout4_submit'
    ]);
     Route::get('billing-detail', [
      'as' => 'index', 'uses' => 'WebController@billingDetail'
    ]);
    Route::post('billing', ['as' => 'index', 'uses' => 'WebController@billingDetails']);
    Route::post('billingdetail2',[  'uses' => 'WebController@billingDetail2','as' => 'index']);
   Route::get('paypal/payment/recipt/{invoice}', [
      'as' => 'index', 'uses' => 'ShoppingCartController@paypal_recipt'
    ]);
   Route::get('paypal/payment/recipt-print/{invoice}', [
      'as' => 'paypal_print', 'uses' => 'ShoppingCartController@paypal_recipt_print'
    ]);
    
  });
  

Route::group([], function()
{
	Route::get('viewTutorials/{id}/{recipe_id}',[
      'as' => 'index', 'uses' =>'OnlineTutorialsController@viewAll'
    ]);

    Route::get('/', [
      'as' => 'index', 'uses' => 'WebController@index'
    ]);
     Route::get('test', [
      'as' => 'index', 'uses' => 'ShoppingCartController@test'
    ]);

    Route::get('about', [
      'as' => 'index', 'uses' => 'WebController@about'
    ]);
    Route::get('gallery/{val}', [
      'as' => 'index', 'uses' => 'WebController@gallery'
    ]);
    Route::get('pricing', [
      'as' => 'index', 'uses' => 'WebController@pricing'
    ]);
    Route::get('flavors', [
      'as' => 'index', 'uses' => 'WebController@flavors'
    ]);
    Route::get('quotes', [
      'as' => 'index', 'uses' => 'WebController@quotes'
    ]);

    Route::get('generalcontact', [
      'as' => 'index', 'uses' => 'WebController@quotes'
    ]);
    Route::get('PrivateClassesLand/{val}', [
      'as' => 'index', 'uses' => 'WebController@PrivateClassesLand'
    ]);
    Route::get('contact', [
      'as' => 'index', 'uses' => 'WebController@contact'
    ]);

    Route::get('blog', [
      'as' => 'index', 'uses' => 'WebController@blog'
    ]);

    Route::get('blog/searchByTag/{tag}', [
        'as' => 'index', 'uses' => 'WebController@searchByTag'
    ]);

    Route::get('blog/search', [
      'as' => 'index', 'uses' => 'WebController@blogSearch'
    ]);

    Route::get('blog/stdSearch', [
      'as' => 'index', 'uses' => 'WebController@blogStdSearch'
    ]);

    Route::get('blog/{id}', [
      'as' => 'index', 'uses' => 'WebController@blogDetails'
    ]);

    Route::get('my-cart', [
         'as' => 'index', 'uses' => 'WebController@myCart'
       ]);


    Route::get('cart/add', [
      'as' => 'index', 'uses' => 'WebController@cartADD'
    ]);

    Route::get('cart/remove/{id}/{type}', [
      'as' => 'index', 'uses' => 'WebController@cartREMOVE'
    ]);
     Route::get('cart/update/qty/{id}/{qty}/{type}', [
      'as' => 'index', 'uses' => 'WebController@cartUpdate'
    ]);
     Route::get('product-detail/{id}', [
      'as' => 'index', 'uses' => 'WebController@productDetails'
    ]);  
     Route::post('product-detail/{id}', [
      'as' => 'index', 'uses' => 'WebController@productDetailSubmit'
    ]);

     Route::post('check/pro_qty', [
      'as' => 'index', 'uses' => 'WebController@checkProQty'
    ]);

    

    Route::get('paypal/payment/success', [
      'as' => 'index', 'uses' => 'ShoppingCartController@paypal_success'
    ]);
    Route::get('paypal/payment/reject', [
      'as' => 'index', 'uses' => 'ShoppingCartController@paypal_reject'
    ]);
     Route::get('get-testimonial', [
            'as' => 'index', 'uses' => '\TestimonialsManager\Http\Controllers\TestimonialsController@viewFront'
        ]);




    Route::get('faq', [
      'as' => 'index', 'uses' => 'WebController@faq'
    ]);

    Route::post('sign-up-to-newsletter', [
      'as' => 'index', 'uses' => 'WebController@newsletterSignUp'
    ]);

    Route::get('privacy', [
      'as' => 'index', 'uses' => 'WebController@privacyPolicy'
    ]);

    Route::get('tos', [
      'as' => 'index', 'uses' => 'WebController@TermsOfService'
    ]);

    Route::post('quotes', [
        'as' => 'index', 'uses' => 'WebController@submitQuote'
    ]);

    Route::get('PrivateClassesReg/{id}', [
        'as' => 'index', 'uses' => 'WebController@PrivateClassesReg'
    ]);

    Route::post('PrivateClasses', [
        'as' => 'index', 'uses' => 'WebController@PrivateClasses'
    ]);

    Route::post('PrivateClassesReg', [
        'as' => 'index', 'uses' => 'WebController@regClz'
    ]);

    Route::get('tasting', [
        'as' => 'index', 'uses' => 'WebController@tasting'
    ]);

    Route::get('news', [
        'as' => 'index', 'uses' => 'WebController@news'
    ]);

    Route::get('newsDetails/{id}', [
        'as' => 'index', 'uses' => 'WebController@newsDetails'
    ]);

    Route::get('tasting', [
        'as' => 'index', 'uses' => 'WebController@tasting'
    ]);

    Route::post('tasting', [
        'as' => 'index', 'uses' => 'WebController@tasting_submit'
    ]);

    Route::get('productload/{category}/{search}/{sort}/{mprice}/{hprice}', [
        'as' => 'index', 'uses' => 'WebController@productload'
    ]);
    Route::get('onlineTutorials', [
        'as' => 'index', 'uses' => 'WebController@onlineTutorials'
    ]);

    Route::get('membershipList', [
      'as' => 'index', 'uses' => 'WebController@membershipList'
    ]);
    
    
    Route::post('sendoffer',['as'=>'index' , 'uses' => 'WebController@sendData']);

    Route::get('search',[
      'as' => 'index', 'uses' => 'OnlineTutorialsController@search'
      ]);
    Route::get('stdSearch',[
        'as' => 'index', 'uses' => 'OnlineTutorialsController@stdSearch'
        ]);
    Route::get('filterMembership',[
        'as' => 'index', 'uses' => 'OnlineTutorialsController@filterMembership'
        ]);
    Route::get('filterPremium', [
        'as' => 'index', 'uses' =>'OnlineTutorialsController@filterPremium'
        ]);
    Route::get('free_count', [
        'as' => 'index', 'uses' =>'OnlineTutorialsController@freeCount'
        ]);
    Route::get('premium_count', [
        'as' => 'index', 'uses' =>'OnlineTutorialsController@premiumCount'
        ]);
    Route::get('shortPriceLowest',[
        'as' => 'index', 'uses' =>'OnlineTutorialsController@shortPriceLowest'
        ]);



    Route::get('filterPaid',[
        'as' => 'index', 'uses' => 'OnlineTutorialsController@filterPaid'
        ]);
    Route::get('paid_count',[
        'as' => 'index', 'uses' => 'OnlineTutorialsController@paidCount'
        ]);
        Route::get('filtertype',[
            'as' => 'index', 'uses' => 'OnlineTutorialsController@filterAll'
            ]);




    Route::get('skillBeginner',[
          'as' => 'index','uses' => 'OnlineTutorialsController@skill'
        ]);

   // Route::get('tutoricalcartadd', 'OnlineTutorialsController@cartADD');
    // Route::get('my-cart', ['as' => 'index', 'uses' => 'OnlineTutorialsController@myCart']);


//privet class filter

Route::get('beginner',[
'as'=> 'index','uses' => 'PrivetClassController@beginner'

]);

Route::get('intermediate',[
  'as' => 'index','uses' => 'PrivetClassController@intermediate'
]);

Route::get('advance',[
  'as' => 'index','uses' => 'PrivetClassController@advance'
]);
Route :: get('kids',[
  'as' => 'index','uses' => 'PrivetClassController@kids'
]);
Route :: get('filterall',[
  'as' => 'index','uses' => 'PrivetClassController@filterall'
]);

});

Route::get('cat/{id}', [
    'as' => 'indexada', 'uses' => 'WebController@categorizedImage'
]);

Route::get('album/{id}', [
    'as' => 'indexada', 'uses' => 'WebController@album'
]);

/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);

Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegisterView'
]);
Route::post('user/register', [
  'as' => 'user.register', 'uses' => 'AuthController@userRegister'
]);

Route::get('user-profile', [
  'as' => 'user.profile', 'uses' => 'WebController@profileView'
]);

Route::post('user-profile-update', [
  'as' => 'user.profile.update', 'uses' => 'WebController@updateProfile'
]);

Route::post('user-profile-password', [
  'as' => 'user.profile.password', 'uses' => 'WebController@password'
]);

Route::post('user/password-reset', [
  'as' => 'user.reset', 'uses' => 'PasswordController@putForgetPassword'
]);

Route::get('user/password-reset/{code}', [
  'as' => 'user.reset.link', 'uses' => 'PasswordController@getResetPassword'
]);

Route::post('user/reset-password/{code}', [
  'as' => 'user.reset', 'uses' => 'PasswordController@passwordReset'
]);

Route::post('comment', 'WebController@addComment');

Route::get('user/varifiedEmail/{email}/{token}', [
  'as' => 'user.varifiedEmail', 'uses' => 'AuthController@varifiedEmail'
]);


/**
 * USER LOGIN VIA FACEBOOK/GOOGLE/TWITTER/LINKDIN ETC
 */
 Route::get('tasting-cancel/{id}', 'WebController@cancelTasting');
 Route::get('tasting-reschedule/{id}', 'WebController@reschedule');

Route::get('tasting-confirm/{id}', 'WebController@tastConfirm');
Route::post('tasting-confirm/{id}', 'WebController@tastPayPal');

Route::get('/paypal/payment/tasting', 'WebController@tastingPaymentSuccess');

Route::get('auth/facebook', 'AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'AuthController@handleFacebookCallback');

Route::get('auth/google', 'AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'AuthController@handleGoogleCallback');
