@extends('layouts.back.master') @section('current_title','New Gallery')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />

<style media="screen">
.imageBox
{
  position: relative;
  height: 670px;
  width: 450px;
  border:1px solid #aaa;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor:move;
}

.imageBox .thumbBox
{
  position: absolute;
  top: 22%;
  left: 33%;
  width: 400px;
  height: 620px;
  margin-top: -124px;
  margin-left: -124px;
  box-sizing: border-box;
  border: 1px solid rgb(102, 102, 102);
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  text-align: center;
  line-height: 400px;
  background: rgba(0,0,0,0.7);
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('gallery/list')}}">Gallery Management</a></li>
       
        <li class="active">
            <span>New Gallery</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">    
                 @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
              <br>            
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}
                    
                    <input type="hidden" name="featuredCrop" id="featuredCrop">
                	<div class="form-group"><label class="col-sm-2 control-label">ALBUM NAME <sup>*</sup></label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="album_name"></div>
                	</div>
                	<div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control"></textarea></div>
                        
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                         <div class="col-sm-10">
                         <select class="js-source-states" multiple="multiple" name="gallery_category[]" style="width: 100%">
                         <?php foreach ($galleryCategory as $key => $value): ?>
                             <option value="{{$value->id}}">{{$value->name}}</option>
                         <?php endforeach ?>
                            
                            
                        </select>
                    </div>
                    	
                	</div>
                    {{-- <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGE</label>
                        <div class="col-sm-10">
                            <input id="featured_image" name="featured_image" type="file" class="file-loading">
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGE</label>
                        <div class="col-sm-6 ">
                            <div class="row">
                                <div class="imageBox">
                                    <div class="thumbBox"></div>
                                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                                </div>
                                <div class="action">
                                    <input type="file" id="file" style="float:left; ">
                                    <input type="button" id="btnCrop" value="Crop" style="float: right">
                                    <input type="button" id="btnZoomIn" value="+" style="float: right">
                                    <input type="button" id="btnZoomOut" value="-" style="float: right">
                                </div>

                            </div>
                                <div class="row" style="margin-top : 30px;">
                                    <div class="cropped">

                                    </div>
                                </div>
                            

                            </div>
                            
                            
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">SUB IMAGES</label>
                        <div class="col-sm-10">
                            <input id="sub_image" name="sub_image[]" type="file" multiple class="file-loading">
                        </div>
                    </div>
                	
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/cropbox/cropbox.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();
         
        // $("#featured_image").fileinput({
        //     uploadUrl: "", // server upload action
        //     uploadAsync: true,
        //     maxFileCount: 1,
        //     showUpload:false,
        //     allowedFileExtensions: ["jpg", "gif", "png"]
        // });
        $("#sub_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 5,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"]
        });

		$("#form").validate({
            rules: {
                album_name: {
                    required: true
                  
                }
                
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
	
	
</script>
 
<script type="text/javascript">

        var options =
        {
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
        var cropper = $('.imageBox').cropbox(options);
        $('#file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = $('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        $('#btnCrop').on('click', function(){
            var img = cropper.getDataURL();
            $("#featuredCrop").val(img)
            $('.cropped').html('<img style="max-width : 100%;" src="'+img+'">');
        })
        $('#btnZoomIn').on('click', function(){
            cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
            cropper.zoomOut();
        })

  </script>
@stop
