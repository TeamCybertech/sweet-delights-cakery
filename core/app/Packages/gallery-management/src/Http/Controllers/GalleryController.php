<?php
namespace GalleryManage\Http\Controllers;

use App\Http\Controllers\Controller;
use GalleryManage\Http\Requests\GalleryRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\Gallery;
use GalleryManage\Models\GalleryImage;
use App\Helpers\QueryArraySort;

// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;


class GalleryController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Gallery Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the GALLERY CATEGORY add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_category()
	{

		return view( 'GalleryManage::category.add');
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_category(Request $request)
	{
		
   			$count= GalleryCategory::where('name', '=',$request->get('name' ))->count();

            $galleryCategory = new GalleryCategory;
            $galleryCategory->name = $request->name;
            $galleryCategory->created_by = Sentinel::getUser()->id;
            $galleryCategory->status = 1;
            $galleryCategory->save();

            if ($galleryCategory) {
                return redirect('gallery/category/list')->with(['success' => true,
                    'success.message' => 'Gallery Category Created successfully!',
                    'success.title' => 'Well Done!']);
            }else{
            	return redirect('gallery/category/add')->with([ 'error' => true,
                	'error.message'=> 'Gallery Category Already Exsist!',
                	'error.title' => 'Duplicate!']);
        	}
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_category()
	{
		return view( 'GalleryManage::category.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_category(Request $request)
	{
		if($request->ajax()){
			$galleryCategories= GalleryCategory::where('status','=',1)->orderBy('id','desc')->get();
			$jsonList = array();
			$i=1;
			foreach ($galleryCategories as $key => $galleryCategory) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$galleryCategory->name);

				$permissions = Permission::whereIn('name',['gallery.category.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('gallery/category/edit/'.$galleryCategory->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery Category Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['gallery.category.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="gallery-category-delete" data-id="'.$galleryCategory->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$galleryCategory = GalleryCategory::find($id);
			if($galleryCategory){
                $galleryCategory->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_category($id)
	{
		$galleryCategory=galleryCategory::find($id);
		if($galleryCategory){
			return view( 'GalleryManage::category.edit' )->with(['galleryCategory'=>$galleryCategory]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_category(GalleryRequest $request, $id)
	{
		$count= GalleryCategory::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();

			if($count==0){
                $galleryCategory =  GalleryCategory::find($id);
                $galleryCategory->name = $request->get('name');
                $galleryCategory->save();

				return redirect( 'gallery/category/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Gallery Category updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('gallery/category/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Gallery Category Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}

	/****************************************GALLERY ******/


	/**
	 * Show the GALLERY  add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_gallery()
	{
		$galleryCategory=GalleryCategory::get();
		return view( 'GalleryManage::gallery.add')->with(['galleryCategory'=>$galleryCategory]);
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_gallery(galleryRequest $request)
	{
		$cat= $request->get('gallery_category');

        $count= Gallery::where('album_name', '=',$request->get('album_name' ))->count();
        if (!file_exists(storage_path('uploads/images/gallery'))) {
			 File::makeDirectory(storage_path('uploads/images/gallery'));
		}
		$path='uploads/images/gallery';

        if ($count==0) {
        	$fileName= $this->base64_img_upload($request->featuredCrop);
			// if ($request->hasFile('featured_image')) {
			// 		$file = $request->file('featured_image');
			// 		$extn =$file->getClientOriginalExtension();
			// 		$destinationPath = storage_path($path);
			// 		$fileName = 'gallery-cover-' .date('YmdHis') .  '.' . $extn;
			// 		$file->move($destinationPath, $fileName);

			//  }

            $gallery = gallery::Create(array(
                    'album_name' => $request->input('album_name'),
                    'description' => $request->input('description'),
                    'categories' =>json_encode( $cat ) ,
                    'path'=>$path,
				 	'filename'=>$fileName,
                    'created_by'=>Sentinel::getUser()->id,
                )
            );

            if ($gallery) {
            	$project_files = $request->file('sub_image');
				$i=0;
				if ($project_files) {
					foreach ($project_files as $key => $project_file) {
						if (File::exists($project_file)) {
							$file = $project_file;
							$extn = $file->getClientOriginalExtension();
							$destinationPath = storage_path($path);
							$project_fileName = 'gallery-sub-' . date('YmdHis') . '-' . $i . '.' . $extn;
							$file->move($destinationPath, $project_fileName);

							GalleryImage::create([
								'gallery_id' => $gallery->id,
								'path' => $path,
								'filename' => $project_fileName
							]);
							$i++;
						}
					}
				}
				

                return redirect('gallery/add')->with(['success' => true,
                    'success.message' => 'Gallery  Created successfully!',
                    'success.title' => 'Well Done!']);
            }
        }
        else{
            return redirect('gallery/add')->with([ 'error' => true,
                'error.message'=> 'Gallery  Already Exsist!',
                'error.message'=> 'Gallery  Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_gallery()
	{
		return view( 'GalleryManage::gallery.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_gallery(Request $request)
	{
		if($request->ajax()){
			$galleries= Gallery::where('status','=',1)->get();
			$galleries = QueryArraySort::orderBy($galleries);
			$jsonList = array();
			$i=1;
			foreach ($galleries as $key => $gallery) {

				$dd = array();
				array_push($dd, $i);
				
				array_push($dd,$gallery->album_name);
				
				$categoriesList = "";
				foreach (GalleryCategory::find(json_decode($gallery->categories)) as $category) {
					$categoriesList .= $category->name." ,";
				}
				array_push($dd, $categoriesList);
				
                array_push($dd, '<input type="number" name="galleryPriority['.$gallery->id.'][]" min="1" class="galleryPriority" value="'.$gallery->priority.'" />');

				$permissions = Permission::whereIn('name',['gallery.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('gallery/edit/'.$gallery->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery "><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['gallery.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="gallery-delete" data-id="'.$gallery->id.'" data-toggle="tooltip" data-placement="top" title="Delete Gallery"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_gallery(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_gallery(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$gallery = Gallery::find($id);
			if($gallery){
                $gallery->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_gallery($id)
	{
		$gallery=gallery::find($id);
		$galleryCategory=GalleryCategory::get();

		if($gallery){
			 $cur_category=json_decode($gallery->categories);
		  ;
			$image_url_array=[];
			 $image_config_array=[];
			  foreach ($gallery->getImages as $key => $value) {
					array_push($image_url_array,"<img style='height:160px' src='". url('') . '/core/storage/' .$value->path.'/'.$value->filename."'>");

			  }

			  foreach ($gallery->getImages as $key => $value) {
					array_push($image_config_array,array('caption'=>$value->filename,'caption'=>$value->filename,'key'=>$value->id,'url'=>url('gallery/image/deleteFile')));

			  }
			return view( 'GalleryManage::gallery.edit' )->with(['gallery'=>$gallery,'galleryCategory'=>$galleryCategory,'cur_category'=>$cur_category,'images'=>$image_url_array,'image_config'=>$image_config_array]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_gallery(GalleryRequest $request, $id)
	{
		if (!file_exists(storage_path('uploads/images/gallery'))) {

			File::makeDirectory(storage_path('uploads/images/gallery'));
		 }
		$count= Gallery::where('id', '!=', $id)->where('album_name', '=',$request->get('album_name' ))->count();

			if($count==0){
				$cat= $request->get('gallery_category');
                $gallery =  Gallery::find($id);
                $gallery->album_name = $request->get('album_name');
                $gallery->description = $request->get('description');
                $gallery->categories = json_encode($cat);
                $image_hidden=$request->get('image_hidden');
				if($image_hidden){
					File::delete(storage_path($gallery->path . '/' . $gallery->filename));
				}
				$path='uploads/images/gallery';
				// if ($request->hasFile('featured_image')) {
				// 		$file = $request->file('featured_image');
				// 		$extn =$file->getClientOriginalExtension();
				// 		$destinationPath = storage_path($path);
				// 		$fileName = 'gallery-cover-' .date('YmdHis') .  '.' . $extn;
				// 		$file->move($destinationPath, $fileName);
				// 		$gallery->filename=$fileName;
				//  }
				if ($request->featuredCrop != "") {
						$gallery->filename= $this->base64_img_upload($request->featuredCrop);
				 }
                $gallery->save();



				if ($request->hasFile('sub_image')) {
					$project_files = $request->file('sub_image');
					$i=0;
					foreach ($project_files as $key => $project_file) {
						if (File::exists($project_file)) {
								$file = $project_file;
								$extn =$file->getClientOriginalExtension();
								$destinationPath = storage_path($path);
								$project_fileName = 'gallery-sub-' .date('YmdHis') .'-'.$i. '.' . $extn;
								$file->move($destinationPath, $project_fileName);

								GalleryImage::create([
									'gallery_id'=>$gallery->id,
									'path'=>$path,
									'filename'=>$project_fileName
								]);
								$i++;
						 }
					}
				}

				return redirect( 'gallery/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Gallery  updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('gallery/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Gallery  Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}
	public function jsonImageFileDelete_gallery(Request $request)
	{
		$image_id=$request->get('key');
		$image=GalleryImage::find($image_id);
		$image->delete();
		return 1;
	}

	public function base64_img_upload($img_data){//save base64 encoded image
	try {
		$newname = str_random(10).".png";
		$data = $img_data;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);


		$save_target = "core/storage/uploads/images/gallery/".$newname;

		$target = $save_target;


		if(file_put_contents($target, $data)){
			$img = imagecreatefrompng($target);
			imagealphablending($img, false);


			imagesavealpha($img, true);

			imagepng($img, $target, 8);
			return $newname;
		}else{
			return "error";
		}
	} catch (Exception $e) {
		return "error";
	}



}

public function updatePriorities(Request $request)
{
	foreach ($request->galleryPriority as $galId => $value) {
		if(($value != "" || $value != null) && $value[0] < 1 ){
			$gallery = gallery::findOrFail($galId);
			 return response()->json([
				 'error' => $gallery->album_name." priority is invalid. Minimum value is 1."
			 ], 422);
		}
	}

	foreach ($request->galleryPriority as $galId => $value) {
		$gallery = gallery::findOrFail($galId);
		$gallery->priority = $value[0];
		$gallery->save();
	}
	 return response()->json([
				  'status' => 'success',
				  'msg' => "Priorities updated successfully"
			 ]);
}

}
