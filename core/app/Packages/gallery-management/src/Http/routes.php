<?php
/**
 * Gallery MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'gallery/category', 'namespace' => 'GalleryManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'gallery.category.add', 'uses' => 'GalleryController@addView_category'
      ]);

      Route::get('edit/{id}', [
        'as' => 'gallery.category.edit', 'uses' => 'GalleryController@editView_category'
      ]);

      Route::get('list', [
        'as' => 'gallery.category.list', 'uses' => 'GalleryController@listView_category'
      ]);

      Route::get('json/list', [
        'as' => 'gallery.category.list', 'uses' => 'GalleryController@jsonList_category'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'gallery.category.add', 'uses' => 'GalleryController@add_category'
      ]);

      Route::post('edit/{id}', [
        'as' => 'gallery.category.edit', 'uses' => 'GalleryController@edit_category'
      ]);
      

      Route::post('status', [
        'as' => 'gallery.category.status', 'uses' => 'GalleryController@status_category'
      ]);

      Route::post('delete', [
        'as' => 'gallery.category.delete', 'uses' => 'GalleryController@delete_category'
      ]);
    });
   Route::group(['prefix' => 'gallery', 'namespace' => 'GalleryManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'gallery.add', 'uses' => 'GalleryController@addView_gallery'
      ]);

      Route::get('edit/{id}', [
        'as' => 'gallery.edit', 'uses' => 'GalleryController@editView_gallery'
      ]);

      Route::get('list', [
        'as' => 'gallery.list', 'uses' => 'GalleryController@listView_gallery'
      ]);

      Route::get('json/list', [
        'as' => 'gallery.list', 'uses' => 'GalleryController@jsonList_gallery'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'gallery.add', 'uses' => 'GalleryController@add_gallery'
      ]);

      Route::post('edit/{id}', [
        'as' => 'gallery.edit', 'uses' => 'GalleryController@edit_gallery'
      ]);

      Route::post('update/priorities', [
        'as' => 'gallery.edit', 'uses' => 'GalleryController@updatePriorities'
      ]);

      Route::post('status', [
        'as' => 'gallery.status', 'uses' => 'GalleryController@status_gallery'
      ]);

      Route::post('delete', [
        'as' => 'gallery.delete', 'uses' => 'GalleryController@delete_gallery'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'gallery.edit', 'uses' => 'GalleryController@jsonImageFileDelete_gallery',
    ]);
    });
});
