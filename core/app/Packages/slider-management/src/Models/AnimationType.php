<?php

namespace SliderManager\Models;

use Illuminate\Database\Eloquent\Model;

class AnimationType extends Model{
		protected $table = 'animation_type';

		protected $fillable = ['css_class'];

    protected $primaryKey = 'animation_type_id';

}
