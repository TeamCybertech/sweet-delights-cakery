<?php
namespace FeatureManage\Http\Controllers;

use FeatureManage\Models\Feature;
use App\Models\Font;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use FeatureManage\Http\Requests\FeatureRequest;
use Permissions\Models\Permission;
use Sentinel;

class FeatureController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Menu Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the menu add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$feature = Feature::where('status','=',1)->get();
		
		return view( 'featureManage::feature.add' )->with([
		    'feature' => $feature
		   
			 ]);
	}

	/**
	 * Add new menu data to database
	 *
	 * @return Redirect to menu add
	 */
	public function add(FeatureRequest $request)
	{
		
		$feature = Feature::create([
			'name'			=> $request->get( 'name' ),			
			'description'			=> $request->get( 'desc' )
		]);        

		return redirect( 'feature/add' )->with([ 'success' => true,
			'success.message'=> 'Feature added successfully!',
			'success.title' => 'Well Done!' ]);
	}

	/**
	 * Show the menu add screen to the user.
	 *
	 * @return Response
	 */
	public function listView()
	{
		return view( 'featureManage::feature.list' );
	}

	/**
	 * Show the menu add screen to the user.
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data = Feature::where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $feature) {
				$dd = array();
				array_push($dd, $i);
				array_push($dd, $feature->name);               
                array_push($dd, $feature->description);
                $permissions = Permission::whereIn('name',['feature.edit','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('feature/edit/'.$feature->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit feature"><i class="fa fa-pencil"></i></a></center>');
                }else{
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name',['feature.delete','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){
                    array_push($dd, '<center><a  class="red feature-delete" data-id="'.$feature->id.'" data-toggle="tooltip" data-placement="top" title="Delete Menu"><i class="fa fa-trash-o"></i></a></center>');
                }else{
                    array_push($dd, '<center><a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a></center>');
                }



                array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate Menu
	 * @param  Request $request menu id with status to change
	 * @return Json           	json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$menu = Menu::find($id);
			if($menu){
				$menu->status = $status;
				$menu->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a Menu
	 * @param  Request $request menu id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$menu = Feature::find($id);
			if($menu){
				$menu->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the menu edit screen to the user.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
	  	$curFeature = Feature::find($id);
		return view( 'featureManage::feature.edit' )->with([
		    
            'feature'=>$curFeature
				 ]);

	}

	/**
	 * Add new menu data to database
	 *
	 * @return Redirect to menu add
	 */
	public function edit(FeatureRequest $request, $id)
	{
		
		$feature = Feature::find($id);
		$feature->name		= $request->get( 'name' );
		$feature->description			= $request->get( 'desc' );        
		$feature->save();
		
		return redirect( 'feature/edit/'.$id )->with([ 'success' => true,
			'success.message'=> 'Feature updated successfully!',
			'success.title' => 'Good Job!' ]);
	}
}
