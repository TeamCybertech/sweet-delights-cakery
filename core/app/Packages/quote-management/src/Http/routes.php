<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'quote', 'namespace' => 'QuoteManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'quote.list', 'uses' => 'QuoteController@index'
        ]);

        Route::get('json/list', [
            'as' => 'quote.list', 'uses' => 'QuoteController@jsonList'
        ]);
        Route::post('complete', [
            'as' => 'quote.add', 'uses' => 'QuoteController@complete'
        ]);

        Route::get('edit/{id}', [
            'as' => 'quote.edit', 'uses' => 'QuoteController@editView'
        ]);


        Route::post('add', [
            'as' => 'quote.add', 'uses' => 'QuoteController@save'
        ]);

        Route::post('delete', [
            'as' => 'quote.delete', 'uses' => 'QuoteController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'quote.edit', 'uses' => 'QuoteController@update'
        ]);

    });
});