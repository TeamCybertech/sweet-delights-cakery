<?php

Route::group(['namespace' => 'CountryManager\Http\Controllers','middleware' => "auth", 'prefix' => 'country'], function()
{
  Route::get('list', ['as' => 'country.list', 'uses' => 'CountryController@index']);
  Route::get('add', ['as' => 'country.add', 'uses' => 'CountryController@create']);
  Route::get('edit/{id}', ['as' => 'country.edit', 'uses' => 'CountryController@edit']);

  Route::post('add', ['as' => 'country.add', 'uses' => 'CountryController@store']);
  Route::post('edit/{id}', ['as' => 'country.edit', 'uses' => 'CountryController@update']);
  Route::post('delete/{id}', ['as' => 'country.delete', 'uses' => 'CountryController@destroy']);


  Route::resource('country', 'CountryController');
});
