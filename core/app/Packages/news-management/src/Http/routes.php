<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'news', 'namespace' => 'NewsManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'news.add', 'uses' => 'NewsController@addView'
      ]); 
      Route::get('news', [
        'as' => 'news', 'uses' => 'NewsController@news'
      ]);

      Route::get('edit/{id}', [
        'as' => 'news.edit', 'uses' => 'NewsController@editView'
      ]);

      Route::get('list', [
        'as' => 'news.list', 'uses' => 'NewsController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'news.list', 'uses' => 'NewsController@jsonList'
      ]);
      Route::get('json/getSeries', [
        'as' => 'news.list', 'uses' => 'NewsController@getSeries'
      ]);
       Route::get('json/getFeature', [
        'as' => 'news.list', 'uses' => 'NewsController@getFeature'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'news.edit', 'uses' => 'NewsController@jsonImageFileDelete',
    ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'news.add', 'uses' => 'NewsController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'news.edit', 'uses' => 'NewsController@edit'
      ]);

      Route::post('status', [
        'as' => 'news.status', 'uses' => 'NewsController@status'
      ]);

      Route::post('delete', [
        'as' => 'news.delete', 'uses' => 'NewsController@delete'
      ]);
     

    });
});