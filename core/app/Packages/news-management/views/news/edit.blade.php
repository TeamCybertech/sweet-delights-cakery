@extends('layouts.back.master') @section('current_title','Update News')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
    .rating-container .rating-stars:before {
        text-shadow: none;
    }
</style>
<style>
       
        .imageBox
    {
      position: relative;
      width: 300px;
      height: 400px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox .thumbBox
    {
      position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 270px;
      height: 370px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox .spinner
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }
    </style>

 <style>
        
 .imageBox-cover-image
    {
        position: relative;
      width: 800px;
      height: 450px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox-cover-image .thumbBox-cover-image
    {
     position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 770px;
      height: 400px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox-cover-image .spinner-cover-image
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }

    </style>


@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('news/list')}}">News Management</a></li>

        <li class="active">
            <span>New News</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}
                     <input type="hidden" name="front_image" id="front_img" form="form">
                    <input type="hidden" name="cover_image" id="cvr_img" form="form">
                    <div class="form-group"><label class="col-sm-2 control-label">TITLE</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="title" value="{{old("title")? old('title') : $news->title}}"></div>
                    </div>

                    {{-- <div class="form-group"><label class="col-sm-2 control-label">ISSUE DATE / VOLUME NO <sup>*</sup></label>
                        <div class="col-sm-2">
                            <div class="input-group date" id="datepicker">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <input type="text" id="date"  name="date" class="form-control" value="{{ old('date') ? old('date'): $news->date}}"/>
                            </div>
                        </div>
                    </div> --}}

                    <div class="form-group"><label class="col-sm-2 control-label">ISSUE DATE / VOLUME NO</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="valume_no" value="{{ old("volume_no")? old('volume_no') : $news->volume_no}}"></div>
                    </div>
                         <div class="form-group"><label class="col-sm-2 control-label">VIDEO LINK</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="video_link" value="{{ old("video_link")? old('video_link') :$news->video_link}}"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">HOME PAGE</label>
                        <div class="col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" value="1" name="home" {{ $news->status ? "checked" : "" }}>
                        </div>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{old("description")? old('description') : $news->description }}</textarea></div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FRONT IMAGE * </label>
                        <div class="col-sm-10">
                              <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#front-image-crop-modal" >Select Front Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="front-image-cropped">
                                    <img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/news/'.$news->front_image}}' >
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">COVER IMAGE </label>
                        <div class="col-sm-10">
                            <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#cover-image-crop-modal" >Select Cover Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="cover-image-cropped">
                                    <img style="height:160px" src='{{ url('') . '/core/storage/uploads/images/news/'.$news->cover_image}}'>
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>
                             <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGES</label>
                        <div class="col-sm-10">
                            <input id="featured-file" name="featured-file[]" type="file" multiple class="file-loading">
                            <p class="notice">Please use to upload 550px width x 670px height images for better view</p>
                        </div>
                    </div>


                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>


 <!-- crop modal -->
        <div class="modal fade" id="front-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Front Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox">
                    <div class="thumbBox"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFile" style="float:left; width: 250px">

                    <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
 <!-- crop modal -->
        <div class="modal fade" id="cover-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Cover Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox-cover-image">
                    <div class=" thumbBox-cover-image"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFileCoverImage" style="float:left; width: 250px">

                    <input type="button" id="btnZoomInCoverImage" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOutCoverImage" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCropCoverImage">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
        $("#input-1").rating();
		$(".js-source-states").select2({ placeholder: "Select a State"});
          $('.date').datepicker(
            {
                 format: 'yyyy-mm-dd',
            });


		$("#form").validate({
            rules: {
                title: {
                    required: true

                },
                  description: {
                    required: true

                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
//   $("#front-file").fileinput({
//      uploadUrl: "", // server upload action
//      uploadAsync: true,
//      maxFileCount: 1,
//      showUpload:false,
//      initialPreview: [
//      "<img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/news/'.$news->front_image}}'>"

//      ],
//      allowedFileExtensions: ["jpg", "gif", "png"]
//   });
//   $("#cover_image").fileinput({
//      uploadUrl: "", // server upload action
//      uploadAsync: true,
//      maxFileCount: 1,
//      showUpload:false,
//      initialPreview: [
//      "<img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/news/'.$news->cover_image}}'>"],
//      allowedFileExtensions: ["jpg", "gif", "png"]
//   });

  $("#featured-file").fileinput({
            uploadUrl: "", // server upload action
            // uploadAsync: true,
            showUpload:false,
      maxFileCount : 3,
      validateInitialCount : true,
      overwriteInitial: false,

            allowedFileExtensions: ["jpg", "gif", "png"],
      initialPreview: <?php echo json_encode($featured_images ); ?>,
      initialPreviewConfig: <?php echo json_encode($featured_img_config) ?>,
        });

tinymce.init({
                  selector: 'textarea',  // change this value according to your HTML
                  plugins: [
                      'advlist autolink lists link image charmap preview hr anchor pagebreak',
                      'searchreplace wordcount visualblocks visualchars code',
                      'insertdatetime media nonbreaking table contextmenu directionality',
                      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
                  ],
                  toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
                  '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
                  fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
                  menubar: false
              });


</script>
<script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          options.imgSrc = e.target.result;
          cropper = $('.imageBox').cropbox(options);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#front_img').val(img);
    document.querySelector('.front-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#front-image-crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
      cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
      cropper.zoomOut();
  })

</script>
<script type="text/javascript">


  var optionsCI =
  {
      thumbBox: '.thumbBox-cover-image',
      spinner: '.spinner-cover-image',
      imgSrc: 'avatar.png'
  }
  var cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
  $('#cropFileCoverImage').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          optionsCI.imgSrc = e.target.result;
          cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCropCoverImage').on('click', function(){
    var img = cropperCI.getDataURL()
    $('#cvr_img').val(img);
    document.querySelector('.cover-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#cover-image-crop-modal').modal('hide');
  })
  $('#btnZoomInCoverImage').on('click', function(){
      cropperCI.zoomIn();
  })
  $('#btnZoomOutCoverImage').on('click', function(){
      cropperCI.zoomOut();
  })

</script>
@stop
