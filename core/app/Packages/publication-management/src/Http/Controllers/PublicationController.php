<?php

namespace PublicationManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Input;
use Illuminate\Support\Facades\Storage;
use PublicationManager\Models\Publication;
use PublicationManager\PublicationServiceProvider;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class PublicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function SliderList()
    {
        return view('PublicationManager::publication.list');
    }

    public function add()
    {
        return view('PublicationManager::publication.add');
    }

    public function save(Request $request)
    {
        $input = $request->file('imgPublication');
        $path = 'uploads/images/publication_slider';
        $destinationPath = storage_path($path);
        $input->move($destinationPath, $input->getClientOriginalName());

        $pub = new Publication();
        $pub->name = $request->txtName;
        $pub->image = $input->getClientOriginalName();
        $pub->save();

        return redirect('publication/list')->with(['success' => true,
            'success.message' => 'Publication Logo Created successfully!',
            'success.title' => 'Well Done!']);

    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = Publication::where('status', '=', 1)->get();
            $jsonList = array();
            $i = 1;
            $path = '/SDCadmin/core/storage/uploads/images/publication_slider/';

            foreach ($data as $key => $pub) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $pub->name);
                array_push($dd, '<a class="btn btn-xs btn-success publicationImg" href="#" role="button" data-id=" ' . $pub->publication_id . '">Show Image</a>');

                $permissions = Permission::whereIn('name', ['publication.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('publication/edit/' . $pub->publication_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit publication"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['publication.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="publication-delete" data-id="' . $pub->publication_id . '" data-toggle="tooltip" data-placement="top" title="Delete publication"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $pub = Publication::where('publication_id', '=', $id)->first();
            if ($pub) {
                $pub->status = 0;
                $pub->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id)
    {
        $pub = Publication::where('publication_id', '=', $id)->first();
        return view('PublicationManager::publication.edit')->with(['pub' => $pub]);
    }

    public function getImage($id)
    {
        $pub = DB::table('publication')
            ->select('image')
            ->where('publication_id', '=', $id)
            ->first();
        return json_encode($pub);
    }

    public function update(Request $request, $id)
    {

        $input = $request->file('imgPublication');
        $pub = Publication::where('publication_id', '=', $id)->first();
        $pub->name = $request->txtName;
        if (Input::hasfile('imgPublication')) {
            $pub->image = $input->getClientOriginalName();
            $path = 'uploads/images/publication_slider';
            $destinationPath = storage_path($path);
            $input->move($destinationPath, $input->getClientOriginalName());
        }
        $pub->save();


        return redirect('publication/list')->with(['success' => true,
            'success.message' => 'Publication Edited successfully!',
            'success.title' => 'Well Done!']);
    }
}
