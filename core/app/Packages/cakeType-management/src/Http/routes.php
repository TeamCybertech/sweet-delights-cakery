<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'caketype', 'namespace' => 'CakeTypeManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'caketype.list', 'uses' => 'CakeTypeController@index'
        ]);

        Route::get('json/list', [
            'as' => 'caketype.list', 'uses' => 'CakeTypeController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'caketype.add', 'uses' => 'CakeTypeController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'caketype.edit', 'uses' => 'CakeTypeController@editView'
        ]);


        Route::post('add', [
            'as' => 'caketype.add', 'uses' => 'CakeTypeController@save'
        ]);

        Route::post('delete', [
            'as' => 'caketype.delete', 'uses' => 'CakeTypeController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'caketype.edit', 'uses' => 'CakeTypeController@update'
        ]);

    });
});