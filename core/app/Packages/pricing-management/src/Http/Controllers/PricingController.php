<?php

namespace PricingManage\Http\Controllers;

use App\Http\Controllers\Controller;
use PricingManage\Http\Requests\PricingRequest;
use Illuminate\Http\Request;
use PricingManage\Models\Pricing;
// use BlogManage\Models\BlogImage;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;
use Image;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
class PricingController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Pricing Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

     /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function addView() {
     
      return view('PricingManage::pricing.add' );
      
      
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      try {
        $pricing=Pricing::where('id', $id)->first();
         return view('PricingManage::pricing.edit' )->with(['pricing'=>$pricing]);
      } catch (ModelNotFoundException $e) {
         return view('errors.404');
      }
     
      
      
    }
      /**
     * View Pricing  List View
     *
     * @return Response
     */
    public function listView()
    {
      $pricing = Pricing::all();
      return view( 'PricingManage::pricing.list', compact('pricing'));
    }
    /**
   * Honor  list
   *
   * @return Response
   */
  public function jsonList(Request $request)
  {
    if($request->ajax()){
      $allHonor= Honor::where('status','=',1)->get();
      $jsonList = array();
      $i=1;
      foreach ($allHonor as $key => $honor) {

        $dd = array();
        array_push($dd, $i);
        array_push($dd,$honor->name);
        array_push($dd,$honor->description);

        $permissions = Permission::whereIn('name',['honor.edit','admin'])->where('status','=',1)->lists('name');
        if(Sentinel::hasAnyAccess($permissions)){
          array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('honor/edit/'.$honor->id).'\'" data-toggle="tooltip" data-placement="top" title="Honor"><i class="fa fa-pencil"></i></a></center>');
        }else{
          array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
        }

        $permissions = Permission::whereIn('name',['honor.delete','admin'])->where('status','=',1)->lists('name');
        if(Sentinel::hasAnyAccess($permissions)){
          array_push($dd, '<center><a href="#" class="honor-delete" data-id="'.$honor->id.'" data-toggle="tooltip" data-placement="top" title="Delete Honor"><i class="fa fa-trash-o"></i></a></center>');
        }else{
          array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
        }

        array_push($jsonList, $dd);
        $i++;
      }
      return Response::json(array('data'=>$jsonList));
    }else{
      return Response::json(array('data'=>[]));
    }
  }

    public function add(Request $request)
    {
      // return $request->all();
       if (!file_exists(storage_path('uploads/images/honor'))) {

        File::makeDirectory(storage_path('uploads/images/honor'));
      }
      $path='uploads/images/honor';
      $fileName='';

       if ($request->hasFile('front_image')) {
          $file = $request->file('front_image');
          $extn =$file->getClientOriginalExtension();
          $destinationPath = storage_path($path);
          $fileName = 'honor-' .date('YmdHis') .  '.' . $extn;
          $file->move($destinationPath, $fileName); 
       }
      $honor=Honor::create([
          'name'=>$request->get('name'),
          'description'=>$request->get('description'),
          'filename'=>$fileName,
          'path'=>$path,
          'url'=>$request->get('url'),
          'more_link'=>$request->get('more_link'),
          'type'=>$request->get('type')
        ]);
       return redirect( 'honor/list')->with([ 'success' => true,
          'success.message'=> 'Honor Created successfully!',
          'success.title' => 'Good Job!' ]);
     
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request,$id) {

      // dd($request->all());

      if (!file_exists(storage_path('uploads/images/pricing'))) {

        File::makeDirectory(storage_path('uploads/images/pricing'));
     }
     

      $path='uploads/images/pricing';
      $fileName='';

      $pricing=Pricing::find($id);
      $pricing->title=$request->get('title');
      $pricing->description=$request->get('description');
      
      if ($request->hasFile('front_image')) {
          $file = $request->file('front_image');
          $extn =$file->getClientOriginalExtension();
          $destinationPath = storage_path($path);
          
          $fileName = 'pricing-' .date('YmdHis') .  '.' . $extn;
          $file->move($destinationPath, $fileName); 
          $pricing->img = $fileName;

       }
       $pricing->save();

   
       
      return redirect( 'pricing/edit/'.$id )->with([ 'success' => true,
          'success.message'=> 'pricing  updated successfully!',
          'success.title' => 'Good Job!' ]);
    }
    public function delete(Request $request)
      {
        if($request->ajax()){
          $id = $request->input('id');

          $honor = Honor::find($id);
          if($honor){
                    $honor->delete();
            return response()->json(['status' => 'success']);
          }else{
            return response()->json(['status' => 'invalid_id']);
          }
        }else{
          return response()->json(['status' => 'not_ajax']);
        }
      }

}
