<?php
/**
 * PrivateClass MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'privateclass/category', 'namespace' => 'PrivateClassManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'privateclass.category.add', 'uses' => 'PrivateClassController@addView_category'
      ]);

      Route::get('edit/{id}', [
        'as' => 'privateclass.category.edit', 'uses' => 'PrivateClassController@editView_category'
      ]);

      Route::get('list', [
        'as' => 'privateclass.category.list', 'uses' => 'PrivateClassController@listView_category'
      ]);

      Route::get('json/list', [
        'as' => 'privateclass.category.list', 'uses' => 'PrivateClassController@jsonList_category'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'privateclass.category.add', 'uses' => 'PrivateClassController@add_category'
      ]);

      Route::post('edit/{id}', [
        'as' => 'privateclass.category.edit', 'uses' => 'PrivateClassController@edit_category'
      ]);

      Route::post('status', [
        'as' => 'privateclass.category.status', 'uses' => 'PrivateClassController@status_category'
      ]);

      Route::post('delete', [
        'as' => 'privateclass.category.delete', 'uses' => 'PrivateClassController@delete_category'
      ]);
        Route::delete('image/deleteFile', [
      'as' => 'privateclass.edit', 'uses' => 'PrivateClassController@jsonImageFileDelete_category',
    ]);
    });
    Route::group(['prefix' => 'privateclass', 'namespace' => 'PrivateClassManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'privateclass.add', 'uses' => 'PrivateClassController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'privateclass.edit', 'uses' => 'PrivateClassController@editView'
      ]);

      Route::get('list', [
        'as' => 'privateclass.list', 'uses' => 'PrivateClassController@listView'
      ]);
 
      Route::get('json/list', [
        'as' => 'privateclass.list', 'uses' => 'PrivateClassController@jsonList'
      ]);

      Route::get('registration/list', [
        'as' => 'privateclass.list', 'uses' => 'PrivateClassController@registrationListView'
      ]);
      
      Route::get('registration/json/list', [
        'as' => 'privateclass.list', 'uses' => 'PrivateClassController@registrationJsonList'
      ]);


      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'privateclass.add', 'uses' => 'PrivateClassController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'privateclass.edit', 'uses' => 'PrivateClassController@edit'
      ]);

      Route::post('status', [
        'as' => 'privateclass.status', 'uses' => 'PrivateClassController@status'
      ]);

      Route::post('delete', [
        'as' => 'privateclass.delete', 'uses' => 'PrivateClassController@delete'
      ]);
      
      Route::delete('image/deleteFile', [
      'as' => 'privateclass.edit', 'uses' => 'PrivateClassController@jsonImageFileDelete',
    ]);

      Route::post('registration/changeStatus', [
        'as' => 'privateclass.delete', 'uses' => 'PrivateClassController@changeStatus'
      ]);
    });
});
