<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PrivateClassManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Description of PrivetclassDateTime
 *
 * @author chathuranga-PC
 */
class PrivetclassDateTime extends Model {
     protected $table = "sa_privetclass_date_time"; 
     
     protected $fillable = ['privateclass_cat_id','date','start','end'];

     public function getStart24Format()
     {
          return \Carbon\Carbon::createFromTimeString($this->start)->format('g:i A');
     }
     public function getEnd24Format()
     {
          return \Carbon\Carbon::createFromTimeString($this->end)->format('g:i A');
     }

     public function getFullDate()
     {
          return \Carbon\Carbon::createFromFormat('Y-m-d', $this->date)->format('l d, M Y');
     }
     
}
