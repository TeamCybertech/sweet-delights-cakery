<?php
namespace PrivateClassManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;

/**
 * PrivateClass Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class PrivateClassRegistration extends Model{
	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'private_class_registation';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['class_datetime_id','parent_is_reg','parent_full_name','first_name','last_name','phone','email','status'];

	public function getPrivateClassCat()
	{
		return $this->belongsTo('PrivateClassManage\Models\PrivateClassCategory', 'class');
	}
	
	public function getDateTime()
	{
		return $this->belongsTo('PrivateClassManage\Models\PrivetclassDateTime', 'class_datetime_id');
	}
	public function sendAdminRegEmail()
	{
		try {
			$adminEmail = env("ADMIN_EMAIL");
			
			$firstname = $this->first_name;
			$lastName = $this->last_name;

			Mail::send('emails.privateClassRegEmail',['details' => $this], function($message) use($adminEmail, $firstname, $lastName){
				$message->to($adminEmail, $adminEmail)->subject('New Private Class Registration | sweetdelightscakery.com | '.$firstname.' '.$lastName);
			});
		} catch (\Throwable $th) {
			//throw $th;
		}
		
	}
	

}
