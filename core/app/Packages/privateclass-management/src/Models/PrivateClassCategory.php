<?php
namespace PrivateClassManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * PrivateClass Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class PrivateClassCategory extends Model{
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_privateclass_category';
    
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','name','description','category','datetime','noofseat','price','path','filename','created_by','status','skill_level'];

	public function getImages()
	{
		return $this->hasMany('PrivateClassManage\Models\PrivateClassImage', 'privateclass_id', 'id');
               
	}

	public function getDateTime()
	{
		return $this->hasMany('PrivateClassManage\Models\PrivetclassDateTime', 'privateclass_cat_id', 'id');
	}

	public function getSkillLevel()
	{
		return $this->belongsTo('PrivateClassManage\Models\PrivateClassSkillLevel', 'skill_level', 'id');
	}
}
