@extends('layouts.back.master') @section('current_title','Update Private Class Category')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
<style media="screen">
.imageBox
{
  position: relative;
  height: 670px;
  width: 450px;
  border:1px solid #aaa;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor:move;
}

.imageBox .thumbBox
{
  position: absolute;
  top: 22%;
  left: 33%;
  width: 400px;
  height: 620px;
  margin-top: -124px;
  margin-left: -124px;
  box-sizing: border-box;
  border: 1px solid rgb(102, 102, 102);
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  text-align: center;
  line-height: 400px;
  background: rgba(0,0,0,0.7);
}
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('privateclass/category/list')}}">Private Class Category </a></li>

        <li class="active">
            <span>Update Private Class Category</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}
                    <input type="hidden" name="featuredCrop" id="featuredCrop" value="">

                    <div class="form-group"><label class="col-sm-2 control-label">NAME <sup>*</sup></label>
                        <div class="col-sm-10"><input type="text" value="{{$privateClassCategory->name}}" class="form-control" name="name"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description">{{$privateClassCategory->description}}</textarea>
                        </div>
                    </div>
                   <!-- <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                         <div class="col-sm-10">
                         <select class="js-source-states" multiple="multiple" name="class_category[]" style="width: 100%">
                         <?php foreach ($privateClassType as $key => $value): ?>
                          <?php if (count($cur_category)>0): ?>
                              <?php if (in_array($value->id, $cur_category)): ?>
                                <option selected="true" value="{{$value->id}}">{{$value->name}}</option>
                            <?php else: ?>
                                <option  value="{{$value->id}}">{{$value->name}}</option>
                            <?php endif ?>
                          <?php else: ?>
                            <option  value="{{$value->id}}">{{$value->name}}</option>

                          <?php endif ?>


                         <?php endforeach ?>


                        </select>
                    </div>

                    </div> -->
                    <div class="form-group"><label class="col-sm-2 control-label">SKILL LEVEL <sup>*</sup></label>
                         <div class="col-sm-10">
                             <select class="standardSelect required js-source-states" id="skill_level"  name="skill_level" style="width: 100%">
                               <!-- <option value="" selected>Select Skill Level</option> -->
                                <?php foreach ($privateClassSkillLevel as $key => $value): ?>
                                    <option value="{{$value->id}}" {{ ($privateClassCategory->skill_level == $value->id?'selected':'') }}>{{$value->name}}</option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>    

                    <div class="form-group"><label class="col-sm-2 control-label">DATE/TIME <sup>*</sup></label>
                        <div class="col-sm-9 datetime_wrap">
                        <?php foreach ($PrivetclassDateTime as $key => $value): ?>
                        <div class="row">
                                <div class="col-sm-3">
                                    <div class="input-group date datetimepicker1" id="datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                         <input type="text" name="datetime[]" placeholder="Date" class="form-control" value={{ $value->date }}>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date datetimepicker2" id="datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                         <input type="text" name="fromtime[]" placeholder="Start Time" class="form-control"  value={{ $value->start }}>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date datetimepicker2" id="datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                         <input type="text" name="totime[]" placeholder="End Time" class="form-control"  value={{ $value->end }}>
                                    </div>

                                </div>
                                <div class="col-sm-1">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-success plus" type="button" name="add">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                                <?php endforeach ?>
                            
                        </div>

                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">NO OF SEATS</label>
                        <div class="col-sm-10"><input type="text" value="{{$privateClassCategory->noofseat}}" class="form-control" name="noofseat"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PRICE</label>
                        <div class="col-sm-10"><input type="text" value="{{$privateClassCategory->price}}" class="form-control" name="price"></div>
                    </div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGE</label>
                        <div class="col-sm-6 ">
                            <div class="row">
                                <div class="imageBox">
                                    <div class="thumbBox"></div>
                                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                                </div>
                                <div class="action">
                                    <input type="file" id="file" style="float:left; ">
                                    <input type="button" id="btnCrop" value="Crop" style="float: right">
                                    <input type="button" id="btnZoomIn" value="+" style="float: right">
                                    <input type="button" id="btnZoomOut" value="-" style="float: right">
                                </div>

                            </div>
                                <div class="row" style="margin-top : 30px;">
                                    <div class="cropped">
                                    <img style="max-width : 100%;" src="{{ url('') . '/core/storage/' .$privateClassCategory->path.'/'.$privateClassCategory->filename}}">
                                    </div>
                                </div>
                            

                            </div>
                            
                            
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">SUB IMAGES <sup>*</sup></label>
                        <div class="col-sm-10">
                            <input id="sub_image" name="sub_image[]" type="file" multiple class="file-loading">
                        </div>
                    </div>


                    <div class="hr-line-dashed"></div>

	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/moment/moment.js')}}"></script>
<script src="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $(".js-source-states").select2();
        $('.datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('.datetimepicker2').datetimepicker({
            format: 'HH:mm'
        });
        $('.datetimepicker3').datetimepicker({
            format: 'HH:mm'
        });

        $('.plus').click(function(e){
            e.preventDefault();
            var wrapper = $('.datetime_wrap');
            $(wrapper).append('<div class="row dynamic"><br><div class="col-sm-3"><div class="input-group date datetimepicker1" id="datetimepicker1"><span class="input-group-addon"><span class="fa fa-calendar"></span></span><input type="text" name="datetime[]" placeholder="Date" class="form-control"/></div></div><div class="col-sm-3"><div class="input-group date datetimepicker2" id="datetimepicker1"><span class="input-group-addon"><span class="fa fa-calendar"></span></span><input type="text" name="fromtime[]" placeholder="Start Time" class="form-control"/></div></div><div class="col-sm-3"><div class="input-group date datetimepicker3" id="datetimepicker1"><span class="input-group-addon"><span class="fa fa-calendar"></span></span><input type="text" name="totime[]" placeholder="End Time" class="form-control"/></div></div><div class="col-sm-1"><div class="input-group"><span class="input-group-btn"><button class="btn btn-danger minus" type="button" name="remove"><i class="glyphicon glyphicon-minus"></i></button></span></div></div></div>');
            $(wrapper).find(".datetimepicker1").datetimepicker({format: 'YYYY-MM-DD'});
            $(wrapper).find(".datetimepicker2").datetimepicker({format: 'HH:mm'});
            $(wrapper).find(".datetimepicker3").datetimepicker({format: 'HH:mm'});
        });
        $(wrapper).on("click", ".minus", function(e){
            e.preventDefault();
            $(this).parents('div.dynamic').html("");
        });


        $("#form").validate({
            rules: {
                name: {
                    required: true

                },

                noofseat: {
                    digits: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $("#featured_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            showUpload:false,
            initialPreview: [
            "<img style='height:160px' src='{{ url('') . '/core/storage/' .$privateClassCategory->path.'/'.$privateClassCategory->filename}}'>"
            ],
            allowedFileExtensions: ["jpg", "gif", "png"]
        });
        $("#sub_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 5,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"],
            initialPreview: <?= json_encode($images ); ?>,
            initialPreviewConfig: <?=json_encode($image_config) ?>,

        });
	});


</script>
<script src="{{asset('assets/back/cropbox/cropbox.js')}}" type="text/javascript"></script>

<script type="text/javascript">

        var options =
        {
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
        var cropper = $('.imageBox').cropbox(options);
        $('#file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = $('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        $('#btnCrop').on('click', function(){
            var img = cropper.getDataURL();
            $("#featuredCrop").val(img)
            $('.cropped').html('<img style="max-width : 100%;" src="'+img+'">');
        })
        $('#btnZoomIn').on('click', function(){
            cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
            cropper.zoomOut();
        })

  </script>
@stop
