@extends('layouts.back.master') @section('current_title','Add New tutorial category')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
    <style>
        .rating-container .rating-stars:before {
            text-shadow: none;
        }
    </style>

@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('recipesCategory/list')}}">Tutorial category management</a></li>

            <li class="active">
                <span>Add category</span>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form class="form-horizontal" id="form" method="post" files="true">
                        {!!Form::token()!!}
                        <div class="form-group"><label class="col-sm-2 control-label">Tutorial category <sup>*</sup></label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="txtType"
                                                          placeholder="Enter Recipes Category"></div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                                </button>
                                <button class="btn btn-primary" type="submit">Done</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#form").validate({
                rules: {
                    txtType: {
                        required: true
                    },
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });

    </script>
@stop