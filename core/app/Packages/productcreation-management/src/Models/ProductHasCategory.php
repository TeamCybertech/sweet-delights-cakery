<?php
namespace ProductCreationManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * ProductHasCategory Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class ProductHasCategory extends Model{
	// use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_product_has_category_many';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['product_id','category_id'];

	public function getProduct()
	{
		return $this->belongsTo('ProductCreationManage\Models\ProductCreation', 'product_id', 'id');
	}
	public function getProductCategory()
	{
		return $this->hasOne('ProductCreationManage\Models\ProductCreationCategory', 'id', 'category_id');
	}





	

}
