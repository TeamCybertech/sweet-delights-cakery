<?php

namespace ProductCreationManage;

use Illuminate\Support\ServiceProvider;

class ProductCreationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'ProductCreationManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProductCreationManage', function($app){
            return new ProductCreationManage;
        });
    }
}
