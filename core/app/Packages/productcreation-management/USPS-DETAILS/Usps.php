<?php

/**
 * Available Laravel Methods
 * Add other USPS API Methods
 * Based on Vincent Gabriel @VinceG USPS PHP-Api https://github.com/VinceG/USPS-php-api
 *
 * @since  1.0
 * @author John Paul Medina
 * @author Vincent Gabriel
 */

namespace Usps;

function __autoload($class_name) {
    include $class_name . '.php';
}

class Usps {

    private $config;

    public function __construct($config) {
        $this->config = $config;
    }

    public function validate($street, $zip, $apartment = false, $city = false, $state = false) {
        $verify = new AddressVerify($this->config['username']);
        $address = new Address;
        $address->setFirmName(null);
        $address->setApt($apartment);
        $address->setAddress($street);
        $address->setCity($city);
        $address->setState($state);
        $address->setZip5($zip);
        $address->setZip4('');

        // Add the address object to the address verify class
        $verify->addAddress($address);

        // Perform the request and return result
        $val1 = $verify->verify();
        $val2 = $verify->getArrayResponse();

        // var_dump($verify->isError());

        // See if it was successful
        if ($verify->isSuccess()) {
            return ['address' => $val2['AddressValidateResponse']['Address']];
        } else {
            return ['error' => $verify->getErrorMessage()];
        }


    }
       public function domesticRate($sevice_type, $mail_type, $zip_origination, $zip_destination, $pounds, $ounces, $size)
    {
       // Initiate and set the username provided from usps
        $rate = new Rate($this->config['username']);

        // During test mode this seems not to always work as expected
        //$rate->setTestMode(true);

        // Create new package object and assign the properties
        // apartently the order you assign them is important so make sure
        // to set them as the example below
        // set the RatePackage for more info about the constants
        $package = new RatePackage;
        $package->setService($sevice_type);
        $package->setFirstClassMailType($mail_type);
        $package->setZipOrigination($zip_origination);
        $package->setZipDestination($zip_destination);
        $package->setPounds($pounds);
        $package->setOunces($ounces);
        $package->setContainer('');
        $package->setSize($size);
        $package->setField('Machinable', true);

        // add the package to the rate stack
        $rate->addPackage($package);
        $rate->getRate();

        // Perform the request and print out the result
        // print_r($rate->getRate());
        // print_r($rate->getArrayResponse());

        // Was the call successful
        if ($rate->isSuccess()) {
          return  $rate->getArrayResponse();
        } else {
            echo 'Error: ' . $rate->getErrorMessage();
        }
    }
}
