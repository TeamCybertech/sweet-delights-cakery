<?php

namespace ConfigManage\Http\Controllers;

use App\Http\Controllers\Controller;
use ConfigManage\Models\Config;
use Input;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use SliderManager\Models\Slider;
use Validator;
use Image;
use Brotzka\DotenvEditor\DotenvEditor;

class ConfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    public function add()
    {
        $config = Config::first();
        return view('ConfigManage::config.add')->with(['config' => $config]);
    }

    public function save(Request $request)
    {
      $env = new DotenvEditor();

      $env->changeEnv([
            'FACEBOOK_CLIENT_ID'   => $request->facebook_client_id,
            'FACEBOOK_CLIENT_SECRET'   => $request->facebook_client_secret,
            'FACEBOOK_REDIRECT'   => $request->facebook_redirect_url,
            'GOOGLE_CLIENT_ID'   => $request->google_client_id,
            'GOOGLE_CLIENT_SECRET'   => $request->google_client_secret,
            'GOOGLE_REDIRECT'   => $request->google_redirect_url
        ]);

        $config = Config::first();
        if ($config) {
            if (isset($request->bg_img)) {
                $input = $request->file('bg_img');
                $path = 'uploads/images/bg_img';
                $destinationPath = storage_path($path);
                $input->move($destinationPath, $input->getClientOriginalName());

                $config->company_name = $request->company_name;
                $config->zip = $request->zip_code;
                $config->email = $request->email;
                $config->phone = $request->phone;
                $config->location = $request->location;
                $config->background_image = $input->getClientOriginalName();
                $config->fb = $request->fb_page;
                $config->fb_api = $request->fb_api;
                $config->instagram = $request->instagram;
                $config->youtube = $request->youtube;
                $config->tasting_days = $request->t_d;
                $config->tasting_err = $request->t_err;
                $config->qoute_days = $request->q_d;
                $config->qoute_err = $request->q_err;
                $config->working_time = $request->working_days;
                $config->google_api = $request->google_api;
                $config->twitter = $request->twitter;
                $config->pinterest = $request->pinterest;
                $config->promotion_text = $request->promotion_text;
                $config->promotion_link = $request->promotion_link;
                // $config->tasting_success = $request->tasting_success;
                $config->save();

                return redirect('config/add')->with(['success' => true,
                    'success.message' => 'Configuartion Save successfully!',
                    'success.title' => 'Well Done!']);
            }
            $config->company_name = $request->company_name;
            $config->zip = $request->zip_code;
            $config->email = $request->email;
            $config->phone = $request->phone;
            $config->location = $request->location;
            $config->fb = $request->fb_page;
            $config->fb_api = $request->fb_api;
            $config->instagram = $request->instagram;
            $config->youtube = $request->youtube;
            $config->tasting_days = $request->t_d;
            $config->tasting_err = $request->t_err;
            $config->qoute_days = $request->q_d;
            $config->qoute_err = $request->q_err;
            $config->working_time = $request->working_days;
            $config->twitter = $request->twitter;
            $config->google_api = $request->google_api;
            $config->pinterest = $request->pinterest;
            $config->promotion_text = $request->promotion_text;
            $config->promotion_link = $request->promotion_link;

            // $config->tasting_success = $request->tasting_success;
            $config->save();

            return redirect('config/add')->with(['success' => true,
                'success.message' => 'Configuartion Save successfully!',
                'success.title' => 'Well Done!']);


        } else {
            if (isset($request->bg_img)) {
                $input = $request->file('bg_img');
                $path = 'uploads/images/bg_img';
                $destinationPath = storage_path($path);
                $input->move($destinationPath, $input->getClientOriginalName());
                $config = new Config();
                $config->company_name = $request->company_name;
                $config->zip = $request->zip_code;
                $config->email = $request->email;
                $config->phone = $request->phone;
                $config->location = $request->location;
                $config->background_image = $input->getClientOriginalName();
                $config->fb = $request->fb_page;
                $config->fb_api = $request->fb_api;
                $config->instagram = $request->instagram;
                $config->youtube = $request->youtube;
                $config->tasting_days = $request->t_d;
                $config->tasting_err = $request->t_err;
                $config->qoute_days = $request->q_d;
                $config->qoute_err = $request->q_err;
                $config->working_time = $request->working_days;
                $config->google_api = $request->google_api;
                $config->twitter = $request->twitter;
                $config->pinterest = $request->pinterest;
                $config->promotion_text = $request->promotion_text;
                $config->promotion_link = $request->promotion_link;
                // $config->tasting_success = $request->tasting_success;
                $config->save();
            }

            $config = new Config();
            $config->company_name = $request->company_name;
            $config->zip = $request->zip_code;
            $config->email = $request->email;
            $config->phone = $request->phone;
            $config->location = $request->location;
            $config->fb = $request->fb_page;
            $config->fb_api = $request->fb_api;
            $config->instagram = $request->instagram;
            $config->youtube = $request->youtube;
            $config->tasting_days = $request->t_d;
            $config->tasting_err = $request->t_err;
            $config->qoute_days = $request->q_d;
            $config->qoute_err = $request->q_err;
            $config->working_time = $request->working_days;
            $config->google_api = $request->google_api;
            $config->twitter = $request->twitter;
            $config->pinterest = $request->pinterest;
            $config->promotion_text = $request->promotion_text;
            $config->promotion_link = $request->promotion_link;

            // $config->tasting_success = $request->tasting_success;
            $config->save();

            return redirect('config/add')->with(['success' => true,
                'success.message' => 'Configuartion Save successfully!',
                'success.title' => 'Well Done!']);

        }


    }
}
