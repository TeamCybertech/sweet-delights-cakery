@extends('layouts.back.master') @section('current_title','New Product')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
    .rating-container .rating-stars:before {
        text-shadow: none;
    }
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('product/list')}}">Product Management</a></li>
       
        <li class="active">
            <span>New Product</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}

                	<div class="form-group"><label class="col-sm-2 control-label">DEVICE <sup>*</sup></label>
                        <div class="col-sm-10">
                             <select class="js-source-states" style="width: 100%" name="device" id="device" onchange="loadSeries();">
                                <option value="0">SELECT A DEVICE</option>
                                <?php foreach ($devices as $key => $value): ?>
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                <?php endforeach ?>
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">SERIES</label>
                        <div class="col-sm-10">
                            <select class="js-source-states" style="width: 100%" name="series" id="series" onchange="">
                                <option value="0">SELECT A SERIES</option>
                                
                                
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">MODEL NUMBER</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="model_number"></div>
                    </div>
                   
                    <div class="form-group"><label class="col-sm-2 control-label">LAUNCH DATE</label>
                        <div class="col-sm-2">
                            <div class="input-group date" >
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                <input type="text" id="date"  name="launch_date" class="form-control" value="{{date('Y-m-d')}}"/>
                            </div>
                       </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">OLD PRICE</label>
                    <label class="col-md-1 control-label">RS</label>
                        <div class="col-sm-2"><input type="text" class="form-control" name="old_price"></div>


                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">PRICE</label>
                     <label class="col-md-1 control-label">RS</label>
                        <div class="col-sm-2"><input type="text" class="form-control" name="price"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGES</label>
                        <div class="col-sm-10">
                            <input id="project-file" name="project-file[]" type="file" multiple class="file-loading">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control"></textarea></div>
                        
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">FEATURES</label>
                          <div class="col-lg-10">
                                <div class="hpanel ">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                       FEATURES
                                    </div>
                                    <div class="panel-body list">
                                        <div class="table-responsive project-list">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th colspan="2">NAME</th>                           
                                                    <th>DESCRIPTION</th>
                                                    <th>VALUE</th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($features as $key => $value): ?>
                                                    <tr>
                                                    <td><input type="checkbox" name="feature_id[]" class="i-checks" value="{{$value->id}}" ></td>
                                                    <td>{{$value->name}}</td>                       
                                                    <td>
                                                        {{$value->description}}
                                                    </td>
                                                    <td><input type="text" class="form-control" name="feature_value[{{$value->id}}]"  ></td>
                                                    
                                                </tr>
                                                    
                                                <?php endforeach ?>
                                                
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">SPECIAL FEATURES</label>
                         <div class="col-sm-10"><textarea name="special_feature" class="form-control"></textarea></div>
                        
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">IN STOCK</label>
                        <div class="col-sm-10"><input type="checkbox" checked="" name="instock" class="i-checks"  ></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">COLOURS</label>
                        <div class="col-sm-10">
                             <select class="js-source-states" style="width: 100%" name="colour[]" multiple="multiple">
                                <?php foreach ($colour as $key => $value): ?>
                                    <option value="{{$value->name}}">{{$value->name}}</option>
                                <?php endforeach ?>
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">RATE</label>
                        <div class="col-sm-10"><input id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false"></div>
                    </div>
                	
                	
                	
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
        $("#input-1").rating();
		$(".js-source-states").select2({ placeholder: "Select a State"});
          $('.date').datepicker(
            {
                 format: 'yyyy-mm-dd',
            });
       
        $("#project-file").fileinput({
        allowedFileExtensions: ['jpg', 'png', 'gif'],
         previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        'showUpload': false,
            overwriteInitial: true,
            removeIcon: '<i class="glyphicon glyphicon-trash"></i>',



        });


		$("#form").validate({
            rules: {
                name: {
                    required: true
                  
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
    function loadSeries() {
        var selected_device = $('#device').val();
        console.log(selected_device);
    }
     function loadSeries(){
        $('#series').empty();
        $('#series').select2("val", "0");
        // $('#series').append(
        //                 '<option value="0">SELECT A SERIES</option>'
        //                 );
        var selected_device=$('#device').val();
        if(selected_device!=0){
             $.ajax({
              method: "GET",
              url: '{{url('product/json/getSeries')}}',
              data:{ 'id' : selected_device  }
            })
              .done(function( msg ) {
                $vehicleDetailArray=jQuery.parseJSON(JSON.stringify(msg));
                console.log($vehicleDetailArray['data']);
                $.each($vehicleDetailArray['data'], function( index, value ) {
                    $('#series').append(
                        '<option value="'+value['id']+'">'+value['name']+'</option>'
                        );
                 
                });
                 
                    
              });    
        }
    }
	
	
</script>
@stop