<?php
namespace ProductManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Product Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Blog extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_blog';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['description'];

	public function getImages()
	{
		return $this->hasMany('ProductManage\Models\BlogImage', 'blog_id', 'id');
	}

	

}
