<?php
namespace ProductManage\Http\Controllers;

use App\Http\Controllers\Controller;
use ProductManage\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use ProductManage\Models\Product;
use ProductManage\Models\Colour;
use ProductManage\Models\ProductFeature;
use ProductManage\Models\Blog;
use ProductManage\Models\ProductImage;
use ProductManage\Models\BlogImage;
use DeviceManage\Models\Device;
use SeriesManage\Models\Series;
use Permissions\Models\Permission;
use FeatureManage\Models\Feature;
use Sentinel;
use Response;
use File;


class ProductController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Product Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$devices=Device::where('status','=',1)->get();		
		$features=Feature::where('status','=',1)->get();
		$colour=Colour::all();
		return view( 'productManage::product.add')->with(['devices'=>$devices,'features'=>$features,'colour'=>$colour]);;
	}
	public function blog()
		{
			$curblog=Blog::with(['getImages'])->find(1);
			   $image_url_array=[];
			 $image_config_array=[];
			  foreach ($curblog->getImages as $key => $value) {
					array_push($image_url_array,"<img style='height:160px' src='". url('') . '/core/storage/' .$value->path.'/'.$value->filename."'>");

			  }

			  foreach ($curblog->getImages as $key => $value) {
					array_push($image_config_array,array('caption'=>$value->filename,'caption'=>$value->filename,'key'=>$value->id,'url'=>url('product/blog/deleteFile')));

			  }
			return view( 'productManage::product.blog')->with([ 
				'curBlog' => $curblog	,
				'images'=>$image_url_array,'image_config'=>$image_config_array					

				 ]);
		}
		public function blogEdit(Request $request)
		{
		$description=$request->get('description');
		$blog=Blog::find(1);		
		$blog->description=$description;		
		$blog->save();

		 if (!file_exists(storage_path('uploads/images/blog'))) {

				File::makeDirectory(storage_path('uploads/images/blog'));
		 }
		 $path='uploads/images/blog';

		$project_files = $request->file('project-file');
			$i=0;
			foreach ($project_files as $key => $project_file) {
				if (File::exists($project_file)) {
						$file = $project_file;
						$extn =$file->getClientOriginalExtension();
						$destinationPath = storage_path($path);
						$project_fileName = 'blog-' .date('YmdHis') .'-'.$i. '.' . $extn;
						$file->move($destinationPath, $project_fileName);

						// $img = InterventionImage::make($destinationPath.'/'.$project_fileName);
						// $img->fit(228, 151);
						// $img->save($destinationPath.'/'.$project_fileName);


						BlogImage::create([
							'blog_id'=>$blog->id,
							'path'=>$path,
							'filename'=>$project_fileName
						]);
						$i++;
				 }
			}
			return redirect( 'product/blog/')->with([ 'success' => true,
					'success.message'=> 'Blog updated successfully!',
					'success.title' => 'Good Job!' ]);
		}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(Request $request)
	{

		
		$name=$request->get('name');
		$model_number=$request->get('model_number');
		$device=$request->get('device');
		$series=$request->get('series');
		$launch_date=$request->get('launch_date');
		$description=$request->get('description');
		$special_feature=$request->get('special_feature');
		$price=$request->get('price');
		$feature_id=$request->get('feature_id');
		$old_price=$request->get('old_price');
		$rate=$request->get('rate');
		$instock=$request->get('instock');
		if ($instock=='on') {
			$instock=1;
		}
		$feature_value=$request->get('feature_value');
		$file = $request->file('file');
		$colour = $request->get( 'colour' );
		if (count($colour)==0) {
			$colour=[];
		}

		
	

		$product=Product::Create([
			'name'=> $name,
			'model_no'=>$model_number,
			'launch_date'=>$launch_date,
			'series_id'=>$series,
			'device_id'=>$device,
			'description'=>$description,
			'special_feature'=>$special_feature,
			'price'=>$price,
			'old_price'=>$old_price,
			'instock'=>$instock,
			'colour'=>json_encode( $colour ),
			'rate'=>$rate,
			'created_by'=>Sentinel::getUser()->id
			]
			
			);
		if($product){
			if (count($feature_id)>0) {
				foreach ($feature_id as $key => $value) {
					
					ProductFeature::Create([
						'product_id'=>$product->id,
						'feature_value'=>$feature_value[$value],
						'feature_id'=>$value
						]);

				}
			}
			
		}

		 if (!file_exists(storage_path('uploads/images/product'))) {

				File::makeDirectory(storage_path('uploads/images/product'));
		 }
		 $path='uploads/images/product';

		
		if($product){
			$project_files = $request->file('project-file');
			$i=0;
			foreach ($project_files as $key => $project_file) {
				if (File::exists($project_file)) {
						$file = $project_file;
						$extn =$file->getClientOriginalExtension();
						$destinationPath = storage_path($path);
						$project_fileName = 'product-' .date('YmdHis') .'-'.$i. '.' . $extn;
						$file->move($destinationPath, $project_fileName);

						// $img = InterventionImage::make($destinationPath.'/'.$project_fileName);
						// $img->fit(228, 151);
						// $img->save($destinationPath.'/'.$project_fileName);


						ProductImage::create([
							'product_id'=>$product->id,
							'path'=>$path,
							'filename'=>$project_fileName
						]);
						$i++;
				 }
			}

			return redirect('product/add')->with([ 'success' => true,
				'success.message'=> 'Product Created successfully!',
				'success.title' => 'Well Done!']);
		}
		


		

		// $count= Device::where('name', '=',$request->get('name' ))->count();
		// if ($count==0) {
		// 	Device::Create(array(
		// 		'name' => $request->input('name')
				
		// 		)
		// 	);		

		// 	return redirect('device/add')->with([ 'success' => true,
		// 		'success.message'=> 'Device Created successfully!',
		// 		'success.title' => 'Well Done!']);
		// }else{
			
		// 		return redirect('device/add')->with([ 'error' => true,
		// 		'error.message'=> 'Device Already Exsist!',
		// 		'error.title' => 'Duplicate!']);
		// 	}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'productManage::product.list' );
	}

	/**
	 * Device list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Product::with(['getDevice','getSeries'])->where('status','=',1)->get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $product) {
				
				$dd = array();
				array_push($dd, $i);
				
				if($product->name != ""){
					array_push($dd, $product->name);
				}else{
					array_push($dd, "-");
				}
				if($product->model_no != ""){
					array_push($dd, $product->model_no);
				}else{
					array_push($dd, "-");
				}
				if($product->launch_date != ""){
					array_push($dd, $product->launch_date);
				}else{
					array_push($dd, "-");
				}
				if($product->getDevice != NULL){
					array_push($dd, $product->getDevice->name);
				}else{
					array_push($dd, "-");
				}
				if($product->getSeries != NULL){
					array_push($dd, $product->getSeries->name);
				}else{
					array_push($dd, "-");
				}
				
				$permissions = Permission::whereIn('name',['product.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('product/edit/'.$product->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['product.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="product-delete" data-id="'.$product->id.'" data-toggle="tooltip" data-placement="top" title="Delete product"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}

		/**
	 * product list
	 *
	 * @return Response
	 */
	public function getSeries(Request $request)
	{
		if($request->ajax()){
			$id=$request->get('id');			
			$data= Series::where('device_id','=',$id)->get();			
		
			return Response::json(array('data'=>$data));
		}else{
			return Response::json(array('data'=>[]));
		}
	}
	public function getFeature(Request $request)
	{
		if($request->ajax()){
						
			$data= json_decode('[{"id":1,"text":"Root node","children":[{"id":2,"text":"Child node 1"},{"id":3,"text":"Child node 2"}]},{"id":4,"text":"Root node","children":[{"id":5,"text":"Child node 1"},{"id":6,"text":"Child node 2"}]}]');			
		
			return Response::json($data);
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a device
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$product = Product::find($id);
			if($product){
				$product->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the devcie edit screen to the devcie.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curproduct=Product::with(['getImages','getFeature'])->find($id);
		$colour = Colour::lists( 'name' , 'name' );;
		$devices=Device::where('status','=',1)->get();
		$series=Series::where('device_id','=',$curproduct->device_id)->get();
		$features=Feature::where('status','=',1)->get();
		$curFeatureWithValue=array();
		$curFeature=$curproduct->getFeature;
		$selected_colour=array();
		foreach ($curFeature as $key => $value) {		
			$curFeatureWithValue[$value->feature_id]=$value->feature_value;
		}

		$newFeature=array();
		foreach ($features as $key => $value) {
			$checked=false;
			$value_featue='';
			if(isset($curFeatureWithValue[$value->id])){
				$checked=true;
				$value_featue=$curFeatureWithValue[$value->id];
			}else{
				$checked=false;
				$value_featue='';
			}
			$sub_array=array(
					'id'=>$value->id,
					'name'=>$value->name,
					'checked'=>$checked,
					'value_featue'=>$value_featue,
					'description'=>$value->description,
				);
			array_push($newFeature, $sub_array);
			
		}
		$features=$newFeature;
		// return $features;
		 $image_url_array=[];
		 $image_config_array=[];
		  foreach ($curproduct->getImages as $key => $value) {
				array_push($image_url_array,"<img style='height:160px' src='". url('') . '/core/storage/' .$value->path.'/'.$value->filename."'>");

		  }

		  foreach ($curproduct->getImages as $key => $value) {
				array_push($image_config_array,array('caption'=>$value->filename,'caption'=>$value->filename,'key'=>$value->id,'url'=>url('product/image/deleteFile')));

		  }
		
		if($curproduct){
			$cur_colour=json_decode($curproduct->colour);
			if(count(json_decode($colour))>0){
				foreach (json_decode($colour) as $key => $value) {
			    	if(in_array($value, $cur_colour, true)){		    		
			    		array_push($selected_colour, '<option selected value="'.$value.'">'.$value.'</option>');
			    	}else{
			    		array_push($selected_colour, '<option  value="'.$value.'">'.$value.'</option>');
			    	}
			    }
			}
			
			return view( 'productManage::product.edit' )->with([ 
				'colour' => $selected_colour,
				'curProduct' => $curproduct	,
				'devices'=>$devices,					
				'series'=>$series,'images'=>$image_url_array,'image_config'=>$image_config_array,
				'features'=>$features					

				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new device data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(Request $request, $id)
	{
		$name=$request->get('name');
		$model_number=$request->get('model_number');
		$device=$request->get('device');
		$series=$request->get('series');
		$launch_date=$request->get('launch_date');
		$description=$request->get('description');
		$special_feature=$request->get('special_feature');
		$price=$request->get('price');
		$feature_id=$request->get('feature_id');
		$old_price=$request->get('old_price');
		$rate=$request->get('rate');
		$instock=$request->get('instock');
		if ($instock=='on') {
			$instock=1;
		}else{
			$instock=0;
		}
		$feature_value=$request->get('feature_value');
		$file = $request->file('file');
		$colour = $request->get( 'colour' );
		if (count($colour)==0) {
			$colour=[];
		}

		$product=Product::find($id);

		$product->name=$name;
		$product->model_no=$model_number;
		$product->launch_date=$launch_date;
		$product->series_id=$series;
		$product->device_id=$device;
		$product->description=$description;
		$product->special_feature=$special_feature;
		$product->old_price=$old_price;
		$product->price=$price;
		$product->rate=$rate;
		$product->instock=$instock;
		$product->colour=json_encode( $colour );
		$product->save();

		ProductFeature::where('status', 1)
          ->where('product_id', $id)
          ->update(['status' => 0]);
          if(isset($feature_id)){
          	foreach ($feature_id as $key => $value) {
				
				ProductFeature::Create([
					'product_id'=>$id,
					'feature_value'=>$feature_value[$value],
					'feature_id'=>$value
					]);

			}
          }
          


		 if (!file_exists(storage_path('uploads/images/product'))) {

				File::makeDirectory(storage_path('uploads/images/product'));
		 }
		 $path='uploads/images/product';

		$project_files = $request->file('project-file');
			$i=0;
			foreach ($project_files as $key => $project_file) {
				if (File::exists($project_file)) {
						$file = $project_file;
						$extn =$file->getClientOriginalExtension();
						$destinationPath = storage_path($path);
						$project_fileName = 'product-' .date('YmdHis') .'-'.$i. '.' . $extn;
						$file->move($destinationPath, $project_fileName);

						// $img = InterventionImage::make($destinationPath.'/'.$project_fileName);
						// $img->fit(228, 151);
						// $img->save($destinationPath.'/'.$project_fileName);


						ProductImage::create([
							'product_id'=>$product->id,
							'path'=>$path,
							'filename'=>$project_fileName
						]);
						$i++;
				 }
			}
			return redirect( 'product/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Product updated successfully!',
					'success.title' => 'Good Job!' ]);
		
		
		   
		
	}

	public function jsonImageFileDelete(Request $request)
	{
		$image_id=$request->get('key');
		$image=ProductImage::find($image_id);
		$image->delete();
		return 1;
	}
}
