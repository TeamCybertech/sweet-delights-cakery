@extends('layouts.back.master') @section('current_title','New Coupon')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
    .rating-container .rating-stars:before {
        text-shadow: none;
    }
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('coupon/list')}}">Coupon Management</a></li>

        <li class="active">
            <span>New Coupon</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}

                    <div class="form-group"><label class="col-sm-2 control-label">NAME <sup>*</sup></label>
                        <div class="col-sm-10"><input type="text" value="{{ $coupon->name or '' }}" class="form-control" name="name"></div>
                    </div>
                      <div class="form-group"><label class="col-sm-2 control-label">NO OF COUPONS</label>
                          <div class="col-sm-10"><input type="number" class="form-control" {{ isset($coupon) ? 'disabled' : "" }} value="{{ $coupon->no_of_coupons or '' }}" name="no_of_coupons"></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">VALUE</label>
                         <div class="col-sm-10">
                         <div class="input-group">
      <div class="input-group-addon">$</div>
      <input type="text" class="form-control" value="{{ $coupon->value or '' }}" name="value" placeholder="Amount">

    </div>
                         </div>
                    </div>


                    <div class="form-group"><label class="col-sm-2 control-label">START DATE <sup>*</sup></label>
                        <div class="col-sm-3">
                            <div class="input-group date" >
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <input type="text" id="startdate"   name="start_date" class="form-control" value="{{ $coupon->start_date or date('Y-m-d') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">END DATE <sup>*</sup></label>
                        <div class="col-sm-3">
                            <div class="input-group date" >
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <input type="text" id="enddate"  name="end_date" class="form-control" value="{{ $coupon->end_date or date('Y-m-d') }}"/>
                            </div>
                        </div>
                    </div>

                    @if (isset($coupon))
                      <div class="form-group"><label class="col-sm-2 control-label">ADD NEW COUPONS</label>
                            <div class="col-sm-10"><input type="number" class="form-control" value="0" name="new_coupons"></div>
                      </div>
                      <div class="from-grop">
                          <label class="col-sm-2 control-label">COUPON CODES</label>
                          <div class="col-md-6">
                            <table class="table">
                              <thead>
                                <th>CODE</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                              </thead>
                              <tbody>
                                @foreach ($coupon->couponItems as $element)
                                  <tr>
                                    <td>{{ $element->code }}</td>
                                    <td>{!! $element->status ? "<label class='label label-danger'>USED</label>" : '<label class="label label-primary">NOT USED</label>'!!}</td>
                                    <td>
                                      <a href="{{ url('coupon/coupon-item/delete/'.$element->id) }}">
                                        <button type="button" class="btn btn-danger btn-xs">
                                          <i class="fa fa-trash"></i>
                                        </button>
                                      </a>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                      </div>
                    @endif







                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            {{--  <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>  --}}
                            <a class="btn btn-default" href="{{ url('coupon/list') }}">Cancel</a>
                            <button class="btn btn-primary" type="submit">Done</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>

    <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#input-1").rating();
                                    $(".js-source-states").select2({placeholder: "Select a State"});
                                    $('.date').datepicker(
                                            {
                                                format: 'yyyy-mm-dd',
                                        autoclose: true
                                            });


var isAfterStartDate = function(startDateStr, endDateStr) {



            if(new Date(startDateStr).getTime() > new Date(endDateStr).getTime()) {

                return false;
            }
            else
                return true;

        };
jQuery.validator.addMethod("isAfterStartDate", function(value, element) {

        return isAfterStartDate($('#startdate').val(), value);
    }, "End date should be after start date");


                                    $("#form").validate({
                                        rules: {
                                            name: {
                                                required: true

                                            },
                                            launch_date:{
                                                required : true,
                                                date: true
                                            },
                                            end_date:{
                                                required : true,
                                                date: true,
                                                  isAfterStartDate: true
                                            },
                                            description:{
                                                required : true
                                            }

                                        },
                                        submitHandler: function (form) {
                                            form.submit();
                                        }
                                    });
                                });
                                function loadSeries() {
                                    var selected_device = $('#device').val();
                                    console.log(selected_device);
                                }
                                function loadSeries() {
                                    $('#series').empty();
                                    $('#series').select2("val", "0");
                                    // $('#series').append(
                                    //                 '<option value="0">SELECT A SERIES</option>'
                                    //                 );
                                    var selected_device = $('#device').val();
                                    if (selected_device != 0) {
                                        $.ajax({
                                            method: "GET",
                                            url: '{{url('product / json / getSeries')}}',
                                            data: {'id': selected_device}
                                        })
                                                .done(function (msg) {
                                                    $vehicleDetailArray = jQuery.parseJSON(JSON.stringify(msg));
                                                    console.log($vehicleDetailArray['data']);
                                                    $.each($vehicleDetailArray['data'], function (index, value) {
                                                        $('#series').append(
                                                                '<option value="' + value['id'] + '">' + value['name'] + '</option>'
                                                                );

                                                    });


                                                });
                                    }
                                }


    </script>
    @stop
