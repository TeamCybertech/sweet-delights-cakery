<?php
namespace CouponManager\Models;

use Illuminate\Database\Eloquent\Model;

class CouponItem extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'coupon_id', 'status'];

    public function coupon()
    {
      return $this->belongsTo(Coupon::class);
    }

}
