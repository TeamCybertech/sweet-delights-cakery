@extends('layouts.back.master') @section('current_title','Update Tasting People Type')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/summernote/dist/summernote.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('tasting/people/type/list')}}">Tasting Management</a></li>
       
        <li class="active">
            <span>Update Tasting People Type</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}
                    
                    <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><input type="text" value="{{$tastingPeopleType->name}}" class="form-control" name="name"></div>
                    </div>
                   
                     <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"> 
                         <textarea class="summernote" name="description">{{$tastingPeopleType->description}}</textarea>
                             
                        </div>
                        
                    </div> 
                                      
                    
                    <div class="form-group"><label class="col-sm-2 control-label">PRICE</label>
                        <div class="col-sm-10"><input type="text" value="{{$tastingPeopleType->price}}" class="form-control" name="price"></div>
                    </div>                    
                    
                    <div class="hr-line-dashed"></div>
                	
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor//summernote/dist/summernote.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $('.summernote').summernote({
            placeholder: 'Hello bootstrap 4',
            tabsize: 2,
            height: 100
          });
		  
        
        $("#form").validate({
            rules: {
                name: {
                    required: true
                  
                },description: {
                    required: true
                  
                },price: {
                    required: true,
                    number:true
                  
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
       
	});
	
	
</script>
@stop
