<?php
namespace TastingManage\Http\Controllers;

use App\Http\Controllers\Controller;
use TastingManage\Http\Requests\TastingRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use TastingManage\Models\TastingPeopleType;
use TastingManage\Models\Tasting;
use Sentinel;
use Response;
use File;


class TastingController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Tasting Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the TASTING add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_people_type()
	{

		return view( 'TastingManage::people.add');
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_people_type(TastingRequest $request)
	{


        $count= TastingPeopleType::where('name', '=',$request->get('name' ))->count();


        if ($count==0) {

            $tastingPeopleType = TastingPeopleType::Create(array(
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'price' => $request->input('price'),
                    'created_by'=>Sentinel::getUser()->id,
                )
            );
            if ($tastingPeopleType) {


                return redirect('tasting/people/type/add')->with(['success' => true,
                    'success.message' => 'People Type  Created successfully!',
                    'success.title' => 'Well Done!']);
            }


        }
        else{
            return redirect('tasting/people/type/add')->with([ 'error' => true,
                'error.message'=> 'PEople Type Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View TASTING PEOPLE TYPE List View
	 *
	 * @return Response
	 */
	public function listView_people_type()
	{
		return view( 'TastingManage::people.list' );
	}

	/**
	 * TASTING PEOPLE TYPE list
	 *
	 * @return Response
	 */
	public function jsonList_people_type(Request $request)
	{
		if($request->ajax()){
			$tastingPeopleType_all= TastingPeopleType::where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($tastingPeopleType_all as $key => $tastingPeopleType) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$tastingPeopleType->name);
                array_push($dd,$tastingPeopleType->description);
                array_push($dd,number_format($tastingPeopleType->price,2));

				$permissions = Permission::whereIn('name',['tasting.people.type.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('tasting/people/type/edit/'.$tastingPeopleType->id).'\'" data-toggle="tooltip" data-placement="top" title="privateclass Category Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['tasting.people.type.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="tasting-people-type-delete" data-id="'.$tastingPeopleType->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_people_type(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$tastingPeopleType = TastingPeopleType::find($id);
			if($tastingPeopleType){
                $tastingPeopleType->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the TASTING PEOPLE TYPE edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_people_type($id)
	{
		$tastingPeopleType=TastingPeopleType::find($id);
		if($tastingPeopleType){
			return view( 'TastingManage::people.edit' )->with(['tastingPeopleType'=>$tastingPeopleType]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * EDIT TASTING PEOPLE TYPE data to database
	 *
	 * @return Redirect to EDIT TASTING PEOPLE TYPE
	 */
	public function edit_people_type(TastingRequest $request, $id)
	{

		$count= TastingPeopleType::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();

			if($count==0){
                $tastingPeopleType =  TastingPeopleType::find($id);
                $tastingPeopleType->name = $request->get('name');
                $tastingPeopleType->description = $request->get('description');
                $tastingPeopleType->price = $request->get('price');
                $tastingPeopleType->save();

				return redirect( 'tasting/people/type/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Tasting People Type updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('tasting/people/type/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Tasting People Type Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}


	/****************************************SCHEDULE TASTING ******/





	/**
	 * View SCHEDULE TASTING List View
	 *
	 * @return Response
	 */
	public function listView_tasting_schedule()
	{
		return view( 'TastingManage::schedule.list' );
	}

	/**
	 * SCHEDULE TASTING  list
	 *
	 * @return Response
	 */
	public function jsonList_tasting_schedule(Request $request)
	{
		if($request->ajax()){
			$tasting_all= Tasting::get();
			$jsonList = array();
			$i=1;
			foreach ($tasting_all as $key => $tasting) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$tasting->first_name.' '.$tasting->last_name);
                array_push($dd,$tasting->email);
                array_push($dd,$tasting->event_date);
                array_push($dd,$tasting->created_at->format('Y-m-d'));
                array_push($dd,$tasting->accepted_date);
                array_push($dd,$tasting->ip_address);
                array_push($dd,$tasting->ip_location);
                array_push($dd,$tasting->details);


				$permissions = Permission::whereIn('name',['tasting.schedule.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('tasting/schedule/edit/'.$tasting->id).'\'" data-toggle="tooltip" data-placement="top" title="ACCEPT "><i class="fa fa-eye"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['tasting.schedule.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="tasting-schedule-delete" data-id="'.$tasting->id.'" data-toggle="tooltip" data-placement="top" title="DELETE"><i class="fa fa-times"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-times"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_gallery(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}


	/**
	 * Show the  TASTING SCHEDULE edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_tasting_schedule($id)
	{
		$tasting=Tasting::find($id);

		if($tasting){
			return view( 'TastingManage::schedule.edit' )->with(['tasting'=>$tasting]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add  TASTING SCHEDULE data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_tasting_schedule(TastingRequest $request, $id)
	{

        $tasting =  Tasting::find($id);
        if($tasting){
        	$tasting->event_date = $request->get('datetime');
        	$tasting->accepted_date = date('y-m-d H:i:s');
        	$tasting->status = 2;
            $tasting->save();
			return redirect( 'tasting/schedule/list')->with([ 'success' => true,
				'success.message'=> 'Tasting  schedule Accepted successfully!',
				'success.title' => 'Good Job!' ]);
        }






	}


}
