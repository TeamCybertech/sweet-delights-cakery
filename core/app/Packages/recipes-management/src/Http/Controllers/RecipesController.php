<?php

namespace RecipesManager\Http\Controllers;

use App\Http\Controllers\Controller;
use CakeTypeManager\Models\CakeType;
use Illuminate\Support\Facades\DB;
use File;
use RecipesCategoryManager\Models\RecipesCategory;
use RecipesManager\Models\Recipes;
use RecipesManager\Models\RecipeCategory;
use RecipesManager\Models\RecipesPhoto;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use TestimonialsManager\Models\Testimonials;
use Validator;
use Image;
use SliderManager\Models\AnimationType;
use Illuminate\Support\Facades\Redirect;

class RecipesController extends Controller
{
    private   $coverpath = 'uploads/images/recipe';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index()
    {
        return view('RecipesManager::recipes.list');
    }

    public function add()
    {
        $cat = RecipesCategory::where('status', '=', 1)->get();
        // $caketype= CakeType::where('status', '=', 1)->get();

        return view('RecipesManager::recipes.add')->with(['cat' => $cat]);
    }

    public function save(Request $request)
    {
        $recipe = new Recipes();
        $recipe->intro = $request->txtIntro;
        $recipe->name = $request->txtName;
        $recipe->main = $request->txtMain;
        $recipe->lesson = $request->txtLesson;
        $recipe->material = $request->txtMaterials;
        $recipe->ingrediant = $request->txtIngrediants;
        $recipe->recipes = $request->txtRecipes;
        $recipe->note = $request->txtNotes;
        $recipe->category = -1;
        $recipe->skill_level = $request->skill_level;
        $recipe->cake_type = -1;
        $recipe->price = $request->txtPrice;
  //var_dump($request);die;
        $fp = '';
        if (isset($request->free)) {
            $fp = 0;
        } elseif (isset($request->paid)) {
            $fp = 1;
        }
     //   var_dump($request->free);die;
        $recipe->free_paid = $request->free;

        if (isset($request->premium)) {
            $recipe->premium = 1;
        }

      
        $coverfileName = 'recipe-cover-' . date('YmdHis') . '.png';
        $this->save_cover_image($request->cvr_img, 'core/storage/'.$this->coverpath.'/'.$coverfileName);

        $recipe->cover_path = $this->coverpath;
        $recipe->cover_file = $coverfileName;



        $pdfpath = 'uploads/pdf/recipe';
        $pdffileName = '';
        if ($request->file('templatePdf')) {
            $file = $request->file('templatePdf');
            $extn = $file->getClientOriginalExtension();
            $destinationPath = storage_path($pdfpath);
            $pdffileName = 'recipe-template-' . date('YmdHis') . '.' . $extn;
            $file->move($destinationPath, $pdffileName);
        }
        $recipe->pdf_path = $pdfpath;
        $recipe->pdf_file = $pdffileName;

        $recipe->save();

        if ($recipe) {
          if ($request->categories && count($request->categories) > 0) {
            foreach($request->categories as $key => $catId){
              RecipeCategory::create([
                'recipe_id' => $recipe->recipe_id,
                'recipes_category_id' => $catId
              ]);
            }
          }


            $i = 0;
          if($request->hasFile('file')){
            //  var_dump( $project_files );die;
            $imgsPath = 'uploads/images/images' ;
            foreach ($request->file as $file) {
                   $path =storage_path($imgsPath );
                    $fileName = $file->getClientOriginalName();
                    $filnamenew='recipe-cover-'.$i.$fileName;
                $file->move($path,$filnamenew);

                    $pp = new RecipesPhoto();
                    $pp->recipe_id = $recipe->recipe_id;
                    $pp->path = $imgsPath;
                    $pp->filename = $filnamenew;
                    $pp->save();
                    $i++;

            }
          }
        }

        return redirect('recipe/list')->with(['success' => true,
            'success.message' => 'Recipe Created successfully!',
            'success.title' => 'Well Done!']);

    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = Recipes::where('status', '=', 1)->get();
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $item) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $item->name);

                $permissions = Permission::whereIn('name', ['recipe.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('recipe/edit/' . $item->recipe_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['recipe.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="recipe-delete" data-id="' . $item->recipe_id . '" data-toggle="tooltip" data-placement="top" title="Delete Recipe"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $recip = Recipes::where('recipe_id', '=', $id)->first();
            if ($recip) {
                $recip->status = 0;
                $recip->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id)
    {
        $recip = Recipes::where('recipe_id', '=', $id)->first();
        $cat = RecipesCategory::where('status', '=', 1)->get();

        return view('RecipesManager::recipes.edit')->with([
            'recip' => $recip,
            'cat' => $cat
        ]);
    }

    public function update(Request $request, $id)
    {
        // echo($id);
        // dd($request);
        $recipe = Recipes::where('recipe_id','=',$id)->first();
        // dd($recipe);
        $recipe->intro = $request->txtIntro;
        $recipe->name = $request->txtName;
        $recipe->main = $request->txtMain;
        $recipe->lesson = $request->txtLesson;
        $recipe->material = $request->txtMaterials;
        $recipe->ingrediant = $request->txtIngrediants;
        $recipe->recipes = $request->txtRecipes;
        $recipe->note = $request->txtNotes;
        $recipe->skill_level = $request->skill_level;

        // $recipe->category = $request->category;
        $recipe->price = $request->txtPrice;

        RecipeCategory::where('recipe_id', $recipe->recipe_id)
        ->delete();

        if ($request->categories && count($request->categories) > 0) {
          foreach($request->categories as $key => $catId){
            RecipeCategory::create([
              'recipe_id' => $recipe->recipe_id,
              'recipes_category_id' => $catId
            ]);
          }
        }

        $fp = '';
        if (($request->free)=="free") {
            $fp = 0;
        } else {
            $fp = 1;
        }

        
        $recipe->free_paid = $fp;

        if (isset($request->premium)) {
            $recipe->premium = 1;
        }

        // $coverpath = 'uploads/images/recipe';
        // $coverfileName = '';
        //    if($request->hasFile('file')){
        //     foreach ($request->file as $file) {
        //
        //
        //             $file = $project_file;
        //             $extn = $file->getClientOriginalExtension();
        //             $destinationPath = storage_path($path);
        //             $project_fileName = 'recipe-' . date('YmdHis') . '-' . $i . '.' . $extn;
        //             $file->storeAs('uploads/images/recipe', $project_fileName);
        //
        //             $pp = new RecipesPhoto();
        //             $pp->recipe_id = $recipe->recipe_id;
        //             $pp->path = $path;
        //             $pp->filename = $project_fileName;
        //             $pp->save();
        //             $i++;
        //
        //     }
        //   }
        if ($request->cvr_img != 'notChanged') {
          $coverfileName = 'recipe-cover-' . date('YmdHis') . '.png';
          $this->save_cover_image($request->cvr_img, 'core/storage/'.$this->coverpath.'/'.$coverfileName);

          $recipe->cover_path = $this->coverpath;
          $recipe->cover_file = $coverfileName;
        }


        $pdfpath = 'uploads/pdf/recipe';
        $pdffileName = '';
        if ($request->file('templatePdf')) {
            $file = $request->file('templatePdf');
            $extn = $file->getClientOriginalExtension();
            $destinationPath = storage_path($pdfpath);
            $pdffileName = 'recipe-template-' . date('YmdHis') . '.' . $extn;
            $file->move($destinationPath, $pdffileName);
        }
        $recipe->pdf_path = $pdfpath;
        $recipe->pdf_file = $pdffileName;

        $recipe->save();

        $path = 'uploads/images/images';
        if ($recipe) {
            $project_files = $request->file('photo');
            $i = 0;
            if($request->hasFile('file')){
             foreach ($request->file as $file) {


                    $file = $project_file;
                    $extn = $file->getClientOriginalExtension();
                    $destinationPath = storage_path($path);
                    $project_fileName = 'recipe-' . date('YmdHis') . '-' . $i . '.' . $extn;
                    $file->storeAs('uploads/images/images', $project_fileName);

                    $pp = new RecipesPhoto();
                    $pp->recipe_id = $recipe->recipe_id;
                    $pp->path = $path;
                    $pp->filename = $project_fileName;
                    $pp->save();
                    $i++;

            }
          }
        }



        return redirect('recipe/list')->with(['success' => true,
            'success.message' => 'Recipe Edited successfully!',
            'success.title' => 'Well Done!']);
    }

    function save_cover_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      if (!file_exists(storage_path($this->coverpath))) {

			File::makeDirectory(storage_path($this->coverpath));
      }
         
      file_put_contents($path, $image);

    }
}
