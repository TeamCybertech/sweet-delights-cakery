<?php

namespace RecipesManager\Models;

use Illuminate\Database\Eloquent\Model;

class RecipesPhoto extends Model
{
    protected $table = 'recipe_photo';

    protected $primaryKey = 'recipe_photo_id';

}
