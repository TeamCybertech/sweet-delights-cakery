<?php

namespace RecipesManager\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Recipes extends Model{
	protected $table = 'recipe';
    protected $appends = ['in_cart'];
    protected $primaryKey = 'recipe_id';

    public function getImages()
    {
        return $this->hasMany('RecipesManager\Models\Recipes', 'recipe_id', 'recipe_id');
    }

		public function categories()
		{
			return $this->hasMany('RecipesManager\Models\RecipeCategory',  'recipe_id', 'recipe_id');
        }
        
        public function getInCartAttribute()
        {
            return !empty(\Cart::get('T-'.$this->recipe_id)) ;
        }

        public static function getFrees()
        {
           return  SELF::where('free_paid', '=', 0)
            ->where('status', '=', 1)
            ->get();
        }
        public static function getPaids()
        {
           return  SELF::where('free_paid', '=', 1)
            ->where('status', '=', 1)
            ->get();
        }
        public static function getPremiums()
        {
           return  SELF::where('premium', '=', 1)
            ->where('status', '=', 1)
            ->get();
        }

}
