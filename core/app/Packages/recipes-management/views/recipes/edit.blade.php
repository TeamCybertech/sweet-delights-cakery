@extends('layouts.back.master') @section('current_title','Update tutorial')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
<link rel="stylesheet"
href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css"
href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
<link rel="stylesheet" type="text/css"
href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
<style>
.rating-container .rating-stars:before {
  text-shadow: none;
}
.imageBox
{
  position: relative;
  width: 400px;
  height: 400px;
  border:1px solid #aaa;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor:move;
}

.imageBox .thumbBox
{
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  width: 350px;
  height: 350px;
  margin-top: auto;
  margin-bottom: auto;
  margin-left: auto;
  margin-right: auto;
  box-sizing: border-box;
  border: 1px solid rgb(102, 102, 102);
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  text-align: center;
  line-height: 400px;
  background: rgba(0,0,0,0.7);
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
  <ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('recipe/list')}}">Online Recipie module</a></li>

    <li class="active">
      <span>Update tutorial</span>
    </li>
  </ol>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="hpanel">
      <div class="panel-body">
        <form class="form-horizontal" id="myForm" method="post" files="true" enctype="multipart/form-data">
          {!!Form::token()!!}
          <div class="form-group"><label class="col-sm-2 control-label">TUTORIAL NAME</label>
            <div class="col-sm-10"><input type="text" class="form-control" name="txtName"
              placeholder="Enter tutorial Name" value="{{$recip->name}}">
            </div>
          </div>

          <div class="form-group">
            <input type="hidden" id="cvr_img" name="cvr_img" value="notChanged" required form="myForm">
            <label class="col-sm-2 control-label required">COVER IMAGE</label>
            <div class="col-sm-10">
              <div class="col-sm-10">
                <div class="col-md-6">
                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#crop-modal" >Select Cover Image</button>
                </div>
                <div class="col-md-6">
                  <div class="cropped">
                    <img src="{{url('core/storage/'.$recip->cover_path.'/'.$recip->cover_file)}}" alt="" style="max-width : 100%;">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group"><label
            class="col-sm-2 control-label">FREE/PAID </label>
            <div class="col-sm-10">
              <input id="free" type="radio" name="free" {{ ($recip->free_paid==0)?'checked':''}} value="free">FREE<br>
              <input id="paid" type="radio" name="free" {{ ($recip->free_paid==1)?'checked':''}}  value="paid">PAID
            </div>
          </div>

          <div class="form-group"><label class="col-sm-2 control-label">INTRO VIDEO</label>
            <div class="col-sm-10"><input type="text" class="form-control" id="txtIntro" name="txtIntro"
              placeholder="Enter Intro Video URL" value="{{$recip->intro}}">
            </div>
          </div>

          <div class=" form-group"><label class="col-sm-2 control-label">MAIN VIDEO</label>
            <div class="col-sm-10"><input type="text" class="form-control" id="txtMain" name="txtMain"
              placeholder="Enter Main Video URL" value="{{$recip->main}}">
            </div>
          </div>

          <div class=" form-group">
            <label class="col-sm-2 control-label">LESSON DESCRIPTION</label>
            <div class="col-sm-10"><textarea rows="2" id="textarea1" class="form-control"
             name="txtLesson">{{$recip->lesson}}</textarea>
           </div>
         </div>

         <div class=" form-group">
          <label class="col-sm-2 control-label">MATERIALS</label>
          <div class="col-sm-10"><textarea rows="2" id="textarea2" class="form-control"
           name="txtMaterials"
           >{{$recip->material}}</textarea>
         </div>
       </div>

       <div class=" form-group">
        <label class="col-sm-2 control-label">INGREDIANTS</label>
        <div class="col-sm-10"><textarea rows="2" id="textarea3"
         class="form-control"
         name="txtIngrediants"
         >{{$recip->ingrediant}}</textarea>
       </div>
     </div>

     <div class=" form-group">
      <label class="col-sm-2 control-label">RECIPE</label>
      <div class="col-sm-10"><textarea rows="2" id="textarea4"
       class="form-control"
       name="txtRecipes"
       >{{$recip->recipes}}</textarea>
     </div>
   </div>

   <div class=" form-group">
    <label class="col-sm-2 control-label required">TEMPLATE PDF</label>
    <div class="col-sm-10">
      <input id="pdf" name="templatePdf" type="file"
      class="file-loading">
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label required">PHOTO</label>
    <div class="col-sm-10">
      <input id="photo" name="photo[]" type="file"
      class="file-loading">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">NOTES</label>
    <div class="col-sm-10"><textarea rows="2" id="textarea5"
     class="form-control"
     name="txtNotes"
     >{{$recip->note}}</textarea>
   </div>
 </div>


 <div class="form-group"><label
  class="col-sm-2 control-label">CATEGORY</label>
  <div class="col-sm-10">
    <select class="js-source-states form-control" name="categories[]" multiple>
      @foreach($cat as $item)
      <option value="{{$item->recipes_category_id}}" {{$recip->categories->where('recipes_category_id', $item->recipes_category_id)->count() > 0 ? 'selected' : ''}}>{{$item->name}}</option>
      @endforeach
    </select>
  </div>
</div>
<div class="form-group"><label class="col-sm-2 control-label">SKILL LEVEL</label>
  <div class="col-sm-10">
    <select class="js-source-states form-control" name="skill_level">

      <option value="Beginner" {{$recip->skill_level == 'Beginner' ? 'selected' : ''}}>Beginner</option>
      <option value="Intermediate" {{$recip->skill_level == 'Intermediate' ? 'selected' : ''}}>Intermediate</option>
      <option value="Advanced" {{$recip->skill_level == 'Advanced' ? 'selected' : ''}}>Advanced</option>
      <option value="Kids" {{$recip->skill_level == 'Kids' ? 'selected' : ''}}>Kids</option>

    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label">PRICE<sup id="sup"></sup></label>
  <div class="col-sm-10">
    <input id="price" type="text" class="form-control"
    name="txtPrice"
    placeholder="Enter Price" value="{{$recip->price}}">
  </div>
</div>

<div class="form-group"><label class="col-sm-2 control-label">PREMIUM
ACCOUNT</label>
<div class="col-sm-10">
  @if($recip->premium == 1)
  <input type="checkbox" name="premium" checked value="premium">
  @else
  <input type="checkbox" name="premium" value="premium">
  @endif
</div>
</div>

<div class="hr-line-dashed"></div>
<div class="form-group">
  <div class="col-sm-8 col-sm-offset-2">
    <button class="btn btn-default" type="button"
    onclick="location.reload();">Cancel
  </button>
  <button class="btn btn-primary" type="submit">Done</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- crop modal -->
<div class="modal fade" id="crop-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >Crop Cover Image</h4>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="imageBox">
            <div class="thumbBox"></div>
            {{-- <div class="spinner" style="display: none">Loading...</div> --}}
          </div>
          <div class="action">
            <input type="file" id="cropFile" style="float:left; width: 250px">

            <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
            <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
        <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>
<!-- crop modal -->
@stop
@section('js')
<script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
    thumbBox: '.thumbBox',
    spinner: '.spinner',
    imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
    var reader = new FileReader();
    reader.onload = function(e) {
      options.imgSrc = e.target.result;
      cropper = $('.imageBox').cropbox(options);
    }
    reader.readAsDataURL(this.files[0]);
    this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#cvr_img').val(img);
    document.querySelector('.cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
    cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
    cropper.zoomOut();
  })

</script>
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/spellchecker/spellchecker.php')}}"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $(".js-source-states").select2();

    if(document.getElementById("free").checked){
      document.getElementById('txtIntro').setAttribute("disabled","");
    }

    if(document.getElementById("paid").checked){
      document.getElementById('sup').innerHTML="*";
      document.getElementById('price').setAttribute("required","");
    }

    $('#free').click(function () {
              // alert("clicked");
              document.getElementById('txtIntro').setAttribute("disabled","");
              document.getElementById('price').setAttribute("disabled","");
              document.getElementById('sup').innerHTML="";
              document.getElementById('price').removeAttribute("required");
            });

    $('#paid').click(function () {
              // alert("clicked");
              document.getElementById('txtIntro').removeAttribute("disabled");
              document.getElementById('price').removeAttribute("disabled","");
              document.getElementById('sup').innerHTML="*";
              document.getElementById('price').setAttribute("required","");
            });  
    $("#cover_image").fileinput({
                uploadUrl: "", // server upload action
                uploadAsync: true,
                maxFileCount: 1,
                showUpload: false,
                allowedFileExtensions: ["jpg", "gif", "png"]
              });

    $("#photo").fileinput({
                uploadUrl: "", // server upload action
                uploadAsync: true,
                maxFileCount: 1,
                showUpload: false,
                allowedFileExtensions: ["jpg", "gif", "png"]
              });

    $("#pdf").fileinput({
                uploadUrl: "", // server upload action
                uploadAsync: true,
                maxFileCount: 1,
                showUpload: false,
                allowedFileExtensions: ["pdf"]
              });


    $("#form").validate({
      rules: {
        txtType: {
          required: true
        },
      },
      submitHandler: function (form) {
        form.submit();
      }
    });
  });

</script>
<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
<script type="text/javascript">

  tinymce.init({
    selector: 'textarea',  
    plugins: [
    'advlist autolink lists link image charmap preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code',
    'insertdatetime media nonbreaking table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample',
    'spellchecker'
    ],
    toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
    '| bullist numlist outdent indent  link spellchecker | undo redo | forecolor backcolor emoticons ',
    spellchecker_rpc_url:'spellchecker.php',
    fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
    menubar: false
  });
        //<![CDATA[
        // bkLib.onDomLoaded(function () {
        //   nicEditors.allTextAreas()
        // });
        // //]]>

        // var myform = document.getElementById('myForm');



        // myform.addEventListener('submit', function () {
        //   var textarea1 = nicEditors.findEditor('textarea1');
        //   var textarea2 = nicEditors.findEditor('textarea2');
        //   var textarea3 = nicEditors.findEditor('textarea3');
        //   var textarea4 = nicEditors.findEditor('textarea4');
        //   var textarea5 = nicEditors.findEditor('textarea5');
        //   textarea1.saveContent();
        //   textarea2.saveContent();
        //   textarea3.saveContent();
        //   textarea4.saveContent();
        //   textarea5.saveContent();
        //   return true;
        // });
      </script>
      @stop
