<?php
namespace BranchManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Branch Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class City extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_city';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	

}
