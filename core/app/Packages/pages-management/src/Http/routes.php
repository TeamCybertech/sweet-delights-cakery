<?php
Route::group(['prefix' => 'pages', 'namespace' => 'PagesManage\Http\Controllers'], function()
{
   Route::get('{slug}', [
        'as' => 'index', 'uses' => 'PagesController@preview'
 ]);
}
);
 

Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'pages', 'namespace' => 'PagesManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
     
      Route::get('add', [
        'as' => 'pages.add', 'uses' => 'PagesController@addView'
      ]); 
      Route::get('blog', [
        'as' => 'pages', 'uses' => 'PagesController@blog'
      ]);

      Route::get('edit/{id}', [
        'as' => 'pages.edit', 'uses' => 'PagesController@editView'
      ]);

      Route::get('list', [
        'as' => 'pages.list', 'uses' => 'PagesController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'pages.list', 'uses' => 'PagesController@jsonList'
      ]);
      Route::get('json/getSeries', [
        'as' => 'pages.list', 'uses' => 'PagesController@getSeries'
      ]);
       Route::get('json/getFeature', [
        'as' => 'pages.list', 'uses' => 'PagesController@getFeature'
      ]);


      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'pages.add', 'uses' => 'PagesController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'pages.edit', 'uses' => 'PagesController@edit'
      ]);
    Route::post('delete', [
        'as' => 'pages.delete', 'uses' => 'PagesController@delete'
      ]);
     

    });
});