<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoverImgToSaProductcreation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sa_productcreation', function ($table) {
        $table->text('cover_path');
        $table->text('cover_file');
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('sa_productcreation', function (Blueprint $table) {
          $table->dropColumn('cover_path');
          $table->dropColumn('cover_file');
      });
    }
}
