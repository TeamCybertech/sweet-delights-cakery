@extends('layouts.front.master')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
@section('css')
<style media="screen">
  input[type="password"]{
    width: 100%;
    height: 40px;
    margin: 0 6px 10px 0;
    padding: 10px;
    text-transform: none;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    line-height: 20px;
    font-size: 11px;
    color: #666;
    font-style: normal;
    border-radius: 0;
    background: #fff;
    border: 2px solid #e9e9e9;
    box-shadow: none;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
  }
  div.fancy-select div.trigger::after{
    top: 6px;
  }
  div.fancy-select div.trigger{
    min-width: 178px;
  }
  div.fancy-select ul.options{
    min-width: 170px;
  }
  .maddjust{
    padding-top: 40px;
    padding-right: 40px;
  }
  .keywords li{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
    }
    .keywords li:hover{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
     }

     .widget_categories li{
          margin-left: -40px;
          border-style: none;
          padding-left: 0px;
          padding-top: 2px;
        }
      .tovar_item_btns{
          margin-bottom: 0px!important;
          height: 195px;
          bottom: 0px!important;
        }
        .open-project-link{
          margin-top: 72px;
          background-color: #F8B4B9;
        }

 .keywords-shop {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
    }

.keywords-shop li{
    vertical-align: bottom;
    float: left;
    padding: 6px 15px 6px 15px;
    /*width: 100px;*/
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.25em;
    border-radius: 5px;
    border: thin solid #8b5730;

}

.keywords-shop li:hover{
    vertical-align: bottom;
    float: left;

    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-shop li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
}
.file-drop-zone{
  height: 250px
}
.kv-file-upload {
  display: none;
}
</style>
<style>
.circle img{
    max-width: 100%;
    height: auto;

}

.profile-pic {
    max-width: 200px;
    max-height: 200px;
    display: block;
}

.file-upload {
    display: none;
}
.circle {
    border-radius: 1000px !important;
    overflow: hidden;
    width: 128px;
    height: 128px;
    border: 8px solid rgba(255, 255, 255, 0.7);
    position: relative;
    /* top: 72px; */
    margin-left: auto;
    margin-right: auto;
}
.p-image {
  position: absolute;
  top: 195px;
  right: 431px;
  color: #ce8b8b;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
    font-size: 3em;
    top: -65px;
    margin-top: -80px;
    z-index: 1900;
    position: relative;
    margin-left: -18px;
    left: 10px;
    cursor: pointer;
}
.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
.file-upload{
    display:none!important;
}

</style>
@endsection

@section('content')
  <section class="breadcrumb parallax margbot30" style="background-position: 50% 0px;"></section>
  <section class="page_header">
    <div class="container">
      <h3 class="text-left">PROFILE</h3>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="panel panel-default">
        <div class="panel-body">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home" id="profileTrigger">Profile Details</a></li>
            <li><a data-toggle="tab" href="#menu1" id="passTrigger">Change Password</a></li>
            <li><a data-toggle="tab" href="#pHistory">Purchased History</a></li>
          </ul>

          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
              <br/>
              <div class="col-md-12">
                {!! Form::open(['method' => 'POST', 'route' => 'user.profile.update', 'class' => 'checkout_form clearfix', 'id' => 'contact_form', 'files' => true]) !!}
                <br/>
                    <div class="col-md-12">
                      <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
                          {!! Form::label('first_name', 'FIRST NAME', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                              {!! Form::text('first_name', $user->first_name, ['class' => 'form-control']) !!}
                              {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                          {!! Form::label('last_name', 'LAST NAME', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                              {!! Form::text('last_name', $user->last_name, ['class' => 'form-control']) !!}
                              {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                          {!! Form::label('email', 'EMAIL', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                              {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
                              {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>

                      <div class="form-group {{ $errors->has('street_addresss_1') ? 'has-error' : ''}}">
                          {!! Form::label('street_addresss_1', 'STREET ADDRES 1', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                            @if($details)
                              {!! Form::text('street_addresss_1', $details->street_addresss_1, ['class' => 'form-control']) !!}
                            @else
                              {!! Form::text('street_addresss_1', '', ['class' => 'form-control']) !!}
                            @endif
                              {!! $errors->first('street_addresss_1', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('street_addresss_2') ? 'has-error' : ''}}">
                          {!! Form::label('street_addresss_2', 'STREET ADDRES 2', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                            @if($details)
                              {!! Form::text('street_addresss_2', $details->street_addresss_2, ['class' => 'form-control']) !!}
                            @else
                              {!! Form::text('street_addresss_2', '', ['class' => 'form-control']) !!}
                            @endif
                              {!! $errors->first('street_addresss_2', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                          {!! Form::label('city', 'CITY', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                            @if($details)
                              {!! Form::text('city', $details->city, ['class' => 'form-control']) !!}
                            @else
                              {!! Form::text('city', '', ['class' => 'form-control']) !!}
                            @endif
                              {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('province') ? 'has-error' : ''}}">
                          {!! Form::label('province', 'PROVINCE', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                            @if($details)
                              {!! Form::text('province', $details->province, ['class' => 'form-control']) !!}
                            @else
                              {!! Form::text('province', '', ['class' => 'form-control']) !!}
                            @endif

                              {!! $errors->first('province', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('postcode') ? 'has-error' : ''}}">
                          {!! Form::label('postcode', 'POSTCODE', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                            @if($details)
                              {!! Form::text('postcode', $details->postcode, ['class' => 'form-control']) !!}
                            @else
                              {!! Form::text('postcode', '', ['class' => 'form-control']) !!}
                            @endif

                              {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                          {!! Form::label('country_id', 'COUNTRY', ['class' => 'col-md-2 control-label']) !!}
                          <div class="col-md-10">
                              <select class="basic form-control col-md-12" name="country_id">
                                @foreach ($countries as $element)
                                @if($details)
                                  <option value="{{ $element->id }}" {{ $element->id ==  $details->country_id ? 'selected' : ""}}>{{ $element->name }}</option>
                                @endif
                                @endforeach
                              </select>
                              {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
                          </div>
                      </div>
                      <div class="form-group">
                        {!! Form::label('country_id', 'IMAGE', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                          <!--<div class="small-12 medium-2 large-2 columns">-->
                            <div class="circle">
                            <!-- User Profile Image -->
                            @if($user->avatar)
                            <img id="cimage" class="profile-pic" src="{{asset('core/storage/'.$user->avatar_path.'/'.$user->avatar)}}">
                            @else
                            <img id="cimage" class="profile-pic" src="{{url('assets/front/images/avatar.jpg')}}">
                            @endif


                            <!-- Default Image -->
                            <!-- <i class="fa fa-user fa-5x"></i> -->
                            </div>
                            <div class="p-image">
                            <i class="fa fa-camera upload-button"></i>

                                <input name="profImage" class="file-upload" type="file" accept="image/x-png, image/jpeg"/>
                                <p id="upload">Upload an image</p>
                            </div>
                        </div>

                        <!--</div>-->

                      </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8 maddjust">
                            {!! Form::submit('Update', ['class' => 'btn btn-primary', 'name' => 'profileFormSubmit']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
              </div>
            </div>
            <div id="menu1" class="tab-pane fade">
              <br/>
              {!! Form::open(['method' => 'POST', 'route' => 'user.profile.password', 'class' => 'checkout_form clearfix']) !!}
              <br/>
                  <div class="col-md-12">
                    <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                        {!! Form::label('old_password', 'OLD PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::password('old_password', ['class' => 'form-control']) !!}
                            {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        {!! Form::label('password', 'NEW PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                        {!! Form::label('password_confirmation', 'RE-TYPE PASSWORD', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                  </div>
                  <div class="form-group mtop">
                      <div class="col-md-offset-4 col-md-8 maddjust">
                          {!! Form::submit('Update', ['class' => 'btn btn-primary', 'name' => 'passFormSubmit']) !!}
                      </div>
                  </div>
              {!! Form::close() !!}
            </div>
          <!-- History -->
            <div id="pHistory" class="tab-pane fade">
              <br/>
              <div class="col-md-12">

              <section class="search-section" style="clear: both; margin-bottom: 10px!important;">
                  <div class="container">
                         <div class="btn-group">
                             <ul class="keywords-shop">
                                 <li>
                                     <a style="cursor: pointer;"  class="filter-button" data-filter="all" id="all" onclick="togglePurchasesFilter('all')"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Filter - All</a>
                                 </li>

                                 <li>
                                     <a  style="cursor: pointer;" class="filter-button" data-filter="products" id="btn" onclick="togglePurchasesFilter('product-tr')"><input type="hidden" id="typeId" value=""> Products </a>
                                 </li>

                                 <li>
                                     <a  style="cursor: pointer;" class="filter-button" data-filter="tutorials" id="btn" onclick="togglePurchasesFilter('tutorial-tr')"><input type="hidden" id="typeId" value=""> Tutorials </a>
                                 </li>

                             </ul>
                         </div>
                     </div>
                 </section>

                <table class="table table-striped" cellspacing="0" id="purchaseTable">
                    <thead>
                        <tr>
                            <th class="table-head">Purchase Date</th>
                            <th class="text-center product-thumbnail">&nbsp;</th>
                            <th class="table-head">Product Name</th>
                            <th class="table-head">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($user->getPurchases as $purchase)
                        @foreach ($purchase->getItems as $pItem)
                    <tr class="woocommerce-cart-form cart_item {{$pItem->type == 1 ? 'product-tr' : 'tutorial-tr'}}">
                                <td class="product-date" data-title="Date">{{$purchase->created_at}}</td>
                                    <td class="product-thumbnail"><a href="{{url().'/product-detail/'}}" ><img src="{{ url('/').'/core/storage/'.$pItem->getProduct->cover_path.'/'.$pItem->getProduct->cover_file }}" width="70px" alt="" /></a></td>
                                    <td class="product-name" data-title="Product">
                                        @if ($pItem->type == 1)
                                            <a href="{{url().'/product-detail/'}}">{{$pItem->name}}</a>
                                        @elseif($pItem->type == 2)
                                        <?php
                                            $geturl = "";
                                            if($pItem->getTutorial->free_paid == 0){
                                                $geturl="free";

                                            }else{

                                                $geturl="paid";

                                            }
                                        ?>
                                            <a href="{{url('viewTutorials'.'/'.$geturl.'/'.$pItem->item_id)}}">{{$pItem->name}}</a>
                                        @endif
                                    </td>
                                <td class="product-price" data-title="Price">US ${{number_format($pItem->qty * $pItem->unit_price)}}</td>
                                </tr>
                        @endforeach
                      @endforeach


                    </tbody>

                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('js')
@if(Request::old('passFormSubmit'))
    <script>
        $('#passTrigger').trigger('click')
    </script>
@elseif(Request::old('profileFormSubmit'))
    <script>
        $('#profileTrigger').trigger('click')
    </script>
@endif
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

  $(document).ready(function(){
    $('#contact_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          first_name: {
              validators: {
                      stringLength: {
                      min: 2,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           last_name: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              //$('#contact_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });

var readURL = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-pic').attr('src', e.target.result);

        }

        reader.readAsDataURL(input.files[0]);
    }
}


  $(".file-upload").on('change', function(){
      readURL(this);
      document.getElementById("upload").outerHTML = "<p class=\"btn btn-default\" style=\"display:block;width:150px;margin-left: auto;margin-right: auto;margin-bottom: 10px;\" id=\"reset\">Clear Upload</p>";
  });

  $(document).on('click', '#reset', function(){
      document.getElementById("cimage").src = "{{url('assets/front/images/avatar.jpg')}}";
      $("p#reset").addClass("button3");
      document.getElementById("reset").outerHTML ="<p id=\"upload\">Upload an image</p>"
      $(".file-upload").replaceWith($(".file-upload").val('').clone(true));
      $(".file-upload").val('');
  });


  $(".upload-button").on('click', function() {
     $(".file-upload").click();
  });


});


</script>
<script>
    function togglePurchasesFilter(trClass){
        if(trClass == "all"){
            $('.product-tr').show();
            $('.tutorial-tr').show();
        }else if(trClass == "product-tr"){
            $('#purchaseTable tbody tr.product-tr').show();
            $('#purchaseTable tbody tr.tutorial-tr').hide();
        }else if(trClass == "tutorial-tr"){
            $('#purchaseTable tbody tr.product-tr').hide();
            $('#purchaseTable tbody tr.tutorial-tr').show();
        }
    }
</script>
@stop
