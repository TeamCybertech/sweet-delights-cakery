@extends('layouts.front.master') @section('title','Register | www.princeofgalle.com')
@section('css')
@section('content')
<!-- MY ACCOUNT PAGE -->
<style>
  .paddingl{
    padding-left: 40px!important;
  }
  .separator{
    border-top: 1px solid #CCCCCC;
    position: relative;
    margin: 10px 0 20px 0;
  }
  .separator-text{
    display: block;
    position: absolute;
    top: -10px;
    left: 50%;
    margin-left: -15px;
    padding: 0px 10px;
    background: #ededed;
    color: #8a8a8a;
  }
  .hyperlink {
      color: #8b5730;

  }
  .hyperlink:hover {
      color: #E5D4CD;
  }
  .modal{
    z-index:99999;
}
.modal-header .close {
    margin-top: -20px;
}
</style>
<style>
.circle img{
    max-width: 100%;
    height: auto;

} 

.profile-pic {
    max-width: 200px;
    max-height: 200px;
    display: block;
}

.file-upload {
    display: none;
}
.circle {
    border-radius: 1000px !important;
    overflow: hidden;
    width: 128px;
    height: 128px;
    border: 8px solid rgba(255, 255, 255, 0.7);
    position: relative;
    /* top: 72px; */
    margin-left: auto;
    margin-right: auto;
}
.p-image {
  position: solid;
  top: 167px;
  right: 30px;
  color: #8b5730;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
    font-size: 3em;
    top: -65px;
    margin-top: -80px;
    z-index: 1900;
    position: relative;
    margin-left: -18px;
    left: 10px;
    cursor: pointer;
}
.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
.file-upload{
    display:none!important;
}
.help-block{
    display: inherit;
}
#condition{
    display: none;
    color: #ff0033;
}
.well {
    background-image: linear-gradient(to bottom,#FCEEEB 0,#FCEEEB 100%);
}
.input-group-addon {
    color: #8b5730;
}
.form-focus:focus{
    border-color: #8b5730;
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #8b5730
}
</style>

<section class="my_account parallax">
    <div class="container">
        <div class="row">
            <div class="my_account_block clearfix">

                <div class="login register_form text-center col-lg-6 col-lg-offset-3">
                    <form onsubmit="return checkForm(this);" class="well form-horizontal" action="{{URL::to('user/register')}}" method="post" id="contact_form" enctype="multipart/form-data"  style="background-color: #FCEEEB;">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <!-- Form Name -->
                            <legend>
                                <center>
                                    <h2 style="color: #8b5730;">Create Your Account</h2>
                                </center>
                            </legend>
                            <!-- Text input-->

                            <div class="form-group col-md-12">
                              <p class="pull-left col-md-offset-1">
                                Already have an account? <a href="{{url('user/login')}}" class="hyperlink" id="login-link">Login to your account</a>
                              </p>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="btn-facebook-login btn-social-login col-md-5 col-md-offset-1">
                                   <!--  <a href="{{url('auth/facebook')}}">Login with <i class="fa fa-facebook-square large"></i></a> -->

                                  <a href="{{url('auth/facebook')}}" class="btn-block btn-social btn-facebook">
                                     <span class="fa fa-facebook"></span>&nbsp;&nbsp;Login with facebook
                                  </a>

                                </div>
                                <div class="btn-google-login btn-social-login col-md-5 col-md-offset-1">
                                  <a href="{{url('auth/google')}}" class="btn-block btn-social btn-google">
                                      <span class="fa fa-google"></span>&nbsp;&nbsp;Login with Google
                                  </a>

                                    <!-- <a href="{{url('auth/google')}}">Login with <i class="fa fa-google-plus-square large"></i></a> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="separator">
                                      <span class="separator-text">OR</span>
                                      <!-- <p class="text-center"> OR </p> -->
                                    </div>
                                  </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input  name="fname" placeholder="First Name" class="form-control form-focus" type="text">
                                    </div>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input name="lname" placeholder="Last Name" class="form-control form-focus" type="text">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input name="email" placeholder="E-Mail Address" class="form-control form-focus"  type="text">
                                    </div>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input name="confirm_email" placeholder="Confirm E-Mail Address" class="form-control form-focus"  type="text">
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <!-- <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input name="user_name" placeholder="Username" class="form-control" type="text">
                                    </div>
                                </div>
                            </div> -->
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input name="password" placeholder="Create Password" class="form-control form-focus" type="password">
                                    </div>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input name="confirm_password" placeholder="Confirm Password" class="form-control form-focus" type="password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="small-12 medium-2 large-2 columns">
                                <div class="circle">
                                <!-- User Profile Image -->
                                <img id="cimage" class="profile-pic" src="{{url('assets/front/images/avatar.jpg')}}">

                                <!-- Default Image -->
                                <!-- <i class="fa fa-user fa-5x"></i> -->
                                </div>
                                <div class="p-image">
                                <i class="fa fa-camera upload-button"></i>

                                    <input name="profImage" class="file-upload" type="file" accept="image/x-png, image/jpeg"/>
                                    <p id="upload">Upload an image</p>
                                </div>
                            </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="form-check pull-left">
                                        <input type="checkbox" class="form-control form-check-input" id="remeberMe" name="remeberMe" value="remeberMe" required >
                                        <label class="form-check-label text-left" for="remeberMe" style="text-transform:none;">By creating an account, you agree to the <a href="{{url('/tos')}}" >Terms of Service</a> and acknowladge our <a href="{{url('/privacy')}}" class="form-check-label" >Privacy Policy</a>. </label>
                                        <label id="condition">Accept our Term and Conditions</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-check pull-left">
                                        <div class="g-recaptcha checkout_form_recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                        <label id="recaptcha-error" style="display: block; color: #ff0033;">Please complete the recaptcha</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <!-- <div class="form-group">

                            <div class="form-group">

                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input name="profilepic" placeholder="Your prifile picture" class="form-control"  type="file">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group pull-right">
                                        <div class="g-recaptcha checkout_form_recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <!-- Success message -->
                            <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>
                            <!-- <div class="form-group">
                              <div class="col-md-8 col-md-offset-2">
                                <input type="submit" class="btn add-to-cart col-md-5 pull-left text-center" value="REGISTER!">
                                <input type="reset" id="contact_reset" name="contact_reset" class="btn view-cart col-md-5 paddingl" value="reset">
                              </div>
                            </div> -->

                            <div class="form-group">
                                <div class="col-md-1 col-md-offset-2"></div>
                              <div class="col-md-6">
                                <input type="submit" class="btn btn-block center col-md-5 text-center" value="REGISTER!" style="border-radius: 5px;">
                              </div>
                                <div class="col-md-1 col-md-offset-2"></div>
                            </div>
                            <!-- Button -->
                            <!-- <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4"><br>
                                    <button type="submit" class="btn btn-warning" ><span class="glyphicon glyphicon-send"></span></button>
                                </div>
                            </div> -->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

  <!--Terms modal-->
  <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="termsModalLabel">Terms of Service</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <div class="text-left">
            <div class="panel-body">
            {!! $ts->terms_of_service !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!--Privacy modal-->
<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="privacyModalLabel">Privacy Policy</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <div class="text-left">
            <div class="panel-body">
                {!! $ts->terms_of_service !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

</section>

<!-- //MY ACCOUNT PAGE -->
<style type="text/css">
    #success_message{ display: none;}
</style>
@stop
@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
$("#recaptcha-error").hide()
    $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        ignore: ".ignore",
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
//           valid: 'glyphicon glyphicon-ok',
//           invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          fname: {
              validators: {
                      stringLength: {
                      min: 2,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           lname: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           user_name: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password: {
              validators: {
                   stringLength: {
                      min: 8,
                      message: 'Please enter minimum 8 letters'
                  },
                  identical: {
                      field: 'confirm_password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  },
                  regexp: {
                    regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[$@!%?&]).{8,}$/,
                    message: 'The password should contain at least one lowercase character, one uppercase character, one numerical character and one special character.'
                  }
              }
          },
          confirm_password: {
              validators: {
                   stringLength: {
                      min: 8,
                      message: 'Please enter minimum 8 letters'
                  },
                  identical: {
                      field: 'password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please confirm your Password'
                  },
                  regexp: {
                    regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[$@!%?&]).{8,}$/,
                    message: 'The password should contain at least one lowercase character, one uppercase character, one numerical character and one special character.'
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
           confirm_email: {
              validators: {
                  notEmpty: {
                      message: 'Please confirm your email address'
                  },
                  identical: {
                      field: 'email',
                      message: 'The email and its confirm are not the same'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
          
//          remeberMe:{
//              validators:{
//                   choice: {
//                        min: 1,
//                        max: 1,
//                        message: 'Please choose 2 - 4 programming languages you are good at'
//                    }
//              }
//              
//          }
   remeberMe: { 
       threshold: 5,
                    validators: {
                        notEmpty: {
                            message: 'Please choose at least one checkbox'
                        }
                    }
                },

          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });




      $('#login_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
           user_name1: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password1: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },
         


          }
      })
      
      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#login_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });

       $("#contact_reset").click(function(){
         $('#contact_form').bootstrapValidator("resetForm",true);
      });
        $("#login_reset").click(function(){
         $('#login_form').bootstrapValidator("resetForm",true);
      });

    });
       

</script>
<script>
$(document).ready(function() {


var readURL = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-pic').attr('src', e.target.result);

        }

        reader.readAsDataURL(input.files[0]);
    }
}


$(".file-upload").on('change', function(){
    //alert('clicked');
    readURL(this);
    document.getElementById("upload").outerHTML = "<p class=\"btn btn-default\" style=\"display:block;width:150px;margin-left: auto;margin-right: auto;margin-bottom: 10px;\" onclick=\"resetFile()\" id=\"reset\">Clear Upload</p>";
});

$("#reset").on('click', function(){
    $(".file-upload").replaceWith($(".file-upload").val('').clone(true));
    $(".file-upload").val('');
});


$(".upload-button").on('click', function() {
   $(".file-upload").click();
});
});

function resetFile(){
    document.getElementById("cimage").src = "{{url('assets/front/images/avatar.jpg')}}";
    $("p#reset").addClass("button3");
    $('input[name="profImage"]').val('');
    document.getElementById("reset").outerHTML ="<p id=\"upload\">Upload an image</p>"
};
</script>

<script type="text/javascript">

  function checkForm(form)
  {
        $("#recaptcha-error").hide()
          if (grecaptcha.getResponse() == '') {
                $("#recaptcha-error").show()
                $("form input[type=submit]").prop("disabled",  false);
                return false;
            }else if(!form.remeberMe.checked) {
                var x = document.getElementById("condition");
                x.style.display = "block";
                //alert("sst");
                form.remeberMe.focus();
                return false;
            }
    return true;
  }

</script>
@stop
