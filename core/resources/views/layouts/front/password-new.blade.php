@extends('layouts.front.master') 
<!-- @section('title','Register | www.princeofgalle.com') -->
@section('css')
@section('content')
<!-- MY ACCOUNT PAGE -->
<style>
  input[type="submit"], input[type="reset"]{
    background-color: #fff;
    border: 3px solid #434343;
    color: #434343;
    padding-bottom: 7px;
    padding-top: 7px;
  }
  input[type="submit"]:hover, input[type="reset"]:hover, .btn:hover{
    background-color: #8b5730;
    border-color: #8b5730;
    color: #fff;
  }
  .mtop-m15{
      margin-top:-15px;
  }
  .modal{
    z-index:99999;
}
.modal-header .close {
    margin-top: -20px;
}
  .paddingl{
    padding-left: 40px!important;
  }
  .separator{
    border-top: 1px solid #CCCCCC;
    position: relative;
    margin: 10px 0 20px 0;
  }
  .separator-text{
    display: block;
    position: absolute;
    top: -10px;
    left: 50%;
    margin-left: -15px;
    padding: 0px 10px;
    background: #ededed;
    color: #8a8a8a;
  }
  a.hyperlink-type{
    color: #0000ff;
  }
  a.hyperlink-type:hover{
    color: #0000ff;
    text-decoration: underline;
  }
</style>
<section class="my_account parallax">
    <div class="container center">
        <div class="row">
            <div class="my_account_block clearfix">
                <!-- <div  class="login  col-lg-2"></div> -->
                <div class="login text-center col-lg-6 col-lg-offset-3">
                    <form class="well form-horizontal" id="login_form" method="POST" action="{{URL::to('user/reset-password/'.$token)}}">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <legend>
                                <center>
                                    <h2>Reset Password</h2>
                                </center>
                            </legend>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input name="email" disabled="disabled" value="{{$email}}" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input name="password" type="password" placeholder="New Password" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input name="confirm_password" type="password" placeholder="Confirm New Password" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-8 col-md-offset-2">
                                <input type="submit" class="btn btn-block text-center" value="Reset">
                              </div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

</section>

<!-- //MY ACCOUNT PAGE -->
<style type="text/css">
    #success_message{ display: none;}
</style>
@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#login_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
           password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },
          confirm_password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  identical: {
                      field: 'password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please confirm your Password'
                  }
              }
          }

          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });

    });

</script>
@stop
