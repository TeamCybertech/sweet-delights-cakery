@extends('layouts.front.master') @section('title','Quotes')
@section('css')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>

  <link rel="stylesheet" href="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">

  <style type="text/css">
  	#dateP-error{
        color:#8b5730;
        font-weight: bold;
    }
  	ul.contact-ul-new {
  		/*margin-top: 20px;*/
        padding: 0 0 0 25px;
  	}

  	.contact-ul-new p {
  		margin-left: 15px;
  		margin-bottom: 5px;
  	}
    span.color_red {
        color: #8b5730;
        font-weight: 700;
    }
  	.contact-ul-new h3 {
  		margin-top: 25px;
  		margin-bottom: 10px;
  	}

  	#map {
  		margin-bottom: 30px;
  	}
    .quote_required{
        margin: 15px 0 0 0;
    }
    .checkout_form label, a{
        color: #8b5730;
        text-decoration: none;
    }
    a:hover,a:focus{
        color: #8b5730;
        text-decoration: none;
    }
    input.add-to-cart {
    background-color:#8b5730;
    color: #ffffff;
    border: 1px solid transparent;
    /* border-radius: 4px;
    font-size: 12px; */
    font-weight: 600;
    padding: 6px 10px;
    margin: -2px 0 0 0;
    }
    input.add-to-cart i {
    font-size: 15px;
    }
    input.add-to-cart:hover {
    background-color:#5d391e;
    color: #eeeeee;
    }
    .bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover{
        background-color: #8A562F !important;
    }

    .bootstrap-datetimepicker-widget table td.today:before{
     border-bottom-color: #8A562F !important;
    }

    @media (max-width:980px){

        .quote_required{
            margin:3px 0 0 0;
        }
    }
    input[type="submit"] {
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    font-family: 'Roboto', sans-serif;
    text-transform: none;
    font-style: normal;
    line-height: 20px;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
 /*   color: #8b5730!important;
    background-color: #E5D4CD!important;
    border-color: #E5D4CD!important;*/
    }

    #fn-error, #ln-error, #email-error,
    #venue-error
     {
        color: #e5d4cd;
    }

    div.fancy-select ul.options li{
        color: #000;
        background-color: #FCEEEB;
    }
    div.fancy-select ul.options li.selected{
        color: #000;
        background-color: #FCEEEB;
    }
    div.fancy-select ul.options li.hover{
        color: #000;
        background-color: #E5D4CD;
    }
    div.fancy-select ul.options li.selected :hover{
        color: #000;
        background-color: #E5D4CD;
    }
    .datepicker table tr td.active:active, 
    .datepicker table tr td.active.highlighted:active, 
    .datepicker table tr td.active.active, 
    .datepicker table tr td.active.highlighted.active {
       background-color: #8b5730;
    }
    .form-control:focus{
      border-color: #8b5730;
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #8b5730
    }
    .checkout_form .inqType .fancy-select .trigger {
        width: 100%;
        height: 40px;
    }

    .checkout_form .country .fancy-select .trigger {
        width: 100%;
    }

  @media (max-width:768px){
    .col-xs-12 {
    width: 100%;
    }
  }

  .intl-tel-input.allow-dropdown, .fancy-select, .trigger {
    width: 100%;
  }
  
  </style>

@stop


@section('content')
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
        
        
        <!-- PAGE HEADER -->
        <section class="page_header">
            
            <!-- CONTAINER -->
             <hr class="banner-top"/>
            <div class="banner-bg center">
            <h3>QUOTES & GENERAL INQUIRIES</h3>
                <p>Scroll below to Request a Quotation or send a General Inquiry!</p>
            </div>
            <hr class="banner-bottom"/>
        </section><!-- //PAGE HEADER -->

    <section class="checkout_page">
      
    </section>

    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">
        <!-- CONTAINER -->
        <div class="container">

          

            <!-- CHECKOUT BLOCK -->
            <div class="checkout_block">
                <div class="col-md-5 pad0-l-r" style="margin:25px 0 40px 0;">

                  <div class="col-md-12">
                      <div id="map col-md-12">
                        <iframe height="300" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2936.3555524471863!2d-83.39265468449851!3d42.611414827183516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824bc926c4af98d%3A0xd1105cc2735e654a!2sSweet+Delights+Cakery!5e0!3m2!1sen!2slk!4v1509861373872"></iframe>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <ul class="contact-ul-new">
                        <li>
                            <h3><i class="fa fa-map-marker"></i>&nbsp;<b>Store location</b></h3>
                            <p>{{ $config->company_name }},</p>
                            <p>{!!$config->location!!}</p>
                        </li>
                        <li>
                            <h3><i class="fa fa-phone"></i>&nbsp;<b>Phone</b></h3>
                            <p class="phone">{{ $config->phone }}</p>
                        </li>
                        <li>
                            <h3><i class="fa fa-envelope"></i>&nbsp;<b>E-mail</b></h3>
                            <p><a href="mailto:{{ $config->email }}">{{ $config->email }}</a></p>
                        </li>
                        <!-- <li>
                            <h3><i class="fa fa-map-marker"></i>&nbsp;<b>Map Location</b></h3>
                            <p><a href="#mapLocation">Click here to check</a></p>
                        </li> -->
                    </ul>
                  </div>
	                
                </div>

                <div class="col-md-7 pad0-l-r">

                    <form class="checkout_form clearfix" method="post" id="form" style="border: none;">

                        <div class="col-md-12" style="padding-left: 0; padding-right: 0;">
                            <div class="alert col-md-12" id="dateErrorMsg" style="display:none; background-color: #e5d4cd;">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! $config->qoute_err !!}
                            </div>
                            @if ($errors->any())
                              <div class="alert alert-danger col-md-12">
                                <ul>
                                  @foreach ($errors->all() as $error)
                                       <li>{{ $error }}</li>
                                   @endforeach
                                </ul>
                              </div>
                            @endif
                        </div>

                        {!!Form::token()!!}
						<div class="row">
                            <div class="checkout_form_input first_name col-md-6 col-sm-12 col-xs-12">
                                <label>First Name <span class="color_red">*</span></label>

                                <input type="text" name="fn" value="{{old('fn') ? old('fn') : ( Sentinel::check() ? Sentinel::getUser()->first_name : "")}}" placeholder=""/>
                                   {{-- Sentinel::getUser()->first_name --}}
                            </div>

                            <div class="checkout_form_input last_name col-md-6 col-sm-12 col-xs-12">
                                <label>Last Name <span class="color_red">*</span></label>
                                <input type="text" name="ln" value="{{ old('ln') ? old('ln') : (Sentinel::check() ? Sentinel::getUser()->last_name : "") }}" placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="checkout_form_input phone col-md-6 col-sm-12 col-xs-12">
                                <label>Phone</label>
                               <!--  <input type="text" name="phone" value="{{ old('phone') ?  old('phone') : (Sentinel::getUser() ? Sentinel::getUser()->mobile : "")}}" placeholder=""/>
                            </div> -->
                                <input onchange="formatNumber()" type="text" name="phone" value="{{ Sentinel::getUser() ? Sentinel::getUser()->mobile : ""}}" id="mobile-number" placeholder="(201) 555-5555">
                            </div>
                                    <script> $(":input").inputmask(); $("#mobile-number").inputmask({"mask": "(999) 999-9999"});</script>

                            <div class="checkout_form_input last E-mail col-md-6 col-sm-12 col-xs-12">
                                <label>E-mail <span class="color_red">*</span></label>
                                <input type="text" name="email" value="{{ old('email') ? old('email') : (Sentinel::getUser() ? Sentinel::getUser()->email : "")}}" placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="checkout_form_input inqType col-md-6 col-sm-12 col-xs-12">
                                <label>Inquiry Type <span class="color_red">*</span></label>
                                @if( Request::path() == 'quotes')
                                <select onchange="checkSelectedValue($(this))" class="basic col-md-12" id="inqType" name="inqType">
                                    <option value="2" {{ old('inqType') == 2 ? 'checked' : "" }}>Quotation Inquiry</option>
                                        <option value="1" {{ old('inqType') == 1 ? 'checked' : "" }}>Genaral Inquiry</option>

                                </select>
                                @elseif(Request::path() == 'generalcontact')
                                    <select onchange="checkSelectedValue($(this))" class="basic col-md-12" id="inqType" name="inqType">
                                        <option value="1" {{ old('inqType') == 1 ? 'checked' : "" }}>Genaral Inquiry</option>
                                        <option value="2" {{ old('inqType') == 2 ? 'checked' : "" }}>Quotation Inquiry</option>
                                    </select>
                                @endif
                            </div>

                            <section style="{{ old('inqType') == 2 ? "" : "display:none" }}" id="quoteSection">


                            <div class="checkout_form_input country col-md-6 col-sm-12 col-xs-12">
                                <label>Cake Type <span class="color_red">*</span></label>
                                <select class="basic" id="caketype" name="caketype">
                                    @foreach($cake as $item)
                                        <option value="{{$item->cake_type_id}}">{{$item->type}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkout_form_input col-md-6 col-sm-12 col-xs-12">
                                <label>Event Date <span class="color_red">*</span></label>
                                <input type="text" class="form-control date" id="dateP" name="date" value="{{ old('date') ? old('date') : date('Y-m-d')}}"
                                       placeholder="" />
                                       {{-- onchange="dateChanege()" --}}
                            </div>

                            <!-- <div class="checkout_form_input territory col-md-6">
                                <label>Province / Territory <span class="color_red">*</span></label>
                                <input type="text" name="province" value="{{ old('province') }}" placeholder=""/>
                            </div>

                            <div class="checkout_form_input last postcode col-md-6">
                                <label>Postcode <span class="color_red">*</span></label>
                                <input type="text" name="postcode" value="{{ old('postcode') }}" placeholder=""/>
                            </div> -->

                            <div class="checkout_form_input last adress col-md-6 col-sm-12 col-xs-12">
                                <label>Venue <span class="color_red">*</span></label>
                                <input type="text" name="venue" value="{{ old('venue') }}" placeholder=""/>
                            </div>

                            </section>
                        </div>
                        <div class="row">
                            {{-- <div class="checkout_form_input_details adress col-md-12 col-sm-12 col-xs-12 margbot15">
                                <label>Details</label>
                                <textarea rows="2" id="textarea1"  class="" name="details"> {{ old('details') }} </textarea>
                            </div> --}}

                            <div class="checkout_form_input_details desc col-md-12 col-sm-12 col-xs-12 margbot15">
                                <label>Message</label>
                                <textarea rows="2" id="Message"  class="Message" name="msg"> {{ old('msg') }}</textarea>
                            </div>
						</div>
                        <hr class="clear">
                        <div class="row">
                            <div class="g-recaptcha checkout_form_recaptcha col-md-6 col-lg-6" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                        </div>
                        <div class="row">
                            <div class="checkout_form_policy col-md-6 col-lg-6">
                                <div class="pull-left">
                                    <h2 class="quote_required">
                                        <label for="mandatory">All fields marked with (<span class="color_red">*</span>) are required.</label>
                                    </h2>
                                    <br/>
                                    <input type="submit" onclick="nicEditors.findEditor('Message').saveContent();" class="btn add-to-cart pull-left asd" value="Submit"/>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div><!-- //CHECKOUT BLOCK -->
        </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->



@stop

@section('js')
  <script src='{{ asset('assets/front/js/quotes.js') }}'></script>

    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/moment/moment.js')}}"></script>
    <script src="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

    <script type="text/javascript">
        var type= document.getElementById('inqType');
      jQuery.validator.addMethod("dateCheck", function(value, element) {

        let allowedDate = moment( "{{ \Carbon\Carbon::now()->addDays($config->qoute_days ? $config->qoute_days : 1)->format('Y-m-d H:i') }}")
        let date = moment(value);

        if (date < allowedDate ) {
          $('#dateErrorMsg').show()
          return false;
        }

        $('#dateErrorMsg').hide()
        return true;

      }, "Date is not Available");

        $(document).ready(function () {


            $('.date').datetimepicker({
              format: 'YYYY-MM-DD'
            })
            checkSelectedValue($(type));

            $("#form").validate({
                rules: {
                    caketype: {
                        required: true

                    },
                    date: {
                        required: true,
                        date: true,
                        dateCheck : true
                    },
                    // province: {
                    //     required: true,
                    // },
                    // postcode: {
                    //     required: true
                    // },
                    venue: {
                        required: true
                    },
                    fn: {
                        required: true
                    },
                    ln: {
                        required: true
                    },
                    email: {
                        required: true
                    },


                },
                submitHandler: function (form) {
                    form.submit();

                }
            });
        });

        //<![CDATA[
        bkLib.onDomLoaded(function () {
            nicEditors.allTextAreas()
        });
        //]]>

    </script>
    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>


<link rel="stylesheet" href="https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css">

<script src="https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js"></script>
<script>
    $("#mobile-number").intlTelInput({
        initialCountry: "us",
        nationalMode: true,
        // geoIpLookup: function(callback) {
        //  $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
        //      var countryCode = (resp && resp.country) ? resp.country : "";
        //      callback(countryCode);
        //  });
        // },
        onlyCountries: ["us", "ca"],
          utilsScript: "../../build/js/utils.js" // just for formatting/placeholders etc
      });

        function formatNumber(){
        var number =  $('#mobile-number').val();
        var classf = $(".selected-flag > div").attr("class");
        var flag = classf.slice(-2);
  
        console.log("check number: ", number);
        console.log("check classf: ", classf);
        console.log("check flag: ", flag);
        
        var formattedNumber = intlTelInputUtils.formatNumber(number, flag, intlTelInputUtils.numberFormat.INTERNATIONAL);
        $('#mobile-number').val(formattedNumber);

}
  </script>

@stop