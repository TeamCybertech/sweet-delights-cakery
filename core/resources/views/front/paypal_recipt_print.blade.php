<!DOCTYPE html>
<html lang="en">
<head>
 <link rel="shortcut icon" href="{{asset('assets/front/images/favicon.ico')}}">
<link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrap-theme.min.css')}}">
     <link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrapValidator.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
      <link href="{{asset('assets/front/font-awesome-4.7.0/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />

<style>
    .main {
        width: 8.27in;
        height: 11.69in;
        background: #ffffff;
        border-bottom: 15px solid #F8B4B9;
        border-top: 15px solid #8B5730;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
        margin-bottom: 30px;
        padding: 40px 30px !important;
        position: relative;
        box-shadow: 0 1px 21px #808080;
        font-size: 10px;
    }

    .main thead {
        background: #8B5730;
        color: #fff;
        }
    p {
    display: block;
    -webkit-margin-before: 0.2em;
    -webkit-margin-after: 0.2em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    }
    .img-responsive{
        margin-top: -18px;
        width: 100px;
        clear: both;

    }

}
</style>
    <title>Sweet Delight Cackery | Invoice {{$purchase->inoice_no}}</title>
</head>
<body onload="window.print();">
    @include('includes.paypal_receipt_body')
</body>
</html>