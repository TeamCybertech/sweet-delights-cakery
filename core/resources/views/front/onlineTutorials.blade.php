@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">
    .tovar_img{
        height: 195px;
    }
    .tovar_view_shop {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        color: #fff;
        border: 2px solid transparent!important;
        background-color: transparent!important;
    }

    .tovar_view_shop:hover {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        border:none!important;
        color: #fff!important;
        border-color: transparent!important;
    }

    .add_lovelist_shop {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 24px;
        color: #fff;
        /*border: 2px solid #333;*/
        background-color: transparent!important;
    }

    .add_lovelist_shop:hover {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 28px;
        color: #ff0000!important;
        /*border: 2px solid #ff0000;*/
        background-color: transparent!important;
    }
    .keywords li{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
    }
    .keywords li:hover{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
     }

     .widget_categories li{
          margin-left: -40px;
          border-style: none;
          padding-left: 0px;
          padding-top: 2px;
        }
      .tovar_item_btns{
          margin-bottom: 0px!important;
          height: 195px;
          bottom: 0px!important;
        }
        .open-project-link{
          margin-top: 72px;
          /* background-color: #F8B4B9; */
          background-color: #FCEEEB;
          color:#000!important;
        }

 .keywords-shop {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
    }

.keywords-shop li{
    vertical-align: bottom;
    float: left;
    padding: 6px 15px 6px 15px;
    /*width: 100px;*/
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.25em;
    border-radius: 5px;
    border: thin solid #8b5730;

}

.keywords-shop li:hover{
    vertical-align: bottom;
    float: left;

    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-shop li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
}

 div.item-text-name-new {
    font-size: 16px;
    min-height: 30px;
    font-weight: 600;
    word-wrap: break-word;
    word-break: break-all;
}
div.item-text-price-new {
    font-size: 15px;
    min-height: 30px;
    font-weight: 600;
}


    button.add-to-cart {
    background-color:#8b5730;
    color: #E5D4CD;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.add-to-cart i {
    font-size: 15px;
    }
    button.add-to-cart:hover {
    background-color:#E5D4CD;
    color: #8b5730;
    }
    button.view-cart {
    background-color: transparent;
    color: #8b5730;
    border: 1px solid #8b5730;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.view-cart:hover {
    background-color: #fff;
    color: #5d391e;
    border: 1px solid #8b5730;
    }
    button.view-cart i {
    font-size: 15px;
    }

</style>
@stop


@section('content')


<!-- BREADCRUMBS -->
<section class="breadcrumb men parallax margbot30">

</section><!-- //BREADCRUMBS -->


<!-- SHOP BLOCK -->
<section class="shop">

        <hr class="banner-top">
        <div class="banner-bg center">
            <h3>Online Classes</h3>
            <p>View our portfolio of online classes!</p>
        </div>
        <hr class="banner-bottom">


    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">

            <!-- SIDEBAR -->
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3">

                <!-- CATEGORIES -->
                <div class="sidepanel widget_categories">
                  <div class="widget_search">
                      <input class="search-inbox query" value="{{app('request')->input('q')}}" id="q" type="text" placeholder="Search">
                  </div>

                  <h3 style="margin-top: 35px;">Required Membership</h3>
                  <ul>
                      <li><a style="cursor: pointer;"  id="free">Free ({{\RecipesManager\Models\Recipes::getFrees()->count()}})</a></li>
                      <li><a style="cursor: pointer;"   id="Paid">Paid ({{\RecipesManager\Models\Recipes::getPaids()->count()}})</a></li>
                      <li><a style="cursor: pointer;"   id="Premium">Premium ({{\RecipesManager\Models\Recipes::getPremiums()->count()}})</a></li>
                      <!-- <li><a href="javascript:void(0);" >Pro</a></li> -->
                  </ul>
              </div><!-- //CATEGORIES -->

                <!-- SORTING -->
                <!-- <div class="sidepanel widget_pricefilter">
                    <h3>Sort by:</h3>
                    <form method="POST">
                        <select class="form-control"  id="short_data">
                        <option value="0">Select</option>
                        <option value="1">Price : Lowest first</option>
                        <option value="2">Price : Highest first</option>
                        <option value="3">Date : Latest first</option>
                        <option value="4">Date : Oldest first</option>
                    </select>
                    </form>

                </div> --><!-- SORTING -->

                <div class="sidepanel widget_categories">

                   <h3>Skill Level</h3>
                    <ul>
                      <li><a style="cursor: pointer;" id="beginner">Beginner</a></li>
                      <li><a style="cursor: pointer;"  id="intermediate">Intermediate</a></li>
                      <li><a style="cursor: pointer;" id="advanced">Advanced</a></li>
                      <li><a  style="cursor: pointer;" id="kids">Kids</a></li>
                    </ul>
                    <!-- <h3>Sort by:</h3>
                    <form method="POST">
                    <select class="basic" id="short_data">
                      <option value="0">Select</option>
                        <option value="1">Price : Lowest first</option>
                        <option value="2">Price : Highest first</option>
                        <option value="3">Date : Latest first</option>
                        <option value="4">Date : Oldest first</option>
                    </select>
                    </form> -->
                </div>


                <!-- SHOP BY SIZE -->
                <!-- <div class="sidepanel widget_sized">
                  <h3>SHOP BY SIZE</h3>
                  <ul>
                    <li><a class="sizeXS" href="javascript:void(0);" >XS</a></li>
                    <li class="active"><a class="sizeS" href="javascript:void(0);" >S</a></li>
                    <li><a class="sizeM" href="javascript:void(0);" >M</a></li>
                    <li><a class="sizeL" href="javascript:void(0);" >L</a></li>
                    <li><a class="sizeXL" href="javascript:void(0);" >XL</a></li>
                  </ul>
                </div> --><!-- //SHOP BY SIZE -->

                <!-- SHOP BY COLOR -->
                <!-- <div class="sidepanel widget_color">
                  <h3>SHOP BY COLOR</h3>
                  <ul>
                    <li><a class="color1" href="javascript:void(0);" ></a></li>
                    <li class="active"><a class="color2" href="javascript:void(0);" ></a></li>
                    <li><a class="color3" href="javascript:void(0);" ></a></li>
                    <li><a class="color4" href="javascript:void(0);" ></a></li>
                    <li><a class="color5" href="javascript:void(0);" ></a></li>
                    <li><a class="color6" href="javascript:void(0);" ></a></li>
                    <li><a class="color7" href="javascript:void(0);" ></a></li>
                    <li><a class="color8" href="javascript:void(0);" ></a></li>
                  </ul>
                </div> --><!-- //SHOP BY COLOR -->

                <!-- SHOP BY BRANDS -->
                <!-- <div class="sidepanel widget_brands">
                  <h3>SHOP BY BRANDS</h3>
                  <input type="checkbox" id="categorymanufacturer1" /><label for="categorymanufacturer1">VERSACE <span>(24)</span></label>
                  <input type="checkbox" id="categorymanufacturer2" /><label for="categorymanufacturer2">J CREW <span>(35)</span></label>
                  <input type="checkbox" id="categorymanufacturer3" /><label for="categorymanufacturer3">Calvin KlEin <span>(48)</span></label>
                  <input type="checkbox" id="categorymanufacturer4" /><label for="categorymanufacturer4">Tommy hilfiger <span>(129)</span></label>
                  <input type="checkbox" id="categorymanufacturer5" /><label for="categorymanufacturer5">Ralph Lauren <span>(69)</span></label>
                </div> --><!-- //SHOP BY BRANDS -->

                <!-- BANNERS WIDGET -->
                <!-- <div class="widget_banners">
                  <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner10.jpg')}}" alt="" /></a>
                  <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner9.jpg')}}" alt="" /></a>
                  <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner8.jpg')}}" alt="" /></a>
                </div> --><!-- //BANNERS WIDGET -->

                <!-- NEWSLETTER FORM WIDGET -->
                <!-- <div class="sidepanel widget_newsletter">
                    <div class="newsletter_wrapper">
                        <h3>NEWSLETTER</h3>
                        <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                            <input type="text" name="newsletter" value="Enter E-mail & Get 10% off" onFocus="if (this.value == 'Enter E-mail & Get 10% off') this.value = '';" onBlur="if (this.value == '') this.value = 'Enter E-mail & Get 10% off';" />
                            <input class="btn newsletter_btn" type="submit" value="Sign up & get 10% off">
                        </form>
                    </div>
                </div> --><!-- //NEWSLETTER FORM WIDGET -->
            </div><!-- //SIDEBAR -->

            <!-- SHOP PRODUCTS -->
            <div class="col-lg-9 col-sm-9 col-sm-9 padbot20">

                <!-- SHOP BANNER -->
                <!-- <div class="banner_block margbot15">
                  <a class="banner nobord" href="#" ><img src="{{asset('assets/front/images/tovar/banner22.jpg')}}" alt="" /></a>
                </div> -->
                <!-- //SHOP BANNER -->

                <!-- SORTING TOVAR PANEL -->
                <!-- <div class="sorting_options clearfix">
 -->
                    <!-- COUNT TOVAR ITEMS -->
                    <!-- <div class="count_tovar_items" id="items_count">
                        <p>Result</p>

                    </div> --><!-- //COUNT TOVAR ITEMS -->

                    <!-- PRODUC SIZE -->
                    <!-- <div id="toggle-sizes">
                        <a class="view_box active" href="javascript:void(0);"><i class="fa fa-th-large"></i></a>
                        <a class="view_full" href="javascript:void(0);"><i class="fa fa-th-list"></i></a>
                    </div> --><!-- //PRODUC SIZE -->
                <!-- </div> --><!-- //SORTING TOVAR PANEL -->
                 <!-- <div class="btn-group" style="margin-top: 10px;">

                    <a href="#"><button type="button" class="btn button-new filter-button active">Filter - All</button></a>

                    <a href="#"><button type="button" class="btn button-new filter-button">Free</button></a>

                    <a href="#"><button type="button" class="btn button-new filter-button">Premium</button>

                    <a href="#"><button type="button" class="btn button-new filter-button">Paid</button>

                 </div> -->
                 <section class="search-section" style="clear: both; margin-bottom: 10px!important;">
                  <div class="container">
                         <div class="btn-group">
                             <ul class="keywords-shop">
                                 <li>
                                     <a style="cursor: pointer;"  class="filter-button" data-filter="all" id="all" onclick="categoryImage(0)"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Filter - All</a>
                                 </li>
                                 <?php

                                  foreach ($type as $key => $value) {
                                    ?>
                                 <li>
                                     <a  style="cursor: pointer;" class="filter-button" data-filter="birthday" id="btn{{ $value->recipes_category_id }}" onclick="categoryImage(2)"><input type="hidden" id="typeId{{ $value->recipes_category_id }}" value="{{ $value->recipes_category_id }}"> {{ $value->name }} </a>
                                 </li>
                               <?php } ?>
                             </ul>
                         </div>
                     </div>
                 </section> 

                    <!-- ROW -->
                    <div class="row shop_block" id="values">

                        <!-- TOVAR1 -->

                        <?php foreach ($data as $row) {  ?>

                        <?php
                              $geturl = "";
                              $getPremium = "";
                              if($row->free_paid == 0){
                                  $geturl="free";

                              }else{

                                  $geturl="paid";

                              }
                        ?>
                        <div id="tutorial-list" class="tovar_wrapper col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 padbot40">
                                <div class="tovar_item clearfix">
                                    <div class="tovar_img">
                                        <div class="tovar_img_wrapper">
                                            <img class='img' src='{{asset("core/storage/".$row->cover_path."/".$row->cover_file)}}' alt='' />
                                            <img class='img_h' src='{{asset("core/storage/".$row->cover_path."/".$row->cover_file)}}' alt='' />
                                        </div>
                                        <div class="tovar_item_btns">
                                            <div class="open-project-link">
                                                <a onmouseout="fnMousOut(this)" onmouseover="fnMouseOver(this)" class="open-project tovar_view_shop" href="{{url('viewTutorials'.'/'.$geturl.'/'.$row->recipe_id)}}" data-url="" >{{ $row->free_paid == 0 ? "Watch Now" : "Watch Trailer" }}</a>
                                            </div>

                                            <!-- <div>
                                            <a class="add_lovelist_shop" onclick="addtocart({{$row->recipe_id}})" ><i class="fa fa-shopping-cart"></i></a>
                                            <a class="add_lovelist_shop" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                          </div> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                       <a  href="{{url('viewTutorials'.'/'.$geturl.'/'.$row->recipe_id)}}" >  
                                            <div class="row item-text-name-new">
                                                {{ substr(strip_tags($row->name),0,50)}}
                                            </div>
                                            <div class="row item-text-price-new">
                                
                                                    <span>{{ $row->free_paid == 0 ? "FREE" : "US $".number_format($row->price) }}</span>
                                                

                                            </div>
                                        {{-- <div>
                                            <a class="tovar_title" href="" ></a>
                                        </div>
                                        <div>
                                        <span class="tovar_price">{{ floatVal($row->price ) == 0 ? "FREE" : number_format($row->price)."$" }}</span>
                                        </div> --}}
                                       </a>
                                    </div>
                                    <div class="row text-center">
                        
                                        <?php if (!empty(Cart::get('T-'.$row->recipe_id))): ?>
                                            <button onclick="removeFromCart({{$row->recipe_id}}, 2)" class="btn add-to-cart">ADDED TO CART <i class="fa fa-check"></i></button>
                                            
                                            <!-- <a style="padding-left : 2px;" href="{{url('my-cart')}}"><button class="btn view-cart">View Cart</button></a> -->
                                        <?php else: ?>
                                            @if ($row->free_paid != 0)
                                                <button onclick="addtocart({{$row->recipe_id}})" class="btn add-to-cart">ADD TO CART <i class="fa fa-cart-plus"></i></button>
                                            @endif
                                            @if ($row->free_paid == 0)
                                                <a style="padding-left : 2px;" href="{{url('viewTutorials'.'/'.$geturl.'/'.$row->recipe_id)}}"><button class="btn view-cart">View Tutorial</button></a>
                                            @endif
                                        <?php endif ?>
                                        
                                
                                    </div>
                                    <div class="tovar_content"><?php echo $row->intro ?></div>
                                </div>
                            </div>


                        <?php } ?>
                        <div class="col-md-12">
                            <div class="col-md-8" style="text-align: right">
                                <?php echo $data->render(); ?>
                            </div>
                        </div>


                    </div><!-- //ROW -->


                <!-- <div class="padbot60 services_section_description">
                  <p>We empower WordPress developers with design-driven themes and a classy experience their clients will just love</p>
                  <span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral. Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral.</span>
                </div>

                <div class="row top_sale_banners center">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-ss-12"><a class="banner nobord margbot30" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner22.jpg')}}" alt="" /></a></div>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-ss-12"><a class="banner nobord margbot30" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner23.jpg')}}" alt="" /></a></div>
                </div> -->

            </div><!-- //SHOP PRODUCTS -->

        </div><!-- //ROW -->

    </div><!-- //CONTAINER -->
</section><!-- //SHOP -->

@stop

@section('js')

<script>
    Number.prototype.format = function(n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    function priceFormat(tutData){
        if(tutData.free_paid == 0){
             return "FREE";
         }
        var price = parseFloat(tutData.price || 0);
         price = price.format(2);
         

         return "$"+price;
    }
     var member_id="";
     var caketype = "";
    $(document).ready(function () {
      //get data free
    $('#free').click(function () {

    $.ajax({
    method: "GET",
    url: '{{url('filterMembership')}}',
          })
    .done(function(data){
            var rows = '';

            $.each(data, function(index, value) {
                member_id=1;
                rows += getTutThumb(value);
            });
            $("#values").html(rows);
            });
    })

            //get count free
   $('#free').click(function () {
    $.ajax({
    method: "GET",
            url: '{{url('free_count')}}',
    })
            .done(function(data){
            var rows_a = '';
            $.each(data, function(index, value) {

            rows_a += '<p>Result</p>';
            rows_a += ' <span>' + value.free_count + ' Items found</span>';
            });
            $("#items_count").html(rows_a);
            });
    })

    });
    $(document).ready(function () {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        //search
        var shop_block = $('.shop_block');
        var resultObj = $('#tutorial-list');
        $('.query').on('keyup',function(){
            if(this.value.length >= 2){
                $.ajax({
                    type: 'GET',
                    url: 'search',
                    data: { q: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        let dataObj = json;
                        shop_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                             $.each(dataObj, function(index, value) {
                            
                                var hitTemplate = getTutThumb(value);

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.shop_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }

                        if(dataObj.length == 0){
                            $('.shop_block').html('<div class="alert alert-warning" role="alert">No Results!!!</div>');
                        }
                    }
                });
            }else{
                $.ajax({
                    type: 'GET',
                    url: 'stdSearch',
                    data: { q: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        let dataObj = json;
                        shop_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                            $.each(dataObj, function(index, value) {

                                var hitTemplate = getTutThumb(value);

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.shop_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }

                        if(dataObj.length == 0){
                            $('.shop_block').html('<div class="alert alert-warning" role="alert">No Results!!!</div>');
                        }
                    }
                });

            }
        });
    

    //get data premium
    $('#Premium').click(function () {
    $.ajax({
    method: "GET",
            url: '{{url('filterPremium')}}',

    })
            .done(function(data){
            var rows = '';
            $.each(data, function(index, value) {
                 member_id=2;

                rows += getTutThumb(value);
            });
            $("#values").html(rows);
            });
    })

            //get count

  $('#Premium').click(function () {
    $.ajax({
    method: "GET",
            url: '{{url('premium_count')}}',
    })
            .done(function(data){
            var rows_a = '';
            $.each(data, function(index, value) {

            rows_a += '<p>Result</p>';
            rows_a += ' <span>' + value.premium_count + ' Items found</span>';
            });
            $("#items_count").html(rows_a);
            });
    });

    //View Paid


    $('#Paid').click(function () {
    $.ajax({
    method: "GET",
            url: '{{url('filterPaid')}}',

    })
            .done(function(data){
            var rows = '';
            $.each(data, function(index, value) {
                 member_id = 3;
                 rows += getTutThumb(value);
            });
            $("#values").html(rows);
            });
    });

             //get count
 $('#Paid').click(function () {
    $.ajax({
    method: "GET",
            url: '{{url('paid_count')}}',
    })
            .done(function(data){
            var rows_a = '';
            $.each(data, function(index, value) {

            rows_a += '<p>Result</p>';
            rows_a += ' <span>' + value.premium_count + ' Items found</span>';
            });
            $("#items_count").html(rows_a);
            });
    });
    //get count

   $('#short_data').change(function(){
    var id=$('#short_data').val();

   $.ajax({
      url:"{{url('shortPriceLowest')}}",
       type:"get",
       data:{id:id,member_id:member_id}
   })
           .done(function(data){
            var rows = '';
            $.each(data, function(index, value) {
                rows += getTutThumb(value);
            });
            $("#values").html(rows);
            });
            })  ;



            $('#all').click(function(){
              var cakeTypeId=document.getElementById('typeId{{ $value->recipes_category_id }}').value;
            //  alert(cakeTypeId);
             $.ajax({
                  url:"{{url('filtertype')}}",
                  method:"GET",
                  data:{type:"all"}

             })
             .done(function(data){
              var rows = '';
              var caketype = 0;
              $.each(data, function(index, value) {

                rows += getTutThumb(value);
              });
              $("#values").html(rows);
              });
           });



 <?php foreach ($type as $key => $value) { ?>
                       $('#btn{{ $value->recipes_category_id }}').click(function(){
                         var cakeTypeId=document.getElementById('typeId{{ $value->recipes_category_id }}').value;
                        // alert(cakeTypeId);
                        $.ajax({
                             url:"{{url('filtertype')}}",
                             method:"GET",
                             data:{type:{{$value->recipes_category_id}}}

                        })
                        .done(function(data){
                         var rows = '';
                         var caketype = {{$value->recipes_category_id}};
                         $.each(data, function(index, value) {

                            rows += getTutThumb(value);
                         });
                         $("#values").html(rows);
                         });
                      });



<?php } ?>





  $('#beginner').click(function(){
   $.ajax({
        url:"{{url('skillBeginner')}}",
        method:"GET",
        data:{skill:"Beginner",member_id:member_id,type:caketype}

   })
   .done(function(data){
    var rows = '';
    $.each(data, function(index, value) {
        rows += getTutThumb(value);
    });
    $("#values").html(rows);
    });
 });


 $('#intermediate').click(function(){
  $.ajax({
       url:"{{url('skillBeginner')}}",
       method:"GET",
       data:{skill:"Intermediate",member_id:member_id,type:caketype}

  })
  .done(function(data){
   var rows = '';
   $.each(data, function(index, value) {
    rows += getTutThumb(value);
   });
   $("#values").html(rows);
   });
});

$('#advanced').click(function(){
 $.ajax({
      url:"{{url('skillBeginner')}}",
      method:"GET",
      data:{skill:"Advanced",member_id:member_id,type:caketype}

 })
 .done(function(data){
  var rows = '';
  $.each(data, function(index, value) {
      rows += getTutThumb(value);
  });
  $("#values").html(rows);
  });
});

$('#kids').click(function(){
 $.ajax({
      url:"{{url('skillBeginner')}}",
      method:"GET",
      data:{skill:"Kids",member_id:member_id,type:caketype}

 })
 .done(function(data){
  var rows = '';
  $.each(data, function(index, value) {

    rows += getTutThumb(value);
  });
  $("#values").html(rows);
  });
});


   })


//for images opacity controll
function fnMouseOver(element){
   var div = $(element).parent().parent().parent();
   var img = $(div).find("img");
   $(img).css("opacity","1");

}

function fnMousOut(element){
     var div = $(element).parent().parent().parent();
   var img = $(div).find("img");
   $(img).css("opacity","0.65");
}


//$('#all').click(function() {
//   e.preventDefault(); // cancel click
//  var page = $(this).attr('href');
//  $('#values').load(page);
//});


</script>

<script>
     function addtocart(product_id){
        var product_id= product_id;
        var qty= 1;

        $.ajax({
            method: "GET",
            url: '{{url('cart/add')}}',
            data:{ 'product_id' : product_id,'qty': qty,'product_type':2 }
        })
            .done(function( msg ) {
                window.location.href='{{url('my-cart')}}';

            });
    }

  function removeFromCart(id, type) {
    $.ajax({method: "GET",
            url:'{{url('cart/remove')}}/'+id+'/'+type})
            .done(function(msg) {
            window.location.reload();
            });
    }

    function getTutThumb(tutData){
        var returnData = "";
        if(tutData && tutData.cover_path){
            var type = 'free';
            if(tutData.free_paid == 1 || tutData.premium == 1){
                    type = 'paid'
                }  
             returnData =   
                '<div class="tovar_wrapper col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 padbot40">'
                    + '<div class="tovar_item clearfix">'
                        + '<div class="tovar_img">'
                            + '<div class="tovar_img_wrapper">'
                                 + "<img class='img' src='core/storage/"+ tutData.cover_path + "/" +tutData.cover_file +"' alt='' />"
                                 + "<img class='img_h' src='core/storage/"+ tutData.cover_path + "/" +tutData.cover_file +"' alt='' />"
                            + '</div>'
                            + '<div class="tovar_item_btns">'
                                 + '<div class="open-project-link">'
                                    + '<a class="open-project tovar_view_shop" href="viewTutorials/'+type+'/'+tutData.recipe_id+'"  >';
                                        if(tutData.free_paid == 0){
                                            returnData += 'Watch Mow';
                                        }else{
                                            returnData += 'Watch Trailer';
                                        }
                                     
                                     returnData += '</a>'
                                 + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<a  href="viewTutorials/'+type+'/'+tutData.recipe_id+'" >'    
                            + '<div class="col-md-12 text-center">'
                                + '<div class="row item-text-name-new">'+  tutData.name 
                            + '</div>'
                            + '<div class="row item-text-price-new">'
                                + '<span> ' + priceFormat(tutData) + '</span>'
                            + '</div>'
                        + '</a>'
                    + '</div>'
                    + '<div class="row text-center">'
                        if ( tutData.in_cart	){
                            returnData +=  '<button onclick="removeFromCart('+tutData.recipe_id+', 2)" class="btn add-to-cart">ADDED TO CART <i class="fa fa-check"></i></button>';
                            returnData += '<a style="padding-left : 2px;" href="my-cart"><button class="btn view-cart">View Cart</button></a>' ;
                        }else {
                            if(tutData.free_paid != 0){
                                returnData +=  '<button onclick="addtocart('+tutData.recipe_id+')" class="btn add-to-cart">ADD TO CART <i class="fa fa-cart-plus"></i></button>';
                            }
                            if(tutData.free_paid == 0){
                                returnData +=  '<a style="padding-left : 2px;" href="viewTutorials/'+type+'/'+tutData.recipe_id +'"><button class="btn view-cart">View Item</button></a>';
                            }
                            }
         returnData += '</div></div></div>'
                // + '<div class="tovar_content">'+ tutData.intro +'</div>';

        }
                return returnData;
         
    }
</script>

@stop
