@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')
<style type="text/css">
   .slick-list{
   text-align: justify!important;
   }
   .slick-dots li button::before{
   font-size:20px;
   }
   .tovar_price {
   margin-bottom: 7px;
   }
   .testimonial-more-btn {
   color: #8b5730;
   font-size: 13px;
   }
   .flexslider .div-flexslider-responsive{
   height: 50vh;
   position: relative;
   }
   .div-flexslider-responsive div{
   margin:20px auto;
   }
   .div-flexslider-responsive .captionDelay1 p span{
   color: rgb(72, 49, 44)!important;
   font-size: 80px !important;
   text-transform: capitalize;
   }
   .div-flexslider-responsive .captionDelay3{
   top: 40%!important;
   }
   .div-flexslider-responsive .captionDelay3 p span{
   color: rgb(184, 35, 25)!important;
   font-size: 80px;
   font-family: 'Open Sans', sans-serif;
   font-weight: 300;
   letter-spacing: 3px;
   }
   .div-flexslider-responsive .captionDelay5.FromRight{
   top: 50%!important;
   }
   .div-flexslider-responsive .captionDelay5 p span{
   color: rgb(72, 49, 44)!important;
   font-size: 80px;
   font-family: 'Open Sans', sans-serif;
   text-transform: capitalize;
   }
   .div-flexslider-responsive .captionDelay7.FromRight{
   top: 57%!important;
   }
   .div-flexslider-responsive .captionDelay9.FromRight{
   top: 63%!important;
   }
   .div-flexslider-responsive .captionDelay5.FromTop{
   top: 32%!important;
   }
   .div-flexslider-responsive .captionDelay7.FromTop{
   top: 44%!important;
   }
   .div-flexslider-responsive .captionDelay9.FromTop{
   top: 52%!important;
   }
   .div-flexslider-responsive .captionDelay9.FromTop{
   top: 52%!important;
   }
   .div-flexslider-responsive .captionDelay11.FromTop{
   top: 62%!important;
   }
   .div-flexslider-responsive .captionDelay7 p span, .div-flexslider-responsive .captionDelay9 p span{
   color: rgb(184, 35, 25)!important;
   background-color: rgba(0,0,0,0)!important;
   font-size: 48px;
   font-family: 'Open Sans', sans-serif;
   text-transform: capitalize;
   }
   .div-flexslider-responsive .captionDelay7 p span em, .div-flexslider-responsive .captionDelay9 p span em{
   font-style: normal;
   }
   .flexslider.top_slider .flex-direction-nav li a:before{
   color: rgba(0,0,0,0.5);
   }
   .flexslider.top_slider .flex-direction-nav li a:hover:before{
   color: #000;
   }
   .top_slider .slide2 .flex_caption2{
   }
   .top_slider .slide2 .flex_caption1{
   padding-left: 50px;
   }
   .max-lines10 { 
   display: block;  
   text-overflow: ellipsis; 
   word-wrap: break-word; 
   overflow: hidden; 
   max-height: 15em; 
   line-height: 1.5em; 
   }
   .flex-active-slide {
   width: 100%;
   top: 210px !important;
   background-attachment: inherit !important;
   background-size: contain !important;
   float: left;
   background-repeat: no-repeat !important;
   margin-right: 0px !important;
   position: absolute !important;
   opacity: 1;
   display: block;
   z-index: 2;
   background-position: 0% 0px !important;
   }
</style>
@stop
@section('content')
<section id="home" class="padbot0">
   <!-- TOP SLIDER -->
   <div class="flexslider top_slider">
      <ul class="slides">
         <?php $i=0; foreach ($slider_details as $key => $value): ?>
         <li class="slide2" style="background-image: url('{{url('').'/core/storage/uploads/images/main_slider/'.$value->image}}'); width:100%; top:0; background-attachment: scroll; background-size:cover;">
            <div class="div-flexslider-responsive" >
               
               @if ($value->message_type == "text")
               <?php $lines = explode("</p>",$value->animation_txt)?>
               @foreach($lines as $key => $line )
               <div class="flex_caption1 title1 captionDelay{{ 1 + ($key * 2)}} {{ $value->animationType->css_class }}" style="padding-left: 5%; padding-right: 5%; top : {{ ($key * 17) + 10}}% !important;">
                  {!! $line !!}
               </div>
               @endforeach

               @elseif($value->message_type == "image")
               <div  class="flex_caption1 title1 captionDelay{{ 1 + (1 * 2)}} {{ $value->animationType->css_class }}" style="top : {{ $value ->top_first}}% !important; text-align: {{$value->img_position}} ">
                  <img src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->message_first_line_img}}" alt="" style="max-width: {{$value->maxwidth_first}}%; width: auto; height: auto;">
               </div>
               <div class="flex_caption1 title1 captionDelay{{ 1 + (2 * 2)}} {{ $value->animationType->css_class }}" style="top : {{$value ->top_second}}% !important;  text-align: {{$value->img_position}};">
                  <img src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->message_second_line_img}}" alt="" style="max-width: {{$value->maxwidth_second}}%; width: auto; height: auto;">
               </div>
               @if ($value->message_third_line_img != "")
               
               <div class="flex_caption1 title1 captionDelay{{ 1 + (3 * 2)}} {{ $value->animationType->css_class }}" style="top : {{$value ->top_third}}% !important; text-align: {{$value->img_position}}; ">
                  <img src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->message_third_line_img}}" alt="" style="max-width: {{$value->maxwidth_third}}%; width: auto; height: auto;">
               </div>
               @endif
               @if ($value->message_fourth_line_img != "")
               
               <div class="flex_caption1 title1 captionDelay{{ 1 + (4 * 2)}} {{ $value->animationType->css_class }}" style="top : {{$value ->top_fourth}}% !important; text-align: {{$value->img_position}}; max-height : 15px;">
                  <img src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->message_fourth_line_img}}" alt="" style="max-width: {{$value ->maxwidth_fourth}}%; width: auto; height: auto;">
               </div>
               @endif
               @endif
               <!-- <div>
                  <a href="{{$value->btn_url}}">
                    <img class="" style="max-width: 10%; height:auto; border-radius: 0; position: absolute;" src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->btn_image}}" alt="">
                  </a>
                  </div> -->
               <div  style="text-align: {{$value->btn_animation_type}} !important;">
                  <a href="{{$value->btn_url}}" >
                  <img class="flex_caption2" style="top : {{$value ->top_button}}% !important; max-width : {{$value->maxwidth_button}}%; {{$value->btn_animation_type == 'left' ? 'left:0%;' : ''}}{{$value->btn_animation_type == 'right' ? 'right:0%;' : ''}}{{$value->btn_animation_type == 'center' ? 'left:'.$value->center_btn.'%;' : ''}} height:auto; border-radius: 0;" src="{{url('').'/core/storage/uploads/images/main_slider/'.$value->btn_image}}" alt="">
                  </a>
               </div>
            </div>
            <!-- //CONTAINER -->
         </li>
         <?php endforeach ?>
      </ul>
   </div>
   <!-- //TOP SLIDER -->
</section>
<!-- //HOME-->
<section id="main_icon">
   <div class="container">
      <div class="row padbot30">
         <div class="col-md-12">
            <h2 class="text-center">MAIN SERVICES WE PROVIDE</h2>
            <p class="text-center">Our services are the best in town, We provide great quality baked products.</p>
         </div>
      </div>
      <div class="row">
         <a href="{{url('/productload/all/all/1/min/hi')}}">
            <div class="col-md-3 col-sm-3 main_services_tile">
               <div class="main_icon" >
                  <div class="singel_icon">
                     <img src="{{asset('assets/front/images/icon/icon1.png')}}" class="img-responsive" alt="Cakes">
                  </div>
                  <h4 class="text-center main_icon">Cakes</h4>
                  <span class="underline-services-header text-center"></span>
                  <p class="p-center service_desc text-justify">All our cakes are custom made to order using freshest and finest ingredients available. Visit our gallery to see a list of our creations.</p>
               </div>
            </div>
         </a>
         <a href="{{url('tasting')}}">
            <div class="col-md-3 col-sm-3 main_services_tile">
               <div class="main_icon" >
                  <div class="singel_icon">
                     <img src="{{asset('assets/front/images/icon/icon3.png')}}" class="img-responsive" alt="Schdulea a Tasting">
                  </div>
                  <h4 class="text-center main_icon">Schdule a Tasting</h4>
                  <p class="p-center service_desc text-justify">Have your dream wedding cake designed by an award winning cake designer. Shedule your one-on-one consultaion and cake tasting with us today.</p>
               </div>
            </div>
         </a>
         <a href="{{url('onlineTutorials')}}">
            <div class="col-md-3 col-sm-3 main_services_tile">
               <div class="main_icon" >
                  <div class="singel_icon">
                     <img src="{{asset('assets/front/images/icon/icon2.png')}}" class="img-responsive" alt="Online Classes">
                  </div>
                  <h4 class="text-center main_icon">Online Classes</h4>
                  <p class="p-center service_desc text-justify">We offer series of classes on our website 24/7. whether you are a beginner, intermediate or an advance cake decorator there's a class for you.</p>
               </div>
            </div>
         </a>
         <a href="{{url('onlineTutorials')}}">
            <div class="col-md-3 col-sm-3 main_services_tile">
               <div class="main_icon" >
                  <div class="singel_icon">
                     <img src="{{asset('assets/front/images/icon/icon4.png')}}" class="img-responsive" alt="Online Classes">
                  </div>
                  <h4 class="text-center main_icon">RECIPES </h4>
                  <p class="p-center service_desc text-justify">Perfect your cake decorating skills with our easy to follow cake decorating recipes.</p>
               </div>
            </div>
         </a>
      </div>
   </div>
</section>
<section class="fix_silder_img" >
   <div class="fix_silder">
      <h1>In the News</h1>
   </div>
   <!-- //TOP SLIDER -->
</section>
<!-- RECENT POSTS -->
<section class="recent_posts">
   <!-- CONTAINER -->
   <div class="container">
      <h2 class="padbot30 text-center">Recent News</h2>
      <!-- ROW -->
      <div class="row" data-appear-top-offset='-100' data-animated='fadeInUp'>
         @foreach($news as $newsItem)
         <div class="col-lg-6 col-md-6 padbot30">
            <div class="recent_post_item clearfix">
               <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newsItem->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newsItem->created_at)->format('F'),0,3)}}</span></div>
               <a class="recent_post_img" href="{{url('newsDetails/'.$newsItem->id)}}" >
               @if(!empty($newsItem->getImages))
               <img class="blog-img-border" src="{{url('').'/core/storage/uploads/images/news'.'/'.$newsItem->front_image}}" alt="" />
               @else
               <img class="blog-img-border" src="{{asset('assets/front/images/blog/2.jpg')}}" alt="" />
               @endif
               </a>
               <a class="recent_post_title" href="{{url('newsDetails/'.$newsItem->id)}}" >
               {{$newsItem->title}}
               </a>
               <div class="recent_post_content text-justify">{!!substr($newsItem->description,0,450)!!}</div>
               <ul class="post_meta valign-bot pull-right">
                  <li>
                     <a href="{{ url('newsDetails/'.$newsItem->id) }}">
                     <button class="btn button-new">Read more <i class="fa fa-arrow-circle-right" style="font-size: 12px;" aria-hidden="true"></i></button>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         @endforeach
      </div>
      <!-- //ROW -->
   </div>
   <!-- //CONTAINER -->
</section>
<!-- //RECENT POSTS -->
<section class="fix_silder_img2" >
   <div class="fix_silder">
      <h1>Blog</h1>
   </div>
   <!-- //TOP SLIDER -->
</section>
<!-- RECENT POSTS -->
<section class="recent_posts">
   <!-- CONTAINER -->
   <div class="container">
      <h2 class="padbot30 text-center">Recent Posts</h2>
      <!-- ROW -->
      <div class="row" data-appear-top-offset='-100' data-animated='fadeInUp'>
         @foreach($blogs as $blog)
         <div class="col-lg-6 col-md-6 padbot30">
            <div class="recent_post_item clearfix">
               <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div>
               <a class="recent_post_img" href="{{url('blog/'.$blog->id)}}" >
               @if(count($blog->getImages) == 0)
               <img class="blog-img-border" src="{{asset('assets/front/images/blog/2.jpg')}}" alt="" />
               @else
               <img class="blog-img-border" src="{{asset($blog->getImages->first()->path.'/'.$blog->front_image)}}" alt="" />
               @endif
               </a>
               <a class="recent_post_title" href="{{url('blog/'.$blog->id)}}" >
               {{$blog->name}}
               </a>
               <div class="recent_post_content text-justify">{!!substr($blog->description,0,400)!!}</div>
               <ul class="post_meta valign-bot pull-right">
                  <li>
                     <a href="{{ url('blog/'.$blog->id) }}">
                     <button class="btn button-new">Read more <i class="fa fa-arrow-circle-right" style="font-size: 12px;" aria-hidden="true"></i></button>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         @endforeach
      </div>
      <!-- //ROW -->
   </div>
   <!-- //CONTAINER -->
</section>
<!-- //RECENT POSTS -->
<section class="fix_silder_img3" >
   <div class="fix_silder">
      <h1>Testimonials</h1>
   </div>
   <!-- //TOP SLIDER -->
</section>
<!-- Testimonies Slide show -->
<section class="new_arrivals padbot20">
   <!-- CONTAINER -->
   <div class="container">
      <div class="container_testimonies">
         <h2 class="text-center">What Our Clients Say About Us</h2>
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="testimonial">
               @foreach($testimonials as $testimonial)
               <div class="tovar_item_new ">
                  <div class="tovar_description clearfix max-lines10" style="margin-left: 25px; margin-right: 15px;">
                     <a class="tovar_title">{!! $testimonial->testimonial !!}</a>
                     <span class="tovar_price">{{$testimonial->name}}</span>
                  </div>
                  <div class="row text-center">
                     @if ($testimonial->url != "")
                     <a class="btn button-new" target="_blank" href="{{$testimonial->url}}" >Visit Url </a>
                     @endif
                     {{-- @if (strlen($testimonial->testimonial) > 800) --}}
                     <button class="btn button-new testiReadMore" data-id="{{$testimonial->testimonials_id}}">Read All</button>
                     {{-- @endif --}}
                  </div>
               </div>
               @endforeach
            </div>
         </div>
         <!-- JCAROUSEL -->
         <!-- <div class="jcarousel-wrapper"> -->
         <!-- NAVIGATION -->
         <!-- <div class="jCarousel_pagination" style="margin-top: 20px;">
            </div> --><!-- //NAVIGATION -->
         <!-- <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
            <ul class="col-lg-12 col-md-12 col-sm-12">
              @foreach($testimonials as $testimonial)
                  <li class="col-lg-10 col-md-10 col-sm-12">
                      <div class="tovar_item_new">
                          <div class="tovar_description clearfix" style="margin-left: 25px; margin-right: 15px;">
                              <a class="tovar_title">{!!substr($testimonial->testimonial,0,800)!!}</a>
                              <span class="tovar_price">{{$testimonial->name}}</span>
                              <a href="#" class="testimonial-more-btn">Click for more...</a>
                          </div>
                      </div>
                  </li>
              @endforeach
            </ul>
            </div>
            <a href="" style="left: 0; top: 50px;" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
            <a href="" style="right: 0; top: 50px;" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
            </div> --><!-- //JCAROUSEL -->
      </div>
      <!-- //CONTAINER -->
   </div>
</section>
<!-- //Testimonies Slide show -->
<!-- BRANDS -->
<section>
   <div class="container">
      <div class="customer-logos">
         @foreach($publicationImages as $publicationImage)
         <div class="slide">
            <img style="width: 225px; margin-right: 10px;" src="{{url('')."/core/storage/uploads/images/publication_slider/".$publicationImage->image}}">
         </div>
         @endforeach
      </div>
   </div>
</section>
<!-- BRANDS -->
@include('includes.commonModal')
@stop
@section('js')
<script type="text/javascript">
   $(document).ready(function(){
       $('.customer-logos').slick({
         slidesToShow: 5,
         slidesToScroll: 1,
         autoplay: true,
         autoplaySpeed: 5000,
         arrows: false,
         dots: false,
           pauseOnHover: false,
           responsive: [{
           breakpoint: 768,
           settings: {
             slidesToShow: 3
           }
         }, {
           breakpoint: 520,
           settings: {
             slidesToShow: 2
           }
         }]
       });
   
       $('.jcarousel').jcarousel({
   
       })
   
     });
   
   
   $('.testimonial').slick({
       slidesToScroll: 1,
       autoplay: true,
       autoplaySpeed: 5000,
       arrows: false,
       dots: true,
       infinite: true,
       speed: 2500,
       slidesToShow: 1,
       adaptiveHeight: true
   });
</script>
<script>
   $('.testiReadMore').on("click", function(){
       
       var id = $(this).data("id");
       $.ajax({
           url : 'get-testimonial',
           data : {
               id : id
           }
       }).done(function(data){
           if(data.name && data.testimonial ){
               $("#commonModalTitle").html("Read All Testimonial Of "+data.name)
               var urlAcnhor = "";
               if(data.url && data.url != ""){
                   urlAcnhor = '<div class="row text-center"><a class="btn button-new" target="_blank" href="'+data.url+'" >Visit Url </a></div>';
               }
               $("#commonModalBody").html( data.testimonial+urlAcnhor)
               $("#commonModal").modal("show")
           }
           
       });
   });
</script>
@stop