@extends('layouts.front.master') @section('title','Welcome | www.princeofgalle.com')
@section('css')

@stop




@section('content')
<section id="home" class="padbot0">
            <!-- TOP SLIDER -->
            <div class="flexslider top_slider" >
                <ul class="slides">
                <?php $i=0; foreach ($slider_details as $key => $value): ?>
                    <li class="slide1" style="background-image: url('{{url('').'/core/storage/uploads/images/main_slider/'.$value->image}}'); width:100%; background-size:cover;">
                        <div class="container">
                            <div class="flex_caption1" style="padding-right: 150px;">
                            <?php echo nl2br($value->animation_txt) ?>
                            </div>
                            <a class="flex_caption2" href="{{$value->btn_url}}" ><div class="middle">{{$value->btn_txt}}</div></a>
                            <div class="flex_caption3 slide_banner_wrapper">

                            </div>
                        </div><!-- //CONTAINER -->
                    </li>
                <?php endforeach ?>
                </ul>
            </div><!-- //TOP SLIDER -->
        </section><!-- //HOME-->

<section id="main_icon">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">MAIN SERVICES WE PROVIDE</h2>
                <p class="text-center">Our services are the best in town, We provide great quality baked products.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 frameStyle">
                <a href="#">
                    <div class="main_icon">
                        <div class="singel_icon">
                            <img src="{{asset('assets/front/images/icon/icon1.png')}}" class="img-responsive" alt="Cakes">
                        </div>
                        <h4 class="text-center">Cakes</h4>
                        <span class="underline-services-header text-center"></span>
                        <p class="text-justify service_desc">All our cakes are custom made to order using freshest and finest ingredients available. Visit our gallery to see a list of our creations.</p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 frameStyle">
                <a href="{{url('tasting')}}">
                    <div class="main_icon">
                        <div class="singel_icon">
                            <img src="{{asset('assets/front/images/icon/icon3.png')}}" class="img-responsive" alt="Schdulea a Tasting">
                        </div>
                        <h4 class="text-center">Schdulea a Tasting</h4>
                        <p class="text-justify service_desc">Have your dream wedding cake designed by an award winning cake designer. Shedule your one-on-one consultaion and cake tasting with us today.</p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 frameStyle">
                <a href="{{url('onlineTutorials')}}">
                    <div class="main_icon">
                        <div class="singel_icon">
                        <img src="{{asset('assets/front/images/icon/icon2.png')}}" class="img-responsive" alt="Online Classes">
                        </div>
                        <h4 class="text-center">Online Classes</h4>
                        <p class="text-justify service_desc">We offer series of classes on our website 24/7. whether you are a beginner, intermediate or an advance cake decorator there's a class for you.</p>
                    </div>
                </a>
            </div>
            <!-- <div class="col-md-3">
                <div class="main_icon">
                    <div class="singel_icon">
                        <img src="{{asset('assets/front/images/icon/icon4.png')}}" class="img-responsive" alt="Recipes">
                    </div>
                <h4 class="text-center">Recipes</h4>
                </div>
            </div> -->
        </div>

    </div>
</section>



            <section class="fix_silder_img" >
                <div class="fix_silder">
                    <h1>In the News</h1>
                </div><!-- //TOP SLIDER -->
            </section>
            <section id="the_news">
                <div class="container text-center">
                    <div class="col-md-6">
                        <h2 class="text-center the_news_h2">Meet Tony & Beth From T.E.P Studios</h2>
                        <p>The data-ride="carousel" attribute is used to mark a carousel as animating starting at page load. It cannot be used in combination with (redundant and unnecessary) explicit JavaScript initialization of the same carousel.</p>
                        <ul>
                            <li>We Have is controlled by a combination of the perspective and the</li>
                            <li>We Have is controlled by a combination of the perspective and the</li>
                            <li>We Have speed is controlled by a combination of the perspective and the</li>
                            <li>We Have speed is controlled by a combination of the perspective and the</li>
                        </ul>
                        <button type="button" class="btn btn_index">Read More</button>
                    </div>
                    <div class="col-md-6">
                        <h2 class="text-center the_news_h2">Meet Tony & Beth From T.E.P Studios</h2>
                        <p>The data-ride="carousel" attribute is used to mark a carousel as animating starting at page load. It cannot be used in combination with (redundant and unnecessary) explicit JavaScript initialization of the same carousel.</p>
                        <ul>
                            <li>We Have is controlled by a combination of the perspective and the</li>
                            <li>We Have is controlled by a combination of the perspective and the</li>
                            <li>We Have speed is controlled by a combination of the perspective and the</li>
                            <li>We Have speed is controlled by a combination of the perspective and the</li>
                        </ul>
                        <button type="button" class="btn btn_index">Read More</button>
                    </div>
                </div>
            </section>
            <section class="fix_silder_img2" >
                <div class="fix_silder">
                    <h1>Blog</h1>
                </div><!-- //TOP SLIDER -->
            </section>

            <!-- RECENT POSTS -->
            <section class="recent_posts">

                <!-- CONTAINER -->
                <div class="container">
                    <h2>New blog posts</h2>

                    <!-- ROW -->
                    <div class="row" data-appear-top-offset='-100' data-animated='fadeInUp'>
                        <div class="col-lg-6 col-md-6 padbot30">
                            <div class="recent_post_item clearfix">
                                <div class="recent_post_date">15<span>oct</span></div>
                                <a class="recent_post_img" href="blog-post.html" ><img src="{{asset('assets/front/images/blog/recent1.jpg')}}" alt="" /></a>
                                <a class="recent_post_title" href="blog-post.html" >Be Unafraid, Self-Hosted WordPress Is WAY Easier Nowadays</a>
                                <div class="recent_post_content">The beauty of self-hosted WordPress, is that you can build your site however you like, want to add forums to your website? Done. Want to add a ecommerce to your blog? Done.</div>
                                <ul class="post_meta">
                                    <li><i class="fa fa-comments"></i>Commetcs <span class="sep">|</span> 15</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 padbot30">
                            <div class="recent_post_item clearfix">
                                <div class="recent_post_date">07<span>dec</span></div>
                                <a class="recent_post_img" href="blog-post.html" ><img src="{{asset('assets/front/images/blog/recent2.jpg')}}" alt="" /></a>
                                <a class="recent_post_title" href="blog-post.html" >True Story: I Went Two Weeks Without Social Media</a>
                                <div class="recent_post_content">Since I began blogging 5.5 years ago, social media (and my blog) have taken hold on my life. I’ve been an early adopter for most major networks and use them extensively.  This past year I’ve been overwhelmed.</div>
                                <ul class="post_meta">
                                    <li><i class="fa fa-comments"></i>Commetcs <span class="sep">|</span> 15</li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- //ROW -->
                </div><!-- //CONTAINER -->
            </section><!-- //RECENT POSTS -->

            <section class="fix_silder_img3" >
                <div class="fix_silder">
                    <h1>TESTIMONIALS</h1>
                </div><!-- //TOP SLIDER -->
            </section>

            <!-- Testimonies Slide show -->
            <section class="new_arrivals padbot50">

                <!-- CONTAINER -->
                <div class="container">
                <div class="container_testimonies">
                    <h2>TESTIMONIALS</h2>

                    <!-- JCAROUSEL -->
                    <div class="jcarousel-wrapper">

                        <!-- NAVIGATION -->
                        <div class="jCarousel_pagination">
                            <a href="javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
                        </div><!-- //NAVIGATION -->

                        <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                            <ul>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                        <!-- <div class="tovar_img">
                                            <img src="images/tovar/women/new/1.jpg" alt="" />
                                            <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a></div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >Nisha was extremely responsive and great to work with! I ordered cookies that were delicious and so pretty! Her attention to detail is great. I will definitely use her again in the future and highly recommend her.</a>
                                            <span class="tovar_price">Nicole D.</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                    <!--    <div class="tovar_img">
                                            <img src="images/tovar/women/new/2.jpg" alt="" />
                                            <div class="open-project-link">
                                                <a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a>
                                            </div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >The cake that I had went above and beyond anything I could have thought of. Nisha is a true artist with baked goods! She was able to create a cake that was not only beautiful but tasted unlike any other cake I have ever had! I would definitely go back to her for other special events and recommend her to anyone that wants something truly special for their event.</a>
                                            <span class="tovar_price">Jackie B</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                        <!-- <div class="tovar_img">
                                            <img src="images/tovar/women/new/3.jpg" alt="" />
                                            <div class="open-project-link">
                                                <a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a>
                                            </div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >Melt in your mouth delicious! The flavors were vibrant, not over powering, well balanced and paired perfectly! If there were any left, I would have gone back for seconds, and thirds!</a>
                                            <span class="tovar_price">Lisette N.</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                        <!-- <div class="tovar_img">
                                            <img src="images/tovar/women/new/4.jpg" alt="" />
                                            <div class="open-project-link">
                                                <a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a>
                                            </div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >Nisha was a pleasure to work with and very attentive. I was very picky with my selections and she was very patient when working with me. She is a perfectionist with her work. I would highly recommend her.</a>
                                            <span class="tovar_price">Michelle H.</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                                <li>

                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                    <!--    <div class="tovar_img">
                                            <img src="images/tovar/women/new/3.jpg" alt="" />
                                            <div class="open-project-link">
                                                <a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a>
                                            </div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >EMBROIDERED BIB PEASANT TOP</a>
                                            <span class="tovar_price">$88.00</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12">
                                    <!-- TOVAR -->
                                    <div class="tovar_item_new">
                                    <!--    <div class="tovar_img">
                                            <img src="images/tovar/women/new/4.jpg" alt="" />
                                            <div class="open-project-link">
                                                <a class="open-project tovar_view" href="javascript:void(0);" data-url="%21projects/women/1.html" >quick view</a>
                                            </div>
                                        </div> -->
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >SILK POCKET BLOUSE</a>
                                            <span class="tovar_price">$98.00</span>
                                        </div>
                                    </div><!-- //TOVAR -->
                                </li>
                            </ul>
                        </div>
                    </div><!-- //JCAROUSEL -->
                </div><!-- //CONTAINER -->
                </div>
            </section><!-- //Testimonies Slide show -->

            <!-- BRANDS -->
            <section class="brands_carousel">

                <!-- CONTAINER -->
                <div class="container">

                    <!-- JCAROUSEL -->
                    <div class="jcarousel-wrapper">

                        <!-- NAVIGATION -->
                        <div class="jCarousel_pagination">
                            <a href="javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
                        </div><!-- //NAVIGATION -->

                        <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                            <ul>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/1.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/2.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/3.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/4.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/5.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/6.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/7.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/8.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/9.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/10.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/11.jpg')}}" alt="" /></a></li>
                                <li><a href="javascript:void(0);" ><img src="{{asset('assets/front/images/brands/12.jpg')}}" alt="" /></a></li>
                            </ul>
                        </div>
                    </div><!-- //JCAROUSEL -->
                </div><!-- //CONTAINER -->
            </section><!-- //BRANDS -->


@stop

@section('js')

@stop
