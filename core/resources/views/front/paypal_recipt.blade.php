@extends('layouts.front.master') @section('title','Recipt| www.princeofgalle.com')
@section('css') 

<style>
    .main {
        width: 8.27in;
        height: 11.69in;
        background: #ffffff;
        border-bottom: 15px solid #F8B4B9;
        border-top: 15px solid #8B5730;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
        margin-bottom: 30px;
        padding: 40px 30px !important;
        position: relative;
        box-shadow: 0 1px 21px #808080;
        font-size: 10px;
    }

    .main thead {
        background: #8B5730;
        color: #fff;
        }
    p {
    display: block;
    -webkit-margin-before: 0.2em;
    -webkit-margin-after: 0.2em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    }
    .img-responsive{
        margin-top: -18px;
        width: 100px;
        clear: both;

    }

}
</style>

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30">

    </section>
    
    <!-- <section class="checkout_page">
      <div class="container">
        <div class="checkout_block">
          <ul class="checkout_nav checkout-padding-added">
            <a href="{{url('checkout1')}}"><li class="active_step">1. Shipping Details</li></a>
            <a href="{{url('checkout2')}}"><li>2. Delivery Details</li></a>
            <a href="{{url('checkout3')}}"><li>3. Payment Details</li></a>
            <a href="{{url('checkout4')}}"><li class="last">4. Confirm Order</li></a>
          </ul>
        </div>
      </div>
    </section> -->

<!-- Invoice - START -->
<br>
<p class="text-center">
                    <a href="{{route('paypal_print', $purchase->inoice_no)}}" target="_blank" class="btn btn-primary btn-sm">
                          <span class="glyphicon glyphicon-print"></span><!-- <i class="fa fa-print"></i> --> Print 
                        </a>
                     </p> 
{!!Form::token()!!}
@include('includes.paypal_receipt_body')
    
@stop

@section('js')

 
@stop