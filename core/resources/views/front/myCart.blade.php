@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<style type="text/css">
    th.table-head{
        font-size: 16px !important;
        font-weight: normal !important;
    }

    .summery h2 {
        font-size: 22px;
        font-weight: 700;
    }

    .summery h2 a {
        font-size: 17px;
        font-weight: 500;
        color: #0000ff;
    }

    .summery h2 a:hover {
        color: #0000ff;
        text-decoration: underline;
    }

    .summery-head h2 {
        font-weight: 400;
        font-size: 20px;
    }

    .summery-total h2 {
        font-weight: 900;
        font-size: 26px;
    }

    .btn-checkout {
        background-color: #8b5730;
        border-color: #8b5730;
        color: white;
        width: 100%;
        padding: 10px 23px;
        font-size: 16px;
        font-weight: 700;
        border-radius: 6px;
    }

    .btn-checkout:hover {
        background-color: #583316;
        border-color: #583316;
        font-size: 12px;
        font-weight: 700;
        border-radius: 10px;
    }

    .btn-shopping {
        background-color: transparent;
        border-color: #8b5730;
        color: #8b5730;
        width: 100%;
        margin: 0 0 20px 0;
        padding: 10px 23px;
        font-size: 14px;
        font-weight: 700;
        border-radius: 6px;
    }

    .btn-shopping:hover {
        background-color: transparent;
        border-color: #583316;
        color: #583316;
        font-size: 12px;
        font-weight: 700;
        border-radius: 10px;
    }

    input[type="text"].coupon-box {
        width: 100%;
        height: 38px!important;
        font-size: 15px!important;
        border-radius: 5px;
    }

    .btn-coupon {
        right: 0;
        width: 100%;
        background-color: transparent;
        border-color: #8b5730;
        color: #8b5730;
        font-size: 14px;
        font-weight: 600;
        border-radius: 5px;
    }

    .btn-coupon:hover {
        right: 0;
        background-color: #8b5730;
        border-color: #8b5730;
        color: #fff;
    }
    textarea, input[type="text"], input[type="password"], input[type="date"], input[type="email"], input[type="number"], .input-group select{
        font-size: 16px!important;
    }
    table tr.cart_item td.item-quantity .update_text a{
        color: #fff!important;
        background: #000;
    }
</style>
@stop
@section('content')
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


        <!-- PAGE HEADER -->
        <section class="page_header">

            <!-- CONTAINER -->
             <hr class="banner-top"/>
            <div class="banner-bg center">
            <h3>MY SHOPPING CART</h3>
            <p>All the products you added to cart will be available here!</p>
            </div>
            <hr class="banner-bottom"/>
        </section><!-- //PAGE HEADER -->
<!-- SHOPPING BAG BLOCK -->
<section class="shopping_bag_block">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- CART TABLE -->
            <div class="col-lg-12 col-md-12 padbot40">
                <h2>Shopping Cart</h2>
                <table class="table table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-center product-remove">&nbsp;</th>
                            <th class="text-center product-thumbnail">&nbsp;</th>
                            <th class="table-head">Product</th>
                            <th class="table-head">Price</th>
                            <th class="table-head">Quantity</th>
                            <th class="text-right table-head" style="padding-right: 45px;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($shoppingCartArray_product as $key => $value): ?>
                        <tr class="woocommerce-cart-form cart_item">
                            <td class="text-center"><a style="cursor: pointer;" onclick="removeFromCart({{$value['id']}},{{$value['type']}})"><span class="fa fa-remove" style="font-size: 18px;"></span></a></td>
                            <td class="product-thumbnail"><a href="{{url().'/product-detail/'.$value['id']}}" ><img src="{{$value['image']}}" width="100px" alt="" /></a></td>
                            <td class="product-name" data-title="Product">
                                <a href="{{url().'/product-detail/'.$value['id']}}">{{$value['name']}}</a>
                            </td>
                            <td class="product-price" data-title="Price">US ${{number_format($value['price'],2)}}</td>
                            <td class="item-quantity">
                                <input type="number" class="item-quantity" min="0" name="quantity" value="{{$value['quantity']}}" onchange="updateQTY({{$value['id']}}, $(this),{{$value['type']}})" onkeydown="updateQTY({{$value['id']}}, $(this,{{$value['type']}}))"><div id="line_{{$value['id']}}" class="update_text"></div>
                            </td>
                            <td class="product-subtotal" data-title="Total">
                                <span class="woocommerce-Price-amount amount">US ${{number_format($value['price']*$value['quantity'],2)}}</span>
                            </td>
                        </tr>
                        <?php endforeach ?>
                          <?php foreach ($shoppingCartArray_tutorial as $key => $value): ?>
                        <tr class="woocommerce-cart-form cart_item">
                            <td><a onclick="removeFromCart({{$value['id']}},{{$value['type']}})"><span class="fa fa-remove fa-2x text-danger"></span></a></td>
                            <td class="product-thumbnail"><a href="{{url().'/product-detail/'.$value['id']}}" ><img src="{{$value['image']}}" width="100px" alt=""/></a></td>
                            <td class="product-name" data-title="Product">
                                <a href="{{url().'/viewTutorials/paid/'.$value['id']}}">{{$value['name']}}</a>
                            </td>
                            <td class="product-price" data-title="Price">US$ {{number_format($value['price'],2)}}</td>
                            <td class="item-quantity">
                                <input type="number" class="item-quantity" readonly="" name="quantity" value="{{$value['quantity']}}" >
                            </td>
                            <td class="product-subtotal" data-title="Total">
                                <span class="woocommerce-Price-amount amount">US ${{number_format($value['price']*$value['quantity'],2)}}</span>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>

                </table>

                <hr>
                <div class="col-md-4 pull-right">
                	<div class="row continue-shopping-btn">
                        <a href="{{url('productload/all/all/1/min/hi')}}"><button class="btn btn-success btn-shopping">Continue shopping</button></a>
                    </div>
                    <div class=" summery-head">
                        <h2>Cart Summary</h2>
                        <hr>
                    </div>
                    <div class="row summery">
                        <div class="col-md-6">
                            <h3>Subtotal</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <h3>US ${{number_format($subTotal,2)}}</h3>
                        </div>
                    </div>
                     <div class="row summery">
                        <div class="col-md-6">
                            <h3>Shipping</h3>
                        </div>
                        <div class="col-md-6 text-right">

                             <?php if (!empty($shipping_condition)): ?>
                                <h3>US ${{number_format(abs($shipping_condition->getValue()),2)}}</h3>

                             <?php else: ?>
                                 <h3><a href="{{url('checkout2')}}">Calculate shipping</a></h3>
                             <?php endif ?>

                        </div>
                    </div>
                    <hr>
                    <?php if ($subTotal>0): ?>
                    <div class="row" style="margin-top: 35px;">
                        <div class="col-md-12">
                            <p>Do you have a coupon code? Enter it here</p>
                        </div>
                        <div class="col-md-8">
                              <?php if (!empty($coupon_condition)): ?>
                                 <input placeholder="5AA-EB7-DB8-717" id="coupon-box" value="{{$coupon_condition->getAttributes()['coupon_code']}}" class="coupon-box" disabled="" type="text"/>
                                  <label><span>coupon value is $ </span><span id="coupon_value">{{number_format(abs($coupon_condition->getValue()),2)}}</span></label>
                             <?php else: ?>
                                <?php if ($subTotal>0): ?>
                                    <input placeholder="5AA-EB7-DB8-717" id="coupon-box" value="" class="coupon-box" type="text"/>
                                   <label><span id="coupon_value"></span></label>
                                <?php endif ?>

                             <?php endif ?>
                        </div>
                         <div class="col-md-4 text-right">
                              <?php if (!empty($coupon_condition)): ?>
                                 <input type="button" id="coupon_button" class="btn btn-info btn-coupon" value="REMOVE" onclick="remove_coupon()" name="">
                             <?php else: ?>

                                    <input type="button" id="coupon_button" class="btn btn-info btn-coupon" value="VERIFY" onclick="verify_coupon()" name="">


                             <?php endif ?>

                        </div>
                    </div>
                     <hr>
                      <?php endif ?>
                    <div class="row summery-total">
                        <div class="col-md-6">
                            <h3>Total</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <h3>US $<span id="total">{{number_format($total,2)}}</span></h3>
                        </div>
                    </div>
                    <hr>
                    <div class="row" style="margin-bottom: 10px;">
                        <?php if ($cartTotalQuantity > 0): ?>
                        <a href="{{url('checkout1')}}"><button class="btn bg-danger btn-checkout">Proceed to payment</button></a>
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <br>


        </div>
        <!-- //CART TABLE -->
        <!-- SIDEBAR -->
        <!-- //SIDEBAR -->
    </div>
    <!-- //ROW -->
    </div>
    <!-- //CONTAINER -->
</section>
<!-- //SHOPPING BAG BLOCK -->
@stop
@section('js')
<script type="text/javascript">
    var chunk_length=0;
    $( document ).ready(function() {
      cupon_code_format();
    });
    $( "#coupon-box" ).keyup(function() {
      cupon_code_format();
    });

    function cupon_code_format() {

        var $this = $('#coupon-box');
        var input = $this.val();


        input = input.replace(/[\W\s\._\-]+/g, '');


        var split = 0;
        var chunk = [];

        for (var i = 0, len = input.length; i < len; i += split) {
            split = 3;
            if(chunk.length<4){
            chunk_length=chunk.length;
            chunk.push( input.substr( i, split ) );
             }
        }


        $this.val(function() {

                return chunk.join("-").toUpperCase();


        });
    }

    function removeFromCart(id, type) {
    $.ajax({method: "GET",
            url:'{{url('cart/remove')}}/'+id+'/'+type})
            .done(function(msg) {
            window.location.href='{{url('my-cart')}}';
            });
    }
    function updateQTY(id, thisObj, type) {

        $('#line_'+id).empty();
        $('#line_'+id).append(
        '<a class="update-text" href="{{url('cart/update/qty')}}/'+id+'/'+thisObj.val()+'/'+type+'">update</a>'
        );
    }

     function verify_coupon() {
        var coupon_code=$('#coupon-box').val();
        if (coupon_code !=='' && chunk_length==3) {
             $.ajax({
              method: "GET",
              url: '{{url('coupon/verify')}}',
              data:{ 'coupon_code' : coupon_code  }
            })
              .done(function( result ) {
                if (result['coupon'].length!==0) {
                    $('#coupon_button').val('REMOVE');
                    $("#coupon-box").attr("disabled",true);
                    $("#coupon_button").attr("onclick","remove_coupon()");
                    $('#total').html(result['cart_total']);
                    $('#coupon_value').html('coupon value is $ '+result['coupon']['coupon']['value']);
                } else {
                     swal("Invalied Coupon!", "", "error");
                }


              });

        } else {
            swal("Invalied Coupon!", "coupon code can not be empty!", "error");

        }
    }
    function remove_coupon() {
        var coupon_code=$('#coupon-box').val();
         $.ajax({
              method: "GET",
              url: '{{url('coupon/remove')}}',
              data:{ 'coupon_code' : coupon_code  }
            })
          .done(function( result ) {
            if (result['done']) {
                $('#coupon_button').val('VERIFY');
                 $('#total').html(result['cart_total']);
                 $("#coupon-box").attr("disabled",false);
                 $("#coupon_button").attr("onclick","verify_coupon()");
                 $("#coupon-box").val('');
                 $('#coupon_value').html('');
            } else {

            }

          });
    }
</script>
@stop
