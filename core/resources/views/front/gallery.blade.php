@extends('layouts.front.master') @section('title','Gallery')

@section('css')
    <style type="text/css">
        .pinterest-icon{
            width: 32px;
        }
        .gal {
            -webkit-column-count: 4;
            -moz-column-count: 4;
            column-count: 4;
        }

        .gal img {
            /*width: 100%;*/
            padding: 7px 0;
        }

        @media (max-width: 500px) {
            .gal {
                -webkit-column-count: 1;
                -moz-column-count: 1;
                column-count: 1;
            }
        }

        .hovereffect { 
            width: 100%;
            /* height: 100%; */
            float: left;
            overflow: hidden;
            position: relative;
            text-align: center;
            cursor: default;
        }

        .hovereffect .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            overflow: hidden;
            top: 0;
            right: 0;
            left: 0;
            opacity: 0;
            background-color: rgba(0,0,0,0.6);
            -webkit-transition: all .4s ease-in-out;
            transition: all .4s ease-in-out
        }

        .hovereffect img {
            display: block;
            position: relative;
            -webkit-transition: all .4s linear;
            transition: all .4s linear;
        }

        .hovereffect h2 {
            text-transform: uppercase;
            color: #fff;
            text-align: center;
            position: relative;
            font-size: 11px;
            background: rgba(255,255,255,0.2);
            -webkit-transform: translatey(-100px);
            -ms-transform: translatey(-100px);
            transform: translatey(-100px);
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            padding: 10px;
            /*margin-top: 25%;*/
        }

        .hovereffect a.info {
            width: 50px;
            height: 50px;
            padding: 16px 16px;
            font-size: 18px;
            line-height: 1;
            border-radius: 25px;
            text-decoration: none;
            /*display: inline-block;*/
            text-transform: uppercase;
            color: #fff;
            /*border: 1px solid #fff;*/
            background-color: rgba(255,255,255,0.2);
            opacity: 0;
            filter: alpha(opacity=0);
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            /*!*margin: 50px 0 0;*!*/
            margin-top: 5px;
            margin-right: 5px;
        }

        .hovereffect a.info:hover {
            box-shadow: 0 0 1px #bbb;
            /*font-size: 20px;
            padding: 14px 14px;
            -webkit-transition: all .3s ease-in-out;*/
        }

        .hovereffect:hover img {
            -ms-transform: scale(1.2);
            -webkit-transform: scale(1.2);
            transform: scale(1.2);
        }

        .hovereffect:hover .overlay {
            opacity: 1;
            filter: alpha(opacity=100);
        }

        .hovereffect:hover h2, .hovereffect:hover a.info {
            opacity: 1;
            filter: alpha(opacity=100);
            -ms-transform: translatey(0);
            -webkit-transform: translatey(0);
            transform: translatey(0);
        }

        .hovereffect:hover a.info {
            -webkit-transition-delay: .2s;
            transition-delay: .2s;
        }

        .img-hover-h2 {
            margin-top: 0px!important;
            margin-bottom: 30px;
        }

        .hover-container {
            padding-top: 50%;
            vertical-align: middle;
            position: relative;
            /*top: 50%;*/


        }
           .keywords-shop {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
}

.keywords-shop li{
    vertical-align: bottom;
    float: left;
    padding: 6px 10px 6px 10px;
    width: auto;
    margin-bottom: 6px;
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.25em;
    border-radius: 5px;
    /*border: thin solid #8b5730;*/
    border: thin solid #e5d4cd;

}

.keywords-shop li:hover{
    vertical-align: bottom;
    float: left;
    padding: 6px 10px 6px 10px;
    width: auto;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: none;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-shop li:hover a {
  color : #e5d4cd;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-hover-new-shop li:hover{
    vertical-align: bottom;
    float: left;
    padding: 6px 0px 6px 0px;
    width: 100px;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    color: #ffffff;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: none;
    -webkit-transition: all 0.4s ease-in-out;
}

    /* input.newsletter_btn{
        margin-bottom: 13px!important;
        width: 100%!important;
        padding: 6px!important;
        font-size: 14px!important;
        font-family: 'Roboto', sans-serif!important;
        text-transform: uppercase!important;
        font-weight: 600!important;
        font-style: normal!important;
        line-height: 20px!important;
        background-color: transparent !important;
        color: #8b5730 !important;
        border: 1px solid #8b5730;
        border-radius: 4px;
        font-size: 12px;
        font-weight: 600;
        padding: 6px 10px;
        margin: 3px 0 0 0;
    }

    input.newsletter_btn:hover {
        background-color:#5d391e !important;
        color: #eeeeee !important;
    } */




#portfolio {
    margin: 1rem 0;
    -webkit-column-count: 4;
    -moz-column-count: 4;
    column-count: 4;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
    -webkit-column-width: 25%;
    -moz-column-width: 25%;
    column-width: 25%;
}
#portfolio .tile {
    -webkit-transform: scale(0);
    transform: scale(0);
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease;

}
#portfolio .tile:hover {

}

#portfolio .scale-anm {
  transform: scale(1);
}


#portfolio p{
  padding:10px;
  border-bottom: 1px #ccc dotted;
  text-decoration: none;
  font-family: lato;
  text-transform:uppercase;
  font-size: 12px;
  color: #333;
  display:block;
  float:left;
}

#portfolio p:hover {
  cursor:pointer;
  background: #333;
  color:#eee; }

#portfolio .tile img {
    max-width: 100%;
    width: 100%;
    height: auto;
    margin-bottom: 1rem;

}

#portfolio .btn {
    font-family: Lato;
    font-size: 1rem;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
    display: inline-block;
    line-height: normal;
    padding: .5rem 1rem;
    margin: 0;
    height: auto;
    border: 1px solid;
    vertical-align: middle;
    -webkit-appearance: none;
    color: #555;
    background-color: rgba(0, 0, 0, 0);
}

#portfolio .btn:hover {
  text-decoration: none;
}

#portfolio .btn:focus {
  outline: none;
  border-color: var(--darken-2);
  box-shadow: 0 0 0 3px var(--darken-3);
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}
.toolbar{
    text-align: center;
}


    .mbottom{
    margin-top: 72px;
    }

#portfolio {
    margin: 1rem 0;
    -webkit-column-count: 4;
    -moz-column-count: 4;
    column-count: 4;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
    -webkit-column-width: 25%;
    -moz-column-width: 25%;
    column-width: 25%;
}
#portfolio .tile {
    -webkit-transform: scale(0);
    transform: scale(0);
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease;

}
#portfolio .tile:hover {

}

#portfolio .scale-anm {
  transform: scale(1);
}


#portfolio p{
  padding:10px;
  border-bottom: 1px #ccc dotted;
  text-decoration: none;
  font-family: lato;
  text-transform:uppercase;
  font-size: 12px;
  color: #333;
  display:block;
  float:left;
}

#portfolio p:hover {
  cursor:pointer;
  background: #333;
  color:#eee; }

#portfolio .tile img {
    max-width: 100%;
    width: 100%;
    height: auto;
    margin-bottom: 1rem;

}

#portfolio .btn {
    font-family: Lato;
    font-size: 1rem;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
    display: inline-block;
    line-height: normal;
    padding: .5rem 1rem;
    margin: 0;
    height: auto;
    border: 1px solid;
    vertical-align: middle;
    -webkit-appearance: none;
    color: #555;
    background-color: rgba(0, 0, 0, 0);
}

#portfolio .btn:hover {
  text-decoration: none;
}

#portfolio .btn:focus {
  outline: none;
  border-color: var(--darken-2);
  box-shadow: 0 0 0 3px var(--darken-3);
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}
.toolbar{
    text-align: center;
}
  .banner-bottom{
    margin-bottom: 72px!important;
  }
</style>
@stop


@section('content')
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


        <!-- PAGE HEADER -->
        <section class="page_header">

            <!-- CONTAINER -->
             <hr class="banner-top"/>
            <div class="banner-bg center">
                <h3>Our Gallery</h3>
                <p>View our portfolio of cakes!</p>
            </div>
            <hr class="banner-bottom"/>
        </section><!-- //PAGE HEADER -->

<div style="clear:both;"></div>



    <!-- TYPOGRAPHY BLOCK -->
    <!-- <section class="typography_block padbot40"> -->

        <!-- CONTAINER -->
        <div class="container" style="text-align: center">

              <!-- <section class="search-section" style="clear: both; margin-bottom: 30px !important;">
                  <div class="container">
                    <div class="row">
                        <ul class="keywords keywords-hover-new">
                        @if($val == 0)
                            <a class="filter-button" data-filter="all" href="{{url('/gallery/0')}}"><li style="color: #fff; background-color:#8b5730;"> Filter - All</li></a>
                        @else
                            <a class="filter-button" data-filter="all" href="{{url('/gallery/0')}}"><li> Filter - All</li></a>
                        @endif
                            
                        
                        @foreach($cat as $c)
                        @if($val == $c->id)
                            <a class="filter-button" data-filter="{{ str_replace(' ', '', $c->name) }}" href="{{url('/gallery/'.$c->id)}}">
                                <li style="color: #fff; background-color:#8b5730;">{{$c->name}}</li>
                            </a>
                        @else
                            <a class="filter-button" data-filter="{{ str_replace(' ', '', $c->name) }}" href="{{url('/gallery/'.$c->id)}}">
                                <li>{{$c->name}}</li>
                            </a>
                        @endif
                        @endforeach
                    </ul>

                </div>
              </div>
            </section> -->


            <section class="search-section" style="clear: both; margin-bottom: 70px !important;">
                    <div class="container" style="padding-left: 0!important; padding-right: 0!important;">
                        <div class="btn-group" style="margin-top: 10px; margin-left: auto; margin-right: auto; width: 100%;">

                            <ul class="keywords-shop">
                              @if($val == 0)
                                <li>
                                    <a class="filter-button active" href="{{url('/gallery/0')}}">Filter - All</a>
                                </li>
                              @else
                                <li>
                                    <a class="filter-button" href="{{url('/gallery/0')}}">Filter - All</a>
                                </li>
                              @endif
                              @foreach($cat as $c)
                                @if($val == $c->id)
                                <li>
                                    <a data-filter="{{ str_replace(' ', '', $c->name) }}" href="{{url('/gallery/'.$c->id)}}" class="filter-button active">{{$c->name}}</a>
                                </li>
                                @else
                                <li>
                                    <a data-filter="{{ str_replace(' ', '', $c->name) }}" href="{{url('/gallery/'.$c->id)}}" class="filter-button">{{$c->name}}</a>
                                @endif
                              @endforeach
                            </ul>
                        </div>
                    </div>
                </section>


<!--

Reference URL for this method:

http://www.brandaiddesignco.com/blog/add-a-custom-pinterest-pin-it-button-to-your-website/375/

-->

            <div class="gal" id="gale">
                @foreach($gal as $key => $item)
                    <div class="hovereffect filter ">
                        {{--<div style="position:absolute">--}}
                            {{--<a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img class="pinterest-icon" src="{{asset('assets/front/images/Pinterest-icon.png')}}" /></a>--}}

                        {{--</div>--}}

                        <img class="img-responsive" style="transform: none;" src="{{url('').'/core/storage/'.$item->path.'/'.$item->filename}}" alt="" data-pin-do="buttonBookmark">

                        <div class="overlay">
                            <div class="row hover-container">

                                <a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img class="pinterest-icon" src="{{asset('assets/front/images/Pinterest-icon.png')}}" /></a>

                                <h2 class="img-hover-h2">{{$item->album_name}}</h2>
                                <a class="info zoom" data-gallery="" href="{{url('').'/core/storage/'.$item->path.'/'.$item->filename}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a class="info" href="{{url('')."/album/".$item->id}}"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">  <!-- commented class // blueimp-gallery-controls -->
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
        </div>
        <div class="mbottom" />
    </section>



@stop

@section('js')
    <script>

        jQuery(window).load(function(){
            $('.gallery_item').magnificPopup({
                delegate: 'a.zoom',
                type: 'image',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });
        });
        $(document).ready(function () {

            $(".filter-button").click(function () {
                var value = $(this).attr('data-filter');

                if (value == "all") {
                    console.log("hello");
                    //$('.filter').removeClass('hidden');
                    $('.filter').show();
                }
                else {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.' + value).hide();
                    $('.filter').filter('.' + value).show();

                }
                $(this).addClass('active').siblings().removeClass('active');
            });

            // if ($(".filter-button").removeClass("active")) {
            //     $(this).removeClass("active");
            // }
            $(this).addClass("active");
        });


    </script>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>

<script type="text/javascript">
    $(function() {
        var selectedClass = "";
        $(".fil-cat").click(function(){
        selectedClass = $(this).attr("data-rel");
     $("#portfolio").fadeTo(100, 0.1);
        $("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
    setTimeout(function() {
      $("."+selectedClass).fadeIn().addClass('scale-anm');
      $("#portfolio").fadeTo(300, 1);
    }, 300);


    });
});
</script>

@stop
