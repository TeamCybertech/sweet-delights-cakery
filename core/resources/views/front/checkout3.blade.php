@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

@stop


@section('content')
  
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
    
    <form method="POST" id="form">
       {!!Form::token()!!}
      <!-- CHECKOUT PAGE -->
    <section class="checkout_page">
      
      <!-- CONTAINER -->
      <div class="container">

        <!-- CHECKOUT BLOCK -->
        <div class="checkout_block">
          <ul class="checkout_nav checkout-padding-added">
            <a href="{{url('checkout1')}}"><li>1. Shipping Details</li></a>
            <a href="{{url('checkout2')}}"><li>2. Delivery Details</li></a>
            <a href="{{url('checkout3')}}"><li class="active_step">3. Payment Details</li></a>
            <a href="{{url('checkout4')}}"><li class="last">4. Confirm Order</li></a>
          </ul>

          <!-- <div class="checkout_delivery_note alert alert-danger alert-dismissable"><i class="fa fa-exclamation-circle"></i>Express delivery options are available for in-stock items only.</div> -->
          <!-- <div class="alert alert-danger alert-dismissable"><i class="fa fa-exclamation-circle"></i> We only allow PayPal as the payment Option!</div> -->
          <div class="checkout_payment clearfix">
            <div class="payment_method padbot70">
              <!-- <p class="checkout_title">payment Method</p> -->
              
              <ul class="clearfix">

                <p style="margin-top: 50px; color: #ff0000; cursor: default; font-weight: 200px; font-size: 18px; text-align: center; line-height: 30px;">We only allow <img style="height: 20px; vertical-align: middle;" src="{{asset('assets/front/images/paypal-logo.png')}}"> Transactions for payments. Please contact us via <a href="{{ ConfigManage\Models\Config::find(1)->phone }}">{{ ConfigManage\Models\Config::find(1)->phone }}</a> or send us an email to <a href="mailto:{{ ConfigManage\Models\Config::find(1)->email }}">{{ ConfigManage\Models\Config::find(1)->email }}</a> for more details.</p>
                <!-- <li>
                  <input id="ridio1" type="radio" name="payment_method" value="visa" hidden />
                  <label for="ridio1">Visa<br><img src="{{asset('assets/front/images/visa.jpg')}}" alt="" /></label>
                </li>
                <li>
                  <input id="ridio2" type="radio" name="payment_method" value="master_card" hidden />
                  <label for="ridio2">Master Card<br><img src="{{asset('assets/front/images/master_card.jpg')}}" alt="" /></label>
                </li> -->
                <li class="hidden">
                  <input id="ridio3" type="radio" name="payment_method" value="paypal" hidden checked="checked" />
                  <label for="ridio3"><img src="{{asset('assets/front/images/paypal.jpg')}}" alt="" /></label>
                </li>
                <!-- <li>
                  <input id="ridio4" type="radio" name="payment_method" value="dicover" hidden />
                  <label for="ridio4">Discover<br><img src="{{asset('assets/front/images/discover.jpg')}}" alt="" /></label>
                </li>
                <li>
                  <input id="ridio5" type="radio" name="payment_method" value="skrill" hidden />
                  <label for="ridio5">Skrill<br><img src="{{asset('assets/front/images/skrill.jpg')}}" alt="" /></label>
                </li> -->
              </ul>
            </div>
            
            <!-- <div class="credit_card_number padbot80">
              <p class="checkout_title">Credit Card Number</p>
              
              <form class="credit_card_number_form clearfix" action="javascript:void(0);" method="get">
                <input type="text" name="card number" value="Number" onFocus="if (this.value == 'Number') this.value = '';" onBlur="if (this.value == '') this.value = 'Number';" />
                <div class="margrightminus20">
                  <select class="basic">
                    <option value="">MM</option>
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                  </select>
                  <select class="basic last">
                    <option value="">YYYY</option>
                    <option>2010</option>
                    <option>2011</option>
                    <option>2012</option>
                    <option>2013</option>
                    <option>2014</option>
                  </select>
                </div>
              </form>
            </div> -->
            
            <div class="clear"></div>
            
            
            <button type="submit" class="btn  pull-right checkout_block_btn">Continue</button>
            <a href="{{url('checkout2')}}"><button type="button" class="btn pull-left checkout_block_btn">Step Back</button></a>
           
          </div>
        </div><!-- //CHECKOUT BLOCK -->
      </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->

    </form>
    

    
@stop

@section('js')


 
@stop
