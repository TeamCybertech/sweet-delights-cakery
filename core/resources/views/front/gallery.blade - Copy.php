@extends('layouts.front.master') @section('title','Gallery')

@section('css')
    <style type="text/css">
        .gal {
            -webkit-column-count: 4;
            -moz-column-count: 4;
            column-count: 4;
        }

        .gal img {
            width: 100%;
            padding: 7px 0;
        }

        @media (max-width: 500px) {
            .gal {
                -webkit-column-count: 1;
                -moz-column-count: 1;
                column-count: 1;
            }
        }

        .hovereffect {
            width: 100%;
            height: 100%;
            float: left;
            overflow: hidden;
            position: relative;
            text-align: center;
            cursor: default;
        }

        .hovereffect .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            overflow: hidden;
            top: 0;
            right: 0;
            left: 0;
            opacity: 0;
            background-color: rgba(0,0,0,0.6);
            -webkit-transition: all .4s ease-in-out;
            transition: all .4s ease-in-out
        }

        .hovereffect img {
            display: block;
            position: relative;
            -webkit-transition: all .4s linear;
            transition: all .4s linear;
        }

        .hovereffect h2 {
            text-transform: uppercase;
            color: #fff;
            text-align: center;
            position: relative;
            font-size: 13px;
            background: rgba(255,255,255,0.2);
            -webkit-transform: translatey(-100px);
            -ms-transform: translatey(-100px);
            transform: translatey(-100px);
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            padding: 10px;
            /*margin-top: 25%;*/
        }

        .hovereffect a.info {
            width: 50px;
            height: 50px;
            padding: 16px 16px;
            font-size: 18px;
            line-height: 1;
            border-radius: 25px;
            text-decoration: none;
            display: inline-block;
            text-transform: uppercase;
            color: #fff;
            /*border: 1px solid #fff;*/
            background-color: rgba(255,255,255,0.2);
            opacity: 0;
            filter: alpha(opacity=0);
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            /*!*margin: 50px 0 0;*!*/
            margin-top: 5px;
            margin-right: 5px;
        }

        .hovereffect a.info:hover {
            box-shadow: 0 0 1px #bbb;
            /*font-size: 20px;
            padding: 14px 14px;
            -webkit-transition: all .3s ease-in-out;*/
        }

        .hovereffect:hover img {
            -ms-transform: scale(1.2);
            -webkit-transform: scale(1.2);
            transform: scale(1.2);
        }

        .hovereffect:hover .overlay {
            opacity: 1;
            filter: alpha(opacity=100);
        }

        .hovereffect:hover h2, .hovereffect:hover a.info {
            opacity: 1;
            filter: alpha(opacity=100);
            -ms-transform: translatey(0);
            -webkit-transform: translatey(0);
            transform: translatey(0);
        }

        .hovereffect:hover a.info {
            -webkit-transition-delay: .2s;
            transition-delay: .2s;
        }

        .img-hover-h2 {
            margin-top: 0px!important;
        }

        .hover-container {
            padding-top: 50%;
            vertical-align: middle;
            position: relative;
            /*top: 50%;*/


        }
        .keywords li{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
        }
        .keywords li:hover{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
        }


#portfolio {  
    margin: 1rem 0;
    -webkit-column-count: 4; 
    -moz-column-count: 4;
    column-count: 4;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
    -webkit-column-width: 25%;
    -moz-column-width: 25%;
    column-width: 25%;
}
#portfolio .tile { 
    -webkit-transform: scale(0);
    transform: scale(0);
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease;

}
#portfolio .tile:hover { 

}

#portfolio .scale-anm {
  transform: scale(1);
}


#portfolio p{ 
  padding:10px; 
  border-bottom: 1px #ccc dotted; 
  text-decoration: none; 
  font-family: lato; 
  text-transform:uppercase; 
  font-size: 12px; 
  color: #333; 
  display:block; 
  float:left;
}

#portfolio p:hover { 
  cursor:pointer; 
  background: #333; 
  color:#eee; }

#portfolio .tile img {
    max-width: 100%;
    width: 100%;
    height: auto;
    margin-bottom: 1rem;
  
}

#portfolio .btn {
    font-family: Lato;
    font-size: 1rem;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
    display: inline-block;
    line-height: normal;
    padding: .5rem 1rem;
    margin: 0;
    height: auto;
    border: 1px solid;
    vertical-align: middle;
    -webkit-appearance: none;
    color: #555;
    background-color: rgba(0, 0, 0, 0);
}

#portfolio .btn:hover {
  text-decoration: none;
}

#portfolio .btn:focus {
  outline: none;
  border-color: var(--darken-2);
  box-shadow: 0 0 0 3px var(--darken-3);
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}
.toolbar{
    text-align: center;
}
        

    .mbottom{
    margin-top: 72px;
    }

#portfolio {  
    margin: 1rem 0;
    -webkit-column-count: 4; 
    -moz-column-count: 4;
    column-count: 4;
    -webkit-column-gap: 1rem;
    -moz-column-gap: 1rem;
    column-gap: 1rem;
    -webkit-column-width: 25%;
    -moz-column-width: 25%;
    column-width: 25%;
}
#portfolio .tile { 
    -webkit-transform: scale(0);
    transform: scale(0);
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease;

}
#portfolio .tile:hover { 

}

#portfolio .scale-anm {
  transform: scale(1);
}


#portfolio p{ 
  padding:10px; 
  border-bottom: 1px #ccc dotted; 
  text-decoration: none; 
  font-family: lato; 
  text-transform:uppercase; 
  font-size: 12px; 
  color: #333; 
  display:block; 
  float:left;
}

#portfolio p:hover { 
  cursor:pointer; 
  background: #333; 
  color:#eee; }

#portfolio .tile img {
    max-width: 100%;
    width: 100%;
    height: auto;
    margin-bottom: 1rem;
  
}

#portfolio .btn {
    font-family: Lato;
    font-size: 1rem;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
    display: inline-block;
    line-height: normal;
    padding: .5rem 1rem;
    margin: 0;
    height: auto;
    border: 1px solid;
    vertical-align: middle;
    -webkit-appearance: none;
    color: #555;
    background-color: rgba(0, 0, 0, 0);
}

#portfolio .btn:hover {
  text-decoration: none;
}

#portfolio .btn:focus {
  outline: none;
  border-color: var(--darken-2);
  box-shadow: 0 0 0 3px var(--darken-3);
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}
.toolbar{
    text-align: center;
}

</style>
@stop


@section('content')
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
        
        
        <!-- PAGE HEADER -->
        <section class="page_header">
            
            <!-- CONTAINER -->
             <hr class="banner-top"/>
            <div class="banner-bg center">
                <h3>Our Gallery</h3>
                <p>View our portfolio of cakes!</p>
            </div>
            <hr class="banner-bottom"/>
        </section><!-- //PAGE HEADER -->

<!-- <div class="toolbar mb2 mt2">
    <button class="btn fil-cat" href="" data-rel="all" onclick="categoryImage(0)">Filter - All</button>
    @foreach($cat as $c)
        <button class="btn fil-cat" data-rel="{{ str_replace(' ', '', $c->name) }}" onclick="categoryImage({{$c->id}})">{{$c->name}}</button>
    @endforeach
</div> 
 
<div id="portfolio">
@foreach($img as $key => $item)
  <div class="tile scale-anm web all">
     
         <div class="hovereffect all filter">
             <img class="img-responsive  {{ str_replace(' ', '', $item[0]) }}" src="{{url('')."/core/storage/".$item[2].'/'.$item[3]}}" alt="" data-pin-do="buttonBookmark">

             <div class="row hover-container">
                <a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /></a>

                <h2 class="img-hover-h2">{{$item[1]}}</h2>
                <a class="info zoom"  data-gallery="" href="{{url('')."/core/storage/".$item[2].'/'.$item[3]}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                <a class="info" href="album/{{$item[4]}}"><i class="fa fa-link" aria-hidden="true"></i></a>
            </div>
         </div>
  </div>
@endforeach
</div>
 -->
<div style="clear:both;"></div>   



    <!-- TYPOGRAPHY BLOCK -->
    <!-- <section class="typography_block padbot40"> -->

        <!-- CONTAINER -->
        <div class="container">

              <section class="search-section" style="clear: both; margin-bottom: 10px !important;">
                  <div class="container">
                    <div class="row">
                        <ul class="keywords keywords-hover-new">
                        <li>
                            <a class="filter-button" data-filter="all" onclick="categoryImage(0)"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Filter - All</a>
                        </li>
                        @foreach($cat as $c)
                            <li>
                                <a class="filter-button" data-filter="{{ str_replace(' ', '', $c->name) }}" onclick="categoryImage({{$c->id}})"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> {{$c->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    
                </div>
              </div>
            </section>



<!--

Reference URL for this method:
    
http://www.brandaiddesignco.com/blog/add-a-custom-pinterest-pin-it-button-to-your-website/375/

-->

            <div class="gal" id="gale">
                @foreach($img as $key => $item)
                
                    <div class="hovereffect filter {{ str_replace(' ', '', $item[0]) }}">
                        
                        <img class="img-responsive" src="{{url('')."/core/storage/".$item[2].'/'.$item[3]}}" alt="" data-pin-do="buttonBookmark">
                        <div class="overlay">
                            <div class="row hover-container">

                                <a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /></a>

                                <h2 class="img-hover-h2">{{$item[1]}}</h2>
                                <a class="info zoom"  data-gallery="" href="{{url('')."/core/storage/".$item[2].'/'.$item[3]}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a class="info" href="album/{{$item[4]}}"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">  <!-- commented class // blueimp-gallery-controls -->
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
        </div>
        <div class="mbottom" />
    </section>
    


@stop

@section('js')
    <script>

        jQuery(window).load(function(){
            $('.gallery_item').magnificPopup({
                delegate: 'a.zoom',
                type: 'image',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });
        });
        $(document).ready(function () {

            $(".filter-button").click(function () {
                var value = $(this).attr('data-filter');

                if (value == "all") {
                    //$('.filter').removeClass('hidden');
                    $('.filter').show('1000');
                }
                else {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.' + value).hide('3000');
                    $('.filter').filter('.' + value).show('3000');

                }
            });

            if ($(".filter-button").removeClass("active")) {
                $(this).removeClass("active");
            }
            $(this).addClass("active");
        });


        //        $(document).ready(function () {
        //            categoryImage(0);
        //        });
        //
        //        function categoryImage(cat) {
        //            $('#gale').empty()
        //            $.ajax({
        //                type: "GET",
        //                data: {id: cat},
        //                url: "cat/" + cat,
        //                success: function (msgg) {
        //                    var len = msgg.length;
        //                    for (var i = 0; i < len; i++) {
        //                        var file = msgg[i]['filename'];
        //                        var album = msgg[i]['album_name'];
        //                        var id = msgg[i]['id'];
        //                        var appendTxt = "<div  class=\"hovereffect\">\n" +
        //                            "                                    <img class=\"img-responsive\" src=\" /sdc-web-cart/core/storage/uploads/images/gallery/" + file + " \" alt=\"\">\n" +
        //                            "                                    <div class=\"overlay\">\n" +
        //                            "                                        <h2>album</h2>\n" +
        //                            "                                        <a class=\"info\" href=\"#\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i></a>\n" +
        //                            "                                        <a class=\"info\" href=\"album/" + id + " \"><i class=\"fa fa-link\" aria-hidden=\"true\"></i></a>\n" +
        //                            "                                    </div>\n" +
        //                            "                                </div>";
        //                        $("#gale").append(appendTxt);
        //                    }
        //                }
        //            });
        //

        //  }


    </script>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>

<script type="text/javascript">
    $(function() {
        var selectedClass = "";
        $(".fil-cat").click(function(){ 
        selectedClass = $(this).attr("data-rel"); 
     $("#portfolio").fadeTo(100, 0.1);
        $("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
    setTimeout(function() {
      $("."+selectedClass).fadeIn().addClass('scale-anm');
      $("#portfolio").fadeTo(300, 1);
    }, 300); 

        
    });
});
</script>

@stop
