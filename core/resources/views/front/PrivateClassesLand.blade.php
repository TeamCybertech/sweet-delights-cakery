  @extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
  @section('css')
  <style type="text/css">
  img{
    width:41%;
  }
  .gal {
  -webkit-column-count: 4;
  -moz-column-count: 4;
  column-count: 4;
  }

  .gal img {
  width: 100%;
  padding: 7px 0;
  }

  @media (max-width: 500px) {
  .gal {
  -webkit-column-count: 1;
  -moz-column-count: 1;
  column-count: 1;
  }
  }

  .hovereffect {
  width: 100%;
  height: 100%;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
  }

  .hovereffect .overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  overflow: hidden;
  top: 0;
  left: 0;
  opacity: 0;
  background-color: rgba(0, 0, 0, 0.8);
  -webkit-transition: all .4s ease-in-out;
  transition: all .4s ease-in-out
  }

  .hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all .4s linear;
  transition: all .4s linear;
  }

  .hovereffect h2 {
  text-transform: uppercase;
  color: #fff;
  text-align: center;
  position: relative;
  font-size: 17px;
  background: rgba(0, 0, 0, 0.6);
  -webkit-transform: translatey(-100px);
  -ms-transform: translatey(-100px);
  transform: translatey(-100px);
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
  padding: 10px;
  /*margin-top: 25%;*/
  }

  .hovereffect a.info {
  width: 50px;
  height: 50px;
  padding: 16px 16px;
  font-size: 18px;
  line-height: 1;
  border-radius: 25px;
  text-decoration: none;
  display: inline-block;
  text-transform: uppercase;
  color: #fff;
  border: 1px solid #fff;
  /*background-color: gainsboro;*/
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
  /*!*margin: 50px 0 0;*!*/
  margin-top: 5px;
  margin-right: 5px;
  }

  .hovereffect a.info:hover {
  box-shadow: 0 0 5px #fff;
  }

  .hovereffect:hover img {
  -ms-transform: scale(1.2);
  -webkit-transform: scale(1.2);
  transform: scale(1.2);
  }

  .hovereffect:hover .overlay {
  opacity: 1;
  filter: alpha(opacity=100);
  }

  .hovereffect:hover h2, .hovereffect:hover a.info {
  opacity: 1;
  filter: alpha(opacity=100);
  -ms-transform: translatey(0);
  -webkit-transform: translatey(0);
  transform: translatey(0);
  }

  .hovereffect:hover a.info {
  -webkit-transition-delay: .2s;
  transition-delay: .2s;
  }
h4{
  margin: 0px;
  line-height: auto!important;
  font-size: 13px;
}
  .private_classes_title {
  font-size: 18px;
  /* line-height: 38px; */
  text-transform: uppercase;
  font-weight: bold;
  color: #333;
  }

  .margin-left-25 {
  margin-left: 25px;
  }

  .margintop{
  margin-top: 25px;
  border:none;
  }
  .b-top{
  border-color: #eee;
  }
  .product_level{
  margin-top: -30px;
  font-weight: 600;
  font-size: 14px;
  color:#fb1d2e;
  }

  .keywords-shop {
  list-style:none;
  text-align: center;
  margin: 0 auto;
  padding: 0px;
  display:table;
  overflow: hidden;
  }

  .keywords-shop li{
  vertical-align: bottom;
  float: none;
  padding: 6px 0px 6px 0px;
  width: 100px;
  margin-left: 3px;
  margin-right: 3px;
  background-color:none;
  font-weight: 600;
  font-size: 1.25em;
  border-radius: 5px;
  border: thin solid #8b5730;
  display: inline-block;
  margin-bottom: 10px;
  }

  .keywords-shop li:hover{
  vertical-align: bottom;
  /* float: left; */
  padding: 6px 0px 6px 0px;
  width: 100px;
  margin-top: 0px;
  margin-left: 3px;
  margin-right: 3px;
  background-color: #8b5730;
  font-size: 1.25em;
  border-radius: 5px;
  cursor: pointer;
  border: no;
  -webkit-transition: all 0.4s ease-in-out;
  }

  .keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
  text-transform: capitalize;
  }

  .keywords-shop li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
  }

  .keywords-hover-new-shop li:hover{
  vertical-align: bottom;
  float: left;
  padding: 6px 0px 6px 0px;
  width: 100px;
  margin-top: 0px;
  margin-left: 3px;
  margin-right: 3px;
  background-color: #8b5730;
  color: #ffffff;
  font-size: 1.25em;
  border-radius: 5px;
  cursor: pointer;
  border: no;
  -webkit-transition: all 0.4s ease-in-out;
  }

  a.register {
    /*background-color: #fff!important;
    border: 3px solid #434343!important;
    color: #434343!important;
    padding-bottom: 7px;
    padding-top: 7px;
    font-size:12px;*/
    background-color: #8b5730 !important;
    color: #E5D4CD !important;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    font-family: 'Roboto', sans-serif;
    text-transform: none;
    font-style: normal;
    line-height: 20px;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    }
  a.register i {
    font-size: 15px;
    }
  a.register:hover {
    background-color: #E5D4CD!important;
    border-color: #8b5730!important;
    color: #8b5730!important;
    font-size:12px;
    }

  .product_catalog_item .product_catalog_list {
      padding-left: 0px;
  }
  .product_catalog_list {
      padding-left: 0px;
  }
  .product_catalog_item img {

    float: left;
    margin: 0 0 20px 0;
    height: 250px;
    width: 500px;
    object-fit: cover;
    

    }
    @media (max-width: 991px) {
      .product_catalog_item img{
        float: none;
      }
    }
    .justify{
      text-align:justify;
    }
    .descrip{
      font-size:12px;
    }
  </style>
  @stop


  @section('content')
  <section class="breadcrumb men parallax margbot30">

  </section><!-- //BREADCRUMBS -->


  <!-- PAGE HEADER -->
  <section class="page_header">

  <hr class="banner-top"/>
  <div class="banner-bg center">
  <h3>PRIVATE CLASSES</h3>
  <p>View our portfolio of private classes!</p>
  </div>
  <hr class="banner-bottom">
  <!-- CONTAINER -->
  <div class="container">

  <!--  <hr class="banner-top">
  <div class="banner-bg center">
  <h3>Blog Page</h3>
  <p>View our portfolio of Blogs!</p>
  </div>
  <hr class="banner-bottom"> -->

  </div><!-- //CONTAINER -->
  </section><!-- //PAGE HEADER -->



  <section class="search-section" style="clear: both; margin-bottom: 10px !important;">
  <div class="container">
  <div class="row">
    <ul class="keywords-shop">
      <li>
        <a href="{{url('/PrivateClassesLand/0')}}"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Filter - All</a>
      </li>
      <li>
        <a href="{{url('/PrivateClassesLand/1')}}"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Beginner</a>
      </li>
      <li>
        <a href="{{url('/PrivateClassesLand/2')}}"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Intermediate</a>
      </li>
      <li>
        <a href="{{url('/PrivateClassesLand/3')}}"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Advance</a>
      </li>
      <li>
        <a href="{{url('/PrivateClassesLand/4')}}"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Kids</a>
      </li>
    </ul>
  </div>
  </div>
  </section>

  <!-- PRODUCT CATALOG SECTION -->
  <section class="product_catalog_block">

  <!-- CONTAINER -->
  <div class="container" id="values">

  @foreach($clz as $item)

  <div class="row">
    <hr class="b-top"/>
    <div class="col-lg-12 col-md-12 product_catalog_item clearfix">
      <div class="col-lg-4 col-md-4">
        <img class="product_catalog_img product_women"  src="{{url('')."/core/storage/".$item->path.'/'.$item->filename}}" alt=""/>
      </div>
      <div class="col-lg-8 col-md-8">
        <p class="private_classes_title">{{$item->name}}</p>
        <ul class="product_catalog_list">
          <p>
            <h4 style="min-height: 75px;">

              @foreach($item->getDateTime as $datetime)
              <div style="height: 25px;">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                {{$datetime->getFullDate()}}
                <i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>

                {{$datetime->getStart24Format()}} to {{$datetime->getEnd24Format()}}
              </div>

              @endforeach

            </h4>
          </p>
          <p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> Level:
            @if($item->skill_level == 1)
              Beginner
            @elseif($item->skill_level == 2)
              Intermediate
            @elseif($item->skill_level == 3)
              Advanced
            @elseif($item->skill_level == 4)
              Kids
            @endif
          </p>
          <!-- <p><h4></h4></p> -->
          <p>$ {{$item->price}}.00</p>
          <p class="justify descrip">{{$item->description}}</p>
          <a href="{{url('PrivateClassesReg/'.$item->id)}}" class="btn register">Register Now</a>
        </ul>
      </div>
    </div>
  </div>

  @endforeach

  </div>
  </section>

  <!-- PRODUCT CATALOG SECTION -->
  <!-- <section class="product_catalog_block"> -->

  <!-- CONTAINER -->
  <!-- <div class="container">
  <div class="row">
  @foreach($clz as $item)
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ss-12 product_catalog_item clearfix">
  <img class="product_catalog_img product_women"
  src="{{url('')."/core/storage/".$item->path.'/'.$item->filename}}" alt=""/>
  <p class="private_classes_title">{{$item->name}}</p>
  <ul class="product_catalog_list">
  <p>
  <h4>
  <i class="fa fa-calendar" aria-hidden="true"></i>
  <?php $date=substr($item->datetime,0,10); echo $date ; ?>
  <i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>
  <?php $date=substr($item->datetime,11); echo $date ;?>
  </h4>
  </p>
  <!-- <p><h4></h4></p> -->
  <!--<p>$ {{$item->price}}.00</p>
  <p align="justify">{{$item->description}}</p>
  <a href="PrivateClassesReg" class="btn active">Register Now</a>
  </ul>
  </div>
  @endforeach
  </div>
  </div>
  </section> -->
  @stop

  @section('js')
  <script>
  $(document).ready(function () {

  $(".filter-button").click(function () {
  var value = $(this).attr('data-filter');

  if (value == "all") {
    //$('.filter').removeClass('hidden');
    $('.filter').show('1000');
  }
  else {
    //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
    //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
    $(".filter").not('.' + value).hide('3000');
    $('.filter').filter('.' + value).show('3000');

  }
  });

  if ($(".filter-button").removeClass("active")) {
  $(this).removeClass("active");
  }
  $(this).addClass("active");
  });


  //        $(document).ready(function () {
  //            categoryImage(0);
  //        });
  //
  //        function categoryImage(cat) {
  //            $('#gale').empty()
  //            $.ajax({
  //                type: "GET",
  //                data: {id: cat},
  //                url: "cat/" + cat,
  //                success: function (msgg) {
  //                    var len = msgg.length;
  //                    for (var i = 0; i < len; i++) {
  //                        var file = msgg[i]['filename'];
  //                        var album = msgg[i]['album_name'];
  //                        var id = msgg[i]['id'];
  //                        var appendTxt = "<div  class=\"hovereffect\">\n" +
  //                            "                                    <img class=\"img-responsive\" src=\" /sdc-web-cart/core/storage/uploads/images/gallery/" + file + " \" alt=\"\">\n" +
  //                            "                                    <div class=\"overlay\">\n" +
  //                            "                                        <h2>album</h2>\n" +
  //                            "                                        <a class=\"info\" href=\"#\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i></a>\n" +
  //                            "                                        <a class=\"info\" href=\"album/" + id + " \"><i class=\"fa fa-link\" aria-hidden=\"true\"></i></a>\n" +
  //                            "                                    </div>\n" +
  //                            "                                </div>";
  //                        $("#gale").append(appendTxt);
  //                    }
  //                }
  //            });
  //

  //  }


  </script>

  <script>

  $(document).ready(function(){

  $('#beginner').click(function(){

  $.ajax({
    url:'{{ url('beginner') }}',
    type: "GET",
    dataType: 'json',
    data: {skill : "beginner"}

  })
  .done(function(data){
  console.log("data");
      var rows = '';

$.each(data,function(index,value){
console.log(value.name);
rows +='<div class="row">';
rows += '<div class="col-lg-12 col-md-12 product_catalog_item clearfix">';
rows += '<div class="col-lg-4 col-md-4">';
rows += "<img class='product_catalog_img product_women'  src='core/storage/" +value.path+"/"+value.filename+"' alt=''/>";
rows += '</div>';
rows += '<div class="col-lg-8 col-md-8">';
rows +=  '<p class="private_classes_title">'+ value.name +'</p>';
rows +=  '<ul class="product_catalog_list"> <p>';
rows +=  '<h4 style="min-height: 75px;">';
rows +=  '<i class="fa fa-calendar" aria-hidden="true"></i>';

rows +=  '<i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>';

rows += '</h4> </p>';
rows += '<p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> {Level: xxxxx}</p>';
rows += '<!-- <p><h4></h4></p> -->';
rows += '<p>'+ value.price +'.00</p>';
rows += '<p class="justify descrip">'+value.description+'</p>';
rows += '<a href="PrivateClassesReg" class="btn register">Register Now</a>';
rows +=  '</ul></div></div></div>';

});
$("#values").html(rows);

});

})


//filter Intermediate

$('#intermediate').click(function(){

$.ajax({
  url:'{{ url('intermediate') }}',
  type: "GET",
  dataType: 'json',
  data: {skill : "intermediate"}

})
.done(function(data){
console.log("data");
    var rows = '';

$.each(data,function(index,value){
console.log(value.name);
rows +='<div class="row">';
rows += '<div class="col-lg-12 col-md-12 product_catalog_item clearfix">';
rows += '<div class="col-lg-4 col-md-4">';
rows += "<img class='product_catalog_img product_women'  src='core/storage/" +value.path+"/"+value.filename+"' alt=''/>";
rows += '</div>';
rows += '<div class="col-lg-8 col-md-8">';
rows +=  '<p class="private_classes_title">'+ value.name +'</p>';
rows +=  '<ul class="product_catalog_list"> <p>';
rows +=  '<h4 style="min-height: 75px;">';
rows +=  '<i class="fa fa-calendar" aria-hidden="true"></i>';

rows +=  '<i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>';

rows += '</h4> </p>';
rows += '<p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> {Level: xxxxx}</p>';
rows += '<!-- <p><h4></h4></p> -->';
rows += '<p>'+ value.price +'.00</p>';
rows += '<p class="justify descrip">'+value.description+'</p>';
rows += '<a href="PrivateClassesReg" class="btn register">Register Now</a>';
rows +=  '</ul></div></div></div>';

});
$("#values").html(rows);

});

});

//filter advance

$('#advance').click(function(){

$.ajax({
  url:'{{ url('advance') }}',
  type: "GET",
  dataType: 'json',
  data: {skill : "advance"}

})
.done(function(data){
console.log("data");
    var rows = '';

$.each(data,function(index,value){
console.log(value.name);
rows +='<div class="row">';
rows += '<div class="col-lg-12 col-md-12 product_catalog_item clearfix">';
rows += '<div class="col-lg-4 col-md-4">';
rows += "<img class='product_catalog_img product_women'  src='core/storage/" +value.path+"/"+value.filename+"' alt=''/>";
rows += '</div>';
rows += '<div class="col-lg-8 col-md-8">';
rows +=  '<p class="private_classes_title">'+ value.name +'</p>';
rows +=  '<ul class="product_catalog_list"> <p>';
rows +=  '<h4 style="min-height: 75px;">';
rows +=  '<i class="fa fa-calendar" aria-hidden="true"></i>';

rows +=  '<i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>';

rows += '</h4> </p>';
rows += '<p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> {Level: xxxxx}</p>';
rows += '<!-- <p><h4></h4></p> -->';
rows += '<p>'+ value.price +'.00</p>';
rows += '<p class="justify descrip">'+value.description+'</p>';
rows += '<a href="PrivateClassesReg" class="btn register">Register Now</a>';
rows +=  '</ul></div></div></div>';

});
$("#values").html(rows);

});

});


//filter kides

$('#kids').click(function(){

$.ajax({
  url:'{{ url('kids') }}',
  type: "GET",
  dataType: 'json',
  data: {skill : "kids"}

})
.done(function(data){
console.log("data");
    var rows = '';

$.each(data,function(index,value){
console.log(value.name);
rows +='<div class="row">';
rows += '<div class="col-lg-12 col-md-12 product_catalog_item clearfix">';
rows += '<div class="col-lg-4 col-md-4">';
rows += "<img class='product_catalog_img product_women'  src='core/storage/" +value.path+"/"+value.filename+"' alt=''/>";
rows += '</div>';
rows += '<div class="col-lg-8 col-md-8">';
rows +=  '<p class="private_classes_title">'+ value.name +'</p>';
rows +=  '<ul class="product_catalog_list"> <p>';
rows +=  '<h4 style="min-height: 75px;">';
rows +=  '<i class="fa fa-calendar" aria-hidden="true"></i>';

rows +=  '<i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>';

rows += '</h4> </p>';
rows += '<p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> {Level: xxxxx}</p>';
rows += '<!-- <p><h4></h4></p> -->';
rows += '<p>'+ value.price +'.00</p>';
rows += '<p class="justify descrip">'+value.description+'</p>';
rows += '<a href="PrivateClassesReg" class="btn register">Register Now</a>';
rows +=  '</ul></div></div></div>';

});
$("#values").html(rows);

});

});


//filter all

$('#filterall').click(function(){

$.ajax({
  url:'{{ url('filterall') }}',
  type: "GET",
  dataType: 'json',
  data: {skill : "filterall"}

})
.done(function(data){
console.log("data");
    var rows = '';

$.each(data,function(index,value){
console.log(value.name);
rows +='<div class="row">';
rows += '<div class="col-lg-12 col-md-12 product_catalog_item clearfix">';
rows += '<div class="col-lg-4 col-md-4">';
rows += "<img class='product_catalog_img product_women'  src='core/storage/" +value.path+"/"+value.filename+"' alt=''/>";
rows += '</div>';
rows += '<div class="col-lg-8 col-md-8">';
rows +=  '<p class="private_classes_title">'+ value.name +'</p>';
rows +=  '<ul class="product_catalog_list"> <p>';
rows +=  '<h4 style="min-height: 75px;">';
rows +=  '<i class="fa fa-calendar" aria-hidden="true"></i>';

rows +=  '<i class="fa fa-clock-o margin-left-25" aria-hidden="true"></i>';

rows += '</h4> </p>';
rows += '<p class="product_level"><i class="fa fa-tasks" aria-hidden="true"></i> {Level: xxxxx}</p>';
rows += '<!-- <p><h4></h4></p> -->';
rows += '<p>'+ value.price +'.00</p>';
rows += '<p class="justify descrip">'+value.description+'</p>';
rows += '<a href="PrivateClassesReg" class="btn register">Register Now</a>';
rows +=  '</ul></div></div></div>';

});
$("#values").html(rows);

});

});






  })
  </script>

  @stop
