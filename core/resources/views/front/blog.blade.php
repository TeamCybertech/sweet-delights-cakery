@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">
     .keywords-blog {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
}

.keywords-blog li{
    vertical-align: bottom;
    float: left;
    padding: 6px 0px 6px 0px;
    width: 100px;
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.25em;
    border-radius: 5px;
    border: thin solid #8b5730;

}

.keywords-blog li:hover{
    vertical-align: bottom;
    float: left;
    padding: 6px 0px 6px 0px;
    width: 100px;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-blog li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-blog li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-hover-new-shop li:hover{
    vertical-align: bottom;
    float: left;
    padding: 6px 0px 6px 0px;
    width: 100px;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    color: #ffffff;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

     .query::placeholder{
         color:#e5d4cd;
     }

     .sidemenu{
         /*height:300px;*/
         padding: 5px 5px 15px 5px;
         border: 2px solid #e9e9e9;
         /*border-radius: 5px;*/
         margin-bottom: 5px;
         /*box-shadow: 0px -1px 5px grey;*/

     }

     .sidemenu-social{
         /*height:300px;*/
         padding: 5px 5px 15px 5px;
         /*border: 2px solid #e9e9e9;*/
         /*border-radius: 5px;*/
         margin-bottom: 5px;
         /*box-shadow: 0px -1px 5px grey;*/

     }
    .sidemenu-title{
        margin-left: 5%;
        margin-top: 5%;
        margin-bottom :inherit;
        padding: 10px;
        max-width: 92%;
        background-color: #fceeeb;
        text-align: center;
        font-weight: 600;
        font-size: 0.9em;
        border: 2px solid #8b5730;
        /*border-radius: 5px;*/
    }

    .blogs{
        position: relative;
        margin: 2px 10px 2px 10px;
        padding: 5px;
        border: thin solid #e9e9e9;
        /*box-shadow: 0px 2px 2px grey;*/
    }

     .tags{
        position: relative;
        margin: 2px 10px 2px 10px;
        padding: 5px;
        /*box-shadow: 0px 2px 2px grey;*/
    }

     .blogs:hover{
         box-shadow: 0px 2px 2px grey;
     }

     .blog-title{
        color:black;
        font-weight: 600;
        font-size: 0.9em;
         overflow:hidden;
    }

    .tagsurl:hover{
        cursor: pointer;
        text-decoration: underline;
    }

     .fa-tag {
         /*padding : 5px 5px;*/
         /*text-shadow: 0px 0px 1px #E5D4CD, 0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD;*/
         text-shadow: 0px 0px 1px #8b5730, 0px 0px 1px #8b5730,0px 0px 1px #8b5730,0px 0px 1px #8b5730,0px 0px 1px #8b5730;
         color: white;
         font-size: 2.1em !important;
     }
    .fa-tag:hover{
        color: #8b5730;
        cursor: pointer;
    }

     .pagination>.active>span:hover{
         color: #ccc !important;
         background-color: #333 !important;
         border: 2px solid #ccc !important;
         /*background-color: #ccc;*/
         /*color: #4d4b4c;*/
     }
     .pagination li a, .pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>a:focus, .pagination>.active>span:focus{
        /*border-color: #8b5730!important;*/
        /*background-color: #E5D4CD !important;*/
        /*color: #8b5730 !important;*/
         background-color: #ccc;
         color: #4d4b4c;
    }
</style>

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

      <hr class="banner-top"/>
            <div class="banner-bg center">
                <h3>Blog Page</h3>
                <p>View our portfolio of Blogs!</p>
            </div>
            <hr class="banner-bottom">
      <!-- CONTAINER -->
      <div class="container">

           <!--  <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Blog Page</h3>
                <p>View our portfolio of Blogs!</p>
            </div>
            <hr class="banner-bottom"> -->

      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->

    <section class="search-section" style="margin-bottom: 2%;">
      <div class="container">
        <div class="row" style="text-align: center">
          {{--<div class="col-md-3">--}}
            {{--<!-- WIDGET SEARCH -->--}}
            {{--<div class="widget_search">--}}

                {{--<input type="text" class="query" name="search" value="{{app('request')->input('qu')}}" id="qu" placeholder="Search"/>--}}

            {{--</div><!-- //WIDGET SEARCH -->--}}
          {{--</div>--}}
          <div class="col-md-12">
            <ul class="keywords-blog">
              <li>
                <a href="{{ url('blog') }}">

                  Filter - All
                </a>
              </li>
              @foreach ($keywords as $element)
                <li>
                  <a href="{{ url('blog?search='.$element) }}">

                    {{$element}}
                  </a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </section>

    <!-- BLOG BLOCK -->
    <section class="blog">

      <!-- CONTAINER -->
      <div class="container">

        <!-- ROW -->
        <div class="row">



          <!-- BLOG LIST -->
          {{--<div class="col-lg-12 col-md-12 col-sm-12 padbot30 blog_block">--}}
          <div class="col-lg-9 col-md-9 col-sm-9 padbot30 blog_block">


            @foreach($blogs as $blog)
            <article class="post margbot40 clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <a class="post_image pull-left" href="{{url('blog/'.$blog->id)}}" >
                <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div>

                <img  src="{{asset('core/storage/uploads/images/blog'.'/'.$blog->front_image)}}" alt="" />
              </a>
              <a class="post_title" href="{{url('blog/'.$blog->id)}}" >{{$blog->name}}</a>
              <div class="post_content text-justify">{{ substr(strip_tags($blog->description),0,1200)}}...</div>

              <ul class="post_meta valign-bot pull-left">
                  <li>
                    <a href="{{ url('blog/'.$blog->id) }}">
                      <button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button>
                    </a>
                  </li>
              </ul>

            </article>
                  <hr>
           @endforeach
                <div class="col-lg-12 col-md-12 col-sm-12 pagination" style="text-align: center">{!! $blogs->render() !!}</div>

          </div><!-- //BLOG LIST -->
            <section class="col-lg-3 col-md-3 col-sm-3">
                <div class="container-fluid">
                    <div class="container-fluid col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                                <!-- WIDGET SEARCH -->
                                <div class="widget_search">

                                    <input type="text" class="query" name="search" value="{{app('request')->input('qu')}}" id="qu" placeholder="Search"/>

                                </div><!-- //WIDGET SEARCH -->

                        </div>
                        <div class="row">
                            <div class="col-sm-12   sidemenu recent_blogs">
                                <div class="sidemenu-title">RECENT POSTS</div>
                                @foreach($recent_blogs as $r)
                                    <a href="{{url('blog/'.$r->id)}}">
                                        <div class="col-sm-11 blogs">
                                            <div class="col-sm-5">
                                                <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 50px;width: auto;">
                                            </div>
                                            <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                                {{$r->name}}<br>
                                                <div style="color: #e0e0e0">{{ date('F d, y',strtotime($r->start_date))}}</div>
                                            </div>
                                        </div>
                                    </a>

                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-sm-12 sidemenu featured_posts">
                                <div class="sidemenu-title">
                                    FEATURED POSTS
                                </div>
                                @foreach($featured_blogs as $r)
                                    <a href="{{url('blog/'.$r->id)}}">
                                        <div class="col-sm-11 blogs">
                                            <div class="col-sm-5" >
                                                <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 50px;width: auto;">
                                            </div>
                                            <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                                {{$r->name}}<br>
                                                <div style="color: #e0e0e0">{{$r->start_date}}</div>
                                            </div>
                                        </div>
                                    </a>

                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="sidemenu instagram">
                                <div class="sidemenu-title">
                                    INSTAGRAM
                                </div>
                                <div id="instagram" class="instagram">

                                </div>
                            </div >
                        </div>
                        <div class="row">
                            <div class="sidemenu">
                                <div class="sidemenu-title">
                                    TAGS
                                </div>
                                <div class="tags">
                                    @foreach($tags as $tag)
                                        <a class="tagsurl" href="{{url('blog/searchByTag/'.$tag)}}">{{$tag}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="">

                                <div class="social-icon">
                                    {{--<div>--}}
                                        {{--<a class="btn-lg btn-social-icon btn-pinterest" style="padding: 5px 5px;">--}}
                                            {{--<i class="fa fa-tag fa-lg fa-pinterest" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="pin-ic mr-3" role="button"><i class="fab fa-lg fa-pinterest"></i></a>--}}
                                        {{--<a class="btn-lg btn-social-icon btn-instagram" style="padding: 5px 5px;">--}}
                                            {{--<i class="fa fa-tag fa-lg fa-instagram" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="ins-ic mr-3" role="button"><i class="fab fa-lg fa-instagram"></i></a>--}}

                                        {{--<a class="btn-lg btn-social-icon btn-facebook" style="padding: 5px 5px;">--}}
                                            {{--<i class="fa fa-tag fa-lg fa-facebook" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="yt-ic mr-3" role="button"><i class="fab fa-lg fa-youtube"></i></a>--}}

                                        {{--<a class="btn-lg btn-social-icon btn-youtube" style="padding: 5px 5px;">--}}
                                            {{--<i class="fa fa-tag fa-lg fa-youtube" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="fb-ic mr-3" role="button"><i class="fab fa-lg fa-facebook-f"></i></a>--}}

                                        {{--<a class="btn-lg btn-social-icon btn-twitter" style="padding: 5px 5px;">--}}
                                            {{--<i class="fa fa-tag fa-lg fa-twitter" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="tw-ic mr-3" role="button"><i class="fab fa-lg fa-twitter"></i></a>--}}
                                    {{--</div>--}}
                                    <div class="social">

                                        <a href="{{ ConfigManage\Models\Config::find(1)->twitter }}" style="margin: 0 5px 10px 0 !important;" target="_blank"  data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>

                                        <a href="{{ ConfigManage\Models\Config::find(1)->fb }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>

                                        <a href="{{ ConfigManage\Models\Config::find(1)->youtube }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Youtube"><i class="fa fa-youtube-square"></i></a>

                                        <a href="{{ ConfigManage\Models\Config::find(1)->pinterest }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>

                                        <a href="{{ ConfigManage\Models\Config::find(1)->instagram }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </div><!-- //ROW -->
      </div><!-- //CONTAINER -->
    </section><!-- //BLOG BLOCK -->

@stop

@section('js')

<script type="text/javascript">
    $(document).ready(function () {
        var blog_block = $('.blog_block');
        var resultObj =  '';
        $('.query').on('keyup',function(){
            if(this.value.length >= 2){
                $.ajax({
                    type: 'GET',
                    url: 'blog/search',
                    data: { qu: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        console.log("success");
                        let dataObj = json;
                        //console.log(dataObj);
                        blog_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                            dataObj.forEach(function(element) {
                                var hitTemplate =
                                        '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.blog_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }

                        if(dataObj.length == 0){
                            $('.blog_block').html('<div class="alert alert-warning" role="alert">No Results!!!</div>');
                        }
                    }
                });
            }else{
                $.ajax({
                    type: 'GET',
                    url: 'blog/stdSearch',
                    data: { qu: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        console.log("success");
                        let dataObj = json;
                        //console.log(dataObj);
                        blog_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                            dataObj.forEach(function(element) {

                                var hitTemplate =
                                        '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.blog_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }

                    }
                });

            }         
        });


    });

    $(document).ready(function (){
        var token = '1351653747.1677ed0.e686f3deeb4c49a5b8e1bcf90176d7c2',
            userid = 1351653747,
            num_photos = 9; // how much photos do you want to get

        $.ajax({
            url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token, count: num_photos},
            success: function(data){
                console.log(data);
                for( n in data.data ){

                    $('#instagram').append('<div class="col-sm-4 insta-preview"><img src="'+data.data[n].images.standard_resolution.url+'" style="height: 50px;width: auto;"></div>');

                }
            },
            error: function(data){
                console.log(data);
            }
        });
    })

</script>

    

@stop
