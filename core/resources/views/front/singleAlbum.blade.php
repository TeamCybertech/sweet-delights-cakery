@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">
.w3-button:hover {
    color: #000!important;
    background-color: #ccc!important;
}
.w3-black, .w3-hover-black:hover {
    color: #fff!important;
    background-color: #000!important;
}
.w3-display-left {
    position: absolute;
    top: 50%;
    left: 0%;
    transform: translate(0%,-50%);
    -ms-transform: translate(-0%,-50%);
}
.w3-display-right {
    position: absolute;
    top: 50%;
    right: 0%;
    transform: translate(0%,-50%);
    -ms-transform: translate(0%,-50%);
}
    .w3-btn, .w3-button {
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    /*background-color: inherit;*/
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
}
.w3-btn, .w3-button {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.title h3{
font-weight: normal!important;
font-size: 22px!important;
}

.mySlides-cover {
    //height: 500px;
    text-align: center;
}

.mySlides {
    max-height: 500px;
}

button.add-to-cart {
     background-color:#E5D4CD;
    color: #8b5730;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.add-to-cart i {
    font-size: 15px;
    }
    button.add-to-cart:hover {
    background-color:#8b5730;
    color: #E5D4CD;

    }
/*
    .pinterest-btn {
        height: 40px;
        top: 25px;
        left: 40px;
        position: absolute;
    }*/
.carousel-control{
    height: 60px;
    width: 30px;
    background-color: #ccc;
    border-radius: 3px;
    top: 41%;
}
.carousel-control i.fa{
  display: block;
  margin: 0 auto;
  line-height: 54px;
  text-align: center;
}
.carousel-control .glyphicon-chevron-right{
    margin-right: 0px;
}
.carousel-control .glyphicon-chevron-left{
    margin-left: 0px;
}
.glyphicon-chevron-left::before {
    color: #a49b9b;
}
.glyphicon-chevron-right::before {
    color: #a49b9b;
}

.item img {
    height: 500px;
    text-align: center;
}

.carousel-control.right {
    background-image: -webkit-linear-gradient(left,rgba(0,0,0,.0001) 0,rgba(0,0,0,.1) 100%);
    //position: fixed;
    //right: 50%;
    top: 41%;
}

.carousel-control.left {
    background-image: -webkit-linear-gradient(left,rgba(0,0,0,.1) 0,rgba(0,0,0,.0001) 100%);
    //position: fixed;
    top: 41%;
}
.carousel-inner > .item > img, .carousel-inner > .item > a > img{
      /* max-height:500px; */
      margin-left: auto!important;
      margin-right: auto!important;
      min-height: 500px;
      object-fit: contain;
  }
span[data-pin-log="button_pinit_floating"]{
    z-index:999!important;
}
</style>
@stop



@section('content')
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->
<div style="margin-top: 150px;"></div>

    <!-- TYPOGRAPHY BLOCK -->
    <section class="typography_block padbot40_" style="min-height: 580px; margin-bottom:60px; padding-top: 20px; padding-bottom:60px">

        <!-- CONTAINER -->
        <div class="container">

            <div class="row">
                <div class="col-sm-6 text-center mySlides-cover">

                  <!--   <img class="pull-left pinterest-btn" align="middle" src="{{asset('assets/front/images/Pinterest-icon.png')}}"/> -->



                      <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                             <!--    <ol class="carousel-indicators">
                                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                  <li data-target="#myCarousel" data-slide-to="1"></li>
                                  <li data-target="#myCarousel" data-slide-to="2"></li>
                                </ol> -->

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                  <div class="item active">
                                      {{--<div style="position:absolute">--}}
                                      {{--<a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img class="pinterest-icon" style="width:40px; height:40px" src="{{asset('assets/front/images/Pinterest-icon.png')}}" /></a>--}}

                                      {{--</div>--}}
                                    <img src="{{url('').'/core/storage/'.$gallery->path.'/'.$gallery->filename}}" alt="Los Angeles" style="height: 100%; max-height: 580px; margin-left: 20%; width: auto; " data-pin-do="buttonBookmark">
                                  </div>
                                  <?php foreach ($gallery->getImages as $key => $value): ?>
                                    <div class="item text-center">
                                        {{--<div style="position:absolute">--}}
                                            {{--<a data-pin-do="buttonBookmark" data-pin-tall="true" data-pin-round="true" href="https://www.pinterest.com/pin/create/button/"><img class="pinterest-icon" style="width:40px; height:40px" src="{{asset('assets/front/images/Pinterest-icon.png')}}" /></a>--}}

                                        {{--</div>--}}
                                        <img src="{{url('').'/core/storage/'.$value->path.'/'.$value->filename}}" alt="Chicago" style="height: auto; margin-left: 20%; max-width: 350px; width: 100%;">
                                  </div>

                                  <?php endforeach ?>

                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                  <i class="fa fa-angle-left" aria-hidden="true"></i>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                                  <span class="sr-only">Next</span>
                                </a>
                      </div>

                </div>
                <div class="col-sm-6">
                    <div class="title left" style="border-bottom: none !important;">
                        <h3>{{$gallery->album_name}}</h3>
                        <hr style="color: #e9e9e9;">
                        <p class="album-desc text-justify">{{$gallery->description}}</p>
                    </div>
                    <div class="" style="margin-top: 25px;">
                        <a href="{{url('gallery/0')}}"><button class="btn add-to-cart">Back to Gallery</button></a>
                    </div>
                </div>
            </div>


        </div>
    </section>


@stop

@section('js')

<script>
        jQuery(window).load(function(){
            $('.gallery_item').magnificPopup({
                delegate: 'a.zoom',
                type: 'image',
                gallery: {
                    enabled: false
                },
                zoom: {
                    enabled: false,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });
            $('.zoom:first').trigger('click');
        });
    </script>


    <script async defer data-pin-hover="true" data-pin-tall="true" data-pin-round="true" src="//assets.pinterest.com/js/pinit.js"></script>

@stop
