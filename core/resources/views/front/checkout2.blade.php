@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

@stop


@section('content')
  
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
    
    
    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">
      
      <!-- CONTAINER -->
      <div class="container">

        <!-- CHECKOUT BLOCK -->
        <div class="checkout_block">
          <ul class="checkout_nav checkout-padding-added">
            <a href="{{url('checkout1')}}"><li>1. Shipping Details</li></a>
            <a href="{{url('checkout2')}}"><li class="active_step">2. Delivery Details</li></a>
            <a href="{{url('checkout3')}}"><li>3. Payment Details</li></a>
            <a href="{{url('checkout4')}}"><li class="last">4. Confirm Order</li></a>
          </ul>
          
          <!-- <div class="checkout_delivery_note alert alert-danger alert-dismissable"><i class="fa fa-exclamation-circle"></i>Express delivery options are available for in-stock items only.</div> -->
          <div class="alert alert-danger alert-dismissable"><i class="fa fa-exclamation-circle"></i>Express delivery options are available for in-stock items only.</div>
          <div class="checkout_delivery clearfix">
            <form method="POST" id="form">
               {!!Form::token()!!}
            <p class="checkout_title">SHIPPING METHOD</p>
            <ul>    
                  <?php foreach ($shippingMethod as $key => $value): ?>
                    <?php if (isset($lastShipping->shipping_method)): ?>
                        <?php if ($lastShipping->shipping_method==$value->id): ?>
                        <li>
                        <input value="{{$value->id}}" id="radio_{{$value->id}}"  type="radio" name="shipping_method" hidden checked="checked"  />
                        <label for="radio_{{$value->id}}">{{$value->name}} <b>{{$value->description}}</b><img src="images/standart_post.jpg" alt="" /></label>
                      </li>
                      <?php else: ?>
                        <li>
                        <input value="{{$value->id}}" id="radio_{{$value->id}}"  type="radio" name="shipping_method" hidden   />
                        <label for="radio_{{$value->id}}">{{$value->name}} <b>{{$value->description}}</b><img src="images/standart_post.jpg" alt="" /></label>
                      </li>
                      <?php endif ?>
                    <?php else: ?>
                       <li>
                        <input value="{{$value->id}}" id="radio_{{$value->id}}"  type="radio" name="shipping_method" hidden   />
                        <label for="radio_{{$value->id}}">{{$value->name}} <b>{{$value->description}}</b><img src="images/standart_post.jpg" alt="" /></label>
                      </li>
                    <?php endif ?>
                    
                    
                  <?php endforeach ?>            
              
              
            </ul>
            
            <a href="{{url('checkout3')}}">
             <button type="submit" class="btn pull-right checkout_block_btn">Continue</button>
            </a>
            <a href="{{url('checkout1')}}">
              <button type="button" class="btn pull-left checkout_block_btn">Step Back</button>
            </a>
            </form>
           
          </div>
        </div><!-- //CHECKOUT BLOCK -->
      </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->

    
@stop

@section('js')


 
@stop
