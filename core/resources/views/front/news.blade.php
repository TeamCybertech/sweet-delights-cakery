@extends('layouts.front.master') @section('title','About | www.theprinceofgalle.com')


<!-- LOADING FONTS AND ICONS -->
<link href="http://fonts.googleapis.com/css?family=Roboto%3A900%2C700%2C400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="{{url('assets/front/revolution-slider/css/settings.css')}}">
<!-- REVOLUTION LAYERS STYLES -->
<!-- REVOLUTION NAVIGATION STYLES -->
<link rel="stylesheet" type="text/css" href="{{url('assets/front/revolution-slider/css/navigation.css')}}">


<link rel="stylesheet" href="{{url('assets/front/slider_news/css/reset.css')}}">
<!-- CSS reset -->
<link rel="stylesheet" href="{{url('assets/front/slider_news/css/style.css')}}">
<!-- Resource style -->
<script src="{{url('assets/front/slider_news/js/modernizr.js')}}"></script> <!-- Modernizr -->
<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>


@section('css')
<style type="text/css">
.caret {
    margin-top: 7px;
}
.slick-initialized .slick-slide{
   /* max-height: 550px;*/
   /* background: #f4f4f4;*/
    overflow: hidden;
}
.slick-disabled{
    display: none!important;
}
.slick-prev::before, .slick-next::before{
    color: #8b5730;
    border: 2px solid #8b5730;
    background: none;
    box-shadow: none;
}
.slick-slide{
    height: auto;
}
.slick-dots li.slick-active button::before{
    border: 2px solid #0000004d;
    border-radius: 100%;
    padding: 0;
    line-height: 16px;
}
.slick-dots li button::before{
    font-size: 22px;
line-height: 24px;
}
.slick-next{
    right: -40px;
}
.slick-prev{
    left: -40px;
}
.cd-slider-wrapper{
    background-color: rgb(238, 238, 238);
    padding: 30px 50px 30px 50px;
}
button.read-more {
    background-color:#8b5730;
    color: #E5D4CD;
    /*border: 1px solid transparent;*/
    border: none;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 10px 10px;
    margin: 5px 0 0 0;
}
button.read-more:hover {
    background-color:#E5D4CD;
    color: #8b5730;
    border: none;
}

    /* button.slick-arrow{
    background-color:#696663;
    } */
    /* button.slick-arrow:hover{
    background-color:#b3afaa;
    } */
    .cd-half-block.content{
        padding: 0 15px 0 15px;
    }
    .cd-half-block-container{
        vertical-align: top!important;
    }
    .cd-half-block-container h2{
        font-weight: 700;
        color: #000;
    }
    .underline{
        border-bottom: 5px solid #e2ad65;
        margin: 0 100px 30px 100px;
    }
    .cd-half-block-container p{
        text-align: center;
        color: #000;
        padding: 0 5px 0 5px;
    }
    .cd-slider p {
        line-height: 1.3;
    }
    .cd-slider .content{
        background-color: #fff!important;
    }
    .cd-slider{
        box-shadow: 1px 1px 20px 4px #888888;
    }
    .article_item{
        /* max-height:400px; */
        margin-bottom: 30px;
    }
    .article_item img{
        /* max-height:250px; */
        width: 100%;
        margin-left:auto;
        margin-right:auto;
    }
    .article_title {
        max-width:180px;
        font-size:12pt;
        margin:auto;
        line-height: 1.5em;
        height: 3em;
        overflow: hidden;
    }
    .header_inTheNews{
        width:300px;
        height:100px;
        border:5px solid #8b5730;
        margin-left:auto;
        margin-right:auto;
        margin-bottom:10px;
        border-radius: 5px !important;
        background:white;
    }

    @media (max-width:768px){
     .header_inTheNews{
        width:200px;
        height:75px;
        border:5px solid #8b5730;
        margin-left:auto;
        margin-right:auto;
        border-radius: 5px !important;
        background:white;
    }
}

.text-single{
    white-space: nowrap;
    max-width: 250px;
    text-overflow: ellipsis;
}

p.honor-description-container {
    display: block; /* or inline-block */
    text-overflow: ellipsis;
    word-wrap: break-word;
    overflow: hidden;
    max-height: 480px;
    line-height: 24px;
}

.honor_more_button {
        /*position: absolute;*/
    bottom: 0;
    vertical-align: bottom;
    top: auto;
    margin-top: auto;
    margin-bottom: auto;
    text-align: center;
}
@media only screen and (max-width: 900px) {
    p.honor-description-container {
    display: block; /* or inline-block */
    text-overflow: ellipsis;
    word-wrap: break-word;
    overflow: hidden;
    max-height: 48px;
    line-height: 24px;
}
}
</style>
@stop

@section('content')
<section class="breadcrumb men parallax margbot30"></section>
<!-- //BREADCRUMBS -->
<!-- PAGE HEADER -->
<section class="page_header">
    <hr class="banner-top">
    <div class="banner-bg center">
        <h3>In the News</h3>
        <p>Our portfolio of News!</p>
    </div>
    <hr class="banner-bottom">
    <!-- CONTAINER -->
    <div class="container">
    </div>
    <!-- //CONTAINER -->
</section>
<!-- //PAGE HEADER -->
<!-- ARTICLES BLOCK -->
<div class="row">
    <div class="clearfix ">
        <div class="container no-padding">
            <div class="col-sm-4 col-middle ">
                <div class="wpb_wrapper">
                </div>
            </div>
            <div class="col-sm-4 col-middle ">
                <div class="wpb_wrapper" style="margin-bottom: 30px;">
                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                        <div class="wpb_wrapper">
                            <div class="header_inTheNews">
                                <div style="text-align:center;margin-top:10%;font-family:inherit;">
                                    <h1 style="color:#8b5730;">FEATURES</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-middle">
                <div class="wpb_wrapper">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ARTICLES BLOCK -->
<section class="articles_block">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- ARTICLES LIST -->
            <div class="articles_list clearfix">
                <!-- {{--
                    <article class="article_item article_large col-lg-12 col-md-12 clearfix margbot30">
                        --}}
                        {{--
                        <a class="article_image" href="{{url('newsDetails')}}" >
                            --}}
                            {{--
                            <div class="article_date">15<span>oct</span></div>
                            --}}
                            {{--<img src="{{asset('assets/front/images/articles/large.jpg')}}" alt="" />--}}
                            {{--
                        </a>
                        --}}
                        {{--<a class="article_title" href="{{url('newsDetails')}}" >See All the Ridiculously Hot, Nearly Naked Looks From Shakira and Rihanna's Video</a>--}}
                        {{--
                        <div class="article_content">
                            --}}
                            {{--
                            <p>This courage of conviction allows such friends of the designer as Caroline Issa and Brazilian princess Paola Orléans e Bragança and other fans—like, say, Gwyneth Paltrow—to stand tall in her designs. (The five-inch pumps she collaborated on with Manolo Blahnik also help).</p>
                            --}}
                            {{--
                            <p>Casasola has a Central Saint Martins degree and design work at Lanvin on her résumé, but she says she learned the most from her seamstress grandmother, who never left the house in anything but a maxi-length. The lesson, she says: “Luxury is simplicity.”</p>
                            --}}
                            {{--
                        </div>
                        --}}
                        {{--
                        <ul class="post_meta">
                            --}}
                            {{--
                            <li><i class="fa fa-user"></i><a hr`ef="javascript:void(0);" > Anna Balashova</a></li>
                            --}}
                            {{--
                            <li><i class="fa fa-comments"></i>Commetcs <span class="sep">|</span> 15</li>
                            --}}
                            {{--
                            <li><i class="fa fa-eye"></i>views <span class="sep">|</span> 259</li>
                            --}}
                            {{--
                        </ul>
                        --}}
                        {{--
                    </article> -->
                --}}
                <div class="respond_clear_768"></div>
                <!-- <div class="row">
                    @foreach ($news as $key => $item)
                    <div class="col-lg-3">
                        <a class="article_image" href="{{url('newsDetails/'.$item->id)}}" >
                        @if(!empty($item->getImages))
                        <img src="{{asset('assets/front/images/articles/1.jpg')}}" alt="" />
                        @else
                        <img src="{{asset($item->getImages->first()->path.'/'.$item->getImages->where('type','front')->first()->file_name)}}" alt="" />
                        @endif
                        </a>
                        <a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" >{{ substr(strip_tags($item->title),0,50)}}...</a>
                        <p class="text-center">  {{$item->created_at->format('M-Y')}}<span></p>
                        <center><a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" ><button  class="btn read-more ">Read more</button></a></center>
                    </div>
                    @endforeach
                </div> -->

                <!-- Do not change styles while doing backend development -->
                 <!-- <section>
                    <div class="col-12 slide">
                        @foreach ($news as $key => $item)
                        <article class="article_item clearfix text-center">
                            <a class="article_image" href="{{url('newsDetails/'.$item->id)}}" >
                            @if(!empty($item->getImages))
                            <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$item->front_image}}" alt="" />

                            @else
                            <img src="{{asset('assets/front/images/articles/1.jpg')}}" alt="" />
                            @endif
                            </a>
                            <a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" >{{ substr(strip_tags($item->title),0,50)}}</a>
                            <p class="text-center">  {{$item->volume_no}}<span></p>
                            <center><a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" ><button  class="btn read-more">Read more</button></a></center>
                        </article>
                        @endforeach
                    </div>
                </section>   -->  

                <section>
                    
                    <div class="col-12 owl-carousel owl-slide">
                        @foreach ($news as $key => $item)
                        <article class="item article_item clearfix text-center">
                            <a class="article_image" href="{{url('newsDetails/'.$item->id)}}" >
                            @if(!empty($item->getImages))
                            <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$item->front_image}}" alt="" />

                            @else
                            <img src="{{asset('assets/front/images/articles/1.jpg')}}" alt="" />
                            @endif
                            </a>
                            <a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" >{{ substr(strip_tags($item->title),0,50)}}</a>
                            <p class="text-center">  {{$item->volume_no}}<span></p>
                            <center><a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" ><button  class="btn read-more">Read more</button></a></center>
                        </article>
                        @endforeach
                    </div>
                </section>  


<!-- 
                <section>
                    <div class="col-12 slide">

                        @foreach ($news as $key => $item)
                        <div class="article_item clearfix margbot30 text-center">
                            <a class="article_image " href="{{url('newsDetails/'.$item->id)}}" >
                                @if(!empty($item->getImages))
                                <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$item->front_image}}" alt="" width="100%"/>

                                @else
                                <img src="{{asset('assets/front/images/articles/1.jpg')}}" alt="" />
                                @endif
                            </a>
                            <a class="article_title text-center text-single" href="{{url('newsDetails/'.$item->id)}}" >{{ substr(strip_tags($item->title),0,50)}}</a>
                            <p class="text-center"> {{$item->volume_no}}<span></p>
                                <center><a class="article_title text-center" href="{{url('newsDetails/'.$item->id)}}" ><button  class="btn read-more ">Read more</button></a></center>
                            </div>

                            @endforeach

                        </div>
                    </section> -->


                    <div class="respond_clear_768 respond_clear_480"></div>
                </div>
                <!-- //ARTICLES LIST -->
            </div>
            <!-- //ROW -->
            <hr>
            @include('front.pagination', ['paginator' => $news])
            <div class="padbot30"></div>
        </div>
        <!-- //CONTAINER -->
    </section>
    <!-- //BLOG BLOCK -->
    <div class="row">
        <div class="clearfix ">
            <div class="container no-padding">
                <div class="col-sm-4 col-middle ">
                    <div class="wpb_wrapper">
                    </div>
                </div>
                <div class="col-sm-4 col-middle">
                <div class="wpb_wrapper">
                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                        <div class="wpb_wrapper" style="margin-bottom: 30px;">
                          <div class="header_inTheNews">
                              <div style="text-align:center;margin-top:10%;font-family:inherit;">
                                  <h1 style="color:#8b5730;">HONORS</h1>
                              </div>
                          </div>
                            {{-- <div style="width:300px;height:100px;border:5px solid #8b5730;margin-left: 7%;border-radius: 5px !important;background:white">
                                <div style="text-align:center;margin-top:10%;font-family:inherit;">
                                    <h1 style="color:#8b5730;">HONORS</h1>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-middle ">
                <div class="wpb_wrapper">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/revolution-slider/js/revolution.extension.video.min.js')}}"></script>

<!-- SLIDER HONOR -->
<section>
    <article>


        <div id="rev_slider_1171_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="magazine-posts189" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
            <div id="rev_slider_1171_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                <ul>    <!-- SLIDE  -->
                    {{$i= 3228}}
                    @foreach ($honor as $key=>$item)
                        @if($item->type ==0)
                            <li data-index="rs-{{$i}}" data-transition="slideremovevertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://server.local/revslider/wp-content/uploads/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="{{url('').'/core/storage/'.$item->path.'/'.$item->filename}}" data-bgcolor="#eeeeee" alt=""  data-lazyload="http://inno.bar/revo-slider-demo/assets/images/transparent.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme  tp-largeshadow"
                                     id="slide-{{$i}}-layer-8"
                                     data-x="['center','center','center','center']" data-hoffset="['0','1','1','942']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-63']"
                                     data-width="['1228','920','920','798']"
                                     data-height="['798','598','598','518']"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"sX:0.8;sY:0.8;opacity:0;","speed":2000,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"sX:0.8;sY:0.8;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 5;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-10"
                                     id="slide-{{$i}}-layer-1"
                                     data-x="['center','center','center','center']" data-hoffset="['-308','-230','-230','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:top;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:bottom;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 6;border-width:0px;"><img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" alt="" data-ww="['615px','461px','461px','538px']" data-hh="['800px','600px','600px','700px']" width="615" height="800" data-lazyload="{{url('').'/core/storage/'.$item->path.'/'.$item->filename}}" data-no-retina> </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                                     id="slide-{{$i}}-layer-11"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','698']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-40']"
                                     data-width="['615','461','461','400']"
                                     data-height="['800','600','600','520']"
                                     data-whitespace="nowrap"
                                     data-visibility="['on','on','on','off']"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":700,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 7;background-color:rgba(229, 229, 229, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"
                                     id="slide-{{$i}}-layer-10"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','719']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-24']"
                                     data-width="['615','461','461','400']"
                                     data-height="['800','600','600','520']"
                                     data-whitespace="nowrap"
                                     data-visibility="['on','on','on','off']"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 8;background-color:rgba(245, 245, 245, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                                     id="slide-{{$i}}-layer-2"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="['615','461','461','420']"
                                     data-height="['800','600','600','650']"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 9;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 6 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-3"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-315','-246','-246','-270']"
                                     data-width="['615','461','461','420']"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":600,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['center','center','center','center']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 10; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 13px; line-height: 13px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;border-width:0px;letter-spacing:3px;">
                                    {{--MOTIVATIONAL ESSAY --}}
                                </div>

                                <!-- LAYER NR. 7 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-14"
                                     id="slide-{{$i}}-layer-4"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-179','-141','-141','-154']"
                                     data-fontsize="['30','25','25','30']"
                                     data-lineheight="['50','40','40','50']"
                                     data-width="['615','461','461','420']"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:-50px;skX:5px;opacity:0;","speed":2500,"to":"o:1;","delay":550,"split":"chars","splitdelay":0.03,"ease":"Power4.easeOut"},{"delay":"wait","speed":500,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['center','center','center','center']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 11; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 30px; line-height: 50px; font-weight: 700; color: rgba(0, 0, 0, 1.00);font-family:Roboto;border-width:0px;letter-spacing:7px;">
                                    {{--CREATING<br/>--}}
                                    {{--YOUR OWN<br/>--}}
                                    {{--MEANING--}}
                                    {{$item->name}}
                                </div>

                                <!-- LAYER NR. 8 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-5"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-46','-40','-40','-41']"
                                     data-width="100"
                                     data-height="6"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"sX:0;","mask":"x:0;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:[-100%];opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 12;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 9 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-6"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['55','76','76','91']"
                                     data-width="['460','380','380','320']"
                                     data-height="none"
                                     data-whitespace="normal"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13; min-width: 460px; max-width: 460px; white-space: normal; font-size: 18px; line-height: 30px; font-weight: 400; color: rgba(119, 119, 119, 1.00);font-family:Roboto;border-width:0px;">
                                    {{--How is it possible that a being with such sensitive jewels as the eyes, such enchanted musical instruments as the ears, and such fabulous arabesque of nerves as the brain can experience itself anything less than a god ...--}}
                                    {{$item->description}}
                                </div>

                                <!-- LAYER NR. 10 -->
                                <a class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-13"
                                   href="{{$item->more_link}}" target="_blank"  id="slide-{{$i}}-layer-7"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['228','224','224','253']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="button"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:20px;sX:1;sY:1;opacity:0;","speed":1500,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(196, 162, 110, 1.00);bw:2px 2px 2px 2px;"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[40,40,40,40]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[40,40,40,40]"

                                     style="z-index: 14; white-space: nowrap; font-size: 14px; line-height: 40px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(196, 162, 110, 1.00);border-style:solid;border-width:2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:3px;cursor:pointer;">KEEP READING </a>
                                {{--<div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-13"--}}
                                     {{--id="slide-{{$i}}-layer-7"--}}
                                     {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['228','224','224','253']"--}}
                                     {{--data-width="none"--}}
                                     {{--data-height="none"--}}
                                     {{--data-whitespace="nowrap"--}}

                                     {{--data-type="button"--}}
                                     {{--data-responsive_offset="on"--}}

                                     {{--data-frames='[{"from":"y:20px;sX:1;sY:1;opacity:0;","speed":1500,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(196, 162, 110, 1.00);bw:2px 2px 2px 2px;"}]'--}}
                                     {{--data-textAlign="['left','left','left','left']"--}}
                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                     {{--data-paddingright="[40,40,40,40]"--}}
                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                     {{--data-paddingleft="[40,40,40,40]"--}}

                                     {{--style="z-index: 14; white-space: nowrap; font-size: 14px; line-height: 40px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(196, 162, 110, 1.00);border-style:solid;border-width:2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:3px;cursor:pointer;">KEEP READING </div>--}}
                            </li>
                        {{$i++}}
                            <!-- SLIDE  -->
                        @elseif($item->type == 1)
                            <?php
                            $url = $item->url;
                            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                           // echo $my_array_of_vars['v'];
                            // Output: C4kxS1ksqtw
                            ?>
                            <li data-index="rs-{{$i}}" data-transition="slideremovevertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://server.local/revslider/wp-content/uploads/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                {{--<img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" data-bgcolor="#eeeeee" alt=""  data-lazyload="http://inno.bar/revo-slider-demo/assets/images/transparent.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>--}}
                                <img src="" data-bgcolor="#eeeeee" alt=""  data-lazyload="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 11 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme  tp-largeshadow"
                                     id="slide-{{$i}}-layer-8"
                                     data-x="['center','center','center','center']" data-hoffset="['0','1','1','942']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-63']"
                                     data-width="['1228','920','920','798']"
                                     data-height="['798','598','598','518']"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"sX:0.8;sY:0.8;opacity:0;","speed":2000,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"sX:0.8;sY:0.8;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 15;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 12 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-10 tp-videolayer"
                                     id="slide-{{$i}}-layer-12"
                                     data-x="['center','center','center','center']" data-hoffset="['-308','-230','-230','599']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-4']"
                                     data-whitespace="nowrap"
                                     data-visibility="['on','on','on','off']"

                                     data-type="video"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:top;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:bottom;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-ytid="{{$my_array_of_vars['v']}}" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;" data-videorate="1" data-videowidth="['615px','461px','461px','461px']" data-videoheight="['800px','600px','600px','600px']" data-videocontrols="controls" data-videoloop="none"          data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"
                                     data-autoplay="off"
                                     data-volume="100"           data-allowfullscreenvideo="true"

                                     style="z-index: 16;border-width:0px;"> </div>

                                <!-- LAYER NR. 13 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                                     id="slide-{{$i}}-layer-11"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','698']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-40']"
                                     data-width="['615','461','461','400']"
                                     data-height="['800','600','600','520']"
                                     data-whitespace="nowrap"
                                     data-visibility="['on','on','on','off']"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":700,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 17;background-color:rgba(229, 229, 229, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 14 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"
                                     id="slide-{{$i}}-layer-10"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','719']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-24']"
                                     data-width="['615','461','461','400']"
                                     data-height="['800','600','600','520']"
                                     data-whitespace="nowrap"
                                     data-visibility="['on','on','on','off']"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 18;background-color:rgba(245, 245, 245, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 15 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                                     id="slide-{{$i}}-layer-2"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="['615','461','461','420']"
                                     data-height="['800','600','600','650']"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 19;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 16 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-3"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-315','-246','-246','-270']"
                                     data-width="['615','461','461','420']"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":600,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['center','center','center','center']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 20; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 13px; line-height: 13px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;border-width:0px;letter-spacing:3px;">
                                    {{--TED TALK--}}
                                </div>

                                <!-- LAYER NR. 17 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-14"
                                     id="slide-{{$i}}-layer-4"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-179','-141','-141','-154']"
                                     data-fontsize="['30','25','25','30']"
                                     data-lineheight="['50','40','40','50']"
                                     data-width="['615','461','461','420']"
                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:-50px;skX:5px;opacity:0;","speed":2500,"to":"o:1;","delay":550,"split":"chars","splitdelay":0.03,"ease":"Power4.easeOut"},{"delay":"wait","speed":500,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['center','center','center','center']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 21; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 30px; line-height: 50px; font-weight: 700; color: rgba(0, 0, 0, 1.00);font-family:Roboto;border-width:0px;letter-spacing:7px;">
                                    {{--THE BIRTH OF<br/>--}}
                                    {{--VIRTUAL REALITY<br/>--}}
                                    {{--AS AN ART FORM --}}
                                    {{$item->name}}
                                </div>

                                <!-- LAYER NR. 18 -->
                                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-5"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['-46','-40','-40','-41']"
                                     data-width="100"
                                     data-height="6"
                                     data-whitespace="nowrap"

                                     data-type="shape"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"sX:0;","mask":"x:0;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:[-100%];opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 22;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                                <!-- LAYER NR. 19 -->
                                <div class="tp-caption   tp-resizeme rs-parallaxlevel-13"
                                     id="slide-{{$i}}-layer-6"
                                     data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['70','92','92','106']"
                                     data-width="['460','380','380','320']"
                                     data-height="none"
                                     data-whitespace="normal"

                                     data-type="text"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 23; min-width: 460px; max-width: 460px; white-space: normal; font-size: 18px; line-height: 30px; font-weight: 400; color: rgba(119, 119, 119, 1.00);font-family:Roboto;border-width:0px;">
                                    {{$item -> description}}
                                    </div>

                                <!-- LAYER NR. 20 -->
                                <a class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-13"
                                   href="{{$item->more_link}}" target="_blank"          id="slide-{{$i}}-layer-7"
                                   data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"
                                   data-y="['middle','middle','middle','middle']" data-voffset="['228','224','224','253']"
                                   data-width="none"
                                   data-height="none"
                                   data-whitespace="nowrap"

                                   data-type="button"
                                   data-actions=''
                                   data-responsive_offset="on"

                                   data-frames='[{"from":"y:20px;sX:1;sY:1;opacity:0;","speed":1500,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(196, 162, 110, 1.00);bw:2px 2px 2px 2px;"}]'
                                   data-textAlign="['left','left','left','left']"
                                   data-paddingtop="[0,0,0,0]"
                                   data-paddingright="[40,40,40,40]"
                                   data-paddingbottom="[0,0,0,0]"
                                   data-paddingleft="[40,40,40,40]"

                                   style="z-index: 24; white-space: nowrap; font-size: 14px; line-height: 40px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(196, 162, 110, 1.00);border-style:solid;border-width:2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:3px;cursor:pointer;">WATCH ON YOUTUBE </a>
                            </li>
                            <!-- SLIDE  -->
                            {{$i++}}
                        @endif
                    @endforeach


                    {{--<li data-index="rs-3230" data-transition="slideremovevertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://server.local/revslider/wp-content/uploads/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                        {{--<!-- MAIN IMAGE -->--}}
                        {{--<img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" data-bgcolor="#eeeeee" alt=""  data-lazyload="http://inno.bar/revo-slider-demo/assets/images/transparent.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>--}}
                        {{--<!-- LAYERS -->--}}

                        {{--<!-- LAYER NR. 21 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme  tp-largeshadow"--}}
                             {{--id="slide-3230-layer-8"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['0','1','1','942']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-63']"--}}
                             {{--data-width="['1228','920','920','798']"--}}
                             {{--data-height="['798','598','598','518']"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"sX:0.8;sY:0.8;opacity:0;","speed":2000,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"sX:0.8;sY:0.8;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 25;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 22 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-10"--}}
                             {{--id="slide-3230-layer-1"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['-308','-230','-230','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                             {{--data-width="none"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="image"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:top;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:bottom;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 26;border-width:0px;"><img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" alt="" data-ww="['615px','461px','461px','538px']" data-hh="['800px','600px','600px','700px']" width="615" height="800" data-lazyload="http://inno.bar/revo-slider-demo/assets/images/post2.jpg" data-no-retina> </div>--}}

                        {{--<!-- LAYER NR. 23 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"--}}
                             {{--id="slide-3230-layer-11"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','698']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-40']"--}}
                             {{--data-width="['615','461','461','400']"--}}
                             {{--data-height="['800','600','600','520']"--}}
                             {{--data-whitespace="nowrap"--}}
                             {{--data-visibility="['on','on','on','off']"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":700,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 27;background-color:rgba(229, 229, 229, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 24 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"--}}
                             {{--id="slide-3230-layer-10"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','719']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-24']"--}}
                             {{--data-width="['615','461','461','400']"--}}
                             {{--data-height="['800','600','600','520']"--}}
                             {{--data-whitespace="nowrap"--}}
                             {{--data-visibility="['on','on','on','off']"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 28;background-color:rgba(245, 245, 245, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 25 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"--}}
                             {{--id="slide-3230-layer-2"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="['800','600','600','650']"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 29;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 26 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3230-layer-3"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-315','-246','-246','-270']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":600,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['center','center','center','center']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 30; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 13px; line-height: 13px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;border-width:0px;letter-spacing:3px;">TECH TALK </div>--}}

                        {{--<!-- LAYER NR. 27 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-14"--}}
                             {{--id="slide-3230-layer-4"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-179','-141','-141','-154']"--}}
                             {{--data-fontsize="['30','25','25','30']"--}}
                             {{--data-lineheight="['50','40','40','50']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"x:-50px;skX:5px;opacity:0;","speed":2500,"to":"o:1;","delay":550,"split":"chars","splitdelay":0.03,"ease":"Power4.easeOut"},{"delay":"wait","speed":500,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['center','center','center','center']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 31; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 30px; line-height: 50px; font-weight: 700; color: rgba(0, 0, 0, 1.00);font-family:Roboto;border-width:0px;letter-spacing:7px;">ARE ALL<br/>--}}
                            {{--THESE GADGETS<br/>--}}
                            {{--REALLY NEEDED? </div>--}}

                        {{--<!-- LAYER NR. 28 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3230-layer-5"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-46','-40','-40','-41']"--}}
                             {{--data-width="100"--}}
                             {{--data-height="6"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"sX:0;","mask":"x:0;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:[-100%];opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 32;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 29 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3230-layer-6"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['71','76','76','91']"--}}
                             {{--data-width="['460','380','380','320']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="normal"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 33; min-width: 460px; max-width: 460px; white-space: normal; font-size: 18px; line-height: 30px; font-weight: 400; color: rgba(119, 119, 119, 1.00);font-family:Roboto;border-width:0px;">The reason we have poverty is that we have no imagination. There are a great many people accumulating what they think is vast wealth, but it's only money... they don't know how to enjoy it, because they have no imagination.  </div>--}}

                        {{--<!-- LAYER NR. 30 -->--}}
                        {{--<div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3230-layer-7"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['228','224','224','253']"--}}
                             {{--data-width="none"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="button"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:20px;sX:1;sY:1;opacity:0;","speed":1500,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(196, 162, 110, 1.00);bw:2px 2px 2px 2px;"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[40,40,40,40]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[40,40,40,40]"--}}

                             {{--style="z-index: 34; white-space: nowrap; font-size: 14px; line-height: 40px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(196, 162, 110, 1.00);border-style:solid;border-width:2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:3px;cursor:pointer;">KEEP READING </div>--}}
                    {{--</li>--}}
                    {{--<!-- SLIDE  -->--}}
                    {{--<li data-index="rs-3231" data-transition="slideremovevertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://server.local/revslider/wp-content/uploads/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                        {{--<!-- MAIN IMAGE -->--}}
                        {{--<img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" data-bgcolor="#eeeeee" alt=""  data-lazyload="http://inno.bar/revo-slider-demo/assets/images/transparent.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>--}}
                        {{--<!-- LAYERS -->--}}

                        {{--<!-- LAYER NR. 31 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme  tp-largeshadow"--}}
                             {{--id="slide-3231-layer-8"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['0','1','1','942']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-63']"--}}
                             {{--data-width="['1228','920','920','798']"--}}
                             {{--data-height="['798','598','598','518']"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"sX:0.8;sY:0.8;opacity:0;","speed":2000,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"sX:0.8;sY:0.8;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 35;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 32 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-10"--}}
                             {{--id="slide-3231-layer-1"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['-308','-230','-230','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                             {{--data-width="none"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="image"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:top;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:bottom;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 36;border-width:0px;"><img src="http://inno.bar/revo-slider-demo/assets/images/dummy.png" alt="" data-ww="['615px','461px','461px','538px']" data-hh="['800px','600px','600px','700px']" width="615" height="800" data-lazyload="http://inno.bar/revo-slider-demo/assets/images/post3.jpg" data-no-retina> </div>--}}

                        {{--<!-- LAYER NR. 33 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"--}}
                             {{--id="slide-3231-layer-11"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','698']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-40']"--}}
                             {{--data-width="['615','461','461','400']"--}}
                             {{--data-height="['800','600','600','520']"--}}
                             {{--data-whitespace="nowrap"--}}
                             {{--data-visibility="['on','on','on','off']"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":700,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 37;background-color:rgba(229, 229, 229, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 34 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"--}}
                             {{--id="slide-3231-layer-10"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','719']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-24']"--}}
                             {{--data-width="['615','461','461','400']"--}}
                             {{--data-height="['800','600','600','520']"--}}
                             {{--data-whitespace="nowrap"--}}
                             {{--data-visibility="['on','on','on','off']"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":600,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 38;background-color:rgba(245, 245, 245, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 35 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"--}}
                             {{--id="slide-3231-layer-2"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="['800','600','600','650']"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:bottom;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:top;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 39;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 36 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3231-layer-3"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-315','-246','-246','-270']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":600,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['center','center','center','center']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 40; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 13px; line-height: 13px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;border-width:0px;letter-spacing:3px;">LEADERSHIP </div>--}}

                        {{--<!-- LAYER NR. 37 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-14"--}}
                             {{--id="slide-3231-layer-4"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-179','-141','-141','-154']"--}}
                             {{--data-fontsize="['30','25','25','30']"--}}
                             {{--data-lineheight="['50','40','40','50']"--}}
                             {{--data-width="['615','461','461','420']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"x:-50px;skX:5px;opacity:0;","speed":2500,"to":"o:1;","delay":550,"split":"chars","splitdelay":0.03,"ease":"Power4.easeOut"},{"delay":"wait","speed":500,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['center','center','center','center']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 41; min-width: 615px; max-width: 615px; white-space: nowrap; font-size: 30px; line-height: 50px; font-weight: 700; color: rgba(0, 0, 0, 1.00);font-family:Roboto;border-width:0px;letter-spacing:7px;">ABOUT GETTING<br/>--}}
                            {{--OUT ON<br/>--}}
                            {{--THE FIELD </div>--}}

                        {{--<!-- LAYER NR. 38 -->--}}
                        {{--<div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3231-layer-5"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['-46','-40','-40','-41']"--}}
                             {{--data-width="100"--}}
                             {{--data-height="6"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="shape"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"sX:0;","mask":"x:0;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:[-100%];opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 42;background-color:rgba(196, 162, 110, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                        {{--<!-- LAYER NR. 39 -->--}}
                        {{--<div class="tp-caption   tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3231-layer-6"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['56','76','76','91']"--}}
                             {{--data-width="['460','380','380','320']"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="normal"--}}

                             {{--data-type="text"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":550,"ease":"Power4.easeOut"},{"delay":"wait","speed":400,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[0,0,0,0]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[0,0,0,0]"--}}

                             {{--style="z-index: 43; min-width: 460px; max-width: 460px; white-space: normal; font-size: 18px; line-height: 30px; font-weight: 400; color: rgba(119, 119, 119, 1.00);font-family:Roboto;border-width:0px;">It is better to lead from behind and to put others in front, especially when you celebrate victory when nice things occur. You take the front line when there is danger. Then people will appreciate your leadership. </div>--}}

                        {{--<!-- LAYER NR. 40 -->--}}
                        {{--<div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-13"--}}
                             {{--id="slide-3231-layer-7"--}}
                             {{--data-x="['center','center','center','center']" data-hoffset="['307','231','231','0']"--}}
                             {{--data-y="['middle','middle','middle','middle']" data-voffset="['228','224','224','253']"--}}
                             {{--data-width="none"--}}
                             {{--data-height="none"--}}
                             {{--data-whitespace="nowrap"--}}

                             {{--data-type="button"--}}
                             {{--data-responsive_offset="on"--}}

                             {{--data-frames='[{"from":"y:20px;sX:1;sY:1;opacity:0;","speed":1500,"to":"o:1;","delay":1250,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"y:-50px;opacity:0;","ease":"Power2.easeIn"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(196, 162, 110, 1.00);bw:2px 2px 2px 2px;"}]'--}}
                             {{--data-textAlign="['left','left','left','left']"--}}
                             {{--data-paddingtop="[0,0,0,0]"--}}
                             {{--data-paddingright="[40,40,40,40]"--}}
                             {{--data-paddingbottom="[0,0,0,0]"--}}
                             {{--data-paddingleft="[40,40,40,40]"--}}

                             {{--style="z-index: 44; white-space: nowrap; font-size: 14px; line-height: 40px; font-weight: 900; color: rgba(196, 162, 110, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(196, 162, 110, 1.00);border-style:solid;border-width:2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:3px;cursor:pointer;">KEEP READING </div>--}}
                    {{--</li>--}}
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
        </div><!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
            var tpj=jQuery;

            var revapi1171;
            tpj(document).ready(function() {
                if(tpj("#rev_slider_1171_1").revolution == undefined){
                    revslider_showDoubleJqueryError("#rev_slider_1171_1");
                }else{
                    revapi1171 = tpj("#rev_slider_1171_1").show().revolution({
                        sliderType:"standard",
                        jsFileLocation:"revolution/js/",
                        sliderLayout:"fullwidth",
                        dottedOverlay:"none",
                        delay:9000,
                        navigation: {
                            keyboardNavigation:"off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation:"off",
                            mouseScrollReverse:"default",
                            onHoverStop:"off",
                            touch:{
                                touchenabled:"on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            }
                            ,
                            arrows: {
                                style:"metis",
                                enable:true,
                                hide_onmobile:true,
                                hide_under:768,
                                hide_onleave:false,
                                tmp:'',
                                left: {
                                    h_align:"left",
                                    v_align:"center",
                                    h_offset:0,
                                    v_offset:0
                                },
                                right: {
                                    h_align:"right",
                                    v_align:"center",
                                    h_offset:0,
                                    v_offset:0
                                }
                            }
                        },
                        responsiveLevels:[1240,1024,778,480],
                        visibilityLevels:[1240,1024,778,480],
                        gridwidth:[1400,1200,1000,480],
                        gridheight:[1000,900,700,700],
                        lazyType:"single",
                        shadow:0,
                        spinner:"off",
                        stopLoop:"on",
                        stopAfterLoops:0,
                        stopAtSlide:1,
                        shuffle:"off",
                        autoHeight:"off",
                        disableProgressBar:"on",
                        hideThumbsOnMobile:"off",
                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        debugMode:false,
                        fallbacks: {
                            simplifyAll:"off",
                            nextSlideOnWindowFocus:"off",
                            disableFocusListener:false,
                        }
                    });
                }
            }); /*ready*/


        </script>


    </article>
</section>


 
<!-- Slider News -->
<!-- <div class="cd-slider-wrapper">
    <ul class="cd-slider container" >
        <?php $i=1; foreach ($honor as $key => $value): ?>
        <li <?php if($i==1){?> class="is-visible" <?php } ?> >
            <?php if ( $value->type==1): ?>
            <div class="cd-half-block content" style="background-color: #000;">
                <div>
                    <iframe width="100%" height="100%" src="{{$value->url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="cd-half-block content  container">
                <div class="cd-half-block-container">
                    <h2 class="text-center">{{$value->name}}</h2>
                    <div class="underline"></div>
                    <p class="text-center honor-description-container">
                        <?php echo nl2br($value->description) ?>
                    </p>
                    <div class="honor_more_button">
                        <a href="{{$value->more_link}}" class="btn">Watch Video</a>
                    </div>
                </div>
            </div>
            <?php else: ?>
            <div class="cd-half-block image" style="background-image: url('{{url('').'/core/storage/uploads/images/honor/'.$value->filename}}');"></div>
            {{--  <div class="cd-half-block image">
                <img src="{{url('').'/core/storage/uploads/images/honor/'.$value->filename}}" alt="">
            </div>  --}}
            <div class="cd-half-block content">
                <div class="cd-half-block-container">
                    <h2 class="text-center honor-title">{{$value->name}}</h2>
                    <div class="underline"></div>
                    <p class="text-center honor-description-container">
                        <?php echo nl2br($value->description) ?>
                    </p>
                    <div class="honor_more_button">
                        <button class="btn view-honor-description">Read more</button>
                    </div>

                </div>
            </div>

            <?php endif ?> -->
<!-- Modal -->
<!-- <div class="modal fade" id="honorDescriptionModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
    </div>
    <div class="modal-body text-justify" >
        <p class="honor-description-content"><?php echo nl2br($value->description) ?></p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div> -->
            <!-- <div class="cd-half-block content">
                <div>
                    <h2>{{$value->name}}</h2>
                    <p>
                        <?php echo nl2br($value->description) ?>
                    </p>
                    <a href="{{$value->more_link}}" class="btn">Read more</a>
                </div>
            </div> -->
            <!-- .cd-half-block.content -->
     <!--    </li>
        <?php $i++; endforeach ?>
    </ul> -->
    <!-- .cd-slider -->
<!-- </div> -->
<!-- .cd-slider-wrapper -->
<!-- Slider NEWS end -->



@stop
<script src="{{url('assets/front/slider_news/js/jquery-2.1.4.js')}}"></script>
<script src="{{url('assets/front/slider_news/js/jquery.mobile.custom.min.js')}}"></script>
<script src="{{url('assets/front/slider_news/js/main.js')}}"></script> <!-- Resource jQuery -->

@section('js')
<!-- javascript -->
    <script src="{{url('assets/front/owlcarousel/owl.carousel.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.view-honor-description').click(function(){
            $('#honorDescriptionModal').modal('show');
            let title = $(this).parent().parent().find('.honor-title').html();
            let description = $(this).parent().parent().find('.honor-description').val();
            setHonorDesModal(title,description);
        });

        // $("#honorDescriptionModal").on('show.bs.modal', function () {


        // });
    });

    function setHonorDesModal(title,description){
        $('#honorDescriptionModal').find('.modal-title').html(title);
        $('#honorDescriptionModal').find('.honor-description-content').html(description);
    }
</script>

<script type="text/javascript">
    @if(session('success'))
    swal('Good Job','User Registered','success');
    @endif

    $("#slideshow > div:gt(0)").hide();
    setInterval(function() {
        $('#slideshow > div:first')
        .fadeOut(1000)
        .next()
        .fadeIn(1000)
        .end()
        .appendTo('#slideshow');
    },  3000);

    $(document).on('ready', function() {

        $("#news_slider").slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            rows:1,
            arrows:true,
            autoplay: true,
            autoplaySpeed: 3000
        });

    });

</script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
    /**
 * Owl2row
 * @since 2.0.2
 */
;(function ($, window, document, undefined) {
    Owl2row = function (scope) {
        this.owl = scope;
        this.owl.options = $.extend({}, Owl2row.Defaults, this.owl.options);
        //link callback events with owl carousel here

        this.handlers = {
            'initialize.owl.carousel': $.proxy(function (e) {
                if (this.owl.settings.owl2row) {
                    this.build2row(this);
                }
            }, this)
        };

        this.owl.$element.on(this.handlers);
    };

    Owl2row.Defaults = {
        owl2row: false,
        owl2rowTarget: 'item',
        owl2rowContainer: 'owl2row-item',
        owl2rowDirection: 'utd' // ltr
    };

    //mehtods:
    Owl2row.prototype.build2row = function(thisScope){
    
        var carousel = $(thisScope.owl.$element);
        var carouselItems = carousel.find('.' + thisScope.owl.options.owl2rowTarget);

        var aEvenElements = [];
        var aOddElements = [];

        $.each(carouselItems, function (index, item) {
            if ( index % 2 === 0 ) {
                aEvenElements.push(item);
            } else {
                aOddElements.push(item);
            }
        });

        carousel.empty();

        switch (thisScope.owl.options.owl2rowDirection) {
            case 'ltr':
                thisScope.leftToright(thisScope, carousel, carouselItems);
                break;

            default :
                thisScope.upTodown(thisScope, aEvenElements, aOddElements, carousel);
        }

    };

    Owl2row.prototype.leftToright = function(thisScope, carousel, carouselItems){

        var o2wContainerClass = thisScope.owl.options.owl2rowContainer;
        var owlMargin = thisScope.owl.options.margin;

        var carouselItemsLength = carouselItems.length;

        var firsArr = [];
        var secondArr = [];

        //console.log(carouselItemsLength);

        if (carouselItemsLength %2 === 1) {
            carouselItemsLength = ((carouselItemsLength - 1)/2) + 1;
        } else {
            carouselItemsLength = carouselItemsLength/2;
        }

        //console.log(carouselItemsLength);

        $.each(carouselItems, function (index, item) {


            if (index < carouselItemsLength) {
                firsArr.push(item);
            } else {
                secondArr.push(item);
            }
        });

        $.each(firsArr, function (index, item) {
            var rowContainer = $('<div class="' + o2wContainerClass + '"/>');

            var firstRowElement = firsArr[index];
                firstRowElement.style.marginBottom = owlMargin + 'px';

            rowContainer
                .append(firstRowElement)
                .append(secondArr[index]);

            carousel.append(rowContainer);
        });

    };

    Owl2row.prototype.upTodown = function(thisScope, aEvenElements, aOddElements, carousel){

        var o2wContainerClass = thisScope.owl.options.owl2rowContainer;
        var owlMargin = thisScope.owl.options.margin;

        $.each(aEvenElements, function (index, item) {

            var rowContainer = $('<div class="' + o2wContainerClass + '"/>');
            var evenElement = aEvenElements[index];

            evenElement.style.marginBottom = owlMargin + 'px';

            rowContainer
                .append(evenElement)
                .append(aOddElements[index]);

            carousel.append(rowContainer);
        });
    };

    /**
     * Destroys the plugin.
     */
    Owl2row.prototype.destroy = function() {
        var handler, property;

        for (handler in this.handlers) {
            this.owl.dom.$el.off(handler, this.handlers[handler]);
        }
        for (property in Object.getOwnPropertyNames(this)) {
            typeof this[property] !== 'function' && (this[property] = null);
        }
    };

    $.fn.owlCarousel.Constructor.Plugins['owl2row'] = Owl2row;
})( window.Zepto || window.jQuery, window,  document );
</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('.owl-carousel').slick({
       infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        rows:2,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
              breakpoint: 980,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: false,
                rows:1,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                rows:1,
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    });
  });

</script>
@stop
