@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">
  .fix_silder5 h1 {
    font-size: 30px;
    font-family: 'Montserrat', sans-serif;
  }

.accordion_title {
  margin-top: 40px;
  font-weight: 600;
  text-indent: 2px;
  line-height: 30px;
  text-align: center;
  font-size: 20px;
  color: brown;
}
  .col-md-3{
    margin-top: 10px;
    color: brown;
    font-weight: 400;
    font-size: 13px;
    padding: 5px;
  }
  .fix_silder_img5{
    margin-bottom: 20px;
  }
  .pricing-para{
    margin-bottom: 35px;
  }

  .price_list h4{
    color:#666;
    font-weight: 900!important;
  }
  .price_list h4:last-child{
    margin-bottom: 0;
  }
  .pricing_details{
    min-height: 385px;
    padding: 80px 0px 100px;
  }
  .pricing_details a{
    display: inline!important;
    font-weight: 600!important;
    font-family: 'Roboto', sans-serif!important;
    font-size: 15px!important;
  }
  .row h2{
    margin-top: 30px!important;
    margin-bottom: 20px!important;
    color: brown;
    font-weight: 700;
  }
  .fix_silder5 {
    height: 235px;
    text-align: center;
    padding: 90px 0 0 0;
  }
  .pricing-page-header .container{
    margin-bottom: 0;
  }
</style> 

@stop


@section('content')


    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header pricing-page-header">
      <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Our Pricing</h3>
                <p>View our portfolio of Pricing!</p>
            </div>
            <hr class="banner-bottom">
      <!-- CONTAINER -->


      @foreach ($pricing as $el)

       <section class="fix_silder_img5" style="background-image: url('{{url('core/storage/uploads/images/pricing/'.$el->img)}}')">
          <div class="fix_silder5">
              <h1>{{$el->title}}</h1>
          </div>
      </section>

      <div class="container">
        <section class="pricing_details">
        <div class="row">
          <div class="col-md-12 text-center">
            <?php echo $el->description?>
          </div>
        </div>
        </section>
      </div>
          
      @endforeach



      {{--  <section class="fix_silder_img5" >
          <div class="fix_silder5">
              <h1>CAKE PRICING</h1>
          </div>
      </section>



    <div class="container">
      <section class="pricing_details">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="pricing-para">
          All wedding cake orders require a consultation, please email or call to setup an appointment.
          </p>
            <section class="price_list">
            <h4><strong>BASIC FLAVORS - $4.75 PER SERVING</strong></h4>
            <h4><strong>PREMIUM FLAVORS - $5.25 PER SERVING</strong></h4>
            <h4><strong>RAINBOW COLORED CAKE LAYERS (6) - $0.75 MORE SERVING</strong></h4>
            <h4><strong>OMBRE COLORED CAKE LAYERS (3) - $0.50 MORE SERVING</strong></h4>
            <section>

        </div>
      </div>
      </section>
      </div>

      <section class="fix_silder_img5" >
          <div class="fix_silder5">
              <h1>KITCHEN CAKE PRICING</h1>
          </div>
      </section>
      <section class="pricing_details">

      <div class="container">
      <div class="row">
        <div class="col-md-11 col-md-offset-1">
          <p class="pricing-para">
          All wedding cake orders <b><a href="{{URL('quotes')}}">click here</a></b> require a consultation, please email or <b><a href="{{URL('quotes')}}">contact us</a></b> to setup an appointment.
          </p>
            <section class="price_list">
            <h4><strong>BASIC FLAVORS - $4.75 PER SERVING</strong></h4>
            <h4><strong>PREMIUM FLAVORS - $5.25 PER SERVING</strong></h4>
            <h4><strong>RAINBOW COLORED CAKE LAYERS (6) - $0.75 MORE SERVING</strong></h4>
            <h4><strong>OMBRE COLORED CAKE LAYERS (3) - $0.50 MORE SERVING</strong></h4>
            <section>
        </div>
      </div>
      </section>
    </div>
    <section class="fix_silder_img5" >
        <div class="fix_silder5">
            <h1>SCULPTED CAKE PRICES</h1>
        </div>
    </section>  --}}

    <div class="container">
      {{--  <section class="pricing_details">
      <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <section class="price_list">
            <h4><strong>BASIC FLAVORS - $4.75 PER SERVING</strong></h4>
            <h4><strong>PREMIUM FLAVORS - $5.25 PER SERVING</strong></h4>
            <h4><strong>RAINBOW COLORED CAKE LAYERS (6) - $0.75 MORE SERVING</strong></h4>
            <h4><strong>OMBRE COLORED CAKE LAYERS (3) - $0.50 MORE SERVING</strong></h4>
            <section>
            <br/>
        </div>
      </div>
      </section>  --}}

        <!-- ROW -->
        <div class="row">
          <div class="col-md-12" style="padding-bottom:60px;">
            <h2>{{$diposit->title}}</h2>
            <p><?php echo $diposit->description;?></p>
          </div>
        </div><!-- //ROW -->
      </div><!-- //CONTAINER -->

    </section><!-- //PAGE HEADER -->
      <!-- Tooltip BLOCK -->

@stop

@section('js')



@stop
