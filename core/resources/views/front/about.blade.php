@extends('layouts.front.master') @section('title','About | www.theprinceofgalle.com')
@section('css')

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

      <hr class="banner-top"/>
            <div class="banner-bg center">
                <h3>About us</h3>
                <p>It's so pleasure to see you check about us further!</p>
            </div>
            <hr class="banner-bottom">
      <!-- CONTAINER -->
      <div class="container">

           <!--  <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Blog Page</h3>
                <p>View our portfolio of Blogs!</p>
            </div>
            <hr class="banner-bottom"> -->

      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->
        
        <!-- ABOUT US INFO -->
        <section class="about_us_info" >
            
            <!-- CONTAINER -->
            <div class="container">
                
                <!-- ROW -->
                <div class="row" style="margin-right: 0px; margin-left: 0px;">
                    <div class="col-md-12" style="background-image: url('{{asset('assets/front/images/about_img_bg.jpg')}}');min-height: 560px;">

                        <div class="col-lg-8 col-md-8" style="margin-top: 150px">
                             <h3><b>Meet the Owner</b></h3> 
                             <p class="meet_owner_text">Nisha Fernando is the owner of Sweet Delights Cakery in West Bloomfield, Michigan and has been decorating cakes for over 10 years. She make custom cakes for any occasion and specializes in sculpted and wedding cakes. She won the grand prize at the 2014 Michigan ICES Sugar Art showcase and her work has been published in the American Cake Decorating and the Cake Masters magazine and was also featured in the Detroit Free Press for new bakers in the Metro Detroit area.</p>
                        </div>
                    </div>
                </div>
                
                <!-- ROW -->
                <div class="row" style="margin-right: 0px; margin-left: 0px; margin-top: 25px;">
                    <div class="team_wrapper padbot60" data-appear-top-offset='-100' data-animated='fadeInUp'>
                        <iframe width="100%" height="650" src="https://www.youtube.com/embed/AtaWIzXDrzA" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div><!-- //ROW -->
            </div><!-- //CONTAINER -->
        </section><!-- //ABOUT US INFO -->


@stop

@section('js')

@stop