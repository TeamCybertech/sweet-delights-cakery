@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')

@section('css')

<style type="text/css">
    .flavors_details{
    margin-bottom: 60px;
    margin-top: 60px;
    }
    .fix_silder4 h1 {
      font-size: 30px;
      font-family: 'Montserrat', sans-serif;
    }
    .accordion_title {
    font-weight: 600;
    text-indent: 2px;
    line-height: 30px;
    text-align: center;
    font-size: 20px;
    color: brown;
    }
    .accordion_title.with-margin{
      margin-top: 60px;
    }
    .col-md-3{
    margin-top: 10px;
    color: #666;
    font-weight: 700;
    font-size: 15px;
    padding: 5px;
    text-transform: uppercase;
    }
    .alert-warning{
    background-color: #8b5730!important;
    border-color: #633d20!important;
    color: #633d20!important;
    margin-bottom: 60px;
    }
</style>

@stop

@section('content')
<section class="breadcrumb men parallax margbot30">
</section>
<!-- //BREADCRUMBS -->
<!-- PAGE HEADER -->
<hr class="banner-top">
        <div class="banner-bg center">
            <h3>Our Flavors</h3>
            <p>View our mouth-watering flavors of cakes!</p>
        </div>
        <hr class="banner-bottom">
<section class="page_header">
    <!-- CONTAINER -->
    <div class="container">

    </div>
    <!-- //CONTAINER -->
</section>
<!-- //PAGE HEADER -->
<!-- FAQ PAGE -->
<section class="faq_page">
    <!-- CONTAINER -->
    <div class="container" style="width: 100%;">


        @foreach ($flavors as $el)
        <section class="fix_silder_img4" style="background-image:url({{url('core/storage/uploads/images/flavors/'.$el->img)}});">
            <div class="fix_silder4">
                <h1>{{$el->flavor_name}}</h1>
                <h4><?php echo $el->description; ?></h4>
            </div>
        </section>
        <section class="flavors_details text-center">
          <div class="container">
              @if (isset($el->basicTypes) && $el->basicTypes->count() > 0)
                  <h4 class="accordion_title">BASIC FLAVORS</h4>
                    <div class="row">
                        @foreach ($el->basicTypes as $item)
                            <div class="col-md-3">{{$item->name}}</div>
                        @endforeach
                    </div>
              @endif

              @if (isset($el->premiumTypes) && $el->premiumTypes->count() > 0)
                  <h4 class="accordion_title">PREMIUM FLAVORS</h4>
                    <div class="row">
                        @foreach ($el->premiumTypes as $item)
                            <div class="col-md-3">{{$item->name}}</div>
                        @endforeach
                    </div>
              @endif
           
          </div>
        </section>
            
        @endforeach

        {{--  <section class="fix_silder_img4">
            <div class="fix_silder4">
                <h1>CAKE FLAVORS</h1>
                <h4>A small description about the cake flavors that we offer</h4>
            </div>
        </section>
        <section class="flavors_details text-center">
          <div class="container">
            <h4 class="accordion_title">BASIC FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Vanilla Bean</div>
                <div class="col-md-3">Chocolate</div>
                <div class="col-md-3">Confetti Vanilla</div>
                <div class="col-md-3">Lemon</div>
                <div class="col-md-3">Red Velvet</div>
            </div>
            <h4 class="accordion_title with-margin">PREMIUM FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Almond</div>
                <div class="col-md-3">Pumpkin Spice</div>
                <div class="col-md-3">Pineapple Supreme</div>
                <div class="col-md-3">Lemon Berry</div>
                <div class="col-md-3">Carrot</div>
                <div class="col-md-3">Strawberry</div>
                <div class="col-md-3">Coconut</div>
            </div>
          </div>
        </section>  --}}
        {{--  <section class="fix_silder_img4">
            <div class="fix_silder4">
                <h1>BUTTERCREAM FLAVORS</h1>
                <h4>A small description about the BUTTERCREAM FLAVORS that we offer</h4>
            </div>
        </section>
        <section class="flavors_details text-center">
          <div class="container">
            <h4 class="accordion_title">BASIC FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Vanilla</div>
                <div class="col-md-3">Chocolate</div>
                <div class="col-md-3">Chocolate Mocha</div>
                <div class="col-md-3">Almond</div>
                <div class="col-md-3">Cream Cheese</div>
                <div class="col-md-3">Lemon</div>
            </div>
            <h4 class="accordion_title with-margin">PREMIUM FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Red Raspberry</div>
                <div class="col-md-3">Salted Caramel</div>
                <div class="col-md-3">Wild Strawberry</div>
                <div class="col-md-3">Maple Pecan</div>
                <div class="col-md-3">Chocolate Ganache</div>
                <div class="col-md-3">Apricot</div>
                <div class="col-md-3">Triple Berry</div>
                <div class="col-md-3">Coconut Rum</div>
                <div class="col-md-3">Passion Fruit</div>
                <div class="col-md-3">Black Cherry</div>
                <div class="col-md-3">White Chocolate</div>
                <div class="col-md-3">Pineapple</div>
            </div>
          </div>
        </section>  --}}
        {{--  <section class="fix_silder_img4" >
            <div class="fix_silder4">
                <h1>SPECIALTY FILLINGS</h1>
                <h4>A small description about the SPECIALTY FILLINGS that we offer</h4>
            </div>
        </section>
        <section class="flavors_details text-center">
          <div class="container">
            <div class="row">
                <div class="col-md-3">Raspberry *</div>
                <div class="col-md-3">Black Cherry *</div>
                <div class="col-md-3">Lemon Curd</div>
                <div class="col-md-3">Strawberry *</div>
                <div class="col-md-3">Pineapple *</div>
                <div class="col-md-3">Chocolate Ganache</div>
            </div>
          </div>
        </section>  --}}
        </div>
        <div class="container">
        <!-- ROW -->
        <div class="row">
            <div class="alert alert-warning" style="margin-top: 15px;">
                <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
                <span style="float: left;margin-top:-2px;padding-right: 10px;font-size: 25px;"><i class="fa fa-info-circle"></i></span>
               <?php echo $msg->description;?>
                {{--  Please note that our products may contain and/or come into contact with nuts, dairy, and/or soy products.  --}}
            </div>
        </div>
        <!-- //ROW -->
    </div>
    <!-- //CONTAINER -->
</section>
<!-- //FAQ PAGE -->
@stop

@section('js')

@stop
