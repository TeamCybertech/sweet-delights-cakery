@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/summernote/dist/summernote.css')}}" />

@stop


@section('content')
    
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

      <!-- CONTAINER -->
      <div class="container">

            <hr class="banner-top">
            <div class="banner-bg center">
                @if ($tasting->status == 2)
                  <h3>Payment for Tasting Schedule</h3>
                  <p>Payment pending for this Schedule!</p>
                @else
                  <h3>Scheduled Tasting Details</h3>
                  <p>Your Tasting Schedule as below, Thank you for Visiting Us!</p>
                @endif
            </div>
            <hr class="banner-bottom">

      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->


    	<section class="checkout_page">

			<!-- CONTAINER -->
			<div class="container">
						<div class="alert alert-danger alert-dismissable" id="alart-warning" style="display: none">

						</div>
				<!-- CHECKOUT BLOCK -->

            <div class="checkout_block checkout_form">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">TASTING PEOPLE </label>
                      <p>{{ $tasting->getPeopleType->name }}</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">EVENT DATE</label>
                      <p>{{ Carbon\Carbon::parse($tasting->event_date)->format('dS F Y') }}</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">FULL NAME</label>
                      <p>{{$tasting->first_name." ".$tasting->last_name }}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">PHONE</label>
                      <p>{{$tasting->phone  }}</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">E-MAIL</label>
                      <p>{{$tasting->email}}</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">ACCEPTED DATE</label>
                      <p>{{ $tasting->accepted_date ? Carbon\Carbon::parse($tasting->accepted_date)->format('dS F Y h:i a') : ""}}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="">DETAILS</label>
                      <p>{!!$tasting->details!!}</p>
                    </div>
                  </div>
                </div>
              </div>

              @if ($tasting->status == 2)

                <form id="main-form" class="text-center  clearfix" method="POST" style="margin-top: 30px;">
                  {!! csrf_field(); !!}
                  <h3>According to your Request, Please pay US ${{ $tasting->getPeopleType->price }} to confirm your Order!</h3>
                  <p style="color: #ff0000; cursor: default;">We only allow <img style="height: 20px;" src="{{asset('assets/front/images/paypal-logo.png')}}"> Transactions for payments</p>

                <a class="btn button-new filter-button" href="{{ URL::previous() }}">Cancel</a>
                  <button class="btn button-new filter-button">Procced to pay</button>
                  {{-- <a class="btn inactive" href="{{ url('tasting-reschedule/'.$tasting->id) }}">RESCHEDULE</a> --}}

      					</form>
              @endif
           </div><!-- //CHECKOUT BLOCK -->



			</div><!-- //CONTAINER -->
		</section><!-- //CHECKOUT PAGE -->
@stop

@section('js')
@stop
