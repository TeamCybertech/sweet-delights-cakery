@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<style media="screen">
</style>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop


@section('content')

    <!-- BREADCRUMBS -->
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

      <!-- CONTAINER -->
      <div class="container">
        <h3 class="pull-left"><b>Blog Post</b></h3>

        <div class="pull-right">
          <a href="#" >Back to shop<i class="fa fa-angle-right"></i></a>
        </div>
      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->


    <!-- BLOG BLOCK -->
    <section class="blog">

      <!-- CONTAINER -->
      <div class="container">

        <!-- ROW -->
        <div class="row">

          <!-- BLOG LIST -->
          <div class="col-lg-12 col-md-9 col-sm-9">

            <article class="post blog_post clearfix margbot20" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <div class="post_title" href="blog-post.html" >{{$blog->name}}</div>

               <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"></a>
              <div class="post_large_image">
                <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div>
                <center>
                  <!-- @if(count($blog->getImages) == 0)
                      <img class="blog-img-border" src="{{asset('assets/front/images/blog/2.jpg')}}" alt="" />
                  @else -->

                      <img class="blog-img-border"  src="{{asset('core/storage/uploads/images/blog/'.$blog->cover_image)}}" alt="" />
                  @endif
                </center>

              </div>

              <div class="blog_post_content">
                <!--<div class="col-md-4">
                   <img class="blog-img-border"  src="{{asset('core/storage/uploads/images/blog/blog-20171230231835-0.png')}}" alt="" />
                </div>-->
                <div class="col-md-6">
                  <div class="flexslider article_slider" style="max-width: 100%; height: auto;">
                      <ul class="slides">
                        @foreach ($blog->getImages as $key => $value)
                          <li class="slide1">
                              <img src="{{url('').'/core/storage/uploads/images/blog'.'/'.$value->filename}}" alt=""  />
                                         
                          </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-6 text-justify">
                    {!! $blog->description !!}
                  </div>


            </div>

            </article> 

            {{-- <iframe src="" width="100%" height=""></iframe> --}}
            @if (isset(explode('v=',$blog->video_url)[1]))
              <iframe width="100%" height="490" src="https://www.youtube.com/embed/{{ explode('v=',$blog->video_url)[1] }}" frameborder="0" allowfullscreen></iframe>
            @endif


            <div class="shared_tags_block clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <div class="pull-left post_tags">

              </div>

              <div class="pull-right tovar_shared clearfix">
                <p>Share item with friends</p>
                <ul>
                  <li>
                    <a class="facebook" target="_blank" href="https://www.facebook.com/sharer.php?u={{ urlencode(url('blog/'.$blog->id)) }}&t={{ urlencode($blog->name) }}" ><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a class="twitter" target="_blank" href="http://twitter.com/intent/tweet?status={{ urlencode($blog->name) }}+{{ urlencode(url('blog/'.$blog->id)) }}" ><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a class="linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(url('blog/'.$blog->id)) }}&title={{ urlencode($blog->name) }}&source={{ urlencode(url('/')) }}" ><i class="fa fa-linkedin"></i></a>
                  </li>
                  <li>
                    <a class="google-plus" target="_blank" href="https://plus.google.com/share?url={{ urlencode(url('blog/'.$blog->id)) }}" ><i class="fa fa-google-plus"></i></a>
                  </li>
                  <li>
                    <a class="tumblr" target="_blank" href="http://www.tumblr.com/share?v=3&u={{ urlencode(url('blog/'.$blog->id)) }}&t={{ urlencode($blog->name) }}" ><i class="fa fa-tumblr"></i></a>
                  </li>
                </ul>
              </div>
            </div>


            <!-- COMMENTS -->
            <div id="comments" data-appear-top-offset='-100' data-animated='fadeInUp' >
              <h2>Comments ({{ $blog->comments->count()}})</h2>
              <ol style="list-style:none">
                @foreach ($blog->comments as $element)
                  <li>
                    {{-- <div class="pull-left avatar"><a href="javascript:void(0);"><img src="{{asset('assets/front/images/avatar1.jpg')}}" alt="" /></a></div> --}}
                    <div class="comment_right">
                      <div class="comment_info">
                        <a class="comment_author" href="javascript:void(0);" >{{ $element->name }}</a>
                        {{-- <a class="comment_reply" href="javascript:void(0);" ><i class="fa fa-share"></i> Reply</a> --}}
                        <div class="clear"></div>
                        <div class="comment_date">{{ $element->created_at->format('d M Y') }}</div>
                      </div>
                      {{$element->content  }}
                    </div>

                    <div class="clear"></div>

                  </li>
                @endforeach


              </ol>
            </div><!-- //COMMENTS -->


            <!-- LEAVE A COMMENT -->
            <div id="comment_form" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <h2>Leave a Comment</h2>
              <div class="alert alert-danger" id="commentErrors" hidden>
                <ul>
                </ul>
              </div>
              <div class="comment_form_wrapper">
                <form action="javascript:void(0);" method="post" id="commentForm">
                  {!! csrf_field(); !!}
                  <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                  <input type="text" name="name" placeholder="Name *"/>
                  <input class="email" type="text" name="email" placeholder="Email *" /></br>
                  <textarea name="content" placeholder="Your comment *"></textarea>
                  <div class="clear"></div>
                  <span class="comment_note">Your email address will not be published. Required fields are marked *</span>
                  <input style="background-color:#8b5730;border-color: white;color: white" type="submit" value="Post Comment" id="commentBtn"/>
                  <div class="clear"></div>
                   <div class="g-recaptcha" data-sitekey="6LeRREcUAAAAAOn6ULfVo70a2-W_g9Lqxn7TlcsH"></div>
                </form>

              </div>
            </div><!-- //LEAVE A COMMENT -->

             {{-- @foreach($latest_blogs as $blog)
            <article class="post margbot40 clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <a class="post_image pull-left" href="{{url('blog/'.$blog->id)}}" >
                <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div>
                @if(count($blog->getImages) == 0)
                    <img class="blog-img-border" src="{{asset('assets/front/images/blog/2.jpg')}}" alt="" />
                @else
                    <img class="blog-img-border" src="{{asset($blog->getImages->first()->path.'/'.$blog->getImages->first()->filename)}}" alt="" />
                @endif
              </a>
              <a class="post_title" href="{{url('blog/'.$blog->id)}}" >{{$blog->name}}</a>
              <div class="post_content">{!!substr(strip_tags($blog->description),0,800)!!}</div>

            </article>

           @endforeach --}}



          </div><!-- //BLOG LIST -->


<!--           SIDEBAR
          <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 padbot50">

             BANNERS WIDGET
            <div class="widget_banners">
              <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner10.jpg')}}" alt="" /></a>
              <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner9.jpg')}}" alt="" /></a>
              <a class="banner nobord margbot10" href="javascript:void(0);" ><img src="{{asset('assets/front/images/tovar/banner8.jpg')}}" alt="" /></a>
            </div> //BANNERS WIDGET
          </div> //SIDEBAR -->
        </div><!-- //ROW -->
      </div><!-- //CONTAINER -->
    </section><!-- //BLOG BLOCK -->

@stop

@section('js')
<script type="text/javascript">
  $('#commentBtn').click(function(event) {
    data = $('#commentForm').serializeArray()

    $.ajax({
      url: '{{ url('comment') }}',
      type: 'POST',
      dataType: 'json',
      data: data
    })
    .done(function() {
      location.reload()
    })
    .fail(function(data) {
      $('#commentErrors ul').html('')
      $.each(data.responseJSON, function(index, val) {
          $('#commentErrors ul').append('<li>'+val+'</li>')
      });
      $('#commentErrors').show()
    })

  });
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
</script>

@stop
