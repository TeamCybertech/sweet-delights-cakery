@extends('layouts.front.master') @section('title','Contact | www.princeofgalle.com')
@section('css')

@stop



@section('content')

		<!-- BREADCRUMBS -->
		<section class="breadcrumb parallax margbot30"></section>
		<!-- //BREADCRUMBS -->


		<!-- PAGE HEADER -->
		<section class="page_header">

			<!-- CONTAINER -->
			<div class="container">
				<h3 class="pull-left"><b>Contacts</b></h3>

				<div class="pull-right">
					<a href="women.html" >Back to shop<i class="fa fa-angle-right"></i></a>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //PAGE HEADER -->


		<!-- CONTACTS BLOCK -->
		<section class="contacts_block">

			<!-- CONTAINER -->
			<div class="container">

				<!-- ROW -->
				<div class="row padbot30">
					<div class="col-lg-6 col-md-6 padbot30">
						<div id="map"><iframe height="490" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2936.3555524471863!2d-83.39265468449851!3d42.611414827183516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824bc926c4af98d%3A0xd1105cc2735e654a!2sSweet+Delights+Cakery!5e0!3m2!1sen!2slk!4v1509861373872"></iframe></div>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 padbot30">
						<ul class="contact_info_block">
							<li>
								<h3><i class="fa fa-map-marker"></i><b>Store locations</b></h3>
								<p>{{ $details->company_name }}</p>
								<span>{!!$details->location!!}</span>
							</li>
							<li>
								<h3><i class="fa fa-phone"></i><b>Phones</b></h3>
								<p class="phone">{{ $details->phone }}</p>
								<!-- <p class="phone">(+55) 800 456 7890</p> -->
							</li>
							<li>
								<h3><i class="fa fa-envelope"></i><b>E-mail</b></h3>
								<!-- <p>Advertising</p> -->
								<a href="mailto:{{ $details->email }}">{{ $details->email }}</a>

								<!-- <p>Partnership</p>
								<a href="mailto:partner@glammyshop.com">partner@glammyshop.com</a>

								<p>Returns and Refunds</p>
								<a href="mailto:return@glammyshop.com">return@glammyshop.com</a> -->
							</li>
						</ul>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-6 padbot30">
						<!-- CONTACT FORM -->
						<div class="contact_form">
							<h3><b>Contacts form</b></h3>
							<!-- <p>Sign in to save and share your Love List.</p> -->
							<div id="note"></div>
							<div id="fields">
								<form id="ajax-contact-form" action="#">
									<label>Name</label>
									<input type="text" name="name" value="" placeholder="Name" />
									<label>E-mail</label>
									<input type="text" name="email" value="" placeholder="E-mail" />
									<label>Phone</label>
									<input type="text" name="phone" value="" placeholder="Phone" />
									<label>Message</label>
									<textarea name="message" placeholder="Message" ></textarea><br>
									<input class="btn active" type="submit" value="Send Message" style="position: relative"/>
								</form>
							</div>
						</div><!-- //CONTACT FORM -->
					</div>
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //CONTACTS BLOCK -->


@stop

@section('js')


@stop
